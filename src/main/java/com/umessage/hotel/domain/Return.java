package com.umessage.hotel.domain;


public class Return {
	//返回值成功否
	public String status;
	//返回值参数内容
	public Object data;
	//返回值描述
	public String msg;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		return "Return [status=" + status + ", data=" + data + ", msg=" + msg + "]";
	}

	
}