package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_breakfast_record")
public class BreakfastRecord {
    @Id
    private Integer id;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    private String memberId;

    /**
     * 数量
     */
    private Integer amount;

    @Column(name = "valid_date")
    private Date validDate;

    /**
     * 表单formId，用于发送模板消息
     */
    @Column(name = "form_id")
    private String formId;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;
    
    @Column(name = "hotel_id")
    private String hotelId; 
    
    @Column(name = "record_sn")
    private String recordSn;
    @Transient
    private String mobile;
    
    public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getRecordSn() {
		return recordSn;
	}

	public void setRecordSn(String recordSn) {
		this.recordSn = recordSn;
	}

	/**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取会员id
     *
     * @return member_id - 会员id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 设置会员id
     *
     * @param memberId 会员id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * 获取数量
     *
     * @return amount - 数量
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * 设置数量
     *
     * @param amount 数量
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * @return valid_date
     */
    public Date getValidDate() {
        return validDate;
    }

    /**
     * @param validDate
     */
    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }


    /**
     * 获取表单formId，用于发送模板消息
     *
     * @return form_id - 表单formId，用于发送模板消息
     */
    public String getFormId() {
        return formId;
    }

    /**
     * 设置表单formId，用于发送模板消息
     *
     * @param formId 表单formId，用于发送模板消息
     */
    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}