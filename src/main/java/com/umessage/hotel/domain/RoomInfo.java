package com.umessage.hotel.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Table(name = "t_room_info")
public class RoomInfo implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 房间id
     */
    @Id
    @Column(name = "room_id")
    private Integer roomId;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 房间名称
     */
    private String name;

    /**
     * 房间类型
     */
    @Column(name = "room_type")
    private String roomType;

    /**
     * 支付类型 // 1到店支付、2担保支付、3全额支付
     */
    @Column(name = "pay_type")
    private String payType;

    /**
     * 房间介绍
     */
    private String summary;

    /**
     * 入住人数上限
     */
    @Column(name = "resident_limit")
    private Integer residentLimit;

    /**
     * 原始价格
     */
    @Column(name = "original_price")
    private BigDecimal originalPrice;

    /**
     * 折扣价格
     */
    @Column(name = "discount_price")
    private BigDecimal discountPrice;

    /**
     * 房间数
     */
    @Column(name = "room_num")
    private Integer roomNum;

    /**
     * 设施
     */
    private String facilities;

    /**
     * 房间政策
     */
    private String policies;

    /**
     * 是否可预订 0不可预定1可预定
     */
    @Column(name = "is_bookable")
    private String isBookable;
    
    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 添加人
     */
    @Column(name = "add_acc")
    private Long addAcc;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 修改人
     */
    @Column(name = "modify_acc")
    private Long modifyAcc;

	/**
     * 获取房间id
     *
     * @return room_id 
     */
    public Integer getRoomId() {
        return roomId;
    }

    /**
     * 设置房间id
     *
     * @param roomId 
     */
    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - �Ƶ�id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取房间名称
     *
     * @return name 
     */
    public String getName() {
        return name;
    }
    /**
     * 获取时间间隔
     */
    @Transient
    private BigDecimal countDay;
    
	public BigDecimal getCountDay() {
		return countDay;
	}

	public void setCountDay(BigDecimal countDay) {
		this.countDay = countDay;
	}

	/**
     * 设置房间名称
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取房间类型
     *
     * @return room_type 
     */
    public String getRoomType() {
        return roomType;
    }

    /**
     * 设置房间类型
     *
     * @param roomType
     */
    public void setRoomType(String roomType) {
        this.roomType = roomType == null ? null : roomType.trim();
    }

    /**
     * 获取支付类型
     *
     * @return pay_type 
     */
    public String getPayType() {
        return payType;
    }

    /**
     * 设置支付类型
     *
     * @param payType
     */
    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    /**
     * 获取房间介绍
     *
     * @return summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 设置房间介绍
     *
     * @param summary
     */
    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    /**
     * 获取入住人数上限
     *
     * @return resident_limit 
     */
    public Integer getResidentLimit() {
        return residentLimit;
    }

    /**
     * 设置入住人数上限
     *
     * @param residentLimit 
     */
    public void setResidentLimit(Integer residentLimit) {
        this.residentLimit = residentLimit;
    }

    /**
     *  获取原始价格
     *
     * @return original_price 
     */
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    /**
     * 设置原始价格
     *
     * @param originalPrice 
     */
    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    /**
     * 获取折扣价格
     *
     * @return discount_price 
     */
    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    /**
     * 设置折扣价格
     *
     * @param discountPrice 
     */
    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * 获取房间数
     *
     * @return room_num 
     */
    public Integer getRoomNum() {
        return roomNum;
    }

    /**
     * 设置房间数
     *
     * @param roomNum
     */
    public void setRoomNum(Integer roomNum) {
        this.roomNum = roomNum;
    }

    /**
     * 获取设施
     *
     * @return facilities 
     */
    public String getFacilities() {
        return facilities;
    }

    /**
     * 设置设施
     *
     * @param facilities
     */
    public void setFacilities(String facilities) {
        this.facilities = facilities == null ? null : facilities.trim();
    }

    /**
     * 获取房间政策
     *
     * @return policies
     */
    public String getPolicies() {
        return policies;
    }

    /**
     * 设置房间政策
     *
     * @param policies
     */
    public void setPolicies(String policies) {
        this.policies = policies == null ? null : policies.trim();
    }

    /**
     * 获取是否课预定状态
     *
     * @return is_bookable
     */
    public String getIsBookable() {
        return isBookable;
    }

    /**
     * 设置是否课预定状态 0不可预定1可预定
     *
     * @param isBookable
     */
    public void setIsBookable(String isBookable) {
        this.isBookable = isBookable == null ? null : isBookable.trim();
    }

    /**
     * @return add_time
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * @param addTime
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取添加人
     *
     * @return add_acc
     */
    public Long getAddAcc() {
        return addAcc;
    }

    /**
     * 设置添加人
     *
     * @param addAcc 
     */
    public void setAddAcc(Long addAcc) {
        this.addAcc = addAcc;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取修改人
     *
     * @return modify_acc 
     */
    public Long getModifyAcc() {
        return modifyAcc;
    }

    /**
     * 设置修改人
     *
     * @param modifyAcc
     */
    public void setModifyAcc(Long modifyAcc) {
        this.modifyAcc = modifyAcc;
    }
    @Transient
    private PictureInfo picture;

	public PictureInfo getPicture() {
		return picture;
	}

	public void setPicture(PictureInfo picture) {
		this.picture = picture;
	}
	@Transient
    private PictureInfo[] pictures;

	public PictureInfo[] getPictures() {
		return pictures;
	}

	public void setPictures(PictureInfo[] pictures) {
		this.pictures = pictures;
	}
	/*
	 * 早餐政策 1、不含早餐 2、免费单早餐3、免费双早餐
	 */
	@Column(name="pol_breakfast")
	private String polBreakfast;

	public String getPolBreakfast() {
		return polBreakfast;
	}

	public void setPolBreakfast(String polBreakfast) {
		this.polBreakfast = polBreakfast;
	}
	
	
}