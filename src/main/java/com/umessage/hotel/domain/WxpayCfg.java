package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_wxpay_cfg")
public class WxpayCfg {
	@Id
	private String id;

	private String mpcfgid;

	private String userid;

	@Column(name = "appId")
	private String appid;

	@Column(name = "mchId")
	private String mchid;

	@Column(name = "appkey")
	private String appkey;

	@Column(name = "appsecret")
	private String appsecret;

	@Column(name = "certPath")
	private String certpath;

	@Column(name = "certPassword")
	private String certpassword;

	private String wxname;

	private Date ts;

	private Integer num1;

	private String str1;

	private String str2;

	/**
	 * 测试配置
	 * @return
	 */
	public static WxpayCfg getTestInstance1() {
		WxpayCfg wxpayCfg = new WxpayCfg();
		wxpayCfg.setAppid("wx06c87b2a3e1cd61c");
		wxpayCfg.setMchid("1497400642");
		wxpayCfg.setAppkey("afb6ab98f71131f162871e88977c559e");
		wxpayCfg.setAppsecret("f540ffe00ee6fa675a9f969afc519e71");
		wxpayCfg.setCertpath("/Users/mixiangliu/cert/apiclient_cert.p12");
		wxpayCfg.setCertpassword("1497400642");
		return wxpayCfg;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * @return mpcfgid
	 */
	public String getMpcfgid() {
		return mpcfgid;
	}

	/**
	 * @param mpcfgid
	 */
	public void setMpcfgid(String mpcfgid) {
		this.mpcfgid = mpcfgid == null ? null : mpcfgid.trim();
	}

	/**
	 * @return userid
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * @param userid
	 */
	public void setUserid(String userid) {
		this.userid = userid == null ? null : userid.trim();
	}

	/**
	 * @return appId
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * @param appid
	 */
	public void setAppid(String appid) {
		this.appid = appid == null ? null : appid.trim();
	}

	/**
	 * @return mchId
	 */
	public String getMchid() {
		return mchid;
	}

	/**
	 * @param mchid
	 */
	public void setMchid(String mchid) {
		this.mchid = mchid == null ? null : mchid.trim();
	}

	/**
	 * @return appkey
	 */
	public String getAppkey() {
		return appkey;
	}

	/**
	 * @param appkey
	 */
	public void setAppkey(String appkey) {
		this.appkey = appkey == null ? null : appkey.trim();
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	/**
	 * @return certPath
	 */
	public String getCertpath() {
		return certpath;
	}

	/**
	 * @param certpath
	 */
	public void setCertpath(String certpath) {
		this.certpath = certpath == null ? null : certpath.trim();
	}

	/**
	 * @return certPassword
	 */
	public String getCertpassword() {
		return certpassword;
	}

	/**
	 * @param certpassword
	 */
	public void setCertpassword(String certpassword) {
		this.certpassword = certpassword == null ? null : certpassword.trim();
	}

	/**
	 * @return wxname
	 */
	public String getWxname() {
		return wxname;
	}

	/**
	 * @param wxname
	 */
	public void setWxname(String wxname) {
		this.wxname = wxname == null ? null : wxname.trim();
	}

	/**
	 * @return ts
	 */
	public Date getTs() {
		return ts;
	}

	/**
	 * @param ts
	 */
	public void setTs(Date ts) {
		this.ts = ts;
	}

	/**
	 * @return num1
	 */
	public Integer getNum1() {
		return num1;
	}

	/**
	 * @param num1
	 */
	public void setNum1(Integer num1) {
		this.num1 = num1;
	}

	/**
	 * @return str1
	 */
	public String getStr1() {
		return str1;
	}

	/**
	 * @param str1
	 */
	public void setStr1(String str1) {
		this.str1 = str1 == null ? null : str1.trim();
	}

	/**
	 * @return str2
	 */
	public String getStr2() {
		return str2;
	}

	/**
	 * @param str2
	 */
	public void setStr2(String str2) {
		this.str2 = str2 == null ? null : str2.trim();
	}
}