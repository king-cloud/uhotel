package com.umessage.hotel.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_member_info")
public class MemberInfo implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 用户id
     */
    @Id
    @Column(name = "member_id")
    private String memberId;

    private String mobile;

    /**
     * 用户编号
     */
    @Column(name = "member_no")
    private String memberNo;

    /**
     * 所属酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 用户级别
     */
    private String grade;

    /**
     * 实际账户
     */
    @Column(name = "real_money")
    private BigDecimal realMoney;

    /**
     * 虚拟账户
     */
    @Column(name = "virt_money")
    private BigDecimal virtMoney;

    /**
     * 接受通知
     */
    private String notifiable;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 最后登录时间
     */
    @Column(name = "last_login_time")
    private Date lastLoginTime;

    /**
     * 获取用户id
     *
     * @return member_id - 用户id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 设置用户id
     *
     * @param memberId 用户id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * @return mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取用户编号
     *
     * @return member_no - 用户编号
     */
    public String getMemberNo() {
        return memberNo;
    }

    /**
     * 设置用户编号
     *
     * @param memberNo 用户编号
     */
    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo == null ? null : memberNo.trim();
    }

    /**
     * 获取所属酒店id
     *
     * @return hotel_id - 所属酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置所属酒店id
     *
     * @param hotelId 所属酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取用户级别
     *
     * @return grade - 用户级别
     */
    public String getGrade() {
        return grade;
    }

    /**
     * 设置用户级别
     *
     * @param grade 用户级别
     */
    public void setGrade(String grade) {
        this.grade = grade == null ? null : grade.trim();
    }

    /**
     * 获取实际账户
     *
     * @return real_money - 实际账户
     */
    public BigDecimal getRealMoney() {
        return realMoney;
    }

    /**
     * 设置实际账户
     *
     * @param realMoney 实际账户
     */
    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    /**
     * 获取虚拟账户
     *
     * @return virt_money - 虚拟账户
     */
    public BigDecimal getVirtMoney() {
        return virtMoney;
    }

    /**
     * 设置虚拟账户
     *
     * @param virtMoney 虚拟账户
     */
    public void setVirtMoney(BigDecimal virtMoney) {
        this.virtMoney = virtMoney;
    }

    /**
     * 获取接受通知
     *
     * @return notifiable - 接受通知
     */
    public String getNotifiable() {
        return notifiable;
    }

    /**
     * 设置接受通知
     *
     * @param notifiable 接受通知
     */
    public void setNotifiable(String notifiable) {
        this.notifiable = notifiable == null ? null : notifiable.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取最后登录时间
     *
     * @return last_login_time - 最后登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置最后登录时间
     *
     * @param lastLoginTime 最后登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    @Transient
    private WXUser wxUser;

	public WXUser getWxUser() {
		return wxUser;
	}

	public void setWxUser(WXUser wxUser) {
		this.wxUser = wxUser;
	}
    
}