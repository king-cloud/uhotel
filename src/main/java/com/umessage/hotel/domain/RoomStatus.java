package com.umessage.hotel.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Table(name = "t_room_status")
public class RoomStatus implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 房态id
     */
    @Id
    private Integer id;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 房间id
     */
    @Column(name = "room_id")
    private String roomId;

    /**
     * 是否可预订 0 可预定  1 不可预定
     */
    @Column(name = "is_book")
    private String isBook;

    /**
     * 每日价格
     */
    private BigDecimal price;

    /**
     * 日期
     */
    @Column(name = "room_date")
    private Date roomDate;
    
    @Transient
    private String daily;
    
    public String getDaily() {
		return daily;
	}

	public void setDaily(String daily) {
		this.daily = daily;
	}

	/**
     * 获取房态id
     *
     * @return id - 房态id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置房态id
     *
     * @param id 房态id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取房间id
     *
     * @return room_id - 房间id
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * 设置房间id
     *
     * @param roomId 房间id
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId == null ? null : roomId.trim();
    }

    /**
     * 获取是否可预订 0 可预定  1 不可预定
     *
     * @return is_book - 是否可预订 0 可预定  1 不可预定
     */
    public String getIsBook() {
        return isBook;
    }

    /**
     * 设置是否可预订 0 可预定  1 不可预定
     *
     * @param isBook 是否可预订 0 可预定  1 不可预定
     */
    public void setIsBook(String isBook) {
        this.isBook = isBook;
    }

    /**
     * 获取每日价格
     *
     * @return price - 每日价格
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 设置每日价格
     *
     * @param price 每日价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 获取日期
     *
     * @return room_date - 日期
     */
    public Date getRoomDate() {
        return roomDate;
    }

    /**
     * 设置日期
     *
     * @param roomDate 日期
     */
    public void setRoomDate(Date roomDate) {
        this.roomDate = roomDate;
    }
}