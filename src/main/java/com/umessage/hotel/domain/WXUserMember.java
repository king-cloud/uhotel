package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_wxuser_member")
public class WXUserMember {
	@Id
	@Column(name = "wxuser_member_id")
	private Integer wxuserMemberId;

	@Column(name = "hotel_id")
	private String hotelId;

	@Column(name = "wxuser_id")
	private String wxuserId;

	@Column(name = "member_id")
	private String memberId;

	@Column(name = "bind_status")
	private String bindStatus;

	@Column(name = "bind_date")
	private Date bindDate;

	@Column(name = "unbind_date")
	private Date unbindDate;

	
	@Transient
	private MemberInfo member;
	@Transient
	private WXUser wxUser;
	
	
	public MemberInfo getMember() {
		return member;
	}

	public void setMember(MemberInfo member) {
		this.member = member;
	}

	public WXUser getWxUser() {
		return wxUser;
	}

	public void setWxUser(WXUser wxUser) {
		this.wxUser = wxUser;
	}


	/**
	 * @return wxuser_member_id
	 */
	public Integer getWxuserMemberId() {
		return wxuserMemberId;
	}

	/**
	 * @param wxuserMemberId
	 */
	public void setWxuserMemberId(Integer wxuserMemberId) {
		this.wxuserMemberId = wxuserMemberId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	/**
	 * @return wxuser_id
	 */
	public String getWxuserId() {
		return wxuserId;
	}

	/**
	 * @param wxuserId
	 */
	public void setWxuserId(String wxuserId) {
		this.wxuserId = wxuserId == null ? null : wxuserId.trim();
	}

	/**
	 * @return member_id
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId == null ? null : memberId.trim();
	}

	/**
	 * @return bind_status
	 */
	public String getBindStatus() {
		return bindStatus;
	}

	/**
	 * @param bindStatus
	 */
	public void setBindStatus(String bindStatus) {
		this.bindStatus = bindStatus == null ? null : bindStatus.trim();
	}

	/**
	 * @return bind_date
	 */
	public Date getBindDate() {
		return bindDate;
	}

	/**
	 * @param bindDate
	 */
	public void setBindDate(Date bindDate) {
		this.bindDate = bindDate;
	}

	/**
	 * @return unbind_date
	 */
	public Date getUnbindDate() {
		return unbindDate;
	}

	/**
	 * @param unbindDate
	 */
	public void setUnbindDate(Date unbindDate) {
		this.unbindDate = unbindDate;
	}
}