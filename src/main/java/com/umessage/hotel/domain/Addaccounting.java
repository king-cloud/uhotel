package com.umessage.hotel.domain;

/**
 * 添加账务实体类
 * @author Administrator
 *
 */
public class Addaccounting {

    private String hotelId;//酒店id

	private String orderId;//订单号     *必传*

    private String pcCode;//费用吗           *必传*  自助机收款码” S”,,,,退款码:” T”
    
    private String amount;//金额              *必传*    如果传退款码  钱数传负数!!!!!!

	public String getOrderId() {
		return orderId;
	}

	public String getPcCode() {
		return pcCode;
	}

	public void setPcCode(String pcCode) {
		this.pcCode = pcCode;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}


    
    
}