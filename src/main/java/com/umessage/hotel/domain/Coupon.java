package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_coupon")
public class Coupon {
    /**
     * id
     */
    @Id
    @Column(name = "coupon_id")
    private Integer couponId;

    @Column
    private String couponName;
    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private Integer hotelId;

    /**
     * 优惠券类型 1订单优惠和2充值优惠
     */
    @Column(name = "coupon_type")
    private String couponType;

    /**
     * 优惠方式 1直减、2满减、3冲增
     */
    private Integer mode;

    /**
     * 第一个值
     */
    @Column(name = "first_number")
    private Integer firstNumber;

    /**
     * 第二个值
     */
    @Column(name = "second_number")
    private Integer secondNumber;
    
    /**
     * 有效期类型 1固定周期 2固定时长
     */
    @Column(name = "valid_type")
    private Integer validType;

    /**
     * 有效期
     */
    @Column(name = "valid_start")
    private String validStart;

    /**
     * 有效期
     */
    @Column(name = "valid_end")
    private String validEnd;

    /**
     * 是否可以与会员叠加
     */
    @Column(name = "with_user")
    private String withUser;

    /**
     * 是否可使用 0不可用 1可用
     */
    @Column(name = "is_usable")
    private String isUsable;

    /**
     * 二维码
     */
    @Column(name = "bar_code")
    private String barCode;

    /**
     * 分享链接
     */
    @Column(name = "share_url")
    private String shareUrl;

    /**
     * 是否可叠加
     */
    private String overlayable;

    /**
     * 叠加次数
     */
    @Column(name = "max_overlay_times")
    private Integer maxOverlayTimes;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 添加人
     */
    @Column(name = "add_acc")
    private Long addAcc;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 修改人
     */
    @Column(name = "modify_acc")
    private Long modifyAcc;

    /**
     * 0删除 1未删除
     */
    @Column(name = "is_delete")
    private Integer isDelete;

    /**
     * 固定周期 天数
     */
    @Column(name = "cycle")
    private String cycle;
    /**
     * 获取id
     *
     * @return coupon_id - id
     */
    public Integer getCouponId() {
        return couponId;
    }

    /**
     * 设置id
     *
     * @param couponId id
     */
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取优惠券类型 1订单优惠和2充值优惠
     *
     * @return coupon_type - 优惠券类型 1订单优惠和2充值优惠
     */
    public String getCouponType() {
        return couponType;
    }

    /**
     * 设置优惠券类型 1订单优惠和2充值优惠
     *
     * @param couponType 优惠券类型 1订单优惠和2充值优惠
     */
    public void setCouponType(String couponType) {
        this.couponType = couponType == null ? null : couponType.trim();
    }

    /**
     * 获取优惠方式 1直减、2满减、3冲增
     *
     * @return mode - 优惠方式 1直减、2满减、3冲增
     */
    public Integer getMode() {
        return mode;
    }

    /**
     * 设置优惠方式 1直减、2满减、3冲增
     *
     * @param mode 优惠方式 1直减、2满减、3冲增
     */
    public void setMode(Integer mode) {
        this.mode = mode;
    }

    /**
     * 获取第一个值
     *
     * @return first_number - 第一个值
     */
    public Integer getFirstNumber() {
        return firstNumber;
    }

    /**
     * 设置第一个值
     *
     * @param firstNumber 第一个值
     */
    public void setFirstNumber(Integer firstNumber) {
        this.firstNumber = firstNumber;
    }

    /**
     * 获取第二个值
     *
     * @return second_number - 第二个值
     */
    public Integer getSecondNumber() {
        return secondNumber;
    }

    /**
     * 设置第二个值
     *
     * @param secondNumber 第二个值
     */
    public void setSecondNumber(Integer secondNumber) {
        this.secondNumber = secondNumber;
    }

    /**
     * 获取有效期类型 1固定周期 2固定时长
     *
     * @return valid_type - 有效期类型 1固定周期 2固定时长
     */
    public Integer getValidType() {
        return validType;
    }

    
    public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	/**
     * 设置有效期类型 1固定周期 2固定时长
     *
     * @param validType 有效期类型 1固定周期 2固定时长
     */
    public void setValidType(Integer validType) {
        this.validType = validType;
    }

    /**
     * 获取有效期
     *
     * @return valid_start - 有效期
     */
    public String getValidStart() {
        return validStart;
    }

    /**
     * 设置有效期
     *
     * @param validStart 有效期
     */
    public void setValidStart(String validStart) {
        this.validStart = validStart == null ? null : validStart.trim();
    }

    /**
     * 获取有效期
     *
     * @return valid_end - 有效期
     */
    public String getValidEnd() {
        return validEnd;
    }

    /**
     * 设置有效期
     *
     * @param validEnd 有效期
     */
    public void setValidEnd(String validEnd) {
        this.validEnd = validEnd == null ? null : validEnd.trim();
    }

    /**
     * 获取是否可以与会员叠加
     *
     * @return with_user - 是否可以与会员叠加
     */
    public String getWithUser() {
        return withUser;
    }

    /**
     * 设置是否可以与会员叠加
     *
     * @param withUser 是否可以与会员叠加
     */
    public void setWithUser(String withUser) {
        this.withUser = withUser == null ? null : withUser.trim();
    }

    /**
     * 获取是否可使用 0不可用 1可用
     *
     * @return is_usable - 是否可使用 0不可用 1可用
     */
    public String getIsUsable() {
        return isUsable;
    }

    /**
     * 设置是否可使用 0不可用 1可用
     *
     * @param isUsable 是否可使用 0不可用 1可用
     */
    public void setIsUsable(String isUsable) {
        this.isUsable = isUsable == null ? null : isUsable.trim();
    }

    /**
     * 获取二维码
     *
     * @return bar_code - 二维码
     */
    public String getBarCode() {
        return barCode;
    }

    /**
     * 设置二维码
     *
     * @param barCode 二维码
     */
    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? null : barCode.trim();
    }

    /**
     * 获取分享链接
     *
     * @return share_url - 分享链接
     */
    public String getShareUrl() {
        return shareUrl;
    }

    /**
     * 设置分享链接
     *
     * @param shareUrl 分享链接
     */
    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl == null ? null : shareUrl.trim();
    }

    /**
     * 获取是否可叠加
     *
     * @return overlayable - 是否可叠加
     */
    public String getOverlayable() {
        return overlayable;
    }

    /**
     * 设置是否可叠加
     *
     * @param overlayable 是否可叠加
     */
    public void setOverlayable(String overlayable) {
        this.overlayable = overlayable == null ? null : overlayable.trim();
    }

    /**
     * 获取叠加次数
     *
     * @return max_overlay_times - 叠加次数
     */
    public Integer getMaxOverlayTimes() {
        return maxOverlayTimes;
    }

    /**
     * 设置叠加次数
     *
     * @param maxOverlayTimes 叠加次数
     */
    public void setMaxOverlayTimes(Integer maxOverlayTimes) {
        this.maxOverlayTimes = maxOverlayTimes;
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取添加人
     *
     * @return add_acc - 添加人
     */
    public Long getAddAcc() {
        return addAcc;
    }

    /**
     * 设置添加人
     *
     * @param addAcc 添加人
     */
    public void setAddAcc(Long addAcc) {
        this.addAcc = addAcc;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取修改人
     *
     * @return modify_acc - 修改人
     */
    public Long getModifyAcc() {
        return modifyAcc;
    }

    /**
     * 设置修改人
     *
     * @param modifyAcc 修改人
     */
    public void setModifyAcc(Long modifyAcc) {
        this.modifyAcc = modifyAcc;
    }

    /**
     * 获取0删除 1未删除
     *
     * @return is_delete - 0删除 1未删除
     */
    public Integer getIsDelete() {
        return isDelete;
    }

    /**
     * 设置0删除 1未删除
     *
     * @param isDelete 0删除 1未删除
     */
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
    
}