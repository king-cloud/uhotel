package com.umessage.hotel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "t_hotel_info")
public class HotelInfo implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 酒店id
     */
    @Id
    @Column(name = "hotel_id")
    private String hotelId;

    @Column(name = "hotel_sn")
    private String hotelSn;

    /**
     * 酒店名称
     */
    @Column(name = "hotel_name")
    private String hotelName;

    private String appid;

    private String appsecret;

    /**
     * 简介
     */
    private String summary;

    private String country;

    private String province;

    private String city;

    private String district;

    private String street;

    private String address;

    private double longitude;

    private double latitude;

    /**
     * 邮编
     */
    private String post;

    /**
     * 酒店标签
     */
    private String tag;
    /**
	 * 地标建筑
	 */
    @Column(name = "markone")
	private String markOne;
    @Column(name = "marktwo")
	private String markTwo;
    /**
     * wifi开启状态 0 关闭  1 开启
     */
    @Column(name = "wifi_status")
    private String wifiStatus;
    
    @Column(name = "wifi_name")
    private String wifiName;
    
    @Column(name = "wifi_password")
    private String wifiPassword;

    /**
     *  1.普票 2.电子 3.专票
     */
    @Column(name ="invoice_type")
    private String invoiceType;
    
    public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getWifiStatus() {
		return wifiStatus;
	}

	public void setWifiStatus(String wifiStatus) {
		this.wifiStatus = wifiStatus;
	}

	public String getWifiName() {
		return wifiName;
	}

	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}

	public String getWifiPassword() {
		return wifiPassword;
	}

	public void setWifiPassword(String wifiPassword) {
		this.wifiPassword = wifiPassword;
	}

	/**
     * 酒店设施
     */
    private String facilities;

    private String phone;

    private String mobile;
	/**
	 * 酒店政策
	 */
    @Column(name = "policies")
    private String policies;
    
    @Column(name = "mypolicies")
    private String myPolicies;
    
    @Column(name = "business_num")
    private String businessNum;
    /**
     * 0 已删除    1 未删除
     */
    @Column(name = "is_delete")
    private String isDelete;
    
    @Transient
    private Object[] pictures;
    
    private String openPms;
    
    
	public Object[] getPictures() {
		return pictures;
	}

	public void setPictures(Object[] pictures) {
		this.pictures = pictures;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getPolicies() {
		return policies;
	}

	public void setPolicies(String policies) {
		this.policies = policies;
	}

	/**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 添加人
     */
    @Column(name = "add_acc")
    private String addAcc;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    /**
     * 修改人
     */
    @Column(name = "modify_acc")
    private String modifyAcc;

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * @return hotel_sn
     */
    public String getHotelSn() {
        return hotelSn;
    }

    /**
     * @param hotelSn
     */
    public void setHotelSn(String hotelSn) {
        this.hotelSn = hotelSn == null ? null : hotelSn.trim();
    }

    /**
     * 获取酒店名称
     *
     * @return hotel_name - 酒店名称
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * 设置酒店名称
     *
     * @param hotelName 酒店名称
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName == null ? null : hotelName.trim();
    }

    /**
     * @return appid
     */
    public String getAppid() {
        return appid;
    }

    /**
     * @param appid
     */
    public void setAppid(String appid) {
        this.appid = appid == null ? null : appid.trim();
    }

    /**
     * @return appsecret
     */
    public String getAppsecret() {
        return appsecret;
    }

    /**
     * @param appsecret
     */
    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret == null ? null : appsecret.trim();
    }

    /**
     * 获取简介
     *
     * @return summary - 简介
     */
    public String getSummary() {
        return summary;
    }

    /**
     * 设置简介
     *
     * @param summary 简介
     */
    public void setSummary(String summary) {
        this.summary = summary == null ? null : summary.trim();
    }

    /**
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     */
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    /**
     * @return province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * @return district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district
     */
    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    /**
     * @return street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street
     */
    public void setStreet(String street) {
        this.street = street == null ? null : street.trim();
    }

    /**
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取邮编
     *
     * @return post - 邮编
     */
    public String getPost() {
        return post;
    }

    /**
     * 设置邮编
     *
     * @param post 邮编
     */
    public void setPost(String post) {
        this.post = post == null ? null : post.trim();
    }

    /**
     * 获取酒店标签
     *
     * @return tag - 酒店标签
     */
    public String getTag() {
        return tag;
    }

    /**
     * 设置酒店标签
     *
     * @param tag 酒店标签
     */
    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    /**
     * 获取酒店设施
     *
     * @return facilities - 酒店设施
     */
    public String getFacilities() {
        return facilities;
    }

    /**
     * 设置酒店设施
     *
     * @param facilities 酒店设施
     */
    public void setFacilities(String facilities) {
        this.facilities = facilities == null ? null : facilities.trim();
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * @return mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取添加人
     *
     * @return add_acc - 添加人
     */
    public String getAddAcc() {
        return addAcc;
    }

    /**
     * 设置添加人
     *
     * @param addAcc 添加人
     */
    public void setAddAcc(String addAcc) {
        this.addAcc = addAcc == null ? null : addAcc.trim();
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * 获取修改人
     *
     * @return modify_acc - 修改人
     */
    public String getModifyAcc() {
        return modifyAcc;
    }

    /**
     * 设置修改人
     *
     * @param modifyAcc 修改人
     */
    public void setModifyAcc(String modifyAcc) {
        this.modifyAcc = modifyAcc == null ? null : modifyAcc.trim();
    }
	
	public String getMarkOne() {
		return markOne;
	}

	public void setMarkOne(String markOne) {
		this.markOne = markOne;
	}

	public String getMarkTwo() {
		return markTwo;
	}

	public void setMarkTwo(String markTwo) {
		this.markTwo = markTwo;
	}

	public String getMyPolicies() {
		return myPolicies;
	}

	public void setMyPolicies(String myPolicies) {
		this.myPolicies = myPolicies;
	}

	public String getBusinessNum() {
		return businessNum;
	}

	public void setBusinessNum(String businessNum) {
		this.businessNum = businessNum;
	}

	public String getOpenPms() {
		return openPms;
	}

	public void setOpenPms(String openPms) {
		this.openPms = openPms;
	}
    
}