package com.umessage.hotel.domain;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

public class TemplateMessage {

	// 酒店id
	private String hotelId;

	// 接口调用凭证
	private String accessToken;

	// 接收者（用户）的 openid
	private String touser;

	// 所需下发的模板消息的id
	private String templateTitleId;

	// 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
	private String page;

	// 表单提交场景下为 submit事件带上的 formId；支付场景下， 为本次支付的 prepay_id
	private String formid;

	// 模板内容，不填则下发空模板
	private JSONObject data;
	
	private List<String> params;

	// 模板需要放大的关键词，不填则默认无放大
	private String emphasisKeyword;

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplateTitleId() {
		return templateTitleId;
	}

	public void setTemplateTitleId(String templateTitleId) {
		this.templateTitleId = templateTitleId;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getFormid() {
		return formid;
	}

	public void setFormid(String formid) {
		this.formid = formid;
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public String getEmphasisKeyword() {
		return emphasisKeyword;
	}

	public void setEmphasisKeyword(String emphasisKeyword) {
		this.emphasisKeyword = emphasisKeyword;
	}

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}
	
}
