package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_breakfast_coupon")
public class BreakfastCoupon {
    /**
     * 主键id
     */
    @Id
    private Integer id;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    private String memberId;

    /**
     * 早餐券订单id
     */
    @Column(name = "bf_order_id")
    private String bfOrderId;

    /**
     * 1赠送2购买
     */
    private String type;

    /**
     * 有效期开始
     */
    @Column(name = "valid_date")
    private String validDate;

    /**
     * 0未使用1已使用
     */
    @Column(name = "use_status")
    private String useStatus;

    /**
     * 使用记录id
     */
    @Column(name = "record_id")
    private String recordId;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 使用时间
     */
    @Column(name = "use_time")
    private Date useTime;
    
    @Column(name = "hotel_id")
    private String hotelId;
    
    
    public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	@Transient
    private String recordSn;
    
	public String getRecordSn() {
		return recordSn;
	}

	public void setRecordSn(String recordSn) {
		this.recordSn = recordSn;
	}

	/**
     * 获取主键id
     *
     * @return id - 主键id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键id
     *
     * @param id 主键id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取会员id
     *
     * @return member_id - 会员id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 设置会员id
     *
     * @param memberId 会员id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * 获取早餐券订单id
     *
     * @return bf_order_id - 早餐券订单id
     */
    public String getBfOrderId() {
        return bfOrderId;
    }

    /**
     * 设置早餐券订单id
     *
     * @param bfOrderId 早餐券订单id
     */
    public void setBfOrderId(String bfOrderId) {
        this.bfOrderId = bfOrderId == null ? null : bfOrderId.trim();
    }

    /**
     * 获取1赠送2购买
     *
     * @return type - 1赠送2购买
     */
    public String getType() {
        return type;
    }

    /**
     * 设置1赠送2购买
     *
     * @param type 1赠送2购买
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取有效期开始
     *
     * @return valid_date - 有效期开始
     */
    public String getValidDate() {
        return validDate;
    }

    /**
     * 设置有效期开始
     *
     * @param validDate 有效期开始
     */
    public void setValidDate(String validDate) {
        this.validDate = validDate == null ? null : validDate.trim();
    }

    /**
     * 获取0未使用1已使用
     *
     * @return use_status - 0未使用1已使用
     */
    public String getUseStatus() {
        return useStatus;
    }

    /**
     * 设置0未使用1已使用
     *
     * @param useStatus 0未使用1已使用
     */
    public void setUseStatus(String useStatus) {
        this.useStatus = useStatus == null ? null : useStatus.trim();
    }

    /**
     * 获取使用记录id
     *
     * @return record_id - 使用记录id
     */
    public String getRecordId() {
        return recordId;
    }

    /**
     * 设置使用记录id
     *
     * @param recordId 使用记录id
     */
    public void setRecordId(String recordId) {
        this.recordId = recordId == null ? null : recordId.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取使用时间
     *
     * @return use_time - 使用时间
     */
    public Date getUseTime() {
        return useTime;
    }

    /**
     * 设置使用时间
     *
     * @param useTime 使用时间
     */
    public void setUseTime(Date useTime) {
        this.useTime = useTime;
    }
}