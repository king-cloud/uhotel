package com.umessage.hotel.domain;


public class CreatBookRq {

	 private String hotelId;//酒店id

	    private String checkInDate;//入住日期 格式yyyy/MM/dd       *必传*

	    private String checkOutDate;//离店日期 格式yyyy/MM/dd        *必传*
	    
	    private String roomType;//房间类型   *必传*
	    
	    private String roomNo;//房间号  (可以为空)
	    
	    private String rmrate;//一天房价    (可以为空)
	    
	    private String fullName;//姓名       *必传*
	    
	    private String idCode;//身份证类型 *必传*  身份证传01
	    
	    private String idNo;//身份证号 *必传*
	    
	    private String sex;//性别       *必传*   1:男   2:女
	    
	    private String address;//  住址       (可以为空)
	    
	    private String phone;//手机号   (可以为空)  有就传

		public String getHotelId() {
			return hotelId;
		}

		public void setHotelId(String hotelId) {
			this.hotelId = hotelId;
		}

		public String getCheckInDate() {
			return checkInDate;
		}

		public void setCheckInDate(String checkInDate) {
			this.checkInDate = checkInDate;
		}

		public String getCheckOutDate() {
			return checkOutDate;
		}

		public void setCheckOutDate(String checkOutDate) {
			this.checkOutDate = checkOutDate;
		}

		public String getRoomType() {
			return roomType;
		}

		public void setRoomType(String roomType) {
			this.roomType = roomType;
		}

		public String getRoomNo() {
			return roomNo;
		}

		public void setRoomNo(String roomNo) {
			this.roomNo = roomNo;
		}

		public String getRmrate() {
			return rmrate;
		}

		public void setRmrate(String rmrate) {
			this.rmrate = rmrate;
		}

		public String getFullName() {
			return fullName;
		}

		public void setFullName(String fullName) {
			this.fullName = fullName;
		}

		public String getIdCode() {
			return idCode;
		}

		public void setIdCode(String idCode) {
			this.idCode = idCode;
		}

		public String getIdNo() {
			return idNo;
		}

		public void setIdNo(String idNo) {
			this.idNo = idNo;
		}

		public String getSex() {
			return sex;
		}

		public void setSex(String sex) {
			this.sex = sex;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}
    
	   
    
    
	
    
    
}