package com.umessage.hotel.domain;

import javax.persistence.*;

@Table(name = "t_member_discount")
public class MemberDiscount {
    /**
     * id  
     */
    @Id
    @Column(name = "member_id")
    private Integer memberId;

    /**
     * 会员等级 1-7 暂定
     */
    @Column(name = "member_type")
    private Integer memberType;

    /**
     * 会员名称 
     */
    @Column(name = "member_name")
    private String memberName;

    /**
     * 折扣内容  例如 满减200  折扣 95%
     */
    @Column(name = "member_discount")
    private String memberDiscount;

    /**
     * 0  为满减   1为折扣
     */
    @Column(name = "member_discount_type")
    private Integer memberDiscountType;

    /**
     * 1开启  0关闭
     */
    @Column(name = "member_switch")
    private Integer memberSwitch;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 获取id  
     *
     * @return member_id - id  
     */
    public Integer getMemberId() {
        return memberId;
    }

    /**
     * 设置id  
     *
     * @param memberId id  
     */
    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    /**
     * 获取会员等级 1-7 暂定
     *
     * @return member_type - 会员等级 1-7 暂定
     */
    public Integer getMemberType() {
        return memberType;
    }

    /**
     * 设置会员等级 1-7 暂定
     *
     * @param memberType 会员等级 1-7 暂定
     */
    public void setMemberType(Integer memberType) {
        this.memberType = memberType;
    }

    /**
     * 获取会员名称 
     *
     * @return member_name - 会员名称 
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     * 设置会员名称 
     *
     * @param memberName 会员名称 
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    /**
     * 获取折扣内容  例如 满减200  折扣 95%
     *
     * @return member_discount - 折扣内容  例如 满减200  折扣 95%
     */
    public String getMemberDiscount() {
        return memberDiscount;
    }

    /**
     * 设置折扣内容  例如 满减200  折扣 95%
     *
     * @param memberDiscount 折扣内容  例如 满减200  折扣 95%
     */
    public void setMemberDiscount(String memberDiscount) {
        this.memberDiscount = memberDiscount == null ? null : memberDiscount.trim();
    }

    /**
     * 获取0  为满减   1为折扣
     *
     * @return member_discount_type - 0  为满减   1为折扣
     */
    public Integer getMemberDiscountType() {
        return memberDiscountType;
    }

    /**
     * 设置0  为满减   1为折扣
     *
     * @param memberDiscountType 0  为满减   1为折扣
     */
    public void setMemberDiscountType(Integer memberDiscountType) {
        this.memberDiscountType = memberDiscountType;
    }

    /**
     * 获取1开启  0关闭
     *
     * @return member_switch - 1开启  0关闭
     */
    public Integer getMemberSwitch() {
        return memberSwitch;
    }

    /**
     * 设置1开启  0关闭
     *
     * @param memberSwitch 1开启  0关闭
     */
    public void setMemberSwitch(Integer memberSwitch) {
        this.memberSwitch = memberSwitch;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }
}