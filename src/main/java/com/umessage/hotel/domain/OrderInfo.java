package com.umessage.hotel.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

import io.swagger.annotations.ApiModelProperty;

@Table(name = "t_order_info")
public class OrderInfo {
	/**
	 * 订单id
	 */
	@Id
	@Column(name = "order_id")
	private String orderId;

	/**
	 * 订单号
	 */
	@Column(name = "order_sn")
	private String orderSn;

	/**
	 * 酒店id
	 */
	@Column(name = "hotel_id")
	private Integer hotelId;

	/**
	 * 微信用户openid
	 */
	@Column(name = "openid")
	private String openid;

	/**
	 * 用户id
	 */
	@Column(name = "member_id")
	private String memberId;

	/**
	 * 订单价格
	 */
	@Column(name = "total_price")
	private BigDecimal totalPrice;

	/**
	 * 折扣价格
	 */
	@Column(name = "discount_price")
	private BigDecimal discountPrice;

	/**
	 * 已付金额
	 */
	@Column(name = "money_paid")
	private BigDecimal moneyPaid;

	/**
	 * 应付金额
	 */
	@Column(name = "order_amount")
	private BigDecimal orderAmount;

	/**
	 * 实付金额
	 */
	@Column(name = "real_pay")
	private BigDecimal realPay;

	/**
	 * 支付状态(0未支付1已支付) 2:到店,3:担保
	 */
	@Column(name = "pay_status")
	private Integer payStatus;

	/**
	 * 支付类型。1到店支付、2担保支付、3全额支付
	 */
	@Column(name = "pay_type")
	private Integer payType;

	/**
	 * 支付方式。1余额、2微信、3到店付、4混合付
	 */
	@Column(name = "pay_way")
	private Integer payWay;

	/**
	 * 联系方式
	 */
	private String mobile;

	/**
	 * 电子邮箱
	 */
	private String email;

	/**
	 * 入住人
	 */
	private String resident;

	/**
	 * 房间id
	 */
	@Column(name = "room_id")
	private Integer roomId;

	/**
	 * 房间单价
	 */
	@Column(name = "room_unit_price")
	private BigDecimal roomUnitPrice;

	/**
	 * 房间数目
	 */
	@Column(name = "room_count")
	private Integer roomCount;

	/**
	 * 用户等级
	 */
	@Column(name = "user_grade")
	private String userGrade;

	/**
	 * 优惠券id
	 */
	@Column(name = "coupon_id")
	private Integer couponId;

	/**
	 * 下单时间
	 */
	@Column(name = "book_time")
	private Date bookTime;

	/**
	 * 入住时间
	 */
	@Column(name = "checkin_time")
	private String checkinTime;

	/**
	 * 离店时间
	 */
	@Column(name = "checkout_time")
	private String checkoutTime;

	/**
	 * 订单状态（0新建，1已确认，2已取消,3已入住）
	 */
	@Column(name = "order_status")
	private String orderStatus;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 是否开票
	 */
	@Column(name = "invoice_flag")
	private String invoiceFlag;

	/**
	 * 开票时间
	 */
	@Column(name = "invoice_time")
	private Date invoiceTime;

	/**
	 * 取消时间
	 */
	@Column(name = "cancel_time")
	private Date cancelTime;

	/**
	 * 取消人。用户或者管理员都可以取消
	 */
	@Column(name = "cancel_person")
	private String cancelPerson;

	@Transient
	private String staffName;
	
	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * 取消类型(1酒店取消2用户取消)
	 */
	@Column(name = "cancel_type")
	private Integer cancelType;

	/**
	 * 取消理由
	 */
	@Column(name = "cancel_reason")
	private String cancelReason;

	/**
	 * 确认时间
	 */
	@Column(name = "confirm_time")
	private Date confirmTime;

	/**
	 * 确认账号
	 */
	@Column(name = "confirm_person")
	private String confirmPerson;

	/**
	 * 订单号
	 */
	@Column(name = "prepay_id")
	private String prepayId;

	/**
	 * 表单formId，用于发送模板消息
	 */
	@Column(name = "form_id")
	private String formId;

	/**
	 * 订单使用的余额
	 */
	@Transient
	private BigDecimal useBalance;
	
	@Column(name="notcancle")
	private String notCancle;
	
	@Column(name="name")
	private String name;

	/**
	 * 获取订单id
	 *
	 * @return order_id - 订单id
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * 设置订单id
	 *
	 * @param orderId 订单id
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId == null ? null : orderId.trim();
	}

	/**
	 * 获取订单号
	 *
	 * @return order_sn - 订单号
	 */
	public String getOrderSn() {
		return orderSn;
	}

	/**
	 * 设置订单号
	 *
	 * @param orderSn 订单号
	 */
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn == null ? null : orderSn.trim();
	}

	/**
	 * 获取酒店id
	 *
	 * @return hotel_id - 酒店id
	 */
	public Integer getHotelId() {
		return hotelId;
	}

	/**
	 * 设置酒店id
	 *
	 * @param hotelId 酒店id
	 */
	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	/**
	 * 获取用户id
	 *
	 * @return member_id - 用户id
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * 设置用户id
	 *
	 * @param memberId 用户id
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId == null ? null : memberId.trim();
	}

	/**
	 * 获取订单价格
	 *
	 * @return total_price - 订单价格
	 */
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * 设置订单价格
	 *
	 * @param totalPrice 订单价格
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * 获取折扣价格
	 *
	 * @return discount_price - 折扣价格
	 */
	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	/**
	 * 设置折扣价格
	 *
	 * @param discountPrice 折扣价格
	 */
	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	/**
	 * 获取已付金额
	 *
	 * @return money_paid - 已付金额
	 */
	public BigDecimal getMoneyPaid() {
		return moneyPaid;
	}

	/**
	 * 设置已付金额
	 *
	 * @param moneyPaid 已付 金额(余额 )
	 */
	public void setMoneyPaid(BigDecimal moneyPaid) {
		this.moneyPaid = moneyPaid;
	}

	/**
	 * 获取应付金额
	 *
	 * @return order_amount - 应付金额
	 */
	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	/**
	 * 设置应付金额
	 *
	 * @param orderAmount 应付金额
	 */
	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public BigDecimal getRealPay() {
		return realPay;
	}

	public void setRealPay(BigDecimal realPay) {
		this.realPay = realPay;
	}

	/**
	 * 获取支付状态
	 *
	 * @return pay_status - 支付状态
	 */
	public Integer getPayStatus() {
		return payStatus;
	}

	/**
	 * 设置支付状态
	 *
	 * @param payStatus 支付状态
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	/**
	 * 获取支付类型。1到店支付、2担保支付、3全额支付
	 *
	 * @return pay_type - 支付类型。1到店支付、2担保支付、3全额支付
	 */
	public Integer getPayType() {
		return payType;
	}

	/**
	 * 设置支付类型。1到店支付、2担保支付、3全额支付
	 *
	 * @param payType 支付类型。1到店支付、2担保支付、3全额支付
	 */
	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	/**
	 * 获取支付方式。1余额、2微信、3到店付、4混合付
	 *
	 * @return pay_way - 支付方式。1余额、2微信、3到店付、4混合付
	 */
	public Integer getPayWay() {
		return payWay;
	}

	/**
	 * 设置支付方式。1余额、2微信、3到店付、4混合付
	 *
	 * @param payWay 支付方式。1余额、2微信、3到店付、4混合付
	 */
	public void setPayWay(Integer payWay) {
		this.payWay = payWay;
	}

	/**
	 * 获取联系方式
	 *
	 * @return mobile - 联系方式
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * 设置联系方式
	 *
	 * @param mobile 联系方式
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile == null ? null : mobile.trim();
	}

	/**
	 * 获取电子邮箱
	 *
	 * @return email - 电子邮箱
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置电子邮箱
	 *
	 * @param email 电子邮箱
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * 获取入住人
	 *
	 * @return resident - 入住人
	 */
	public String getResident() {
		return resident;
	}

	/**
	 * 设置入住人
	 *
	 * @param resident 入住人
	 */
	public void setResident(String resident) {
		this.resident = resident == null ? null : resident.trim();
	}

	/**
	 * 获取房间id
	 *
	 * @return room_id - 房间id
	 */
	public Integer getRoomId() {
		return roomId;
	}

	/**
	 * 设置房间id
	 *
	 * @param roomId 房间id
	 */
	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public BigDecimal getRoomUnitPrice() {
		return roomUnitPrice;
	}

	public void setRoomUnitPrice(BigDecimal roomUnitPrice) {
		this.roomUnitPrice = roomUnitPrice;
	}

	/**
	 * 获取房间数目
	 *
	 * @return room_count - 房间数目
	 */
	public Integer getRoomCount() {
		return roomCount;
	}

	/**
	 * 设置房间数目
	 *
	 * @param roomCount 房间数目
	 */
	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	/**
	 * 获取用户等级
	 *
	 * @return user_grade - 用户等级
	 */
	public String getUserGrade() {
		return userGrade;
	}

	/**
	 * 设置用户等级
	 *
	 * @param userGrade 用户等级
	 */
	public void setUserGrade(String userGrade) {
		this.userGrade = userGrade == null ? null : userGrade.trim();
	}

	/**
	 * 获取优惠券id
	 *
	 * @return coupon_id - 优惠券id
	 */
	public Integer getCouponId() {
		return couponId;
	}

	/**
	 * 设置优惠券id
	 *
	 * @param couponId 优惠券id
	 */
	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	/**
	 * 获取下单时间
	 *
	 * @return book_time - 下单时间
	 */
	public Date getBookTime() {
		return bookTime;
	}

	/**
	 * 设置下单时间
	 *
	 * @param bookTime 下单时间
	 */
	public void setBookTime(Date bookTime) {
		this.bookTime = bookTime;
	}

	/**
	 * 获取入住时间
	 *
	 * @return checkin_time - 入住时间
	 */
	public String getCheckinTime() {
		return checkinTime;
	}

	/**
	 * 设置入住时间
	 *
	 * @param checkinTime 入住时间
	 */
	public void setCheckinTime(String checkinTime) {
		this.checkinTime = checkinTime;
	}

	/**
	 * 获取离店时间
	 *
	 * @return checkout_time - 离店时间
	 */
	public String getCheckoutTime() {
		return checkoutTime;
	}

	/**
	 * 设置离店时间
	 *
	 * @param checkoutTime 离店时间
	 */
	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}

	/**
	 * 获取订单状态
	 *
	 * @return order_status - 订单状态
	 */
	public String getOrderStatus() {
		return orderStatus;
	}

	/**
	 * 设置订单状态
	 *
	 * @param orderStatus 订单状态
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus == null ? null : orderStatus.trim();
	}

	/**
	 * 获取备注
	 *
	 * @return remark - 备注
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置备注
	 *
	 * @param remark 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	/**
	 * 获取是否开票
	 *
	 * @return invoice_flag - 是否开票
	 */
	public String getInvoiceFlag() {
		return invoiceFlag;
	}

	/**
	 * 设置是否开票
	 *
	 * @param invoiceFlag 是否开票
	 */
	public void setInvoiceFlag(String invoiceFlag) {
		this.invoiceFlag = invoiceFlag == null ? null : invoiceFlag.trim();
	}

	/**
	 * 获取开票时间
	 *
	 * @return invoice_time - 开票时间
	 */
	public Date getInvoiceTime() {
		return invoiceTime;
	}

	/**
	 * 设置开票时间
	 *
	 * @param invoiceTime 开票时间
	 */
	public void setInvoiceTime(Date invoiceTime) {
		this.invoiceTime = invoiceTime;
	}

	/**
	 * 获取取消时间
	 *
	 * @return cancel_time - 取消时间
	 */
	public Date getCancelTime() {
		return cancelTime;
	}

	/**
	 * 设置取消时间
	 *
	 * @param cancelTime 取消时间
	 */
	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}

	/**
	 * 获取取消人。用户或者管理员都可以取消
	 *
	 * @return cancel_person - 取消人。用户或者管理员都可以取消
	 */
	public String getCancelPerson() {
		return cancelPerson;
	}

	/**
	 * 设置取消人。用户或者管理员都可以取消
	 *
	 * @param cancelPerson 取消人。用户或者管理员都可以取消
	 */
	public void setCancelPerson(String cancelPerson) {
		this.cancelPerson = cancelPerson;
	}

	/**
	 * 获取取消类型
	 *
	 * @return cancel_type - 取消类型
	 */
	public Integer getCancelType() {
		return cancelType;
	}

	/**
	 * 设置取消类型
	 *
	 * @param cancelType 取消类型
	 */
	public void setCancelType(Integer cancelType) {
		this.cancelType = cancelType;
	}

	/**
	 * 获取取消理由
	 *
	 * @return cancel_reason - 取消理由
	 */
	public String getCancelReason() {
		return cancelReason;
	}

	/**
	 * 设置取消理由
	 *
	 * @param cancelReason 取消理由
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason == null ? null : cancelReason.trim();
	}

	/**
	 * 获取确认时间
	 *
	 * @return confirm_time - 确认时间
	 */
	public Date getConfirmTime() {
		return confirmTime;
	}

	/**
	 * 设置确认时间
	 *
	 * @param confirmTime 确认时间
	 */
	public void setConfirmTime(Date confirmTime) {
		this.confirmTime = confirmTime;
	}

	/**
	 * 获取确认账号
	 *
	 * @return confirm_person - 确认账号
	 */
	public String getConfirmPerson() {
		return confirmPerson;
	}

	/**
	 * 设置确认账号
	 *
	 * @param confirmPerson 确认账号
	 */
	public void setConfirmPerson(String confirmPerson) {
		this.confirmPerson = confirmPerson;
	}

	/**
	 * 房间信息
	 */
	@Transient
	private RoomInfo roomInfo;

	public RoomInfo getRoomInfo() {
		return roomInfo;
	}

	public void setRoomInfo(RoomInfo roomInfo) {
		this.roomInfo = roomInfo;
	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public BigDecimal getUseBalance() {
		return useBalance;
	}

	public void setUseBalance(BigDecimal useBalance) {
		this.useBalance = useBalance;
	}

	public String getNotCancle() {
		return notCancle;
	}

	public void setNotCancle(String notCancle) {
		this.notCancle = notCancle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}