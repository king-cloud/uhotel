package com.umessage.hotel.domain;

import javax.persistence.*;

@Table(name = "t_user_menu")
public class UserMenu {
    @Id
    @Column(name = "user_menu_id")
    private Integer userMenuId;

    /**
     * 员工id
     */
    @Column(name = "user_id")
    private String userId;

    /**
     * 菜单id
     */
    @Column(name = "menu_id")
    private String menuId;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private String addTime;

    /**
     * 添加人
     */
    @Column(name = "add_acc")
    private String addAcc;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private String modifyTime;

    /**
     * 修改人
     */
    @Column(name = "modify_acc")
    private String modifyAcc;

    /**
     * @return user_menu_id
     */
    public Integer getUserMenuId() {
        return userMenuId;
    }

    /**
     * @param userMenuId
     */
    public void setUserMenuId(Integer userMenuId) {
        this.userMenuId = userMenuId;
    }

    /**
     * 获取员工id
     *
     * @return user_id - 员工id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置员工id
     *
     * @param userId 员工id
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * 获取菜单id
     *
     * @return menu_id - 菜单id
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     * 设置菜单id
     *
     * @param menuId 菜单id
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId == null ? null : menuId.trim();
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public String getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(String addTime) {
        this.addTime = addTime == null ? null : addTime.trim();
    }

    /**
     * 获取添加人
     *
     * @return add_acc - 添加人
     */
    public String getAddAcc() {
        return addAcc;
    }

    /**
     * 设置添加人
     *
     * @param addAcc 添加人
     */
    public void setAddAcc(String addAcc) {
        this.addAcc = addAcc == null ? null : addAcc.trim();
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public String getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime == null ? null : modifyTime.trim();
    }

    /**
     * 获取修改人
     *
     * @return modify_acc - 修改人
     */
    public String getModifyAcc() {
        return modifyAcc;
    }

    /**
     * 设置修改人
     *
     * @param modifyAcc 修改人
     */
    public void setModifyAcc(String modifyAcc) {
        this.modifyAcc = modifyAcc == null ? null : modifyAcc.trim();
    }
}