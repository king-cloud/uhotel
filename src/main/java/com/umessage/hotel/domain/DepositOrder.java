package com.umessage.hotel.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_deposit_order")
public class DepositOrder {
	@Id
	@Column(name = "deposit_id")
	private String depositId;

	@Column(name = "hotel_id")
	private String hotelId;

	@Column(name = "member_id")
	private String memberId;

	@Column(name = "real_money")
	private BigDecimal realMoney;

	@Column(name = "virt_money")
	private BigDecimal virtMoney;

	@Column(name = "pay_money")
	private BigDecimal payMoney;

	@Column(name = "coupon_id")
	private Integer couponId;

	@Column(name = "pay_status")
	private String payStatus;

	@Column(name = "pay_time")
	private Date payTime;

	@Column(name = "add_time")
	private Date addTime;

	/**
	 * @return deposit_id
	 */
	public String getDepositId() {
		return depositId;
	}

	/**
	 * @param depositId
	 */
	public void setDepositId(String depositId) {
		this.depositId = depositId == null ? null : depositId.trim();
	}

	/**
	 * @return hotel_id
	 */
	public String getHotelId() {
		return hotelId;
	}

	/**
	 * @param hotelId
	 */
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId == null ? null : hotelId.trim();
	}

	/**
	 * @return member_id
	 */
	public String getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId
	 */
	public void setMemberId(String memberId) {
		this.memberId = memberId == null ? null : memberId.trim();
	}

	/**
	 * @return real_money
	 */
	public BigDecimal getRealMoney() {
		return realMoney;
	}

	/**
	 * @param realMoney
	 */
	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}

	/**
	 * @return virt_money
	 */
	public BigDecimal getVirtMoney() {
		return virtMoney;
	}

	/**
	 * @param virtMoney
	 */
	public void setVirtMoney(BigDecimal virtMoney) {
		this.virtMoney = virtMoney;
	}

	/**
	 * @return pay_money
	 */
	public BigDecimal getPayMoney() {
		return payMoney;
	}

	/**
	 * @param payMoney
	 */
	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	/**
	 * @return pay_status
	 */
	public String getPayStatus() {
		return payStatus;
	}

	/**
	 * @param payStatus
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus == null ? null : payStatus.trim();
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

}