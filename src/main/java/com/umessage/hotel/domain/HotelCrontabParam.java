package com.umessage.hotel.domain;

import javax.persistence.Column;

public class HotelCrontabParam {
    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private Integer hotelId;
    
   
    
    @Column(name = "run_times")
    private Integer runTimes;
    @Column(name = "status")
    private Integer status;
    
    @Column(name = "value1")
    private Integer value1;
    @Column(name = "status1")
    private Integer status1;
    
    
    
    @Column(name = "value2")
    private Integer value2;
    @Column(name = "status2")
    private Integer status2;
    
    
    
    @Column(name = "value3")
    private Integer value3;
    @Column(name = "status3")
    private Integer status3;
    
    
    
    @Column(name = "value4")
    private Integer value4;
    @Column(name = "status4")
    private Integer status4;
    
    
    
    @Column(name = "value5")
    private Integer value5;
    @Column(name = "status5")
    private Integer status5;
	public Integer getHotelId() {
		return hotelId;
	}
	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}
	public Integer getValue1() {
		return value1;
	}
	public void setValue1(Integer value1) {
		this.value1 = value1;
	}
	public Integer getStatus1() {
		return status1;
	}
	public void setStatus1(Integer status1) {
		this.status1 = status1;
	}
	public Integer getValue2() {
		return value2;
	}
	public void setValue2(Integer value2) {
		this.value2 = value2;
	}
	public Integer getStatus2() {
		return status2;
	}
	public void setStatus2(Integer status2) {
		this.status2 = status2;
	}
	public Integer getValue3() {
		return value3;
	}
	public void setValue3(Integer value3) {
		this.value3 = value3;
	}
	public Integer getStatus3() {
		return status3;
	}
	public void setStatus3(Integer status3) {
		this.status3 = status3;
	}
	public Integer getValue4() {
		return value4;
	}
	public void setValue4(Integer value4) {
		this.value4 = value4;
	}
	public Integer getStatus4() {
		return status4;
	}
	public void setStatus4(Integer status4) {
		this.status4 = status4;
	}
	public Integer getValue5() {
		return value5;
	}
	public void setValue5(Integer value5) {
		this.value5 = value5;
	}
	public Integer getStatus5() {
		return status5;
	}
	public void setStatus5(Integer status5) {
		this.status5 = status5;
	}
	public Integer getRunTimes() {
		return runTimes;
	}
	public void setRunTimes(Integer runTimes) {
		this.runTimes = runTimes;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
    
    
    
   
   
   
}





























