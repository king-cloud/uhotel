package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_picture")
public class PictureInfo {
    /**
     * 图片id
     */
    @Id
    @Column(name = "pic_id")
    private Integer picId;

    /**
     * 图片所属表id
     */
    @Column(name = "table_id")
    private String tableId;

    /**
     * 图片所属表
     */
    @Column(name = "table_name")
    private String tableName;

    /**
     * 图片类型 酒店0 房间列表1 房间详情2
     */
    private String type;

    /**
     * 图片地址
     */
    @Column(name = "pic_url")
    private String picUrl;

    /**
     * 图片链接
     */
    @Column(name = "pic_href")
    private String picHref;

    /**
     * 图片排序
     */
    private String sort;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 添加人
     */
    @Column(name = "add_acc")
    private String addAcc;

    /**
     * 获取图片id
     *
     * @return pic_id - 图片id
     */
    public Integer getPicId() {
        return picId;
    }

    /**
     * 设置图片id
     *
     * @param picId 图片id
     */
    public void setPicId(Integer picId) {
        this.picId = picId;
    }

    /**
     * 获取图片所属表id
     *
     * @return table_id - 图片所属表id
     */
    public String getTableId() {
        return tableId;
    }

    /**
     * 设置图片所属表id
     *
     * @param tableId 图片所属表id
     */
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    /**
     * 获取图片所属表
     *
     * @return table_name - 图片所属表
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 设置图片所属表
     *
     * @param tableName 图片所属表
     */
    public void setTableName(String tableName) {
        this.tableName = tableName == null ? null : tableName.trim();
    }

    /**
     * 获取图片类型 酒店0 房间列表1 房间详情2
     *
     * @return type - 图片类型 酒店0 房间列表1 房间详情2
     */
    public String getType() {
        return type;
    }

    /**
     * 设置图片类型 酒店0 房间列表1 房间详情2
     *
     * @param type 图片类型 酒店0 房间列表1 房间详情2
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取图片地址
     *
     * @return pic_url - 图片地址
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * 设置图片地址
     *
     * @param picUrl 图片地址
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

    /**
     * 获取图片链接
     *
     * @return pic_href - 图片链接
     */
    public String getPicHref() {
        return picHref;
    }

    /**
     * 设置图片链接
     *
     * @param picHref 图片链接
     */
    public void setPicHref(String picHref) {
        this.picHref = picHref == null ? null : picHref.trim();
    }

    /**
     * 获取图片排序
     *
     * @return sort - 图片排序
     */
    public String getSort() {
        return sort;
    }

    /**
     * 设置图片排序
     *
     * @param sort 图片排序
     */
    public void setSort(String sort) {
        this.sort = sort == null ? null : sort.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取添加人
     *
     * @return add_acc - 添加人
     */
    public String getAddAcc() {
        return addAcc;
    }

    /**
     * 设置添加人
     *
     * @param addAcc 添加人
     */
    public void setAddAcc(String addAcc) {
        this.addAcc = addAcc;
    }
}