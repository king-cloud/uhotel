package com.umessage.hotel.domain;

/**
 * 取消订单
 * @author CancelBook
 *
 */
public class CancelBook {

    private String hotelId;//酒店id

	private String orderId;//订单号 *必传*


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}


    
    
}