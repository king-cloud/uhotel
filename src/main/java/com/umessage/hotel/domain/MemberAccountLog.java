package com.umessage.hotel.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

@Table(name = "t_member_account_log")
public class MemberAccountLog implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    private Integer id;

    @Column(name = "hotel_id")
    private String hotelId;

    @Column(name = "member_id")
    private String memberId;

    private String type;

    @Column(name = "pay_status")
    private String payStatus;

    @Column(name = "pay_mode")
    private String payMode;

    @Column(name = "pay_time")
    private String payTime;

    @Column(name = "coupon_id")
    private String couponId;

    @Column(name = "real_money")
    private BigDecimal realMoney;

    @Column(name = "virt_money")
    private BigDecimal virtMoney;

    @Column(name = "add_time")
    private String addTime;
    
    @Column(name = "order_id")
    private String orderId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id ;
    }

    /**
     * @return hotel_id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * @param hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * @return member_id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * @param memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * @return type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * @return pay_status
     */
    public String getPayStatus() {
        return payStatus;
    }

    /**
     * @param payStatus
     */
    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus == null ? null : payStatus.trim();
    }

    /**
     * @return pay_mode
     */
    public String getPayMode() {
        return payMode;
    }

    /**
     * @param payMode
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode == null ? null : payMode.trim();
    }

    /**
     * @return pay_time
     */
    public String getPayTime() {
        return payTime;
    }

    /**
     * @param payTime
     */
    public void setPayTime(String payTime) {
        this.payTime = payTime == null ? null : payTime.trim();
    }

    /**
     * @return coupon_id
     */
    public String getCouponId() {
        return couponId;
    }

    /**
     * @param couponId
     */
    public void setCouponId(String couponId) {
        this.couponId = couponId == null ? null : couponId.trim();
    }

    /**
     * @return real_money
     */
    public BigDecimal getRealMoney() {
        return realMoney;
    }

    /**
     * @param realMoney
     */
    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    /**
     * @return virt_money
     */
    public BigDecimal getVirtMoney() {
        return virtMoney;
    }

    /**
     * @param virtMoney
     */
    public void setVirtMoney(BigDecimal virtMoney) {
        this.virtMoney = virtMoney;
    }

    /**
     * @return add_time
     */
    public String getAddTime() {
        return addTime;
    }

    /**
     * @param addTime
     */
    public void setAddTime(String addTime) {
        this.addTime = addTime == null ? null : addTime.trim();
    }

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
    
}
