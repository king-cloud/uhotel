package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_wx_msg_tmpl")
public class WxMsgTmpl {
    /**
     * 微信消息模版
     */
    @Id
    private Integer id;

    @Column(name = "hotel_id")
    private String hotelId;

    private String appid;

    @Column(name = "template_title_id")
    private String templateTitleId;

    @Column(name = "keyword_id_list")
    private String keywordIdList;

    @Column(name = "template_id")
    private String templateId;

    @Column(name = "add_date")
    private Date addDate;

    /**
     * 获取微信消息模版
     *
     * @return id - 微信消息模版
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置微信消息模版
     *
     * @param id 微信消息模版
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return hotel_id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * @param hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * @return appid
     */
    public String getAppid() {
        return appid;
    }

    /**
     * @param appid
     */
    public void setAppid(String appid) {
        this.appid = appid == null ? null : appid.trim();
    }

    /**
     * @return template_title_id
     */
    public String getTemplateTitleId() {
        return templateTitleId;
    }

    /**
     * @param templateTitleId
     */
    public void setTemplateTitleId(String templateTitleId) {
        this.templateTitleId = templateTitleId == null ? null : templateTitleId.trim();
    }

    /**
     * @return keyword_id_list
     */
    public String getKeywordIdList() {
        return keywordIdList;
    }

    /**
     * @param keywordIdList
     */
    public void setKeywordIdList(String keywordIdList) {
        this.keywordIdList = keywordIdList == null ? null : keywordIdList.trim();
    }

    /**
     * @return template_id
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * @param templateId
     */
    public void setTemplateId(String templateId) {
        this.templateId = templateId == null ? null : templateId.trim();
    }

    /**
     * @return add_date
     */
    public Date getAddDate() {
        return addDate;
    }

    /**
     * @param addDate
     */
    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }
}