package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_dining_room")
public class DiningRoom {
    /**
     * 主键
     */
    @Id
    private Integer id;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private Long hotelId;

    /**
     * 餐厅名称
     */
    private String name;

    /**
     * 用餐地点
     */
    private String place;

    /**
     * 餐厅电话
     */
    private String phone;

    /**
     * 单价
     */
    @Column(name = "unit_price")
    private String unitPrice;

    /**
     * 用餐时间
     */
    @Column(name = "opening_time")
    private String openingTime;

    /**
     * 0不可用1可用
     */
    private String status;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public Long getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * 获取餐厅名称
     *
     * @return name - 餐厅名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置餐厅名称
     *
     * @param name 餐厅名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取用餐地点
     *
     * @return place - 用餐地点
     */
    public String getPlace() {
        return place;
    }

    /**
     * 设置用餐地点
     *
     * @param place 用餐地点
     */
    public void setPlace(String place) {
        this.place = place == null ? null : place.trim();
    }

    /**
     * 获取餐厅电话
     *
     * @return phone - 餐厅电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置餐厅电话
     *
     * @param phone 餐厅电话
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 获取单价
     *
     * @return unit_price - 单价
     */
    public String getUnitPrice() {
        return unitPrice;
    }

    /**
     * 设置单价
     *
     * @param unitPrice 单价
     */
    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice == null ? null : unitPrice.trim();
    }

    /**
     * 获取用餐时间
     *
     * @return opening_time - 用餐时间
     */
    public String getOpeningTime() {
        return openingTime;
    }

    /**
     * 设置用餐时间
     *
     * @param openingTime 用餐时间
     */
    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime == null ? null : openingTime.trim();
    }

    /**
     * 获取0不可用1可用
     *
     * @return status - 0不可用1可用
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置0不可用1可用
     *
     * @param status 0不可用1可用
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}