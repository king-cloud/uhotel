package com.umessage.hotel.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Table(name = "t_member_coupon")
public class MemberCoupon {
    /**
     * id
     */
    @Id
    private Integer id;

    /**
     * 用户id
     */
    @Column(name = "member_id")
    private String memberId;

    @Column(name = "coupon_type")
    private String couponType;

    /**
     * 优惠券id
     */
    @Column(name = "coupon_id")
    private Integer couponId;

    /**
     * 使用状态
     */
    private String status;

    /**
     * 使用时间
     */
    @Column(name = "use_time")
    private Date useTime;

    /**
     * 领取时间
     */
    @Column(name = "get_time")
    private Date getTime;
    
    @Column(name = "valid_first_time")
    private String validFirstTime;
    
    @Column(name = "valid_last_time")
    private String validLastTime;
    
    @Transient
    private String mode;//1 直减    2 满减

    
    public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getValidFirstTime() {
		return validFirstTime;
	}

	public void setValidFirstTime(String validFirstTime) {
		this.validFirstTime = validFirstTime;
	}

	public String getValidLastTime() {
		return validLastTime;
	}

	public void setValidLastTime(String validLastTime) {
		this.validLastTime = validLastTime;
	}

	/**
     * 获取id
     *
     * @return id - id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public String getMemberId() {
  		return memberId;
  	}

  	
    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setMemberId(String memberId) {
  		this.memberId = memberId;
  	}
    /**
     * @return coupon_type
     */
    public String getCouponType() {
        return couponType;
    }


	/**
     * @param couponType
     */
    public void setCouponType(String couponType) {
        this.couponType = couponType == null ? null : couponType.trim();
    }

    /**
     * 获取优惠券id
     *
     * @return coupon_id - 优惠券id
     */
    public Integer getCouponId() {
        return couponId;
    }

    /**
     * 设置优惠券id
     *
     * @param couponId 优惠券id
     */
    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    /**
     * 获取使用状态
     *
     * @return status - 使用状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置使用状态
     *
     * @param status 使用状态
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取使用时间
     *
     * @return use_time - 使用时间
     */
    public Date getUseTime() {
        return useTime;
    }

    /**
     * 设置使用时间
     *
     * @param useTime 使用时间
     */
    public void setUseTime(Date useTime) {
        this.useTime = useTime;
    }

    /**
     * 获取领取时间
     *
     * @return get_time - 领取时间
     */
    public Date getGetTime() {
        return getTime;
    }

    /**
     * 设置领取时间
     *
     * @param getTime 领取时间
     */
    public void setGetTime(Date getTime) {
        this.getTime = getTime;
    }
    
	@Transient
	public Coupon cou;
	
	public Coupon getCoup() {
		return cou;
	}

	public void setCoup(Coupon cou) {
		this.cou = cou;
	}
	
}