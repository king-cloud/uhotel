package com.umessage.hotel.domain;

import java.util.Date;
import javax.persistence.*;

@Table(name = "t_wx_cfg")
public class WxCfg {
    @Id
    private String wxcfgid;

    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 1:客户端 2酒店端
     */
    @Column(name = "app_type")
    private String appType;

    /**
     * 支付渠道名称
     */
    private String description;

    /**
     * 微信商户号
     */
    @Column(name = "mch_id")
    private String mchId;

    /**
     * 公众账号ID
     */
    @Column(name = "app_id")
    private String appId;

    private String appkey;

    /**
     * API秘钥
     */
    @Column(name = "secret_key")
    private String secretKey;

    /**
     * 证书pkcs12存储路径
     */
    @Column(name = "apiclient_cert")
    private String apiclientCert;

    /**
     * 生成时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 是否有效
     */
    @Column(name = "is_valid")
    private String isValid;

    /**
     * 支付类型
     */
    @Column(name = "pay_type")
    private String payType;

    /**
     * 微信客户端证书
     */
    @Column(name = "cert_pem")
    private String certPem;

    /**
     * 微信客户端证书密钥
     */
    @Column(name = "key_pem")
    private String keyPem;

    /**
     * @return wxcfgid
     */
    public String getWxcfgid() {
        return wxcfgid;
    }

    /**
     * @param wxcfgid
     */
    public void setWxcfgid(String wxcfgid) {
        this.wxcfgid = wxcfgid == null ? null : wxcfgid.trim();
    }

    /**
     * @return hotel_id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * @param hotelId
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取01:客户端 02酒店端
     *
     * @return app_type - 01:客户端 02酒店端
     */
    public String getAppType() {
        return appType;
    }

    /**
     * 设置01:客户端 02酒店端
     *
     * @param appType 01:客户端 02酒店端
     */
    public void setAppType(String appType) {
        this.appType = appType == null ? null : appType.trim();
    }

    /**
     * 获取支付渠道名称
     *
     * @return description - 支付渠道名称
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置支付渠道名称
     *
     * @param description 支付渠道名称
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 获取微信商户号
     *
     * @return mch_id - 微信商户号
     */
    public String getMchId() {
        return mchId;
    }

    /**
     * 设置微信商户号
     *
     * @param mchId 微信商户号
     */
    public void setMchId(String mchId) {
        this.mchId = mchId == null ? null : mchId.trim();
    }

    /**
     * 获取公众账号ID
     *
     * @return app_id - 公众账号ID
     */
    public String getAppId() {
        return appId;
    }

    /**
     * 设置公众账号ID
     *
     * @param appId 公众账号ID
     */
    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    /**
     * @return appkey
     */
    public String getAppkey() {
        return appkey;
    }

    /**
     * @param appkey
     */
    public void setAppkey(String appkey) {
        this.appkey = appkey == null ? null : appkey.trim();
    }

    /**
     * 获取API秘钥
     *
     * @return secret_key - API秘钥
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * 设置API秘钥
     *
     * @param secretKey API秘钥
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey == null ? null : secretKey.trim();
    }

    /**
     * 获取证书pkcs12存储路径
     *
     * @return apiclient_cert - 证书pkcs12存储路径
     */
    public String getApiclientCert() {
        return apiclientCert;
    }

    /**
     * 设置证书pkcs12存储路径
     *
     * @param apiclientCert 证书pkcs12存储路径
     */
    public void setApiclientCert(String apiclientCert) {
        this.apiclientCert = apiclientCert == null ? null : apiclientCert.trim();
    }

    /**
     * 获取生成时间
     *
     * @return create_time - 生成时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置生成时间
     *
     * @param createTime 生成时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取是否有效
     *
     * @return is_valid - 是否有效
     */
    public String getIsValid() {
        return isValid;
    }

    /**
     * 设置是否有效
     *
     * @param isValid 是否有效
     */
    public void setIsValid(String isValid) {
        this.isValid = isValid == null ? null : isValid.trim();
    }

    /**
     * 获取支付类型
     *
     * @return pay_type - 支付类型
     */
    public String getPayType() {
        return payType;
    }

    /**
     * 设置支付类型
     *
     * @param payType 支付类型
     */
    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    /**
     * 获取微信客户端证书
     *
     * @return cert_pem - 微信客户端证书
     */
    public String getCertPem() {
        return certPem;
    }

    /**
     * 设置微信客户端证书
     *
     * @param certPem 微信客户端证书
     */
    public void setCertPem(String certPem) {
        this.certPem = certPem == null ? null : certPem.trim();
    }

    /**
     * 获取微信客户端证书密钥
     *
     * @return key_pem - 微信客户端证书密钥
     */
    public String getKeyPem() {
        return keyPem;
    }

    /**
     * 设置微信客户端证书密钥
     *
     * @param keyPem 微信客户端证书密钥
     */
    public void setKeyPem(String keyPem) {
        this.keyPem = keyPem == null ? null : keyPem.trim();
    }
}