package com.umessage.hotel.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "t_hotel_crontab")
public class HotelCrontab implements Serializable{
	/**
     * 主键
     */
    @Id
    private Integer id;
    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private Integer hotelId;
    /**
     * 酒店名称
     */
    @Column(name = "hotel_name")
    private String hotelName;
    /**
     * 任务ID
     */
    @Column(name = "task_id")
    private Integer taskId;
    /**
     * 任务名称
     */
    @Column(name = "task_name")
    private String taskName;
    
    /**
     * 任务描述
     */
    @Column(name = "task_description")
    private String taskDescription;
	
    /**
     * 任务是否有效
     */
    @Column(name = "status")
    private Integer status;
    /**
     * 是否执行中
     */
    @Column(name = "running")
    private Integer running;
    /**
     * 执行时间类型
     */
    @Column(name = "run_type")
    private Integer runType;
    /**
     * 执行时间列表
     */
    @Column(name = "run_times")
    private Integer runTimes;
    
    /**
     * 执行开始时间
     */
    @Column(name = "start_run_time")
    private Date startRunTime;
    /**
     * 执行结束时间
     */
    @Column(name = "end_run_time")
    private Date endRunTime;
    
    /**
    * 执行时间列表
    */
   @Column(name = "remark")
   private String remark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHotelId() {
		return hotelId;
	}

	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getRunning() {
		return running;
	}

	public void setRunning(Integer running) {
		this.running = running;
	}

	public Integer getRunType() {
		return runType;
	}

	public void setRunType(Integer runType) {
		this.runType = runType;
	}

	

	public Integer getRunTimes() {
		return runTimes;
	}

	public void setRunTimes(Integer runTimes) {
		this.runTimes = runTimes;
	}

	public Date getStartRunTime() {
		return startRunTime;
	}

	public void setStartRunTime(Date startRunTime) {
		this.startRunTime = startRunTime;
	}

	public Date getEndRunTime() {
		return endRunTime;
	}

	public void setEndRunTime(Date endRunTime) {
		this.endRunTime = endRunTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
   
   
}





























