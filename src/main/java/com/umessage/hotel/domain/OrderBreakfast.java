package com.umessage.hotel.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "t_order_breakfast")
public class OrderBreakfast {
    @Id
    @Column(name = "bf_order_id")
    private Integer bfOrderId;

    @Column(name = "bf_order_sn")
    private String bfOrderSn;
    
    @Column(name = "openid")
    private String openid;
    
    @Column(name = "bkf_order_id")
    private String bkfOrderId;
    

    public String getBkfOrderId() {
		return bkfOrderId;
	}

	public void setBkfOrderId(String bkfOrderId) {
		this.bkfOrderId = bkfOrderId;
	}

	/**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    private String memberId;

    /**
     * 1赠送2购买
     */
    private String type;

    /**
     * 支付状态。0未支付1已支付2支付失败 5不需支付订单
     */
    @Column(name = "pay_status")
    private String payStatus;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 房间号
     */
    @Column(name = "room_no")
    private String roomNo;

    /**
     * 有效期开始
     */
    @Column(name = "valid_start")
    private String validStart;

    /**
     * 有效期结束
     */
    @Column(name = "valid_end")
    private String validEnd;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 表单formId，用于发送模板消息
     */
    @Column(name = "form_id")
    private String formId;

    /**
     * 微信支付预订单id
     */
    @Column(name = "prepay_id")
    private String prepayId;

    /**
     * 总数量
     */
    private Integer amount;

    /**
     * 总价
     */
    @Column(name = "sum_price")
    private BigDecimal sumPrice;

    /**
     * 每天数量
     */
    @Column(name = "num_daily")
    private Integer numDaily;

    /**
     * 单价
     */
    @Column(name = "unit_price")
    private BigDecimal unitPrice;
    
    @Transient
    private Integer days;

    
    public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	/**
     * @return bf_order_id
     */
    public Integer getBfOrderId() {
        return bfOrderId;
    }

    /**
     * @param bfOrderId
     */
    public void setBfOrderId(Integer bfOrderId) {
        this.bfOrderId = bfOrderId;
    }

    /**
     * @return bf_order_sn
     */
    public String getBfOrderSn() {
        return bfOrderSn;
    }

    /**
     * @param bfOrderSn
     */
    public void setBfOrderSn(String bfOrderSn) {
        this.bfOrderSn = bfOrderSn == null ? null : bfOrderSn.trim();
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取会员id
     *
     * @return member_id - 会员id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 设置会员id
     *
     * @param memberId 会员id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * 获取1赠送2购买
     *
     * @return type - 1赠送2购买
     */
    public String getType() {
        return type;
    }

    /**
     * 设置1赠送2购买
     *
     * @param type 1赠送2购买
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取支付状态。0未支付1已支付2支付失败
     *
     * @return pay_status - 支付状态。0未支付1已支付2支付失败
     */
    public String getPayStatus() {
        return payStatus;
    }

    /**
     * 设置支付状态。0未支付1已支付2支付失败
     *
     * @param payStatus 支付状态。0未支付1已支付2支付失败 5不需支付订单
     */
    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus == null ? null : payStatus.trim();
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取房间号
     *
     * @return room_no - 房间号
     */
    public String getRoomNo() {
        return roomNo;
    }

    /**
     * 设置房间号
     *
     * @param roomNo 房间号
     */
    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo == null ? null : roomNo.trim();
    }

    /**
     * 获取有效期开始
     *
     * @return valid_start - 有效期开始
     */
    public String getValidStart() {
        return validStart;
    }

    /**
     * 设置有效期开始
     *
     * @param validStart 有效期开始
     */
    public void setValidStart(String validStart) {
        this.validStart = validStart == null ? null : validStart.trim();
    }

    /**
     * 获取有效期结束
     *
     * @return valid_end - 有效期结束
     */
    public String getValidEnd() {
        return validEnd;
    }

    /**
     * 设置有效期结束
     *
     * @param validEnd 有效期结束
     */
    public void setValidEnd(String validEnd) {
        this.validEnd = validEnd == null ? null : validEnd.trim();
    }

    /**
     * 获取添加时间
     *
     * @return add_time - 添加时间
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * 设置添加时间
     *
     * @param addTime 添加时间
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 获取表单formId，用于发送模板消息
     *
     * @return form_id - 表单formId，用于发送模板消息
     */
    public String getFormId() {
        return formId;
    }

    /**
     * 设置表单formId，用于发送模板消息
     *
     * @param formId 表单formId，用于发送模板消息
     */
    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    /**
     * 获取微信支付预订单id
     *
     * @return prepay_id - 微信支付预订单id
     */
    public String getPrepayId() {
        return prepayId;
    }

    /**
     * 设置微信支付预订单id
     *
     * @param prepayId 微信支付预订单id
     */
    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId == null ? null : prepayId.trim();
    }

    /**
     * 获取总数量
     *
     * @return amount - 总数量
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * 设置总数量
     *
     * @param amount 总数量
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * 获取总价
     *
     * @return sum_price - 总价
     */
    public BigDecimal getSumPrice() {
        return sumPrice;
    }

    /**
     * 设置总价
     *
     * @param sumPrice 总价
     */
    public void setSumPrice(BigDecimal sumPrice) {
        this.sumPrice = sumPrice;
    }

    /**
     * 获取每天数量
     *
     * @return num_daily - 每天数量
     */
    public Integer getNumDaily() {
        return numDaily;
    }

    /**
     * 设置每天数量
     *
     * @param numDaily 每天数量
     */
    public void setNumDaily(Integer numDaily) {
        this.numDaily = numDaily ;
    }

    /**
     * 获取单价
     *
     * @return unit_price - 单价
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * 设置单价
     *
     * @param unitPrice 单价
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
    
}