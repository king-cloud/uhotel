package com.umessage.hotel.domain;

import javax.persistence.*;

@Table(name = "t_order_pms")
public class OrderPms {
    /**
     * 主键自增
     */
    @Id
    private Integer id;

    /**
     * 酒店id
     */
    @Column(name = "hotel_id")
    private String hotelId;

    /**
     * 会员id
     */
    @Column(name = "member_id")
    private String memberId;

    /**
     * 小程序订单号
     */
    @Column(name = "order_id")
    private String orderId;

    /**
     * pms订单号
     */
    @Column(name = "pmsorder_id")
    private String pmsorderId;

    /**
     * 入住时间 yyyy/mm/dd
     */
    @Column(name = "checkin_date")
    private String checkinDate;

    /**
     * 离店日期 yyyy/mm/dd
     */
    @Column(name = "checkout_date")
    private String checkoutDate;

    /**
     * 房间号
     */
    @Column(name = "room_no")
    private String roomNo;

    /**
     * 房类代码
     */
    @Column(name = "room_type")
    private String roomType;

    /**
     * 预定人姓名
     */
    private String fname;

    /**
     * 入住人
     */
    private String resident;

    /**
     * 创建时间 yyyy/mm/dd HH:MM:
     */
    @Column(name = "create_time")
    private String createTime;
    
    /**
     *是否退房  0 未退   1退房  2已取消
     */
    @Column(name = "ischeckout")
    private String ischeckout;

    /**
    *是否退钱 0 未退   否者退款金额 
    */
   @Column(name = "isrefund")
   private String isrefund;
    /**
     * 获取主键自增
     *
     * @return id - 主键自增
     */
    public Integer getId() {
        return id;
    }

    public String getIscheckout() {
		return ischeckout;
	}

	public void setIscheckout(String ischeckout) {
		this.ischeckout = ischeckout;
	}

	public String getIsrefund() {
		return isrefund;
	}

	public void setIsrefund(String isrefund) {
		this.isrefund = isrefund;
	}

	/**
     * 设置主键自增
     *
     * @param id 主键自增
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取酒店id
     *
     * @return hotel_id - 酒店id
     */
    public String getHotelId() {
        return hotelId;
    }

    /**
     * 设置酒店id
     *
     * @param hotelId 酒店id
     */
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    /**
     * 获取会员id
     *
     * @return member_id - 会员id
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * 设置会员id
     *
     * @param memberId 会员id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * 获取小程序订单号
     *
     * @return order_id - 小程序订单号
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * 设置小程序订单号
     *
     * @param orderId 小程序订单号
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    /**
     * 获取pms订单号
     *
     * @return pmsorder_id - pms订单号
     */
    public String getPmsorderId() {
        return pmsorderId;
    }

    /**
     * 设置pms订单号
     *
     * @param pmsorderId pms订单号
     */
    public void setPmsorderId(String pmsorderId) {
        this.pmsorderId = pmsorderId == null ? null : pmsorderId.trim();
    }

    /**
     * 获取入住时间 yyyy/mm/dd
     *
     * @return checkin_date - 入住时间 yyyy/mm/dd
     */
    public String getCheckinDate() {
        return checkinDate;
    }

    /**
     * 设置入住时间 yyyy/mm/dd
     *
     * @param checkinDate 入住时间 yyyy/mm/dd
     */
    public void setCheckinDate(String checkinDate) {
        this.checkinDate = checkinDate == null ? null : checkinDate.trim();
    }

    /**
     * 获取离店日期 yyyy/mm/dd
     *
     * @return checkout_date - 离店日期 yyyy/mm/dd
     */
    public String getCheckoutDate() {
        return checkoutDate;
    }

    /**
     * 设置离店日期 yyyy/mm/dd
     *
     * @param checkoutDate 离店日期 yyyy/mm/dd
     */
    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate == null ? null : checkoutDate.trim();
    }

    /**
     * 获取房间号
     *
     * @return room_no - 房间号
     */
    public String getRoomNo() {
        return roomNo;
    }

    /**
     * 设置房间号
     *
     * @param roomNo 房间号
     */
    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo == null ? null : roomNo.trim();
    }

    /**
     * 获取房类代码
     *
     * @return room_type - 房类代码
     */
    public String getRoomType() {
        return roomType;
    }

    /**
     * 设置房类代码
     *
     * @param roomType 房类代码
     */
    public void setRoomType(String roomType) {
        this.roomType = roomType == null ? null : roomType.trim();
    }

    /**
     * 获取预定人姓名
     *
     * @return fname - 预定人姓名
     */
    public String getFname() {
        return fname;
    }

    /**
     * 设置预定人姓名
     *
     * @param fname 预定人姓名
     */
    public void setFname(String fname) {
        this.fname = fname == null ? null : fname.trim();
    }

    /**
     * 获取入住人
     *
     * @return resident - 入住人
     */
    public String getResident() {
        return resident;
    }

    /**
     * 设置入住人
     *
     * @param resident 入住人
     */
    public void setResident(String resident) {
        this.resident = resident == null ? null : resident.trim();
    }

    /**
     * 获取创建时间 yyyy/mm/dd HH:MM:
     *
     * @return create_time - 创建时间 yyyy/mm/dd HH:MM:
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间 yyyy/mm/dd HH:MM:
     *
     * @param createTime 创建时间 yyyy/mm/dd HH:MM:
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }
}