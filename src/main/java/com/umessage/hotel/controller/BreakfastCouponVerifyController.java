package com.umessage.hotel.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.service.RedisService;
import com.umessage.common.util.Atools;
import com.umessage.common.util.HttpUtils;
import com.umessage.hotel.domain.BreakfastCoupon;
import com.umessage.hotel.domain.BreakfastRecord;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.service.BreakfastCouponService;
import com.umessage.hotel.util.RestResultGenerator;
import com.umessage.system.domain.User;

@Controller
@RequestMapping("verification")
public class BreakfastCouponVerifyController extends BaseController{
	@Autowired
	private BreakfastCouponService breakfastCouponService;
	@Autowired
	private RedisService redisService;
	
	@RequestMapping("index")
	public String index() {
		return "hotel/breakfast/breakfastverify";
	}
	
	//private SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * 扫码盒核销早餐
	 * @param code
	 * @return
	 */
	@RequestMapping("jiemi")
	@ResponseBody
	public Map<String,Object> jiemi(String code) {
		Map<String,Object> map=new HashMap<>();
		try {
			String data = Atools.jiemi(code);
			JSONObject obj = JSONObject.parseObject(data); 
			String memberId=(String) obj.get("mid");
			Long timeStamp=(Long) obj.get("time_stamp");
			String useDate=(String) obj.get("use_date");
			Integer useAmount=(Integer) obj.get("use_amount");
			//判断是否是酒店二维码
			if(memberId==null||timeStamp==null||useDate==null||useAmount==null) {
				map.put("code", 500);
				map.put("msg", "核销失败");
				return map;
			}
			//判断二维码是否超过30分钟失效
			if(System.currentTimeMillis()-30*60*1000L>timeStamp) {
				map.put("code", 500);
				map.put("msg", "该二维码已过期！");
				return map;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			BreakfastCoupon coupon=new BreakfastCoupon();
			coupon.setValidDate(sdf.format(new Date()));
			coupon.setMemberId(memberId);
			//判断二维码是否是最新，早餐券可用张数是否正确
			Integer countNum=this.breakfastCouponService.countUseToday(coupon);
			if(useAmount>countNum) {
				map.put("code", 500);
				map.put("msg", "该二维码已过期！");
				return map;
			}
			String BCCache  = redisService.get(timeStamp+memberId);
			String BCData   = memberId+useDate+useAmount;
			if(BCCache != null && !"".equals(BCCache)){ 
				/*String[] BCCacheArray = BCCache.split(",");
				HashSet<String> set = new HashSet<String>();
				for (String str : BCCacheArray) {
					set.add(str);
				}
				if(!set.contains(BCData)){
					redisService.set(timeStamp+memberId, BCCache+","+BCData);
				}else{*/
					map.put("code", 500);
					map.put("msg", "该二维码已过期！");
					return map;
				//}
			}
			map.put("code", 0);
			map.put("msg", useAmount);
			return map;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 核销早餐券
	 * @param request
	 * @param couponType
	 * @return
	 */
	@RequestMapping("check")
	@ResponseBody
	public String checkCoupons(String code){
		try {
			String data = Atools.jiemi(code);
			JSONObject obj = JSONObject.parseObject(data); 
			String memberId=(String) obj.get("mid");
			Long timeStamp=(Long) obj.get("time_stamp");
			String useDate=(String) obj.get("use_date");
			Integer useAmount=(Integer) obj.get("use_amount");
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("mid", memberId);
			map.put("time_stamp", timeStamp);
			map.put("use_date", useDate);
			map.put("use_amount", useAmount);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date validDate = sdf.parse(useDate);
			// 开始核销
			BreakfastRecord useRecord = new BreakfastRecord();
			useRecord.setMemberId(memberId);
			useRecord.setValidDate(validDate);
			useRecord.setAmount(useAmount);
			breakfastCouponService.useBreakfastCoupon(useRecord);
			String BCData   = memberId+useDate+useAmount;
			redisService.set(timeStamp+memberId, BCData);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return "操作成功";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> listHotels(QueryRequest request,BreakfastRecord br) {
		//HotelInfo hotel=new HotelInfo();
		User user=getCurrentUser();
		List<BreakfastRecord> list = this.breakfastCouponService.findRecords(user.getHotelId(),request.getPageNum(), request.getPageSize(),br.getMobile());
		PageInfo<BreakfastRecord> pageInfo = new PageInfo<BreakfastRecord>(list);
		return getDataTable(pageInfo);
	} 
	
}
