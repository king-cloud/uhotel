package com.umessage.hotel.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.MemberInvoice;
import com.umessage.hotel.service.MemberInvoiceService;
import com.umessage.hotel.service.WxMsgTmplService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.system.domain.User;

/**
 * 发票
 * @author zmj
 *
 */
@Controller
@RequestMapping("memberinvoice") 
public class MemberInvoiceController extends BaseController {

	@Autowired
	private MemberInvoiceService memberInvoiceService;
	
	@Autowired
	private WxMsgTmplService wxMsgTmplService;
	
	/**
	 * 发票首页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(){
		return "hotel/invoice/memberInvoice";
	}
	
	/**
	 * 发票列表
	 * @param query
	 * @param request
	 * @return
	 */
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> listMemberInvoice(QueryRequest query,HttpServletRequest request, MemberInvoice invoice){
			try {
				User currentUser = super.getCurrentUser();
				invoice.setHotelId(Integer.parseInt(currentUser.getHotelId()));
				List<MemberInvoice> list = memberInvoiceService.findInvoiceList(invoice,query.getPageNum(),query.getPageSize());
				PageInfo<MemberInvoice> pageInfo = new PageInfo<>(list);
				return getDataTable(pageInfo);
			} catch (NumberFormatException e) {
				e.printStackTrace();
				throw new RuntimeException("操作失败");
			}
	}
	
	/**
	 * 开发票
	 */
	@RequestMapping("/openinvoice")
	@ResponseBody
	public ResponseBo openInvoice(MemberInvoice memberInvoice){
		try {
			memberInvoiceService.openInvoice(memberInvoice.getId(),HotelConstant.INVOICE_STATUS_OPEN);
			return ResponseBo.ok("开票成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("操作失败");
		}
	}
	/**
	 * 取消发票
	 */
	@RequestMapping("/cancle")
	@ResponseBody
	public ResponseBo cancleInvoice(MemberInvoice memberInvoice){
		try {
			memberInvoiceService.updateInvoiceStatus(memberInvoice.getId(),HotelConstant.INVOICE_STATUS_NO);
			return ResponseBo.ok("更新成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("更新失败");
		}
	}
	
}
