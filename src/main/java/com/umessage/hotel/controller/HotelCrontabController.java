package com.umessage.hotel.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.HotelCrontab;
import com.umessage.hotel.domain.HotelCrontabParam;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.PictureInfo;
import com.umessage.hotel.service.HotelCrontabService;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.system.domain.User;

@Controller
@RequestMapping("crontab")
public class HotelCrontabController extends BaseController{
	
	@Autowired
	private HotelInfoService hotelService;
	@Autowired
	private HotelCrontabService hotelCrontabService ;
	
	/**
	 * 设置酒店定时任务首页
	 * @return
	 */
	@RequestMapping("index")
	public String index() {
		return "hotel/hotel/crontab";
	}
	
	/**
	 * 列出酒店所有定时任务
	 * @param request
	 * @param crontab
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> listHotelCrontabs(QueryRequest request,HotelCrontab crontab) {
		//HotelInfo hotel=new HotelInfo();
		//  session中取得当前的用户
		User user=getCurrentUser() ;
		Integer hotelId = Integer.parseInt(user.getHotelId()) ;
		//String hotelId = crontab.getHotelId() ;
		// chuli
		Map<String, Integer> result = new HashMap<String, Integer>() ;
		result.put("hotel_id", hotelId) ;
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<HotelCrontab> list = hotelCrontabService.findCrontabsByHotelId(hotelId) ;
		// 第一次没有数据的时候，需要在表内复制。
		if(list == null || list.size() < 5) {
			hotelCrontabService.insertCrontabsByHotelId(hotelId, crontab.getHotelName()) ;
			list = hotelCrontabService.findCrontabsByHotelId(hotelId) ;
		}
		for(int i = 0 ; i < list.size() ; i ++) {
			HotelCrontab hc = list.get(i) ;
			if(hc.getTaskId() == HotelConstant.HOTEL_CRONTAB_TASK_1) {
				result.put("value1", hc.getRunTimes()) ;
				result.put("status1", hc.getStatus()) ;
			}else if(hc.getTaskId() == HotelConstant.HOTEL_CRONTAB_TASK_2) {
				result.put("value2", hc.getRunTimes()) ;
				result.put("status2", hc.getStatus()) ;
			}else if(hc.getTaskId() == HotelConstant.HOTEL_CRONTAB_TASK_3) {
				result.put("value3", hc.getRunTimes()) ;
				result.put("status3", hc.getStatus()) ;
			}else if(hc.getTaskId() == HotelConstant.HOTEL_CRONTAB_TASK_4) {
				result.put("value4", hc.getRunTimes()) ;
				result.put("status4", hc.getStatus()) ;
			}else if(hc.getTaskId() == HotelConstant.HOTEL_CRONTAB_TASK_5) {
				result.put("value5", hc.getRunTimes()) ;
				result.put("status5", hc.getStatus()) ;
			}
		}
		List values = new ArrayList() ;
		values.add(result) ;
		PageInfo<HotelCrontab> pageInfo = new PageInfo<HotelCrontab>(values);
		return getDataTable(pageInfo);
	}
	
	
	/**
	 * 更新一个酒店中所有的定时任务设置
	 * @param params
	 * @return
	 */
	@RequestMapping("update")
	@ResponseBody
	public ResponseBo updateHotelCrontabs(HotelCrontabParam param) {
		try {
			//Integer hotelId = param.getHotelId() ;
			User user=getCurrentUser() ;
			Integer hotelId = Integer.parseInt(user.getHotelId()) ;
			if(hotelId == null || hotelId < 0) {
				return ResponseBo.error("编辑酒店定时任务信息失败，请联系网站管理员！");
			}
			hotelCrontabService.updateHotelCrontabs(param) ;
			return ResponseBo.ok("酒店定时任务信息编辑成功！");
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("编辑酒店定时任务信息失败，请联系网站管理员！");
		}
	}
//	/**
//	 * 更新一个酒店中所有的定时任务设置
//	 * @param params
//	 * @return
//	 */
//	@RequestMapping("update")
//	@ResponseBody
//	public ResponseBo updateHotelInfo(HotelCrontabParam params) {
//		try {
//			User user = super.getCurrentUser();
//			hotel.setModifyTime(new Date());
//			hotel.setModifyAcc(user.getUserId()+"");
//			Integer type = hotelService.updateHotel(hotel);
//			if(type==0) {
//				return ResponseBo.ok(type);
//			}else {
//				if(user.getHotelId()==null) {
//					return ResponseBo.ok(1);
//				}
//				PictureInfo pic=new PictureInfo();
//				pic.setAddTime(new Date());
//				pic.setAddAcc(user.getUserId()+"");
//				pic.setTableId(hotel.getHotelId()+"");
//				pic.setType("0");
//				pic.setTableName("t_hotel_info");
//				Object[] objs=hotel.getPictures();
//				this.picService.savePics(objs,pic);
//				return ResponseBo.ok(type);
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			return ResponseBo.error("编辑酒店信息失败，请联系网站管理员！");
//		}
//	}
	
}
