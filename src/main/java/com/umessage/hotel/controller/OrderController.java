package com.umessage.hotel.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.annotation.Log;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.OrderBreakfast;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.WxCfg;
import com.umessage.hotel.service.OrderBreakfastService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.OrderPmsService;
import com.umessage.hotel.service.WxCfgService;
import com.umessage.system.domain.User;

@Controller
@RequestMapping("order")
public class OrderController extends BaseController {

	@Autowired
	private OrderInfoService orderInfoService;
	@Autowired
	private WxCfgService wxcfgService;
	@Autowired
	private OrderPmsService orderPmsService;
	
	@Autowired
	private OrderBreakfastService orderBreakfastService;
	
	private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@RequestMapping("index")
	public String index(){
		return "hotel/order/order";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String,Object> listOrder(QueryRequest query,OrderInfo orderInfo){
		try {
			orderInfo.setCheckinTime(orderInfo.getCheckinTime().replace("-", ""));
			PageHelper.startPage(query.getPageNum(), query.getPageSize());
			User user = super.getCurrentUser();
			if(user.getHotelId() != null){
				orderInfo.setHotelId(Integer.parseInt(user.getHotelId()));
			}else{
				orderInfo.setHotelId(null);
			}
			ArrayList<OrderInfo> orederList = new ArrayList<OrderInfo>();
			orederList = orderInfoService.findOrderListByQuery(orderInfo, query.getPageNum(), query.getPageSize());
			
			if( null != orederList && orederList.size()>0 ){
				for (OrderInfo orderInfo2 : orederList) {
					orderInfo2.setCheckinTime(orderInfo2.getCheckinTime() != null ?df2.format(df2.parse(orderInfo2.getCheckinTime())): "");
					orderInfo2.setCheckoutTime(orderInfo2.getCheckoutTime()!= null ? df2.format(df2.parse(orderInfo2.getCheckoutTime())) :"");
				}
			}
			PageInfo<OrderInfo> pageInfo = new PageInfo<OrderInfo>(orederList);
			return getDataTable(pageInfo);
		} catch (Exception e ) {
			e.printStackTrace();
			throw new RuntimeException("网络波动,请联系管理员");
		} 
	}
	
	@Log("提交订单")
	@RequestMapping("submit")
	@ResponseBody
	public ResponseBo submitOrder(OrderInfo orderInfo){
		try{
			User user = super.getCurrentUser();
			
			orderInfoService.confirmOrder(orderInfo.getHotelId()+"", user.getUserId()+"",orderInfo.getOrderId());
			
			orderInfo  = orderInfoService.selectByKey(orderInfo.getOrderId());
			//发放早餐券
			orderBreakfastService.addOrderBreakfastByRoomInfo(orderInfo);
			
			return ResponseBo.ok("提交订单成功");
		}catch (Exception e){
			e.printStackTrace();
			return ResponseBo.error(e.getMessage());
		}
	}
	
	@Log("取消订单")
	@RequestMapping("cancel")
	@ResponseBody
	public ResponseBo cancelOrder(OrderInfo orderInfo){
		try{
			User user = super.getCurrentUser();
			WxCfg wxCfg = wxcfgService.queryWxCfg(String.valueOf(orderInfo.getHotelId()), "1");//客户端小程序配置
			if(wxCfg == null) {
				return ResponseBo.error("没有找到酒店端小程序wxCfg配置信息");
			}
			String reason = "酒店pc人员操作";
			orderInfoService.cancelOrderByHotel(wxCfg.getWxcfgid(), orderInfo.getHotelId()+"",user.getUserId()+"", orderInfo.getOrderId(), reason);
			
			//释放早餐券
			OrderBreakfast obf=new OrderBreakfast();
			obf.setBkfOrderId(String.valueOf(orderInfo.getOrderId()));
			this.orderBreakfastService.delCouponsByCon(obf);
			
			return ResponseBo.ok("取消订单成功");
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error(e.getMessage());
		}
	}
}
