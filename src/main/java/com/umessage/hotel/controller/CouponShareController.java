package com.umessage.hotel.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.umessage.common.controller.BaseController;
import com.umessage.common.util.HttpUtils;
import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.MemberCoupon;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.service.CouponService;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.MemberCouponService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.util.HotelConstant;


@Controller
@RequestMapping("couponshare")
public class CouponShareController extends BaseController {

	@Autowired
	private CouponService couponService;
	
	@Autowired
	private HotelInfoService hotelInfoService;
	
	@Autowired
	private MemberInfoService memberInfoService;
	
	@Autowired
	private MemberCouponService memberCouponService;
	
	/**
	 * 扫码
	 * @param couponId 优惠券id
	 */
	@RequestMapping("index")
	public String index(@RequestParam(value="coupon_id")String couponId,Model model){
		Coupon  coupon =  couponService.getCouponById(couponId);
		String hotelName = "";
		if(coupon!=null){
			HotelInfo hotelInfo = hotelInfoService.findHotelsById(coupon.getHotelId()+"");
			if(hotelInfo!=null){
				hotelName = hotelInfo.getHotelName();
			}
		}
		model.addAttribute("coupon", coupon);
		model.addAttribute("hotelName", hotelName);
		return "hotel/hotel/couponshareIndex";
	}
	
	/**
	 * 领取首页
	 */
	@RequestMapping("receiveIndex")
	public String receive(@RequestParam(value="coupon_id") String couponId,Model model){
		model.addAttribute("couponId", couponId);
		return "hotel/hotel/receive";
	}
	
	/**
	 * 领取
	 * @param couponId 优惠券id
	 * @param phoneNumber 手机号
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("receive")
	public String receive(String couponId,String phoneNumber,Model model) throws Exception{
//		MemberCoupon memberCoupon = new MemberCoupon();
//		memberCoupon.setCouponId(Integer.parseInt(couponId));
//		memberCoupon.setCouponType(coupon.getCouponType());
//		memberCoupon.setStatus("0");
//		memberCoupon.setGetTime(new Date());
//		//判断是否为会员
//		MemberInfo memberInfo = memberInfoService.findMemberByMobile(coupon.getHotelId()+"", phoneNumber);
//		if(memberInfo ==null){//成为会员
//			MemberInfo member = new MemberInfo();
//			member.setHotelId(coupon.getHotelId()+"");
//			member.setMobile(phoneNumber);
//			String memberId = memberInfoService.saveMember(member);
//			memberCoupon.setMemberId(memberId);
//		}else{
//			memberCoupon.setMemberId(memberInfo.getMemberId());
//		}
//		//领取优惠
//		memberCouponService.saveMemberCoupon(memberCoupon);
		String memberId=null;
		Coupon  coupon =  couponService.getCouponById(couponId); 
		MemberInfo memberInfo = this.memberInfoService.findMemberByMobile(coupon.getHotelId()+"", phoneNumber);
		if(memberInfo==null) {
			MemberInfo member=new MemberInfo();
			member.setHotelId(coupon.getHotelId()+"");
			member.setMobile(phoneNumber);
			member.setAddTime(new Date());
			member.setRealMoney(BigDecimal.ZERO);
			member.setVirtMoney(BigDecimal.ZERO);
			member.setLastLoginTime(new Date());
			member.setGrade(HotelConstant.USER_GRADE_NEW);
			member.setAddTime(new Date());
			memberId = this.memberInfoService.saveMember(member);
		}else {
			memberId = memberInfo.getMemberId();
		}
		String hotelName = "";
		if(coupon!=null){
			HotelInfo hotelInfo = hotelInfoService.findHotelsById(coupon.getHotelId()+"");
			if(hotelInfo!=null){
				hotelName = hotelInfo.getHotelName();
			}
		}
		model.addAttribute("coupon", coupon);
		model.addAttribute("hotelName", hotelName);
		List<MemberCoupon> mclist=this.memberCouponService.findCouponByContion(memberId,couponId);
		if(mclist!=null&&mclist.size()>=1) {
			return "hotel/hotel/alreadycoupon";
		}
		//List<Coupon> coupons = this.couponService.findCoupons(coupon.getHotelId()+"", "1",couponId);
		//if(!coupons.isEmpty()) {
			//Coupon coupon = coupons.get(0);
			MemberCoupon mc=new MemberCoupon();
			mc.setGetTime(new Date());
			mc.setStatus("0");
			mc.setMemberId(memberId);
			mc.setMode(coupon.getMode()+"");
			mc.setCouponId(Integer.valueOf(couponId));
			mc.setCouponType(coupon.getCouponType());
			if(coupon.getValidType()==1) {
				mc.setValidFirstTime(coupon.getValidStart());
				mc.setValidLastTime(coupon.getValidEnd());
			}else
			if(coupon.getValidType()==2) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String valudStart = sdf.format(new Date());
				mc.setValidFirstTime(valudStart);
				Integer cycle = Integer.valueOf(coupon.getCycle());
				Calendar cale = Calendar.getInstance();
				cale.setTime(sdf.parse(valudStart));
				int month1 = cale.get(Calendar.MONTH);
				int pm = month1+cycle;
				cale.set(Calendar.MONTH, pm);
				Date time = cale.getTime();
				String validEnd = sdf.format(time);
				mc.setValidLastTime(validEnd);
			}
			this.memberCouponService.saveMemberCoupon(mc);
			model.addAttribute("mc", mc);
		//}
		return "hotel/hotel/couponshare";
	}
	
	/**
	 * 直接访问二维码
	 */
	@RequestMapping("quCode")
	@ResponseBody
	public String getQRCode(String couponId){
		String utilUrl="http://b.bshare.cn/barCode?site=weixin&url=";
		String targetUrl="http://127.0.0.1:8080/couponshare/receiveIndex";
		String param = "coupon_id="+couponId;
		String result = HttpUtils.sendGet(utilUrl+targetUrl,param);
		System.out.println(result);
		return result;
	}
	
	/**
	 * 分享好友
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("share")
	public String shareInfo(String hotelName,String couponId ,Model model){
		return "";
	}
}
