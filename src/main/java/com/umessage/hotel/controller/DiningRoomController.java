package com.umessage.hotel.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.DiningRoom;
import com.umessage.hotel.service.DiningRoomService;
import com.umessage.system.domain.User;

/**
 * 餐厅
 * @author zmj
 *
 */
@Controller
@RequestMapping("diningroom") 
public class DiningRoomController extends BaseController {

	@Autowired
	private DiningRoomService diningRoomService;
	
	/**
	 * 餐厅首页
	 * @return
	 */
	@RequestMapping("index")
	public String index(){
		return "hotel/breakfast/diningRoom";
	}
	
	/**
	 * 餐厅列表
	 * @param query
	 * @param request
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> listDiningRoom(QueryRequest query,HttpServletRequest request){
			User currentUser = super.getCurrentUser();
			String hotelId = currentUser.getHotelId();
			List<DiningRoom> list = diningRoomService.findDiningRoomList(hotelId, null, query.getPageNum(),query.getPageSize());
			PageInfo<DiningRoom> pageInfo = new PageInfo<>(list);
			return getDataTable(pageInfo);
	}
	
	/**
	 * 新增
	 * @param diningRoom
	 * @param request
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public ResponseBo addDiningRoom(DiningRoom diningRoom,HttpServletRequest request){
		try {
			User currentUser = super.getCurrentUser();
			String hotelId = currentUser.getHotelId();
			diningRoom.setHotelId(Long.parseLong(hotelId));
			diningRoom.setStatus("0");//默认不可用
			diningRoom.setAddTime(new Date());
			diningRoom.setUpdateTime(new Date());
			diningRoomService.save(diningRoom);
			return ResponseBo.ok("新增成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("新增失败");
		}
	}
	
	/**
	 * 更新
	 * @param diningRoom
	 * @param request
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public ResponseBo udpateDiningRoom(DiningRoom diningRoom,HttpServletRequest request){
		try {
			diningRoom.setUpdateTime(new Date());
			diningRoomService.updateNotNull(diningRoom);
			return ResponseBo.ok("更新成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("更新失败");
		}
	}
	
	@RequestMapping("single")
	@ResponseBody
	public DiningRoom DiningRoom(Integer id){
		try {
			return diningRoomService.findDiningRoomById(id);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("查询失败"+e.getMessage());
		}
	}
	
	/**
	 * 改变状态
	 */
	@RequestMapping("/changestate")
	@ResponseBody
	public ResponseBo changeDiningRoomState(DiningRoom diningRoom){
		try {
			diningRoomService.updateNotNull(diningRoom);
			return ResponseBo.ok("更新成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("更新失败");
		}
	}
	
	/**
	 * 验证名称是否重复
	 */
	@RequestMapping("/checkRepeat")
	@ResponseBody
	public ResponseBo checkRepeat(DiningRoom diningRoom){
		try {
			User currentUser = super.getCurrentUser();
			String hotelId = currentUser.getHotelId();
			diningRoom.setHotelId(Long.parseLong(hotelId));
			List<DiningRoom> list = diningRoomService.selectDiningRoom(diningRoom);
			if (null != list && list.size() > 0) {
				return ResponseBo.error();
			} 
			return ResponseBo.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error(e);

		}
	}
	
	
}
