package com.umessage.hotel.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.annotation.Log;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.common.domain.Tree;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.UserMenu;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.UserMemberService;
import com.umessage.system.domain.Dict;
import com.umessage.system.domain.Menu;
import com.umessage.system.domain.User;
import com.umessage.system.service.DictService;
import com.umessage.system.service.MenuService;
import com.umessage.system.service.UserService;


/**
 * 员工 staff
 * @author XJH
 *
 */
@Controller
@RequestMapping("staff")
public class StaffController extends BaseController{
	
	@Autowired
	private UserService userService;
	@Autowired
	private DictService dictService;
	@RequestMapping("index")
	public String index() {
		return "hotel/hotel/staff";
	}
	@RequestMapping("list")
	@ResponseBody
	public Map<String,Object> list(QueryRequest request,String username){
		User currentUser=getCurrentUser();
		User user=new User();
		user.setUsername(username);
		user.setHotelId(currentUser.getHotelId());
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<User> list = this.userService.findUserByHotelId(user);
		PageInfo<User> pageInfo = new PageInfo<User>(list);
		if(list!=null) {
			Dict dict = new Dict();
			dict.setFieldName("postion");
			dict.setTableName("t_user_postion");
			List<Dict> dicts = this.dictService.findAllDicts(dict);
			for (User us : list) {
				for (Dict dic : dicts) {
					if(us.getPosition().equals(dic.getKeyy())) {
						us.setPosition(dic.getValuee());
						break;
					}
				}
			}
			
		}
		return getDataTable(pageInfo);
	}
	@RequestMapping("add")
	@ResponseBody
	public ResponseBo addStaff(User staff){
		try {
			User user=getCurrentUser();
			//查询该手机号是否有其他可用信息
			User mobuser=this.userService.findUserByMobileAll(staff.getMobile());
			//判断该酒店员工手机号是否绑定
			if(mobuser!=null) {
				return ResponseBo.ok("手机号已存在，请确认是本人操作！");
			}
			staff.setPassword("123456");
			staff.setHotelId(user.getHotelId());
			staff.setCrateTime(new Date());
			staff.setModifyTime(new Date());
			/**
			 * 用于区分酒店员工和酒店超级管理员
			 * 1  酒店超级管理员
			 * 0  酒店员工
			 */
			staff.setDeptId(0l);
			this.userService.addUserAndRoleNull(staff,null);
			return ResponseBo.ok("操作成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("操作失败，请联系系统管理员！");
		}
	}
	@RequestMapping("delete")
	@ResponseBody
	public ResponseBo delStaff(Long userId){
		try {
			User staff=new User();
			staff.setUserId(userId);
			staff.setStatus("0");//被禁用（删除）
			this.userService.updateUserAndRoleNull(staff, null);
			return ResponseBo.ok("操作成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("操作失败，请联系系统管理员！");
		}
		
	}
	@RequestMapping("privileges")
	public String privileges(Long userId,Model model) {
		User user1 = super.getCurrentUser();	
		if(userId!=null){
			User user= new User();
			user=userService.getUserByid(userId);
			model.addAttribute("user",user);
		}else{
			model.addAttribute("user",new User());
		}
		HotelInfo hotelInfo=hotelInfoService.findHotelsById(user1.getHotelId());
		model.addAttribute("hotelInfo",hotelInfo);
		Dict dict = new Dict();
		dict.setFieldName("postion");
		dict.setTableName("t_user_postion");
		List<Dict> dicts = this.dictService.findAllDicts(dict);
		List<User> userList=userService.findUserListByHotelId(user1.getHotelId());
		model.addAttribute("userList",userList );
		model.addAttribute("dicts",dicts );
		return "hotel/hotel/privileges";
	}
	@Autowired
	private MenuService menuService;
	@Autowired
	private UserMemberService userMemberService;
	@Autowired 
	private HotelInfoService hotelInfoService;
	@RequestMapping("geUser")
	@ResponseBody
	public User geUser(Long userid){
		if(userid!=null){
		User user =userService.selectByKey(userid);
		HotelInfo hotelInfo=hotelInfoService.findHotelsById(user.getHotelId());
		user.setHotelName(hotelInfo.getHotelName());
			return user;
		}else{
			return new User();
		}
	}
	@RequestMapping("geUsermenus")
	@ResponseBody
	public String geUsermenus(String userid){
		User user = super.getCurrentUser();
		List<UserMenu> userMenulist = userMemberService.getUserMeList(userid);
		String srt="";
		for (int i = 0; i < userMenulist.size(); i++) {
			srt=srt+","+userMenulist.get(i).getMenuId();
		}
		return srt;
	}
	@RequestMapping("save")
	@ResponseBody
	public Integer save(String userid,String mids,String hotelId,String mobile,String ssex,String position){
		User user = super.getCurrentUser();
		Integer n =1;
		try {
			n = userMemberService.del(userid);
			String str[] = mids.split(",");
			UserMenu serMenu =new UserMenu();
			SimpleDateFormat sm= new SimpleDateFormat("yyyy-MM-dd HH:mm");
			for (int i = 0; i < str.length; i++) {
				if(str[i]==""){
				}else{
					serMenu.setUserId(userid);
					serMenu.setMenuId(str[i]);
					serMenu.setHotelId(hotelId);
					serMenu.setAddAcc(user.getUserId().toString());
					serMenu.setAddTime(sm.format(new Date()));
					n= userMemberService.addUserM(serMenu);
				}
			}
		} catch (Exception e) {
		}
		
		User updUser = new User();
		updUser.setUserId(Long.parseLong(userid));
		updUser.setMobile(mobile);
		updUser.setSsex(ssex);
		updUser.setPosition(position);
		userService.updateUserProfile(updUser);
		
		return n;
	}
	
	/**
	 * 员工权限
	 */
	@Log("查询酒店权限列表")
	@RequestMapping("menus")
	@ResponseBody
	public ResponseBo getMenus() {
		try {
			Tree<Menu> menus=this.menuService.getUserMenus();
			return ResponseBo.ok(menus);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取用户菜单失败！");
		}
	}
	@Log("查询权限")
	@RequestMapping("getUserMenu")
	@ResponseBody
	public ResponseBo getUserMenuBycon(String userName) {
		try {
			User user=getCurrentUser();
			Tree<Menu> menus=this.menuService.getUserMenuById(userName,user.getHotelId());
			return ResponseBo.ok(menus);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取用户菜单失败！");
		}
	}
	@Log("查询权限")
	@RequestMapping("saveUserAuth")
	@ResponseBody
	public ResponseBo saveUserAuth() {//UserAuthority ua
		try {
			return ResponseBo.ok("成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("失败");
		}
	}
	@Log("查询职位列表")
	@RequestMapping("postions")
	@ResponseBody
	public List<Dict> getPostions() {
		Dict dict = new Dict();
		dict.setFieldName("postion");
		dict.setTableName("t_user_postion");
		List<Dict> dicts = this.dictService.findAllDicts(dict);
		return dicts;
	}
}
