package com.umessage.hotel.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.annotation.Log;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.MemberDiscount;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.MemberCouponService;
import com.umessage.hotel.service.MemberDiscountService;
import com.umessage.system.domain.User;
@Controller
@RequestMapping("memberdiscount")
public class MemberDiscountController extends BaseController{

	@Autowired 
	private MemberDiscountService memberService;
	@Autowired 
	private MemberCouponService userCouponService;
	@Autowired 
	private HotelInfoService hotelInfoService;
	@Log("获取会员管理信息")
	@RequestMapping("index")
	@RequiresPermissions("member:list")
	public String index() {
		return "hotel/memberdiscount/member";
	}
	@RequestMapping("getCoupons")
	@ResponseBody
	public HotelInfo getCoupons(){
		User user = super.getCurrentUser();
		HotelInfo hotelInfo=hotelInfoService.findHotelsById(user.getHotelId());
		return hotelInfo;
	}
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getCoupons(QueryRequest request,Integer memberSwitch){
		MemberDiscount member=new MemberDiscount();
		User user = super.getCurrentUser();
		member.setHotelId(user.getHotelId());
		member.setMemberSwitch(memberSwitch);
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		//List<MemberDiscount> memberlist=this.memberService.getMemberList(member);
		List<MemberDiscount> memberlist=this.memberService.getMemberList2(member);
		PageInfo<MemberDiscount> pageInfo = new PageInfo<MemberDiscount>(memberlist);
		return getDataTable(pageInfo);
	}
	@RequestMapping("add")
	@ResponseBody
	public Integer add(String memberName,String memberDiscount,Integer memberDiscountType){
		if( !StringUtils.isNotBlank(memberName)  || !StringUtils.isNotBlank(memberDiscount) ){
			//会员和折扣非法
			return 0;
		}
		User user = super.getCurrentUser();
		MemberDiscount member=new MemberDiscount();
		member.setHotelId(user.getHotelId());
		member.setMemberDiscount(memberDiscount);
		member.setMemberDiscountType(memberDiscountType);
		member.setMemberName(memberName);
		return memberService.add(member);
	}
	@RequestMapping("del")
	@ResponseBody
	public Integer del(Integer id){
	 	Integer s= memberService.delete(id);
		System.out.println(s);
		return s ;
	}
	@RequestMapping("update")
	@ResponseBody
	public Integer update(Integer id,String jiudianid,String memberName,String memberDiscount,Integer memberDiscountType){
		MemberDiscount member=new MemberDiscount();
		member.setHotelId(jiudianid);
		member.setMemberId(id);
		member.setMemberDiscount(memberDiscount);
		member.setMemberDiscountType(memberDiscountType);
		member.setMemberName(memberName);
		return memberService.updateNotNull(member);
	}
	@RequestMapping("memberdiscount")
	@ResponseBody
	public MemberDiscount memberdiscount(Integer id){
		MemberDiscount member=memberService.selectByKey(id);
		return member;
	}
	/**
	 * 会员折扣的开启与关闭
	 * @param id
	 * @return
	 */
	@RequestMapping("updateSwitch")
	@ResponseBody
	public ResponseBo updateSwitch(MemberDiscount mdt){
		Integer stval=mdt.getMemberSwitch();
		if(stval==0) stval=1; else stval=0;
		mdt.setMemberSwitch(stval);
		memberService.updateNotNull(mdt);
		return ResponseBo.ok("修改成功");
	}
}
