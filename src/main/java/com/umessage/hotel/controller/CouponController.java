package com.umessage.hotel.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.service.CouponService;
import com.umessage.hotel.service.MemberCouponService;
import com.umessage.system.domain.User;
@Controller
@RequestMapping("coupon")
public class CouponController extends BaseController{

	@Autowired 
	private CouponService couponService;
	@Autowired 
	private MemberCouponService userCouponService;
	
	@RequestMapping("index")
	public String index() {
		return "hotel/hotel/coupon";
	}
	
	private SimpleDateFormat  format = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * 获取优惠券列表
	 * @param request
	 * @param couponType
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getCoupons(QueryRequest request,String couponType){
		Coupon coupon=new Coupon();
		User user = super.getCurrentUser();
		coupon.setHotelId(Integer.valueOf(user.getHotelId()));
		coupon.setCouponType(couponType);
//		coupon.setIsUsable("1");
		coupon.setValidEnd(format.format(new Date()));
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<Coupon> coupons=this.couponService.getCoupons(coupon);
		PageInfo<Coupon> pageInfo = new PageInfo<Coupon>(coupons);
		return getDataTable(pageInfo);
	}
	/**
	 * 删除优惠券
	 * @param couponId
	 * @return
	 */
	@RequestMapping("delete")
	@ResponseBody
	public ResponseBo deleteCoupon(Integer couponId) {
		try {
			if(couponId!=null) {
				Coupon coupon=new Coupon();
				coupon.setIsDelete(1);
				coupon.setCouponId(couponId);
				couponService.updateByCouponId(coupon);
			}
			return ResponseBo.ok("删除成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("删除失败，请联系网站管理员！");
		}
	}
	/**
	 * 设置优惠券  禁用/可用
	 * @param couponId
	 * @param isUsable
	 * @return
	 */
	@RequestMapping("isUsable")
	@ResponseBody
	public ResponseBo isUsableCoupon(Integer couponId,Integer isUsable) {
		try {
			if(couponId!=null) {
				Coupon coupon=new Coupon();
				if(isUsable==1)
				coupon.setIsUsable("0");
				else coupon.setIsUsable("1");
				coupon.setCouponId(couponId);
				couponService.updateByCouponId(coupon);
			}
			return ResponseBo.ok("编辑成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("编辑失败，请联系网站管理员！");
		}
	}
	/**
	 * 优惠券新增
	 * @param Coupon
	 * @return
	 */
	@RequestMapping("add")
	@ResponseBody
	public ResponseBo addCoupon(Coupon coupon) {
		try {
			//金额校验
			if( coupon.getSecondNumber() == null){ //直减 ,满减  第二个值非空校验
				return ResponseBo.error("金额不合法");
			}
			if( coupon.getMode() ==2 && coupon.getFirstNumber() == null) { //满减第一金额校验
				return ResponseBo.error("金额不合法");
			}
			
			//固定周期校验 
			if(coupon.getValidType() == 1 ){
				if ( !StringUtils.isNotBlank(coupon.getValidStart()) ||  !StringUtils.isNotBlank(coupon.getValidEnd()) ){
					return ResponseBo.error("非法时间段");
				}
				if (true){ 
					Date start = format.parse(coupon.getValidStart());
					Date end = format.parse(coupon.getValidEnd());
					if(!start.before(end)){
						return ResponseBo.error("非法时间段");
					}
				}
			}
			//固定时长校验
			if(coupon.getValidType() ==2){
				if(!StringUtils.isNotBlank(coupon.getCycle())){
					return ResponseBo.error("非法月份数");
				}
			}
			User user=getCurrentUser();
			if( coupon.getMode() == 1){ //直接减去
				coupon.setCouponName("直减券");
			}else{
				coupon.setCouponName("满减券");
			}
			coupon.setCouponType(1+"");
			coupon.setAddAcc(user.getUserId());
			coupon.setAddTime(new Date());
			coupon.setModifyAcc(user.getUserId());
			coupon.setModifyTime(new Date());
			coupon.setHotelId(Integer.valueOf(user.getHotelId()));
			coupon.setIsDelete(0);
			coupon.setIsUsable("0");
			this.couponService.save(coupon);
			return ResponseBo.ok("添加成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("添加失败"+e.getMessage());
		}
	}
	
	/**
	 * 统计优惠券
	 */
	@RequestMapping("count")
	@ResponseBody
	public Map<String,Object> countCouponNum(){
		Map<String,Object> map=new HashMap<>();
		Integer sumCount=this.userCouponService.getSumCount();
		Integer useCount=this.userCouponService.getUseCount();
		map.put("sumCount", sumCount);
		map.put("useCount", useCount);
		return map;
	}
	/**
	 * 预存款
	 * @return
	 */
	@RequestMapping("store")
	public String storeIndex() {
		return "hotel/hotel/store";
	}
	@RequestMapping("storelist")
	@ResponseBody
	public Map<String, Object> getStoreCoupons(QueryRequest request){
		Coupon coupon=new Coupon();
		User user = super.getCurrentUser();
		coupon.setCouponType("2");
		coupon.setHotelId(Integer.valueOf(user.getHotelId()));
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<Coupon> coupons=this.couponService.getCoupons(coupon);
		PageInfo<Coupon> pageInfo = new PageInfo<Coupon>(coupons);
		return getDataTable(pageInfo);
	}
	/**
	 * 充值券新增
	 * @param Coupon
	 * @return
	 */
	@RequestMapping("addstore")
	@ResponseBody
	public ResponseBo addStoreCoupon(Coupon coupon) {
		try {
			User user=getCurrentUser();
			coupon.setCouponName("充值券");
			coupon.setCouponType(2+"");
			coupon.setMode(3);
			coupon.setValidType(2);
			coupon.setHotelId(Integer.valueOf(user.getHotelId()));;
			coupon.setAddAcc(user.getUserId());
			coupon.setAddTime(new Date());
			coupon.setModifyAcc(user.getUserId());
			coupon.setModifyTime(new Date());
			coupon.setIsDelete(0);
			coupon.setIsUsable("0");
			this.couponService.save(coupon);
			return ResponseBo.ok("添加成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("添加失败，请联系网站管理员！"+e.getMessage());
		}
	}
}
