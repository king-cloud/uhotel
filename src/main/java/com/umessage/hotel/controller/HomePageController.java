package com.umessage.hotel.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.umessage.common.annotation.Log;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.service.DepositOrderService;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.MemberAccountLogService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.WXUserMemberService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.hotel.util.TimeUtil;
import com.umessage.system.domain.User;

@Controller
@RequestMapping("homepage")
public class HomePageController extends BaseController{
	
	@Autowired
	private OrderInfoService orderInfoService;
	@Autowired
	private MemberInfoService memberInfoService;
	@Autowired
	private DepositOrderService depositOrderService;
	@Autowired
	private HotelInfoService hotelInfoService;
	@Autowired
	private WXUserMemberService wxUserMemberService;
	@Autowired
	private MemberAccountLogService accountLogService;
	
	
	@Log("数据统计信息")
	@RequestMapping("datapannel")
	public String datapannel() {
		return "hotel/hotel/statistics";
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping("getStatInfo")
	@ResponseBody
	public ResponseBo getStatInfo(String type) {
		
		User user = super.getCurrentUser();
		String hotelId = user.getHotelId();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String endTime = format.format(new Date());
		String startTime ="";
		List<Map> list = new ArrayList<Map>();
		if(type != null && !"".equals(type)){
			if("0".equals(type)){
				//当前时间  TODO 
				startTime = format.format(TimeUtil.getCurrentDayStartTime());
			}else if("1".equals(type)){
				//三天内时间	
				startTime = format.format(TimeUtil.getCurrentThreeDayStartTime());
			}else if("2".equals(type)){
				//七天内时间
				startTime = TimeUtil.getCurrentWeekDayStartTime();
			}else if("3".equals(type)){
				//一个月时间
				startTime = format.format(TimeUtil.getCurrentMonthStartTime());
			}else if("4".equals(type)){
				//当前季度时间
				startTime = format.format(TimeUtil.getCurrentQuarterStartTime());
			}else if("5".equals(type)){
				//当前年时间
				startTime =  format.format(TimeUtil.getCurrentYearStartTime()) ;
			}
			
			try {
				
				Map<String, Object> map2 = new HashMap<String, Object>();
				// Integer countNewUser = wxUserMemberService.getCountNewMemberByDate(hotelId, startTime, endTime);
				Integer countNewUser = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_MEMBER, startTime, endTime);
				map2.put("name", "新增会员用户/人");
				if(countNewUser == 0){
					map2.put("number", 0);
					countNewUser =0;
				}else{
					map2.put("number", countNewUser);
				}
				list.add(map2);
				
				Map<String, Object> map1 = new HashMap<String, Object>();
				Integer countUser = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_NEW,
						startTime, endTime);
				map1.put("name", "新增用户/人");
				if(countUser == 0){
					map1.put("number", 0+countNewUser);
				}else{
					map1.put("number", countUser+countNewUser);
				}
				list.add(map1);
				
				Map<String, Object> map3 = new HashMap<String, Object>();
				Integer countOrders = orderInfoService.countEffectiveOrders(hotelId, startTime, endTime);
				map3.put("name", "成交订单量/单");
				if(countOrders == 0){
					map3.put("number", 0);
				}else{
					map3.put("number", countOrders);
				}
				list.add(map3);
				Map<String, Object> map4 = new HashMap<String, Object>();
				BigDecimal depositMoney = depositOrderService.taotalPay(hotelId, startTime, endTime);
				map4.put("name", "充值金额/元");
				if(depositMoney == null || depositMoney.intValue() == 0){
					map4.put("number", 0);
				}else{
					map4.put("number", depositMoney.intValue());
				}
				list.add(map4);
				//TODO
				Map<String, Object> map5 = new HashMap<String, Object>();
				BigDecimal countOrdersIncomes = orderInfoService.countEffectiveOrdersIncome(hotelId, startTime, endTime);
				map5.put("name", "收益/元");
				if(countOrdersIncomes == null || countOrdersIncomes.intValue() == 0){
					map5.put("number", 0);
				}else{
					map5.put("number", countOrdersIncomes.intValue());
				}
				list.add(map5);
				return ResponseBo.ok(list);
			} catch (Exception e) {
				return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
			}
		}else{
			return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping("getSumStatInfo")
	@ResponseBody
	public ResponseBo getSumStatInfo( String type) {
		User user = super.getCurrentUser();
		String hotelId = user.getHotelId();
		
		List<Map> list = new ArrayList<Map>();
		try {
			
			Map<String, Object> map1 = new HashMap<String, Object>();
			Integer countUser = memberInfoService.getCountUserByDate(hotelId,"","");
			map1.put("name", "用户量/人");
			if(countUser == 0){
				map1.put("num", 0);
			}else{
				map1.put("num", countUser);
			}
			list.add(map1);
			Map<String, Object> map2 = new HashMap<String, Object>();
			Integer countNewUser = memberInfoService.getCountNewMemberByDate(hotelId,"1","","");
			map2.put("name", "会员用户/人");
			if(countNewUser == 0){
				map2.put("num", 0);
			}else{
				map2.put("num", countNewUser);
			}
			list.add(map2);
			Map<String, Object> map3 = new HashMap<String, Object>();
			Integer countOrders = orderInfoService.countOrders(hotelId, "3", "", "");
			map3.put("name", "成交订单量/单");
			if(countOrders == 0){
				map3.put("num", 0);
			}else{
				map3.put("num", countOrders);
			}
			list.add(map3);
			Map<String, Object> map4 = new HashMap<String, Object>();
			BigDecimal countOrdersIncome = depositOrderService.taotalPay(hotelId, "", "");
			map4.put("name", "充值金额/元");
			if(countOrdersIncome == null || countOrdersIncome.intValue() == 0){
				map4.put("num", 0);
			}else{
				map4.put("num", countOrdersIncome.intValue());
			}
			list.add(map4);
			Map<String, Object> map5 = new HashMap<String, Object>();
			BigDecimal countOrdersIncomes = orderInfoService.countOrdersIncome(hotelId, "", "");;
			map5.put("name", "收益/元");
			if(countOrdersIncomes == null || countOrdersIncomes.intValue() == 0){
				map5.put("num", 0);
			}else{
				map5.put("num", countOrdersIncomes.intValue());
			}
			list.add(map5);
			return ResponseBo.ok(list);
		} catch (Exception e) {
			return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
		}
	}
	
	@Log("数据统计折线图信息")
	@RequestMapping("datafingerprints")
	public String datafingerprints() {
		return "hotel/hotel/lineChart";
	}
	
	@RequestMapping("getLineInfo")
	@ResponseBody
	public ResponseBo getLineInfo( String type,String hotelId) {
		
		String currentHotelId ="";
		if(hotelId !=null && !"".equals(hotelId)){
			currentHotelId =hotelId;
		}else{
			User user = super.getCurrentUser();
			currentHotelId = user.getHotelId();
		}
		
		String userData = "";
		String memberData = "";
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> vals = new HashMap<String, Object>();
		
		//根据时间 + 酒店ID 进行查询;
		if(type == null || "".equals(type) || "0".equals(type)){ //今日
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");// 设置日期格式
			String time = df.format(new Date());
			int hour = 0;
			String beginTime = "";
			String endTime = "";
			for (int i = 0; i < 7; i++) {//维度为：12
				
				if (hour == 0) {
					beginTime = time + " 0" + hour + ":00";
					int addHou = hour + 4;

					endTime = time + " 0" + addHou + ":00";
					hour = hour + 4;
				} else {
					int addHou = hour + 4;

					if (hour > 10) {
						beginTime = time + " " + hour + ":00";
						endTime = time + " " + addHou + ":00";
					} else {

						beginTime = time + " 0" + hour + ":00";
						endTime = time + " 0" + addHou + ":00";
					}
					hour = hour + 4;
				}

				if (endTime.length() == 17) {
					endTime = endTime.replace("012:00", "12:00");
				}
				
				userData += memberInfoService.getCountUserByDate(currentHotelId, beginTime, endTime) + ",";
				memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1", beginTime, endTime) + ",";
				
		    }
			
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            
            return ResponseBo.ok(list);
		}else if("1".equals(type)){ //3日
			      	
			String pastDate = TimeUtil.getPastDate(2);
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd 00:00");// 设置日期格式
			String time = df.format(new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date dBegin;
			try {
				dBegin = sdf.parse(pastDate);
				Date dEnd = sdf.parse(time);
				List<Date> listDate = TimeUtil.getDatesBetweenTwoDate(dBegin, dEnd);
				for(int i=0;i<listDate.size();i++){
					userData += memberInfoService.getCountUserByDate(currentHotelId, sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
					memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
				}
			} catch (ParseException e) {
				
			}
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            
            return ResponseBo.ok(list);
			
		}else if("2".equals(type)){ //7日
		
			// 七天前 日期
			String pastDate = TimeUtil.getPastDate(6);
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd 00:00");// 设置日期格式
			String time = df.format(new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date dBegin;
			try {
				dBegin = sdf.parse(pastDate);
				Date dEnd = sdf.parse(time);
				List<Date> listDate = TimeUtil.getDatesBetweenTwoDate(dBegin, dEnd);
				for(int i=0;i<listDate.size();i++){
					userData += memberInfoService.getCountUserByDate(currentHotelId, sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
					memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            
            return ResponseBo.ok(list);
			
		}else if("3".equals(type)){ //月份
			Date beginDayOfMonth = TimeUtil.getBeginDayOfMonth();
			Date endDayOfMonth = TimeUtil.getEndDayOfMonth();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			List<Date> listDate = TimeUtil.getDatesBetweenTwoDate(beginDayOfMonth, endDayOfMonth);
			for(int i=0;i<listDate.size();i++){
				userData += memberInfoService.getCountUserByDate(currentHotelId, sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
				memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",sdf.format(listDate.get(i)), sdf.format(listDate.get(i)).replace("-", "/").replace(" 00:00", " 23:59")) + ",";
			
			}
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            
            return ResponseBo.ok(list);
		}else if("4".equals(type)){ //季度
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				
			try {
				userData += memberInfoService.getCountUserByDate(currentHotelId, sdf.format(TimeUtil.getCurrentMonthStartTime()), sdf.format(TimeUtil.getCurrentMonthEndTime())) + ",";
				memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",sdf.format(TimeUtil.getCurrentMonthStartTime()), sdf.format(TimeUtil.getCurrentMonthEndTime())) + ",";
				userData += memberInfoService.getCountUserByDate(currentHotelId, TimeUtil.getBeforeMonthStartdate(), TimeUtil.getBeforeMonthEnddate()) + ",";
				memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",TimeUtil.getBeforeMonthStartdate(), TimeUtil.getBeforeMonthEnddate()) + ",";
				userData += memberInfoService.getCountUserByDate(currentHotelId, sdf.format(TimeUtil.getCurrentQuarterStartTime()), sdf.format(TimeUtil.getCurrentQuarterEndTime())) + ",";
				memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",sdf.format(TimeUtil.getCurrentQuarterStartTime()), sdf.format(TimeUtil.getCurrentQuarterEndTime())) + ",";
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            return ResponseBo.ok(list);
		}else if("5".equals(type)){ //年
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			
			try {
				for (int i = 11; i >=0 ; i--) {
					userData += memberInfoService.getCountUserByDate(currentHotelId, TimeUtil.getBeforeMonthStartdate(i), TimeUtil.getBeforeMonthEnddate(i)) + ",";
					memberData += memberInfoService.getCountNewMemberByDate(currentHotelId, "1",TimeUtil.getBeforeMonthStartdate(i), TimeUtil.getBeforeMonthEnddate(i)) + ",";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			vals.put("value1", userData.substring(0, userData.length()-1));
	        vals.put("value2", memberData.substring(0, memberData.length()-1));
            list.add(vals);
            return ResponseBo.ok(list);
		}
		
		return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping("getLineStatInfo")
	@ResponseBody
	public ResponseBo getLineStatInfo() {
		User user = super.getCurrentUser();
		String hotelId = user.getHotelId();
		
		List<Map> list = new ArrayList<Map>();
		try {
			
			Map<String, Object> map1 = new HashMap<String, Object>();
			Integer countUser = memberInfoService.getCountUserByDate(hotelId,"","");
			map1.put("name", "用户/人");
			if(countUser == 0){
				map1.put("number", 0);
			}else{
				map1.put("number", countUser);
			}
			list.add(map1);
			Map<String, Object> map2 = new HashMap<String, Object>();
			Integer countNewUser = memberInfoService.getCountNewMemberByDate(hotelId,"1","","");
			map2.put("name", "会员/人");
			if(countNewUser == 0){
				map2.put("number", 0);
			}else{
				map2.put("number", countNewUser);
			}
			list.add(map2);
			Map<String, Object> map3 = new HashMap<String, Object>();
			Integer countOrders = orderInfoService.countOrders(hotelId, "3", "", "");
			map3.put("name", "订单/单");
			if(countOrders == 0){
				map3.put("number", 0);
			}else{
				map3.put("number", countOrders);
			}
			list.add(map3);
			//TODO
			Map<String, Object> map4 = new HashMap<String, Object>();
			//BigDecimal countOrdersIncome = orderInfoService.countOrdersIncome(hotelId, "", "");
			Integer repeatedPurchaseUsers = orderInfoService.countRepeatedPurchaseUsers(hotelId, "3", 2) ;
			map4.put("name", "复购用户/人");
			map4.put("number", repeatedPurchaseUsers);
			//if(countOrdersIncome == null || countOrdersIncome.intValue() == 0){
			//}else{
			//	map4.put("num", countOrdersIncome.intValue());
			//}
			list.add(map4);
			Map<String, Object> map5 = new HashMap<String, Object>();
			BigDecimal countDepositOrderRechargeAmount = depositOrderService.taotalPay(hotelId, "", "");
			map5.put("name", "充值金额/元");
			if(countDepositOrderRechargeAmount == null || countDepositOrderRechargeAmount.intValue() == 0){
				map5.put("number", 0);
			}else{
				map5.put("number", countDepositOrderRechargeAmount.intValue());
			}
			list.add(map5);
			Map<String, Object> map6 = new HashMap<String, Object>();
			BigDecimal countDepositOrderVirtMoney = depositOrderService.countDepositOrderVirtMoney(hotelId, "", "");
			map6.put("name", "赠送金额/元");
			if(countDepositOrderVirtMoney == null || countDepositOrderVirtMoney.intValue() == 0){
				map6.put("number", 0);
			}else{
				map6.put("number", countDepositOrderVirtMoney.intValue());
			}
			list.add(map6);
			Map<String, Object> map7 = new HashMap<String, Object>();
			BigDecimal countRealAndVirtMonry = accountLogService.countRealAndVirtMonry(hotelId, "",  "");
			map7.put("name", "余额消费(充值/赠送)/元");
			if(countRealAndVirtMonry == null || countRealAndVirtMonry.intValue() == 0){
				map7.put("number", 0);
			}else{
				map7.put("num", countRealAndVirtMonry.intValue());
			}
			list.add(map7);
			return ResponseBo.ok(list);
		} catch (Exception e) {
			return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
		}
	}
	
	@RequestMapping("getHotelInfo")
	@ResponseBody
	public ResponseBo getHotelInfo() {
		User user = super.getCurrentUser();
		String hotelId = user.getHotelId();
		List<HotelInfo> list = new ArrayList<HotelInfo>();
		
		try {
			if(hotelId != null && !"".equals(hotelId)){
				HotelInfo hotelInfo = hotelInfoService.queryHotelsByHotelId(hotelId);
				list.add(hotelInfo);
			}else{
				List<HotelInfo> allHotels = hotelInfoService.queryAllHotels();
				for (HotelInfo hotelInfo : allHotels) {
					list.add(hotelInfo);
				}
			}
			return ResponseBo.ok(list);
		} catch (Exception e) {
			return ResponseBo.error("获取数据统计信息失败，请联系网站管理员！");
		}
		
	}
}
