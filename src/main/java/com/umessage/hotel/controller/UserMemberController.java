package com.umessage.hotel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.WXUser;
import com.umessage.hotel.domain.WXUserMember;
import com.umessage.hotel.service.UserMemberService;
import com.umessage.system.domain.User;

@Controller
@RequestMapping("umember")
public class UserMemberController extends BaseController {
	@Autowired
	private UserMemberService userMemberService;
	@RequestMapping("index")
	public String index() {
		return "hotel/hotel/member";
	}
	/**
	 * 会员列表
	 * @param request
	 * @param mem
	 * @param wxUserName
	 * @return
	 * @throws Exception
	 */
/*	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(QueryRequest request,MemberInfo mem,String wxUserName) throws Exception {
		User user=getCurrentUser();
		if(wxUserName!=null&&!"".equals(wxUserName)) {
			WXUser wxUser = new WXUser();
			wxUser.setName(wxUserName);
			mem.setWxUser(wxUser);
		}
		mem.setHotelId(user.getHotelId());
		List<MemberInfo> ums=this.userMemberService.findUMListManual(mem,request.getPageNum(), request.getPageSize());
		
		//PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<MemberInfo> sum=this.userMemberService.findUMListManual(mem,0,0);
		
		int total = sum.size();
		int pages = (sum.size()%request.getPageSize()==0)?sum.size()/request.getPageSize():(sum.size()/request.getPageSize()+1);
		PageInfo<MemberInfo> pageInfo = new PageInfo<MemberInfo>(ums);
		pageInfo.setPages(pages);
		pageInfo.setPageNum(request.getPageNum());
		pageInfo.setPageSize(request.getPageSize());
		pageInfo.setTotal(total);
		
		return getDataTable(pageInfo);
	}*/
	@RequestMapping("count")
	@ResponseBody
	public Map<String, Integer> count() {
		Map<String, Integer> map = new HashMap<>();
		User user=getCurrentUser();
		String hotelId=user.getHotelId();
		//Integer countUser=this.userMemberService.getCountUser(user.getHotelId());
		//总用户量
		Integer countUserSum=this.userMemberService.getCountUserSum(hotelId);
		map.put("countUserSum", countUserSum);
		//Integer countNewMember=this.userMemberService.getCountNewMember(user.getHotelId());
		//总会员量
		Integer countMemberSum=this.userMemberService.getCountMemberSum(hotelId);
		map.put("countMemberSum", countMemberSum);
		//Integer countNewUser=this.userMemberService.getCountNewUser(user.getHotelId());
		Integer countOrderSum=this.userMemberService.getCountOrderSum(hotelId);
		map.put("countOrderSum", countOrderSum);
		return map;
	}
	/**
	 * 会员列表
	 * @param request
	 * @param mem
	 * @param wxUserName
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(QueryRequest request,String mobile,String wxUserName) throws Exception {
		WXUserMember umember=new WXUserMember();
		User user=getCurrentUser();
		umember.setHotelId(user.getHotelId());
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<WXUserMember> ums=this.userMemberService.findUMemberList(umember);
		PageInfo<WXUserMember> pageInfo = new PageInfo<>(ums);
		return getDataTable(pageInfo);
	}
}
