package com.umessage.hotel.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.QueryRequest;
import com.umessage.hotel.dao.MemberInfoMapper;
import com.umessage.hotel.domain.BreakfastCoupon;
import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.domain.OrderBreakfast;
import com.umessage.hotel.service.BreakfastCouponService;
import com.umessage.hotel.service.OrderBreakfastService;
import com.umessage.system.domain.User;

import io.swagger.annotations.Api;

/**
 * 早餐券管理
 * 
 * @author xiajh
 *
 */
@Api("早餐优惠券管理")
@Controller
@RequestMapping("breakfastcoupon")
public class BreakfastCouponController extends BaseController{
	@Autowired
	private BreakfastCouponService breakfastCouponService;
	@Autowired
	private OrderBreakfastService orderBreakfastService;
	/**
	 * 早餐订单首页
	 * @return
	 */
	@RequestMapping("index")
	public String getCouponIndex() {
		return "hotel/breakfast/breakfast";
	}
	/**
	 *  新增早餐券
	 * @param coupon
	 * @return
	 */
	@RequestMapping("add")
	@ResponseBody
	public String addCoupons(OrderBreakfast obf){
		User user=getCurrentUser();
		String hotelId=user.getHotelId();
		obf.setHotelId(hotelId);
		String msg=this.orderBreakfastService.addOrderBreakfastPC(obf);
		return msg;
	}
	/**
	 * 统计早餐券数
	 * @return
	 */
	@RequestMapping("count")
	@ResponseBody
	public Map<String,Object> count(){
		Map<String,Object> map=new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		User user=getCurrentUser();
		BreakfastCoupon coupon=new BreakfastCoupon();
		coupon.setHotelId(user.getHotelId());
		//validDate 存的是当前日期的凌晨 如：（2018-10-25 00:00:00）
		coupon.setValidDate(sdf.format(new Date()));
		Integer check=this.breakfastCouponService.countCheckToday(coupon);
		coupon.setType("1");//赠送即发放
		Integer countType1=this.breakfastCouponService.countNumTodayByType(coupon);
		coupon.setType("2");//购买
		Integer countType2=this.breakfastCouponService.countNumTodayByType(coupon);
		map.put("check", check);
		map.put("countType1", countType1);
		map.put("countType2", countType2);
		return map;
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String,Object> getCoupons(QueryRequest request,OrderBreakfast obf) {
		obf.setMobile("%"+obf.getMobile()+"%");
		User user=getCurrentUser();
		obf.setHotelId(user.getHotelId());
		PageHelper.startPage(request.getPageNum(), request.getPageSize());
		List<OrderBreakfast> coupons=this.orderBreakfastService.getOrderBKFCoupons(obf);
		PageInfo<OrderBreakfast> pageInfo = new PageInfo<OrderBreakfast>(coupons);
		return getDataTable(pageInfo);
	}
}
