package com.umessage.hotel.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.umessage.common.controller.BaseController;
import com.umessage.common.util.HttpRequestUtil;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.InvoiceApplication;
import com.umessage.hotel.domain.InvoiceTitle;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.InvoiceApplicationService;
import com.umessage.hotel.service.InvoiceTitleService;

@Controller
@RequestMapping("invoice")
public class InvoiceController extends BaseController{

	@Autowired
	private InvoiceTitleService invoiceTitleService;
	@Autowired
	private InvoiceApplicationService invoiceApplicationService;
	@Autowired
	private HotelInfoService hotelInfoService;
	
	/**
	 * @添加发票抬头接口
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("addInvoiceTitleInfo")
	@ResponseBody
	public  String addInvoiceTitleInfo(String xmlStr) {
		
		JSONObject obj = JSON.parseObject(xmlStr);
    	HashMap<Object, Object> resultData = new HashMap<>();
    	
		try {
			if(xmlStr!=null && !"".equals(xmlStr)){
	
				InvoiceTitle invoiceTitle = new InvoiceTitle();
				invoiceTitle.setAccount(obj.getString("account"));
				invoiceTitle.setAddress(obj.getString("address"));
				invoiceTitle.setBank(obj.getString("bank"));
				invoiceTitle.setHotelId(obj.getString("hotelId"));
				invoiceTitle.setMemberId(obj.getString("memberId"));
				invoiceTitle.setPhone(obj.getString("phone"));
				invoiceTitle.setTaxNo(obj.getString("taxNo"));
				invoiceTitle.setTitle(obj.getString("title"));
				invoiceTitle.setType(obj.getString("type"));
				invoiceTitle.setAddTime(new Date());
				
				Integer type = invoiceTitleService.addInvoiceTitle(invoiceTitle);
				if(type==1){
					resultData.put("state",1);
					resultData.put("msg", "success");
				}else{
					resultData.put("state",0);
					resultData.put("msg", "fail");
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}
	
	/**
	 * @修改发票抬头接口
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("updateInvoiceTitleInfo")
	@ResponseBody
	public  String updateInvoiceTitleInfo(String xmlStr) {
		
		JSONObject obj = JSON.parseObject(xmlStr);
    	HashMap<Object, Object> resultData = new HashMap<>();
    	
		try {
			if(xmlStr!=null && !"".equals(xmlStr)){
	
				InvoiceTitle invoiceTitle = new InvoiceTitle();
				String id = obj.getString("id"); 
				if(id !=null && !"".equals(id)){
					invoiceTitle.setId(Integer.parseInt(id));
				}
				invoiceTitle.setAccount(obj.getString("account"));
				invoiceTitle.setAddress(obj.getString("address"));
				invoiceTitle.setBank(obj.getString("bank"));
				invoiceTitle.setHotelId(obj.getString("hotelId"));
				invoiceTitle.setMemberId(obj.getString("memberId"));
				invoiceTitle.setPhone(obj.getString("phone"));
				invoiceTitle.setTaxNo(obj.getString("taxNo"));
				invoiceTitle.setTitle(obj.getString("title"));
				invoiceTitle.setType(obj.getString("type"));
				invoiceTitle.setUpdateTime(new Date());
				
				Integer type = invoiceTitleService.updateByPrimaryKeySelective(invoiceTitle);
				if(type==1){
					resultData.put("state",1);
					resultData.put("msg", "success");
				}else{
					resultData.put("state",0);
					resultData.put("msg", "fail");
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}
	
	/**
	 * @删除发票抬头信息接口
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("deleteInvoiceTitleInfo")
	@ResponseBody
	public  String deleteInvoiceTitleInfo(String xmlStr) {
		
		JSONObject obj = JSON.parseObject(xmlStr);
		HashMap<Object, Object> resultData = new HashMap<>();
		String id = obj.getString("id");
		
		try {
			
			if(id !=null && !"".equals(id)){
				Integer result = invoiceTitleService.deleteByPrimaryKey(Integer.parseInt(id));
				
				if(result ==1 ){
					resultData.put("state",1);
					resultData.put("msg", "success");
				}else{
					resultData.put("state",0);
					resultData.put("msg", "fail");
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		
		return JSONObject.toJSONString(resultData);
	}
	
	/**
	 * @获取发票抬头接口:通过会员号进行查询
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("getInvoiceTitleInfo")
	@ResponseBody
	public  String getInvoiceTitleInfo(String xmlStr) {
		JSONObject obj = JSON.parseObject(xmlStr);
		HashMap<Object, Object> resultData = new HashMap<>();
		String memberId = obj.getString("memberId");
		
		try {
			
			if(memberId !=null && !"".equals(memberId)){
				List<InvoiceTitle> invoiceTitleInfo = invoiceTitleService.getInvoiceTitleInfo(memberId);
				if(invoiceTitleInfo !=null && !"".equals(invoiceTitleInfo) && invoiceTitleInfo.size()>0){
					resultData.put("state",1);
					resultData.put("msg", "success");
					resultData.put("data", invoiceTitleInfo);
					resultData.put("count", invoiceTitleInfo.size());
				}else{
					resultData.put("state",0);
					resultData.put("msg", "fail");
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		
		return JSONObject.toJSONString(resultData);
	}
	
	/**
	 * @开票申请接口
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("addInvoiceApplicationInfo")
	@ResponseBody
	public  String addInvoiceApplicationInfo(String xmlStr) {
		
		JSONObject obj = JSON.parseObject(xmlStr);
    	HashMap<Object, Object> resultData = new HashMap<>();
    	
		try {
			if(xmlStr!=null && !"".equals(xmlStr)){
	
				InvoiceApplication invoiceApplication = new InvoiceApplication();
				
				BigDecimal amount = new BigDecimal(obj.getString("amount"));
				invoiceApplication.setAmount(amount);
				invoiceApplication.setEmail(obj.getString("email"));
				invoiceApplication.setHotelId(obj.getString("hotelId"));
				invoiceApplication.setInvoiceNo(obj.getString("invoiceNo"));
				invoiceApplication.setMobile(obj.getString("mobile"));
				invoiceApplication.setMemberId(obj.getString("memberId"));
				invoiceApplication.setOrderId(obj.getString("orderId"));
				invoiceApplication.setResult(obj.getString("result"));
				invoiceApplication.setStatus(obj.getString("status"));
				invoiceApplication.setTitleId(obj.getString("titleId"));
				invoiceApplication.setAddTime(new Date());
				invoiceApplication.setStatus("0");
				//1.完成数据插入
				invoiceApplicationService.addInvoiceApplicationInfo(invoiceApplication);
				//2.调用开发票接口，根据返回结果进行相关参数的返回  
				Map<String, String> map = new HashMap<String, String>();
	    	    map.put("hotelId", obj.getString("hotelId")); 
	    	    map.put("orderId",obj.getString("orderId"));
	    	    map.put("mobile", obj.getString("mobile"));
	    	    map.put("amount", obj.getString("amount"));
	    	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	    String formatStr =formatter.format(new Date());
	    	    map.put("openDate", formatStr);
	    	    map.put("email", obj.getString("email"));
	    	    String titleId = obj.getString("titleId");
	    	    if(titleId !=null && !"".equals(titleId)){
	    	    	InvoiceTitle invoiceTitle = invoiceTitleService.selectByPrimaryKey(Integer.parseInt(titleId));
	    	    	map.put("enterpriseName", invoiceTitle.getTitle());
	    	    	map.put("TaxpayerNum", invoiceTitle.getTaxNo());
	    	    	map.put("address", invoiceTitle.getAddress());
	    	    	map.put("bankAccount", invoiceTitle.getAccount()); 
	    	    	map.put("bankName", invoiceTitle.getBank());
	    	    	map.put("openType", invoiceTitle.getType());
	    	    	HotelInfo hotelInfo = hotelInfoService.queryHotelsByHotelId(obj.getString("hotelId"));
	    	    	map.put("businessNum", hotelInfo.getBusinessNum()); 
	    	    }
	    	    String jsonStr = JSONObject.toJSONString(map);
	    	    JSONObject jsonObject = null;
	    	    String urls ="http://39.104.68.154:8093/invoice/ticket";
	    	        
	        	Map<String, String> mapParam = new HashMap<String, String>();
	            mapParam.put("jsonStr", jsonStr);
	            String send = HttpRequestUtil.send(urls, mapParam, "utf-8");
	            jsonObject = JSONObject.parseObject(send);
	            
	            
	            if(jsonObject.getString("status").equals("1") ){  
	                 
	            	 String djh = jsonObject.getString("djh");
	            	 String orderId = jsonObject.getString("orderId");
	            	 
	            	 invoiceApplication.setStatus("1");
	            	 invoiceApplication.setUpdateTime(new Date());
	            	 invoiceApplicationService.updateByTitleIdSelective(invoiceApplication);
	            	 
	            	 resultData.put("state",1);
					 resultData.put("msg", "success");
					 resultData.put("djh", djh);
					 resultData.put("orderId", orderId);
	            }else{
	            	 invoiceApplication.setStatus("2");
	            	 invoiceApplication.setUpdateTime(new Date());
	            	 invoiceApplication.setResult(jsonObject.getString("msg"));
	            	 invoiceApplicationService.updateByTitleIdSelective(invoiceApplication);
					 resultData.put("state",0);
					 resultData.put("msg", "fail");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}
	
	/**
	 * @查询开票进度接口:通过酒店号和手机号进行查询
	 * @param xmlStr
	 * @return
	 */
	@RequestMapping("getInvoiceApplicationStatus")
	@ResponseBody
	public  String getInvoiceApplicationStatus(String xmlStr) {
		JSONObject obj = JSON.parseObject(xmlStr);
		HashMap<Object, Object> resultData = new HashMap<>();
		HashMap<Object, Object> data = new HashMap<>();
		String orderId = obj.getString("orderId");
		
		try {
			
			if(orderId !=null && !"".equals(orderId)){
				InvoiceApplication applicationStatus = invoiceApplicationService.getInvoiceApplicationStatus(orderId);
				if(applicationStatus!=null && !"".equals(applicationStatus)){
					resultData.put("state",1);
					resultData.put("msg", "success");
					data.put("status", applicationStatus.getStatus());
					data.put("result", applicationStatus.getResult());
					resultData.put("data", data);
				}else{
					resultData.put("state",0);
					resultData.put("msg", "fail");
				}
			}else{
				resultData.put("state",0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state",0);
			resultData.put("msg", "fail");
		}
		
		return JSONObject.toJSONString(resultData);
	}
}
