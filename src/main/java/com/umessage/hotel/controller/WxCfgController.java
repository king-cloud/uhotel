package com.umessage.hotel.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.umessage.common.annotation.Log;
import com.umessage.common.controller.BaseController;
import com.umessage.common.domain.ResponseBo;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.WxCfg;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.WxCfgService;
import com.umessage.system.domain.User;
@Controller
@RequestMapping("wxCfg")
public class WxCfgController extends BaseController{
	@Autowired
	private WxCfgService wxCfgService;
	@Autowired
	private HotelInfoService hotelService;
	
	@RequestMapping("queryWxCfg")
	@ResponseBody
	public WxCfg queryWxCfg(String hotelId,String appType) {
		WxCfg wxcf= new WxCfg();
		wxcf.setHotelId(hotelId);
		wxcf.setAppType(appType);
		WxCfg wxcf1 =wxCfgService.getWxCfg(wxcf);
		return wxcf1;
	}
	@RequestMapping("addWxCfg")
	@ResponseBody
	public Integer addWxCfg(String hotelId,String appType,String wxcfgId,String description,String appId,
			String appkey,String mchId,String secretKey,String certPem,String keyPem,String apiclientCert
			,String isValid,String payType) {
		HotelInfo hotelInfo = hotelService.findHotelsById(hotelId) ;
		Integer i= 0;
		if("".equals(wxcfgId)){
			SimpleDateFormat sm=new SimpleDateFormat();
			WxCfg wxcf= new WxCfg();
			wxcf.setWxcfgid(new Date().getTime()+"");
			wxcf.setHotelId(hotelId);
			wxcf.setAppType(appType);
			wxcf.setDescription(hotelInfo.getHotelName() + ("1".equals(appType) ? "--C端" : ("2".equals(appType) ? "--H端" : "")));
			wxcf.setAppId(appId);
			wxcf.setAppkey(appkey);
			wxcf.setMchId(mchId);
			wxcf.setSecretKey(secretKey);
			wxcf.setCertPem(certPem);
			wxcf.setKeyPem(keyPem);
			wxcf.setApiclientCert(apiclientCert);
			wxcf.setIsValid(isValid);
			wxcf.setPayType(payType);
			wxcf.setCreateTime(new Date());
			 i =wxCfgService.addwxcf(wxcf);
		}else {
			WxCfg wxcf= new WxCfg();
			wxcf.setHotelId(hotelId);
			wxcf.setAppType(appType);
			wxcf.setWxcfgid(wxcfgId);
			wxcf.setDescription(hotelInfo.getHotelName() + ("1".equals(appType) ? "--C端" : ("2".equals(appType) ? "--H端" : "")));
			wxcf.setAppId(appId);
			wxcf.setAppkey(appkey);
			wxcf.setMchId(mchId);
			wxcf.setSecretKey(secretKey);
			wxcf.setCertPem(certPem);
			wxcf.setKeyPem(keyPem);
			wxcf.setApiclientCert(apiclientCert);
			wxcf.setIsValid(isValid);
			wxcf.setPayType(payType);	
			i =wxCfgService.updateNotNull (wxcf);
		}
		return i;
	}
	//上传文件
		@RequestMapping(value = "upload/certificate")
		@ResponseBody
		public Map uploadPic(@RequestParam(required = false) MultipartFile file,String hotelid
				,HttpServletRequest request) throws IOException, Exception{
			Map map=new HashMap<>();
	                // 保存文件
	                //rb=saveFile(request, file, String);
					 // 判断文件是否为空
			        if (!file.isEmpty()) {
			        	String filePath =  file.getOriginalFilename();
			        	String suffix = filePath.substring(filePath.lastIndexOf('.'));
			        	String uuidPicName=request.getSession().getServletContext().getRealPath("/certificate/"+hotelid+"/")+UUID.randomUUID().toString() + suffix;
			        	//上传图片路径
			                File oldDir = new File(uuidPicName);
			                if (!oldDir.getParentFile().exists()) {
			                	oldDir.getParentFile().mkdirs();
			                }
			                // 转存文件
			                file.transferTo(oldDir);
			                map.put("status","1");
			                map.put("data",uuidPicName);
			        } else {
			        	map.put("status","0");;
					}
			        return map;
		}
}
