package com.umessage.hotel.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.model.UserCouponModel;
import com.umessage.hotel.service.CouponService;
import com.umessage.hotel.service.MemberCouponService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;
import io.swagger.annotations.ApiOperation;

@RequestMapping(value = "/rest/usercoupons")
@RestController
public class UserCouponResource {

	@Autowired
	private MemberCouponService memberCouponService;
	@Autowired
	private MemberInfoService memberInfoService;
	@Autowired
	private CouponService couponService;
	
	private SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//	private UserCouponModel getUserCoupon() {
//		UserCouponModel usercoupon = new UserCouponModel();
//		usercoupon.setCouponId(UUID.randomUUID().toString());
//		usercoupon.setCouponName("满500减100");
//		usercoupon.setCouponType("1");
//		usercoupon.setMode("1");
//		usercoupon.setFirstValue(500);
//		usercoupon.setSecondValue(100);
//
//		return usercoupon;
//	}
//
//
//	/**
//	 * 获取个人优惠券信息 （过滤已经使用的优惠券）
//	 * 
//	 * @param hotelId
//	 * @param userId
//	 * @param pageSize
//	 * @param pageNum
//	 * @return
//	 */
//	@RequestMapping(method = RequestMethod.GET)
//	public RestResult<List<UserCouponModel>> listUserCoupons(ServletRequest request,
//			@RequestParam(defaultValue = "10", required = true) int pageSize,
//			@RequestParam(defaultValue = "1", required = true) int pageNum) {
//		String userId = (String) request.getParameter("userId");
//		String hotelId = (String) request.getParameter("hotelId");
//		List<UserCouponModel> list = new ArrayList<UserCouponModel>();
////		list.add(getUserCoupon());
//		List<Coupon> coupons=this.couponService.findCouponsByWXUser(userId,hotelId);
//		if(coupons!=null) {
//			for (Coupon coupon : coupons) {
//				UserCouponModel ucm=new UserCouponModel();
//				ucm.setCouponId(coupon.getCouponId()+"");
//				ucm.setUserId(userId);
//				ucm.setCouponName(coupon.getCouponName());
//				ucm.setCouponType(coupon.getCouponType());
//				ucm.setFirstValue(coupon.getFirstNumber());
//				ucm.setMode(coupon.getMode()+"");
//				ucm.setSecondValue(coupon.getSecondNumber());
//				ucm.setValidEnd(coupon.getValidEnd());
//				ucm.setValidStart(coupon.getValidStart());
//				list.add(ucm);
//			}
//		}
//		return RestResultGenerator.genSuccessResult(list);
//	}
//
//	/**
//	 * 领取优惠券
//	 * 
//	 * @param hotelId    酒店id
//	 * @param userId     用户id
//	 * @return
//	 */
//	@RequestMapping(value="addUserCoupon", method = RequestMethod.POST)
//	public RestResult<UserCouponModel> addUserCoupon(@RequestParam(required=true)Integer couponId,@RequestParam(required=true)String hotelId, @RequestParam(required=true)String phone) {
//		
//		MemberInfo memberInfo = this.memberInfoService.selectByPhone(phone);
//		String memberId=null;
//		if(memberInfo==null) {
//			MemberInfo member=new MemberInfo();
//			member.setHotelId(hotelId);
//			member.setMobile(phone);
//			memberId = this.memberInfoService.saveMember(member);
//		}else {
//			memberId = memberInfo.getMemberId();
//		}
//		MemberCoupon mc=new MemberCoupon();
//		mc.setGetTime(new Date());
//		mc.setStatus("1");
//		mc.setMemberId(memberId);
//		mc.setCouponId(couponId);
//		mc.setCouponType("1");
//		this.memberCouponService.saveMemberCoupon(mc);
//		return RestResultGenerator.genSuccessResult(null);
//	}
	
	@ApiOperation(value = "统计优惠券数", notes = "统计优惠券数")
	@RequestMapping(value = "countNum", method=RequestMethod.GET)
	public RestResult<Integer> countNum(ServletRequest request) {
		
		String hotelId = (String) request.getAttribute("issuer");
		Integer sumCount=this.couponService.getCountCoupons(hotelId,dateFormat.format(new Date())); 
		return RestResultGenerator.genSuccessResult(sumCount);
	}
	/**
	 * 优惠券列表（酒店）
	 * 
	 * @param hotelId    酒店id
	 * @return
	 */
	@ApiOperation(value = "酒店优惠券列表", notes = "酒店优惠券列表（酒店）")
	@RequestMapping(value="coupons", method = RequestMethod.POST)
	public RestResult<List<UserCouponModel>> getUserCoupons(String hotelId) {
		try {
			Coupon cou = new Coupon();
			cou.setHotelId(Integer.valueOf(hotelId));
			cou.setCouponType("1");
			cou.setIsUsable("1");
			cou.setValidEnd(dateFormat.format(new Date()));
			List<Coupon> coupons = this.couponService.getCoupons(cou);
			List<UserCouponModel> list = new ArrayList<UserCouponModel>();
			if (coupons != null) {
				for (Coupon coupon : coupons) {
					UserCouponModel ucm = new UserCouponModel();
					ucm.setCouponId(coupon.getCouponId() + "");
					ucm.setCouponName(coupon.getCouponName());
					ucm.setCouponType(coupon.getCouponType());
					if (coupon.getFirstNumber() != null) {
						ucm.setFirstValue(coupon.getFirstNumber());
					}
					ucm.setMode(coupon.getMode() + "");
					ucm.setSecondValue(coupon.getSecondNumber());
					ucm.setValidType(coupon.getValidType());
					ucm.setValidEnd(coupon.getValidEnd());
					ucm.setValidStart(coupon.getValidStart());
					ucm.setCycle(coupon.getCycle());
					list.add(ucm);
				}
			} 
			return RestResultGenerator.genSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}
	}

}
