package com.umessage.hotel.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.common.service.RedisService;
import com.umessage.common.util.Atools;
import com.umessage.common.util.SimpleSmsUtil;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;
import com.umessage.system.domain.User;
import com.umessage.system.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api("短信管理")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/sms")
public class SMSResource {
	@Autowired
	private RedisService redisService;
	@Autowired
	private HotelInfoService hotelInfoService;
	@Autowired
	private UserService userService;
	@Autowired
	private SimpleSmsUtil simpleSmsUtil;

	/**
	 * 发送手机验证码
	 * 
	 * @return
	 */
	@ApiOperation(value = "发送手机验证码", notes = "发送手机验证码")
	@RequestMapping(value = "/sendvercode", method = RequestMethod.POST)
	@ResponseBody
	@ApiImplicitParams({ @ApiImplicitParam(name = "type", value = "类型：1客户端、2酒店端", required = true, paramType = "query"),
			@ApiImplicitParam(name = "hotelId", value = "酒店id", required = true, paramType = "query"),
			@ApiImplicitParam(name = "mobile", value = "手机号码", required = true, paramType = "query"),
			@ApiImplicitParam(name = "sign", value = "签名（用于接口身份认证）", required = true, paramType = "query") })
	public RestResult<String> sendvercode(String type, String hotelId, String mobile, String sign) {

		// TODO:验证合法性。要求hotel+mobile加密后和sign相等。
		
		//验证类型
		if (!HotelConstant.VERCODE_TYPE_CLIENT.equals(type) && !HotelConstant.VERCODE_TYPE_HOTEL.equals(type)) {
			return RestResultGenerator.genErrorResult("错误类型");
		}
		
		//如果hotelI为空，根据手机号查询hotelId
		if(hotelId == null || "".equals(hotelId)){
			User user = userService.findUserByMobileNum(mobile);
			if(user==null) {
				return RestResultGenerator.genErrorResult("此账号暂无权限，请联系酒店管理员！");
			}
			hotelId = user.getHotelId();
		}

		String key = HotelConstant.VERCODE_PREFIX + "_" + type + "_" + mobile;
		String value = redisService.get(key);
		if (value == null || "".equals(value)) {
			HotelInfo hotelInfo = hotelInfoService.findHotelsById(hotelId);
			String vercode = Atools.birthNumString(4);
			simpleSmsUtil.sendMsg(mobile, hotelInfo.getHotelName(), "验证码：" + vercode);
			redisService.set(key, vercode);
			redisService.pexpire(key, 1000L * 60);// 30s内有效
			return RestResultGenerator.genResult(true, null, "发送验证码成功");
		} else {
			return RestResultGenerator.genResult(true, null, "验证码已发送，请勿频繁提交！");
		}
	}

}
