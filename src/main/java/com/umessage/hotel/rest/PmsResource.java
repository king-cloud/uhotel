package com.umessage.hotel.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.OrderPms;
import com.umessage.hotel.domain.Return;
import com.umessage.hotel.domain.WxCfg;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.OrderPmsService;
import com.umessage.hotel.service.WxCfgService;
import com.umessage.hotel.util.JsonListMapUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api("pms")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/pms")
public class PmsResource {


	@Autowired
	private  OrderPmsService orderPmsService;
	@Autowired
	private OrderInfoService orderService;
	@Autowired
	private WxCfgService wxCfgService;
	
	@ApiOperation(value = "退房", notes = "退房接口")
	@ApiImplicitParams({ @ApiImplicitParam(name = "hotelId", value = "酒店ID", required = true, paramType = "query"),
			@ApiImplicitParam(name = "orderId", value = "订单号", required = true, paramType = "query"),
			@ApiImplicitParam(name = "roomNum", value = "房间号", required = true, paramType = "query"),
			@ApiImplicitParam(name = "retreatMoney", value = "应退钱数", required = true, paramType = "query") })
	@RequestMapping(value = "/checkOut", method = RequestMethod.POST)
	public String checkOut(String hotelId, String orderId, String roomNum, String retreatMoney) {
		Return retur = new Return(); 
		try {
			OrderPms orderPms = orderPmsService.getorderPmsByorderpmsAndHotelId(hotelId,orderId);
			orderPms.setIscheckout("1");
			orderPms.setIsrefund(retreatMoney);
			orderPmsService.update(orderPms);
			String order = orderPms.getOrderId();
			List<OrderPms> list = orderPmsService.getrderListOrderPms(hotelId,order);
			OrderInfo orderInfo=new OrderInfo();
			orderInfo.setOrderId(order);
			orderInfo= orderService.getOrderInfo(orderInfo);
			Integer stutas=orderInfo.getPayStatus();
			if(stutas != 1){
				retur.setStatus("1");
				retur.setData(stutas);
				retur.setMsg("成功");
			}else{
				OrderInfo o1=orderInfo;
				new Thread(new Runnable() {
					@Override
					public void run() {
						boolean bool = false;
						int money = 0;
								for (int i = 0; i < list.size(); i++) {
									if("1".equals(list.get(i).getIscheckout())){
										bool=true;
									money=money+Integer.valueOf(list.get(i).getIsrefund()); 
									}else {
										bool=false;
										break;
									}
								}
						if(bool){
							if (stutas==1){
								WxCfg queryWxCfg = wxCfgService.queryWxCfg(hotelId, "1");
								String wxcfgid =queryWxCfg.getWxcfgid();
								orderService.refundRequest(wxcfgid, order, money*100,  o1.getTotalPrice().intValue()*100);
								System.out.println("Thread");
							}
						}
					}
				}).start();
			}
			retur.setStatus("1");
			retur.setData(stutas);
			retur.setMsg("成功");
			System.out.println("res:"+retur);
			
		} catch (Exception e) {
			retur.setStatus("1");
			retur.setData("0");
			retur.setMsg("失败");
		}
		return JsonListMapUtil.toJSONString(retur);
	}

	@ApiOperation(value = "修改入住状态", notes = "修改入住状态接口")
	@ApiImplicitParams({ @ApiImplicitParam(name = "hotelId", value = "酒店ID", required = true, paramType = "query"),
			@ApiImplicitParam(name = "orderId", value = "订单号", required = true, paramType = "query")})
	@RequestMapping(value = "/upOderStatus", method = RequestMethod.POST)
	public void upOderStatus(String hotelId, String orderId) {
		try {
			OrderPms orderPms = orderPmsService.getorderPmsByorderpmsAndHotelId(hotelId,orderId);
			if(orderPms!=null){
				String order = orderPms.getOrderId();
				OrderInfo orderInfo=new OrderInfo();
				orderInfo.setOrderId(order);
				orderInfo= orderService.getOrderInfo(orderInfo);
				orderInfo.setOrderStatus("3");
				orderService.updateAll(orderInfo);
			}
		} catch (Exception e) {
			
		}
	}
	
	@ApiOperation(value = "测试", notes = "测试")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "orderId", value = "订单号", required = true, paramType = "query")})
	@RequestMapping(value = "/text", method = RequestMethod.POST)
	public void text(String orderId) {
		try {
			OrderInfo orderInfo=new OrderInfo();
			orderInfo.setOrderId(orderId);
			orderInfo =orderService.getOrderInfo(orderInfo);
			WxCfg queryWxCfg = wxCfgService.queryWxCfg("10044", "1");
			String wxcfgid =queryWxCfg.getWxcfgid();
			orderService.refundRequest(wxcfgid, orderId, 3,  3);
		} catch (Exception e) {
			
		}
	}
}
