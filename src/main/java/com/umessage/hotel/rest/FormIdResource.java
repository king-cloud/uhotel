package com.umessage.hotel.rest;

import java.util.List;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.hotel.model.FormModel;
import com.umessage.hotel.service.FormService;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("表单")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/formid")
public class FormIdResource {

	@Autowired
	private FormService formService;

	@ApiOperation(value = "单个保存表单id", notes = "单个保存表单id")
	@RequestMapping(value = "/saveformid", method = RequestMethod.POST)
	public RestResult saveFormIdList(ServletRequest request, FormModel form) {

		try {
			String openid = (String) request.getAttribute("openid");
			formService.saveFormId(openid, form);
			return RestResultGenerator.genSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}

	}

	@ApiOperation(value = "保存表单id", notes = "保存表单id")
	@RequestMapping(value = "/saveformids", method = RequestMethod.POST)
	public RestResult saveFormIdList(ServletRequest request, @RequestBody List<FormModel> formList) {

		try {
			String openid = (String) request.getAttribute("openid");
			formService.saveFormIdList(openid, formList);
			return RestResultGenerator.genSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}

	}

}
