package com.umessage.hotel.rest;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.umessage.hotel.domain.DiningRoom;
import com.umessage.hotel.model.DiningRoommModel;
import com.umessage.hotel.service.DiningRoomService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 餐厅列表管理
 *
 */
@Api("餐厅列表管理")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/diningRoom")
public class DiningRoomResource extends BaseResourceController {

	@Autowired
	private DiningRoomService diningRoomService;
	
	/**
	 * 餐厅列表
	 * @param page
	 * @param rows
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "餐厅列表", notes = "餐厅列表")
	@RequestMapping( value = "list" ,method  = RequestMethod.GET)
	public RestResult<List<DiningRoommModel>> list (@RequestParam(defaultValue = "1", required = true) int page,
			@RequestParam(defaultValue = "10", required = true) int rows, ServletRequest request){
		try {
			assertMobileLogin(request);
			String type = (String) request.getAttribute("type");
			String memberId = (String) request.getAttribute("subject");
			String hotelId = (String) request.getAttribute("issuer");
			//查询酒店所有餐厅 (酒店id 状态 )
			List<DiningRoom> diningRoomList = null;
			List<DiningRoommModel> modeList = new ArrayList<>();
			//c
			if (HotelConstant.LOGIN_TYPE_MOBILE_ClIENT.equals(type)) {
				diningRoomList = diningRoomService.findDiningRoomList(hotelId, "1", page, rows);
			}
			//h
			if (HotelConstant.LOGIN_TYPE_MOBILE_HOTEL.equals(type)) {
				diningRoomList = diningRoomService.findDiningRoomList(hotelId, "1", page, rows);
			}
			if( null !=diningRoomList  && diningRoomList.size()>0){
				for (DiningRoom diningRoom : diningRoomList) {
					DiningRoommModel model = new DiningRoommModel();
					BeanUtils.copyProperties(diningRoom, model);
					modeList.add(model);
				}
			}
			return RestResultGenerator.genSuccessResult(modeList);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}
	}
	
	/**
	 *
	 */
}
