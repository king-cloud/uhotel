package com.umessage.hotel.rest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.alibaba.fastjson.JSONObject;
import com.umessage.hotel.util.RestResultGenerator;

public class BaseResourceController {
	/**
	 * 登录认证异常
	 */
	@ExceptionHandler({ UnauthenticatedException.class, AuthenticationException.class })
	public String authenticationException(HttpServletRequest request, HttpServletResponse response) {
		// 输出JSON
		Map<String, Object> map = new HashMap<>();
		map.put("code", "-999");
		map.put("message", "未登录");
		writeJson(map, response);
		return null;
	}

	/**
	 * 权限异常
	 */
	@ExceptionHandler({ UnauthorizedException.class, AuthorizationException.class })
	public String authorizationException(HttpServletRequest request, HttpServletResponse response) {

		// 输出JSON
		Map<String, Object> map = new HashMap<>();
		map.put("code", "-998");
		map.put("message", "无权限");
		writeJson(map, response);
		return null;

	}

	/**
	 * 输出JSON
	 *
	 * @param response
	 * @author SHANHY
	 * @create 2017年4月4日
	 */
	private void writeJson(Map<String, Object> map, HttpServletResponse response) {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json; charset=utf-8");
			out = response.getWriter();
			JSONObject rs = new JSONObject(map);
			out.write(rs.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 手机登录
	 * @param request
	 */
	protected void assertMobileLogin(ServletRequest request) {
		// 验证vercode是否正确,如果正确返回sessionid，如果不正确返回验证失败信息。
		String type = (String) request.getAttribute("type");
		// 微信登录，未手机登录
		if (!"2".equals(type) && !"3".equals(type)) {
			throw new RuntimeException("please login with mobile");
		} 
	}
}
