package com.umessage.hotel.rest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.umessage.common.controller.BaseController;
import com.umessage.common.util.HttpRequestUtil;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.InvoiceApplication;
import com.umessage.hotel.domain.InvoiceTitle;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.model.InvoiceApplicationModel;
import com.umessage.hotel.model.InvoiceTitleModel;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.InvoiceApplicationService;
import com.umessage.hotel.service.InvoiceTitleService;
import com.umessage.hotel.service.OrderInfoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("发票管理")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/invoice")
public class InvoiceResource extends BaseController {

	@Autowired
	private InvoiceTitleService invoiceTitleService;
	@Autowired
	private InvoiceApplicationService invoiceApplicationService;
	@Autowired
	private HotelInfoService hotelInfoService;
	@Autowired
	private OrderInfoService orderInfoService;

	/**
	 * @添加发票抬头接口
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "添加发票抬头信息", notes = "添加发票抬头信息")
	@RequestMapping(value = "addInvoiceTitleInfo", method = RequestMethod.GET)
	@ResponseBody
	public String addInvoiceTitleInfo(ServletRequest request,InvoiceTitleModel invoiceTitleModel) {

		HashMap<Object, Object> resultData = new HashMap<>();
		String memberId = (String) request.getAttribute("subject");
		String hotelId = (String) request.getAttribute("issuer");
		
		try {
			if (invoiceTitleModel != null) {
				
				invoiceTitleModel.setHotelId(hotelId);
				invoiceTitleModel.setAddTime(new Date());
				invoiceTitleModel.setMemberId(memberId);
				Integer type = invoiceTitleService.addInvoiceTitleModel(invoiceTitleModel);
				if (type == 1) {
					resultData.put("state", 1);
					resultData.put("msg", "success");
				} else {
					resultData.put("state", 0);
					resultData.put("msg", "fail");
				}
			} else {
				resultData.put("state", 0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}

	/**
	 * @修改发票抬头接口
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "修改发票抬头", notes = "修改发票抬头")
	@RequestMapping(value = "updateInvoiceTitleInfo", method = RequestMethod.GET)
	@ResponseBody
	public String updateInvoiceTitleInfo(ServletRequest request,InvoiceTitleModel invoiceTitleModel) {

		HashMap<Object, Object> resultData = new HashMap<>();
		String memberId = (String) request.getAttribute("subject");

		try {
			if (invoiceTitleModel != null) {
				invoiceTitleModel.setMemberId(memberId);
				invoiceTitleModel.setUpdateTime(new Date());
				Integer type = invoiceTitleService.updateByPrimaryKeyModelSelective(invoiceTitleModel);
				if (type == 1) {
					resultData.put("state", 1);
					resultData.put("msg", "success");
				} else {
					resultData.put("state", 0);
					resultData.put("msg", "fail");
				}
			} else {
				resultData.put("state", 0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}

	/**
	 * @删除发票抬头信息接口
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "删除发票抬头信息", notes = "删除发票抬头信息")
	@RequestMapping(value = "deleteInvoiceTitleInfo", method = RequestMethod.GET)
	@ResponseBody
	public String deleteInvoiceTitleInfo(String id) {

		HashMap<Object, Object> resultData = new HashMap<>();

		try {

			if (id != null && !"".equals(id)) {
				Integer result = invoiceTitleService.deleteByPrimaryKey(Integer.parseInt(id));

				if (result == 1) {
					resultData.put("state", 1);
					resultData.put("msg", "success");
				} else {
					resultData.put("state", 0);
					resultData.put("msg", "fail");
				}
			} else {
				resultData.put("state", 0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}

		return JSONObject.toJSONString(resultData);
	}

	/**
	 * @获取发票抬头接口:通过会员号进行查询
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "获取发票抬头信息", notes = "获取发票抬头信息")
	@RequestMapping(value = "getInvoiceTitleInfo", method = RequestMethod.GET)
	@ResponseBody
	public String getInvoiceTitleInfo(ServletRequest request) {
		HashMap<Object, Object> resultData = new HashMap<>();
		try {
			String memberId = (String) request.getAttribute("subject");
			if (memberId != null && !"".equals(memberId)) {
				List<InvoiceTitle> invoiceTitleInfo = invoiceTitleService.getInvoiceTitleInfo(memberId);
				
				resultData.put("state", 1);
				resultData.put("msg", "success");
				if (invoiceTitleInfo != null && invoiceTitleInfo.size() > 0) {
					resultData.put("data", invoiceTitleInfo);
					resultData.put("count", invoiceTitleInfo.size());
				} else {
					resultData.put("data", "");
					resultData.put("count", 0);
				}
			} else {
				resultData.put("state", 0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}

		return JSONObject.toJSONString(resultData);
	}

	/**
	 * @开票申请接口
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "开票申请接口", notes = "开票申请接口")
	@RequestMapping(value = "addInvoiceApplicationInfo", method = RequestMethod.GET)
	@ResponseBody
	public String addInvoiceApplicationInfo(ServletRequest request, InvoiceApplicationModel invoiceApplicationModel) {

		HashMap<Object, Object> resultData = new HashMap<>();

		try {
			String hotelId = (String) request.getAttribute("issuer");
			String memberId = (String) request.getAttribute("subject");

			if (invoiceApplicationModel != null) {
				invoiceApplicationModel.setHotelId(hotelId);
				invoiceApplicationModel.setMemberId(memberId);
				invoiceApplicationModel.setAddTime(new Date());
				invoiceApplicationModel.setStatus("0");
				// 1.完成数据插入
				invoiceApplicationService.addInvoiceApplicationInfoModel(invoiceApplicationModel);
				// 2.调用开发票接口，根据返回结果进行相关参数的返回
				Map<String, String> map = new HashMap<String, String>();
				map.put("hotelId", invoiceApplicationModel.getHotelId());
				map.put("orderId", invoiceApplicationModel.getOrderId());
				map.put("mobile", invoiceApplicationModel.getMobile());
				map.put("amount", String.valueOf(invoiceApplicationModel.getAmount()));
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String formatStr = formatter.format(new Date());
				map.put("openDate", formatStr);
				map.put("email", invoiceApplicationModel.getEmail());
				String titleId = invoiceApplicationModel.getTitleId();
				if (titleId != null && !"".equals(titleId)) {
					InvoiceTitle invoiceTitle = invoiceTitleService.selectByPrimaryKey(Integer.parseInt(titleId));
					map.put("enterpriseName", invoiceTitle.getTitle());
					map.put("TaxpayerNum", invoiceTitle.getTaxNo());
					map.put("address", invoiceTitle.getAddress());
					map.put("bankAccount", invoiceTitle.getAccount());
					map.put("bankName", invoiceTitle.getBank());
					map.put("openType", invoiceTitle.getType());
					HotelInfo hotelInfo = hotelInfoService.queryHotelsByHotelId(invoiceApplicationModel.getHotelId());
					map.put("businessNum", hotelInfo.getBusinessNum());
				}
				String jsonStr = JSONObject.toJSONString(map);
				JSONObject jsonObject = null;
				String urls = "http://39.104.68.154:8093/invoice/ticket";

				Map<String, String> mapParam = new HashMap<String, String>();
				mapParam.put("jsonStr", jsonStr);
				String send = HttpRequestUtil.send(urls, mapParam, "utf-8");
				jsonObject = JSONObject.parseObject(send);
				
				if (jsonObject.getString("status").equals("1")) {

					String djh = jsonObject.getString("djh");
					String orderId = jsonObject.getString("orderId");

					invoiceApplicationModel.setStatus("1");
					invoiceApplicationModel.setUpdateTime(new Date());
					invoiceApplicationService.updateByModelTitleIdSelective(invoiceApplicationModel);

					resultData.put("state", 1);
					resultData.put("msg", "success");
					resultData.put("djh", djh);
					resultData.put("orderId", orderId);
	            	
					OrderInfo orderInfo = new OrderInfo();
					orderInfo.setOrderSn(orderId);
					orderInfo.setInvoiceFlag("1");
					orderInfo.setInvoiceTime(new Date());
					orderInfoService.updateByOrderSnSelective(orderInfo);
					
				} else {
					invoiceApplicationModel.setStatus("2");
					invoiceApplicationModel.setUpdateTime(new Date());
					invoiceApplicationModel.setResult(jsonObject.getString("msg"));
					invoiceApplicationService.updateByModelTitleIdSelective(invoiceApplicationModel);
					resultData.put("state", 0);
					resultData.put("msg", "fail");
					
					//OrderInfo orderInfo = new OrderInfo();
					//orderInfo.setOrderId(invoiceApplicationModel.getOrderId());
					//orderInfo.setInvoiceFlag("0");
					//orderInfoService.updateNotNull(orderInfo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}
		return JSONObject.toJSONString(resultData);
	}

	/**
	 * @查询开票进度接口:通过酒店号和手机号进行查询
	 * @param xmlStr
	 * @return
	 */
	@ApiOperation(value = "查询开票进度", notes = "查询开票进度")
	@RequestMapping(value = "getInvoiceApplicationStatus", method = RequestMethod.GET)
	@ResponseBody
	public String getInvoiceApplicationStatus(ServletRequest request, String orderId) {
		HashMap<Object, Object> resultData = new HashMap<>();
		HashMap<Object, Object> data = new HashMap<>();

		try {
			if (orderId != null && !"".equals(orderId)) {
			
				InvoiceApplication applicationStatus = invoiceApplicationService.getInvoiceApplicationStatus(orderId);
				
				if (applicationStatus != null) {
					
					InvoiceTitle invoiceTitle = invoiceTitleService.selectByPrimaryKey(Integer.parseInt(applicationStatus.getTitleId()));
					
					resultData.put("state", 1);
					resultData.put("msg", "success");
					data.put("status", applicationStatus.getStatus());
					data.put("openType", invoiceTitle.getType());
					data.put("hotelId", invoiceTitle.getHotelId());
					data.put("taxNo", invoiceTitle.getTaxNo());
					data.put("title", invoiceTitle.getTitle());
					data.put("phone", invoiceTitle.getPhone());
					data.put("address", invoiceTitle.getAddress());
					data.put("bank", invoiceTitle.getBank());
					data.put("account", invoiceTitle.getAccount());
					data.put("email", applicationStatus.getEmail());
					data.put("mobile", applicationStatus.getMobile());
					
					resultData.put("data", data);
				} else {
					resultData.put("state", 0);
					resultData.put("msg", "fail");
					resultData.put("data", "");
				}
			} else {
				resultData.put("state", 0);
				resultData.put("msg", "fail");
			}
		} catch (Exception e) {
			resultData.put("state", 0);
			resultData.put("msg", "fail");
		}

		return JSONObject.toJSONString(resultData);
	}
}
