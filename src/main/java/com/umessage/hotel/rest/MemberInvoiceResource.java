package com.umessage.hotel.rest;

import javax.servlet.ServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.hotel.domain.MemberInvoice;
import com.umessage.hotel.model.MemberInvoiceModel;
import com.umessage.hotel.service.MemberInvoiceService;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;
import com.umessage.hotel.util.TimeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 用户发票
 *
 */
@Api("发票列表管理")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/memberInvoice")
public class MemberInvoiceResource extends BaseResourceController {

	@Autowired
	private MemberInvoiceService memberInvoiceService;
	
	@ApiOperation(value="创建发票", notes="新增发票")
	@RequestMapping(method =RequestMethod.POST)
	public RestResult<String> addInvoice(MemberInvoiceModel memberInvoiceModel, ServletRequest request){
		try {
			assertMobileLogin(request);
			MemberInvoice memberInvoice = new MemberInvoice();
			BeanUtils.copyProperties(memberInvoiceModel, memberInvoice);
			
			String memberId = (String) request.getAttribute("subject");
			String hotelId = (String) request.getAttribute("issuer");
			String openid = (String) request.getAttribute("openid");
			memberInvoice.setHotelId(Integer.parseInt(hotelId));
			memberInvoice.setMemberId(memberId);
			memberInvoice.setOpenId(openid);
			memberInvoice.setCheckoutTime(memberInvoiceModel.getCheckoutTime());
			memberInvoiceService.addInvoice(memberInvoice);
			return RestResultGenerator.genSuccessResult();
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult("新增失败"+e.getMessage());
		}
	}

}
