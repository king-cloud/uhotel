package com.umessage.hotel.rest;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.hotel.model.HotelIndexModel;
import com.umessage.hotel.service.DepositOrderService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.WXUserMemberService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("仪表盘")
@RestController
@RequestMapping(value = "/rest/dashboard")
public class DashboardResource {
	/**
	 * 已确认订单
	 */
	public static final String ORDER_STATUS_CONFIRM = "1";

	@Autowired
	private MemberInfoService memberInfoService;
	@Autowired
	private OrderInfoService orderService;
	@Autowired
	private DepositOrderService depositOrderService;
	@Autowired
	private WXUserMemberService wxUserMemberService;

	private DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * 小程序首页信息 今日详情
	 */
	@ApiOperation(value = "小程序首页", notes = "首页今日详情")
	@RequestMapping(value = "/today", method = RequestMethod.GET)
	public RestResult<HotelIndexModel> hotelIndexToday(ServletRequest request) {
		try {
			String hotelId = (String) request.getAttribute("issuer");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String startTime = LocalDate.now().format(formatter) +" 00:00:00";

			String endTime = LocalDate.now().format(formatter) +" 23:59:59";
			
			// 新增会员
			Integer vipCount = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_MEMBER, startTime, endTime);
			
			// 新增用户
			Integer registerCount = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_NEW,
					startTime, endTime);
			// 成交订单
//			Integer orderCount = this.orderService.countOrders(hotelId, ORDER_STATUS_CONFIRM, startTime, endTime);
			Integer orderCount = this.orderService.countEffectiveOrders(hotelId, startTime, endTime) ;
			// 充值金额
			BigDecimal depositMoney = this.depositOrderService.taotalPay(hotelId, startTime, endTime);
			// 总收入
			BigDecimal ordersIncome = this.orderService.countEffectiveOrdersIncome(hotelId, startTime, endTime);
			//BigDecimal ordersIncome = this.orderService.countOrdersIncome(hotelId, startTime, endTime);// 订单总收入
			HotelIndexModel hotelIndexModel = new HotelIndexModel();
			hotelIndexModel.setUserCount(registerCount);
			hotelIndexModel.setVipCount(vipCount);
			hotelIndexModel.setOrderCount(orderCount);
			hotelIndexModel.setDepositMoney(depositMoney);
			hotelIndexModel.setTotalIncome(ordersIncome);
			return RestResultGenerator.genSuccessResult(hotelIndexModel);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}

	}

	/**
	 * 小程序首页信息 统计
	 */
	@ApiOperation(value = "小程序首页", notes = "首页统计详情")
	@RequestMapping(value = "/total", method = RequestMethod.GET)
	public RestResult<HotelIndexModel> hotelIndexTotal(ServletRequest request,
			@RequestParam(required = true) String startTime, @RequestParam(required = true) String endTime) {
		try {
			String hotelId = (String) request.getAttribute("issuer");
			startTime+=" 00:00:00";
			endTime+=" 23:59:59";
			// 新增会员
			//Integer vipCount = this.wxUserMemberService.getCountNewMemberByDate(hotelId, startTime, endTime);
			
			// 新增用户
			//Integer registerCount = this.memberInfoService.countNewMember(hotelId, HotelConstant.USER_GRADE_NEW,
			//		startTime, endTime)+vipCount;
			// 新增用户
			Integer registerCount = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_NEW,
					startTime, endTime);
			// 新增会员
			Integer vipCount = this.memberInfoService.countNewMemberByDate(hotelId, HotelConstant.USER_GRADE_MEMBER, startTime, endTime);
			
			// 成交订单
//			Integer orderCount = this.orderService.countOrders(hotelId, ORDER_STATUS_CONFIRM, startTime, endTime);
			Integer orderCount = this.orderService.countEffectiveOrders(hotelId, startTime, endTime) ;
			// 充值金额
			BigDecimal depositMoney = this.depositOrderService.taotalPay(hotelId, startTime, endTime);
			// 总收入
			BigDecimal ordersIncome = this.orderService.countEffectiveOrdersIncome(hotelId, startTime, endTime);
//			BigDecimal ordersIncome = this.orderService.countOrdersIncome(hotelId, startTime, endTime);// 订单总收入
			HotelIndexModel hotelIndexModel = new HotelIndexModel();
			hotelIndexModel.setUserCount(registerCount);
			hotelIndexModel.setVipCount(vipCount);
			hotelIndexModel.setOrderCount(orderCount);
			hotelIndexModel.setDepositMoney(depositMoney);
			hotelIndexModel.setTotalIncome(ordersIncome);
			return RestResultGenerator.genSuccessResult(hotelIndexModel);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}
	}

}
