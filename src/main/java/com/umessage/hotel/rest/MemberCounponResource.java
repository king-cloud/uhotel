package com.umessage.hotel.rest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.domain.MemberCoupon;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.model.CouponModel;
import com.umessage.hotel.service.CouponService;
import com.umessage.hotel.service.MemberCouponService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.hotel.util.RestResult;
import com.umessage.hotel.util.RestResultGenerator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("会员优惠券管理")
@RestController
@EnableAutoConfiguration
@RequestMapping("/rest/membercoupons")
public class MemberCounponResource {
	@Autowired
	private CouponService couponService;
	@Autowired
	private MemberCouponService memberCouponService;
	@Autowired
	private MemberInfoService memberInfoService;
/*	public CouponModel getCoupon() {

		CouponModel coupon = new CouponModel();
		coupon.setCouponId(UUID.randomUUID().toString());
		coupon.setCouponId("2");
		coupon.setHotelId(UUID.randomUUID().toString());
		coupon.setMode("1");
		coupon.setFirstValue(500);
		coupon.setSecondValue(100);
		coupon.setIsUsable("1");
		coupon.setShareUrl("");
		coupon.setValidStart("2018-10-10");
		coupon.setValidEnd("2018-09-09");

		return coupon;
	}*/

	@ApiOperation(value = "会员优惠券列表", notes = "会员优惠券列表")
	@RequestMapping(method = RequestMethod.GET)
	public RestResult<List<CouponModel>> list(ServletRequest request) {
		String hotelId = (String) request.getAttribute("issuer");
		String memberId = (String) request.getAttribute("subject");
		try {
			List<CouponModel> list = new ArrayList<CouponModel>();
			String couponType="1";
			List<MemberCoupon> mcs=this.memberCouponService.findCoupons(memberId,hotelId,couponType);
			if(!mcs.isEmpty()) {
				for (MemberCoupon mc : mcs) {
					CouponModel cmodel = new CouponModel();
					cmodel.setCouponId(mc.getCouponId()+"");
					cmodel.setHotelId(hotelId);
					cmodel.setCouponType(couponType);
					cmodel.setIsUsable(mc.getCoup().getIsUsable());
					cmodel.setMode(mc.getCoup().getMode()+"");
					if(mc.getCoup().getBarCode()!=null) {
						cmodel.setBarCode(mc.getCoup().getBarCode());
					}
					if(mc.getCoup().getShareUrl()!=null) {
						cmodel.setShareUrl(mc.getCoup().getShareUrl());
					}
					if(mc.getCoup().getFirstNumber()!=null) {
						cmodel.setFirstValue(mc.getCoup().getFirstNumber());
					}
					cmodel.setSecondValue(mc.getCoup().getSecondNumber());
					if(mc.getCoup().getValidType()==1) {
						cmodel.setValidEnd(mc.getCoup().getValidEnd());
						cmodel.setValidStart(mc.getCoup().getValidStart());
					}else if(mc.getCoup().getValidType()==2){
						cmodel.setValidEnd(mc.getValidLastTime());
						cmodel.setValidStart(mc.getValidFirstTime());
					}
					cmodel.setStatus(mc.getStatus());
					list.add(cmodel);
				}
			}
			return RestResultGenerator.genSuccessResult(list);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult("查询失败"+e.getMessage());
		}
	}
	
	/*@ApiOperation(value = "会员可用充值券列表", notes = "会员可用充值券列表")
	@RequestMapping(value = "coupons/fill", method = RequestMethod.GET)
	public RestResult<List<CouponModel>> filledList(ServletRequest request) {
		try{
			String hotelId = (String) request.getAttribute("issuer");
			String couponType="2";
			List<CouponModel> list = new ArrayList<CouponModel>();
			//List<Coupon> coupons=this.couponService.findCouponsByWXUser(memberId,hotelId,couponType);
			List<Coupon> coupons=this.couponService.findCoupons(hotelId,couponType,null);
			if(!coupons.isEmpty()) {
				for (Coupon coupon : coupons) {
					CouponModel ucm=new CouponModel();
					ucm.setCouponId(coupon.getCouponId()+"");
					ucm.setCouponType(coupon.getCouponType());
					if(coupon.getFirstNumber()!=null) {
						ucm.setFirstValue(coupon.getFirstNumber());
					}
					ucm.setMode(coupon.getMode()+"");
					ucm.setSecondValue(coupon.getSecondNumber());
					ucm.setValidEnd(coupon.getValidEnd());
					ucm.setValidStart(coupon.getValidStart());
					if(coupon.getBarCode()!=null) {
						ucm.setBarCode(coupon.getBarCode());
					}
					if(coupon.getShareUrl()!=null) {
						ucm.setShareUrl(coupon.getShareUrl());
					}
					list.add(ucm);
				}
				return RestResultGenerator.genSuccessResult(list);
			}
			return RestResultGenerator.genSuccessResult(list);
		}catch(Exception e){
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}
	}*/
	/**
	 * 获取优惠券二维码 或者二维码生成位静态地址节省计算资源
	 * 
	 * @param couponId
	 * @return
	 */
	@RequestMapping(value = "{couponId}/barcode", method = RequestMethod.GET)
	public RestResult<String> getBarCode(@PathVariable("couponId") String couponId) {
		// 返回图片地址
		return RestResultGenerator.genSuccessResult("http://1111.jpg");
	}

	/**
	 * 分享优惠券
	 * 
	 * @param couponId
	 * @return
	 */
	@RequestMapping(value = "{couponId}/share", method = RequestMethod.GET)
	public RestResult share(@PathVariable("couponId") String couponId) {
		return RestResultGenerator.genSuccessResult();
	}

	/**
	 * 领取优惠券
	 * 
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "领取优惠券", notes = "领取优惠券")
	@RequestMapping(value = "receiceCoupon", method = RequestMethod.GET)
	public RestResult<String> receiceCoupon(@RequestParam(required = true) String couponId,
			@RequestParam(required = true) String mobile,ServletRequest request) {
		try {
			String hotelId = (String) request.getAttribute("issuer");
			MemberInfo memberInfo = this.memberInfoService.findMemberByMobile(hotelId,mobile);
			String memberId=null;
			if(memberInfo==null) {
				MemberInfo member=new MemberInfo();
				member.setHotelId(hotelId);
				member.setMobile(mobile);
				member.setRealMoney(BigDecimal.ZERO);
				member.setVirtMoney(BigDecimal.ZERO);
				member.setLastLoginTime(new Date());
				member.setGrade(HotelConstant.USER_GRADE_NEW);
				member.setAddTime(new Date());
				memberId = this.memberInfoService.saveMember(member);
			}else {
				memberId = memberInfo.getMemberId();
			}
			List<Coupon> coupons = this.couponService.findCoupons(hotelId, "1",couponId);
			if(!coupons.isEmpty()) {
				Coupon coupon = coupons.get(0);
				MemberCoupon mc=new MemberCoupon();
				mc.setGetTime(new Date());
				mc.setStatus("1");
				mc.setMemberId(memberId);
				mc.setCouponId(Integer.valueOf(couponId));
				mc.setCouponType(coupon.getCouponType());
				if(coupon.getValidType()==1) {
					mc.setValidFirstTime(coupon.getValidStart());
					mc.setValidLastTime(coupon.getValidEnd());
				}else
				if(coupon.getValidType()==2) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String valudStart = sdf.format(new Date());
					mc.setValidFirstTime(valudStart);
					Integer cycle = Integer.valueOf(coupon.getCycle());
					Calendar cale = Calendar.getInstance();
					cale.setTime(sdf.parse(valudStart));
					int month1 = cale.get(Calendar.MONTH);
					int pm = month1+cycle;
					cale.set(Calendar.MONTH, pm);
					Date time = cale.getTime();
					String validEnd = sdf.format(time);
					mc.setValidLastTime(validEnd);
				}
				
				this.memberCouponService.saveMemberCoupon(mc);
				return RestResultGenerator.genSuccessResult("领取成功！");
			}
			return RestResultGenerator.genSuccessResult("时间过时！");
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult("领取失败");
		}
	}

	/**
	 * 统计个人优惠券数量
	 * 
	 * @return
	 */
	@ApiOperation(value = "统计个人优惠券数量", notes = "统计个人优惠券数量")
	@RequestMapping(value = "countNum", method = RequestMethod.GET)
	public RestResult<Integer> countNum(ServletRequest request) {
		try {
			String memberId = (String) request.getAttribute("subject");
			String hotelId = (String) request.getAttribute("issuer");
			String couponType = "1";
			Integer sumCount = this.memberCouponService.getSumCountByUserId(memberId, hotelId, couponType);
			return RestResultGenerator.genSuccessResult(sumCount);
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult(e.getMessage());
		}
	}
	
	/**
	 * 查询当前酒店充值券
	 */
/*	@ApiOperation(value="查询当前酒店充值券" , notes = "查询当前酒店充值券" )
	@RequestMapping(value="allRecharge", method=RequestMethod.GET)
	public RestResult<List<Coupon>> selectAllCoupon(ServletRequest request){
		try {
			String hotelId = (String) request.getAttribute("issuer");
			Coupon coupon = new Coupon();
			coupon.setHotelId(hotelId == null ? 0 : Integer.parseInt(hotelId));
			coupon.setCouponType("2");
			List<Coupon> couponList = couponService.getCoupons(coupon);
			return RestResultGenerator.genSuccessResult(couponList);			
		} catch (Exception e) {
			e.printStackTrace();
			return RestResultGenerator.genErrorResult("查询酒店优惠券失败"+e.getMessage());
		}
	}*/
	
}
