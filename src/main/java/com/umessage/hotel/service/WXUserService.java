package com.umessage.hotel.service;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.WXUser;

public interface WXUserService extends IService<WXUser> {
	
	public WXUser findUserByOpenid(String openId);

}
