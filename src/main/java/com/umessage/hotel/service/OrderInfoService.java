package com.umessage.hotel.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.model.PaySignModel;

public interface OrderInfoService extends IService<OrderInfo> {

	/**
	 * 获取会员单个订单
	 * 
	 * @param memberId
	 * @param orderId
	 * @return
	 */
	public OrderInfo findSingleOrder(String memberId, String orderId);

	/**
	 * 获取订单列表
	 * 
	 * @param memberId
	 * @return
	 */
	public List<OrderInfo> findOrderList(OrderInfo orderInfo, int page, int rows);

	/**
	 * 获取用户订单
	 * 
	 * @param memberId
	 * @return
	 */
	public List<OrderInfo> findUserOrderList(String memberId, String orderStatus, int page, int rows);

	/**
	 * 获取酒店订单
	 * 
	 * @param hotelId
	 * @return
	 */
	public List<OrderInfo> findHotelOrderList(String hotelId, String orderStatus, int page, int rows);

	/**
	 * 确认订单
	 * 
	 * @param hotelId
	 * @param accountId
	 * @param orderId
	 */
	public void confirmOrder(String hotelId, String accountId, String orderId);;

	/**
	 * 酒店取消订单
	 * 
	 * @param hotelId   酒店id
	 * @param accountId 员工id
	 * @param orderId   订单id
	 * @param reason    取消原因
	 */
	public void cancelOrderByHotel(String wxcfgid, String hotelId, String accountId, String orderId, String reason);

	/**
	 * 会员取消订单
	 * 
	 * @param hotelId  酒店id
	 * @param memberId 会员id
	 * @param orderId  订单id
	 * @param reason   取消原因
	 */
	public void cancelOrderByMember(String wxcfgid, String hotelId, String memberId, String orderId, String reason);

	/**
	 * 统计订单数
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 */
	public Integer countOrders(String hotel, String orderStatus, String startTime, String endTime);
	
	/**
	 * 查询复购用户数量
	 * select member_id, count(*) from t_order_info where order_status ==3 having count(*) > 1 ;
	 * @param hotelId
	 * @param memberId
	 * @param repeatCount
	 * @return
	 */
	public Integer countRepeatedPurchaseUsers(String hotelId, String orderStatus, Integer repeatCount);
	
	/**
	 * 根据用户ID统计订单数
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 */
	public Integer countMemberTypeOrders(String hotelId, String memberId, String payStatus, String orderStatus, String startTime, String endTime);
	
	
	
	/**
	 * 统计当前已经正常结束订单数
	 * orderStatus=3 checkoutime=今天
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 */
	public Integer countEffectiveOrders(String hotel, String startTime, String endTime);
	/**
	 *  统计当前已经正常结束订单列表有效总收入
	 *  orderStatus=3 checkoutime=今天
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public BigDecimal countEffectiveOrdersIncome(String hotel, String startTime, String endTime);
	
	/**
	 * h统计  待确认-订单数
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 */
	public Integer countWaitOrder(String hotel, String orderStatus,String payStatus);

	/**
	 * 统计订单收入
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public BigDecimal countOrdersIncome(String hotel, String startTime, String endTime);
	
	/**
	 * 统计会员已完成订单总金额
	 * 
	 * @param hotel
	 * @param orderStatus
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public BigDecimal countMemberOrdersIncome(String memberId, String hotelId);

	/**
	 * 保存新订单
	 * 
	 * @param orderInfo
	 * @return
	 */
	public Integer saveNewOrder(OrderInfo orderInfo);

	/**
	 * 客户取消订单
	 * 
	 * @param operatorType 操作员类型
	 * @param operator     操作员id
	 * @param orderId      订单id
	 * @param byPolicies   是否依据政策
	 * @param reason       取消原因
	 */
	public void cancelOrder(String wxcfgid, Integer operatorType, String operator, String orderId, boolean byPolicies,
			String reason);
	/**
	 * 酒店取消订单
	 * 
	 * @param operatorType 操作员类型
	 * @param operator     操作员id
	 * @param orderId      订单id
	 * @param byPolicies   是否依据政策
	 * @param reason       取消原因
	 */
	public void cancelOrderByHotel(String wxcfgid, Integer operatorType, String operator, String orderId, boolean byPolicies,
			String reason);
	
	public void cancelOrderByAuto(String wxcfgid, Integer operatorType, String operator, OrderInfo order, boolean byPolicies,
			String reason);

	/**
	 * 创建统一订单
	 * 
	 * @param openid    openid
	 * @param orderType 订单类型（订房、充值）
	 * @param orderId   订单id
	 * @param productId 产品id
	 * @param createdIp 创建ip
	 * @param notifyUrl 通知地址
	 * @param fee       支付金额
	 * @return
	 */
	public PaySignModel createUnifiedOrder(String wxcfgid, String openid, String createdIp, String orderId);

	/**
	 * 查询所有订单
	 * 
	 * @param hotelId
	 * @param page
	 * @param rows
	 * @return
	 */
	public ArrayList<OrderInfo> findOrderListByQuery(OrderInfo orderInfo, int page, int rows);

	/**
	 * 获取用户订单
	 * 
	 * @param memberId
	 * @param orderStatus 
	 * @param payStatus
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findUserOrderListByMoreState(String memberId, String orderStatus, String payStatus, int page,
			int rows);

	/**
	 * 获取酒店订单
	 * @param hotelId
	 * @param orderStatus
	 * @param payStatus
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findHotelOrderListByMoreState(String hotelId, String orderStatus, String payStatus,int page, 
			int rows);
	
	

	/**
	 * 查询用户待入住的订单
	 * @param subject
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findUserOrderListByUnCheckIn(String subject, int page, int rows);

	/**
	 * 查询酒店代入住用户订单
	 * @param hotelId
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findHotelOrderListByUnCheckIn(String hotelId, int page, int rows);

	/**
	 * 自动调度取消订单
	 * @param beginTime
	 */
	public void cancelOrderByAutoTask(String beginTime);
	/**
	 * 取消订单列表
	 * @param hotelId
	 * @param orderStatus
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findCancleOrderListSql(String hotelId,String orderStatus, int page, int rows);
	/**
	 * 确认订单列表
	 * @param hotelId
	 * @param orderStatus
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findConfirmOrderListSql(String hotelId,String orderStatus, int page, int rows);
	
	/**
	 * 微信退款请求
	 * 
	 * @param order
	 */
	public void refundRequest(String wxcfgid, String orderId, int refundFee, int totalFee);
	
	/**
	 * 更新订单状态
	 * @param oldStatus 
	 * @param orderInfo
	 * @return
	 */
	public Integer updateByOrderStatusSelective(String orderStatus,String startTime,String endTime, String oldStatus);
	
	public Integer updateByOrderSnSelective(OrderInfo orderInfo);

	public OrderInfo getOrderInfo(OrderInfo orderInfo);
	
	/**
	 * 发送短信通知
	 * @param messageType ("0": 新建 ,"1":已确认, "2":已取消, "3":支付成功)
	 * @param orderInfo
	 * @return
	 */
	public void sendSms(String messageType, OrderInfo orderInfo);
	
	/**
	 * 发送短信通知
	 * @param messageType ("0": 新建 ,"1":已确认, "2":已取消, "3":支付成功)
	 * @param orderId
	 * @return
	 */
	public void sendSmsByOrderId(String messageType, String orderId);

	/**
	 * 查询酒店当日订单
	 * @param hoteId
	 * @param book_time
	 * @return
	 */
	public List<OrderInfo> findHotelOrderAll(String hoteId, String bookTime);

	/**
	 * h 全部订单
	 * @param hotelId
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<OrderInfo> findHotelOrderAll(String hotelId, int page, int rows);

	public Integer updateByOrderStatusOver(String orderStatus, String startTime, String endTime, String oldStatus);
	
	
	public Integer updateByOrderStatusOverByHotelId(String orderStatus, String startTime, String endTime, String oldStatus, String hotelId);

	/**
	 * 当天入住订单
	 * @param oldStatus
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<OrderInfo> findOrderToDay(String oldStatus, String startTime, String endTime);
	/**
	 * 根据酒店ID查询当天入住订单
	 * @param oldStatus
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<OrderInfo> findOrderToDayByHotelId(String oldStatus, String startTime, String endTime, String hotelId);

	public List<OrderInfo> select30TimeoutOrder(String beginTime);

	public List<OrderInfo> select12hTimeoutOrder(String beginTime);
	
	public List<OrderInfo> select12hTimeoutOrderByHotelId(String beginTime, String hotelId);

	public Integer countMemberConfirmedOrders(String hotelId, String subject, String string, String string2,
			String startTime, String endTime);

	public Integer countMemberOrders(String hotelId, String subject, String string, String startTime, String endTime);


}

























