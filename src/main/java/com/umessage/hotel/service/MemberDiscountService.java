package com.umessage.hotel.service;


import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberDiscount;

public interface MemberDiscountService  extends IService<MemberDiscount> {

	int add(MemberDiscount member);

	List<MemberDiscount> getMemberList(MemberDiscount member);

	MemberDiscount getMemberDiscount(String gradeId,String hotelId);
	
	public MemberDiscount getMemberDiscountList(String gradeId,String hotelId);
	
	public List<MemberDiscount> getMemberList2(MemberDiscount mds);
}
