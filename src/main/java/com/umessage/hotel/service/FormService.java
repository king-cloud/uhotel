package com.umessage.hotel.service;

import java.util.List;

import com.umessage.hotel.model.FormModel;

public interface FormService {
	
	public void saveFormId(String openid, FormModel form);

	void saveFormIdList(String openid, List<FormModel> list);

	String getValidFormId(String openid);

}
