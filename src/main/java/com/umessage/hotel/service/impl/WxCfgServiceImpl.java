package com.umessage.hotel.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.WxCfgMapper;
import com.umessage.hotel.domain.WxCfg;
import com.umessage.hotel.service.WxCfgService;

@Service("wxCfgService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WxCfgServiceImpl extends BaseService<WxCfg> implements WxCfgService {

	@Autowired
	private WxCfgMapper wxcfgMapper;

	@Override
	public WxCfg queryWxCfg(String hotelId, String appType) {
		return wxcfgMapper.queryWxCfg(hotelId, appType);
	}

	@Override
	public WxCfg getWxCfg(WxCfg wxcf) {
		// TODO Auto-generated method stub
		return wxcfgMapper.selectOne(wxcf);
	}

	@Override
	public Integer addwxcf(WxCfg wxcf) {
		// TODO Auto-generated method stub
		return wxcfgMapper.insert(wxcf);
	}

}
