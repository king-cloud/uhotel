package com.umessage.hotel.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.hotel.dao.InvoiceTitleMapper;
import com.umessage.hotel.domain.InvoiceTitle;
import com.umessage.hotel.model.InvoiceTitleModel;
import com.umessage.hotel.service.InvoiceTitleService;

@Service("invoiceTitleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class InvoiceTitleServiceImpl implements InvoiceTitleService{
	
	@Autowired
	private InvoiceTitleMapper invoiceTitleMapper;

	@Override
	public Integer addInvoiceTitle(InvoiceTitle invoiceTitle) {
		return invoiceTitleMapper.insertSelective(invoiceTitle);
	}

	@Override
	public List<InvoiceTitle> getInvoiceTitleInfo(String memberId) {
		return invoiceTitleMapper.getInvoiceTitleInfo(memberId);
	}

	@Override
	public Integer deleteByPrimaryKey(Integer id) {
		return invoiceTitleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Integer updateByPrimaryKeySelective(InvoiceTitle record) {
		return invoiceTitleMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public InvoiceTitle selectByPrimaryKey(Integer id) {
		return invoiceTitleMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer addInvoiceTitleModel(InvoiceTitleModel invoiceTitleModel) {
		InvoiceTitle invoiceTitle = new InvoiceTitle();
	  	String ignoreProperties = "residentLimit";
		BeanUtils.copyProperties(invoiceTitleModel, invoiceTitle , ignoreProperties);
		return invoiceTitleMapper.insertSelective(invoiceTitle);
	}

	@Override
	public Integer updateByPrimaryKeyModelSelective(InvoiceTitleModel invoiceTitleModel) {
		InvoiceTitle invoiceTitle = new InvoiceTitle();
	  	String ignoreProperties = "residentLimit";
		BeanUtils.copyProperties(invoiceTitleModel, invoiceTitle , ignoreProperties);
		return invoiceTitleMapper.updateByPrimaryKeySelective(invoiceTitle);
	}

	
	
	
	
}
