package com.umessage.hotel.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.CouponMapper;
import com.umessage.hotel.domain.Coupon;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.service.CouponService;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("couponService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class CouponServiceImpl extends BaseService<Coupon> implements CouponService {

	@Autowired
	private CouponMapper couponMapper;
	@Override
	public List<Coupon> getCoupons(Coupon coupon) {
		try {
			Example example = new Example(Coupon.class);
			Criteria createCriteria = example.createCriteria();
			if (coupon.getHotelId()!=null) {
				createCriteria.andCondition("hotel_id=", coupon.getHotelId());
			}
			if (StringUtils.isNotBlank(coupon.getCouponType())) {
				createCriteria.andCondition("coupon_type=", coupon.getCouponType());
			}
			if (StringUtils.isNotBlank(coupon.getIsUsable())) {
				createCriteria.andCondition("is_usable=", coupon.getIsUsable());
			}
			createCriteria.andCondition("is_delete=", 0);
			
			if (StringUtils.isNotBlank(coupon.getValidEnd())){  //用于酒店端查询未过期的优惠券和没有过期时间的优惠券
				createCriteria.andCondition("(valid_end >= '"+coupon.getValidEnd()+"' OR valid_end IS NULL )");
			}
			example.setOrderByClause("coupon_id desc");
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	@Override
	public void updateByCouponId(Coupon coupon) {
		this.updateNotNull(coupon);
	}

	@Override
	public List<Coupon> findCouponsByWXUser(String userId, String hotelId,String couponType) {
		return this.couponMapper.findCouponsByWXUser(userId,hotelId,couponType);
	}

	/**
	 * h统计优惠券
	 * @param nowtTime 当前时间
	 */
	@Override
	public Integer getCountCoupons(String hotelId,String nowTime) {
		return this.couponMapper.getCountCoupons(hotelId,nowTime); 
	}

	public Coupon getCouponById(String couponId) {
			try {
				if (StringUtils.isNotBlank(couponId)) {
					Coupon coupon = new Coupon();
					coupon.setCouponId(Integer.parseInt(couponId));
					return this.couponMapper.getCouponById(coupon);
				}
				return null;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
	}

	@Override
	public List<Coupon> findCoupons(String hotelId, String couponType,String couponId) {
		try {
			Example example = new Example(Coupon.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(hotelId)) {
				createCriteria.andCondition("hotel_id=", hotelId);
			}
			if (StringUtils.isNotBlank(couponId)) {
				createCriteria.andCondition("coupon_id=", couponId);
			}
				createCriteria.andCondition("coupon_type=", couponType);
			//查询未删除酒店信息 0 未删除
			createCriteria.andCondition("is_delete=", 0);
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	
}
