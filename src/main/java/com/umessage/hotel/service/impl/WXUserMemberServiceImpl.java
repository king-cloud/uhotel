package com.umessage.hotel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.WXUserMemberMapper;
import com.umessage.hotel.domain.WXUserMember;
import com.umessage.hotel.service.WXUserMemberService;

@Service("wxUserMemberService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WXUserMemberServiceImpl extends BaseService<WXUserMember> implements WXUserMemberService {

	@Autowired
	private WXUserMemberMapper wxUserMemberMapper;

	@Override
	public List<WXUserMember> findByWXUserId(String wxUserId, String memberId) {
		return wxUserMemberMapper.findByWXUserId(wxUserId, memberId);
	}

	@Override
	public List<WXUserMember> findByMemberId(String memberId) {
		return wxUserMemberMapper.findByMemberId(memberId);
	}

	@Override
	public Integer getCountNewMemberByDate(String hotelId, String startTime, String endTime) {
		return wxUserMemberMapper.getCountNewMemberByDate(hotelId, startTime, endTime);
	}

	@Override
	public WXUserMember findByhotelIdAndWxUserId(String hotelId, String wxUserId) {
		//根据wxuserid和hotelid查询酒店 用户关联信息
		return wxUserMemberMapper.findByhotelIdAndWxUserId(hotelId,wxUserId);
	}

	@Override
	public Integer getCountUserSum(String hotelId) {
		return this.wxUserMemberMapper.getCountUserSum(hotelId);
	}

}
