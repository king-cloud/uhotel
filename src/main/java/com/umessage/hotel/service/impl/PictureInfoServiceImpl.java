package com.umessage.hotel.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.PictureInfo;
import com.umessage.hotel.service.PictureInfoService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
@Service("picService")
@EnableCaching
@CacheConfig(cacheNames = {"pictures"})
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PictureInfoServiceImpl extends BaseService<PictureInfo>  implements PictureInfoService {

	@Override
	@Transactional
	public void savePics(Object[] pictures, PictureInfo pic) {
		if(pictures!=null) {
			for (Object obj : pictures) {
				pic.setPicUrl((String)obj);
				this.save(pic);
			}
		}
	}

	@Override
	@Cacheable(key = "#pic.tableName + '_' + #pic.tableId + '_' + #pic.type")
	public List<PictureInfo> findPictures(PictureInfo pic) {
		try {
			Example example = new Example(HotelInfo.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(pic.getTableName())) {
				createCriteria.andCondition("table_name=", pic.getTableName());
			}
			if (StringUtils.isNotBlank(pic.getTableId())) {
				createCriteria.andCondition("table_id=", pic.getTableId());
			}
			if (StringUtils.isNotBlank(pic.getType())) {
				createCriteria.andCondition("type=", pic.getType());
			}
			example.setOrderByClause("pic_id");
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	
	@Override
	public List<PictureInfo> findPicturesPC(PictureInfo pic) {
		try {
			Example example = new Example(HotelInfo.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(pic.getTableName())) {
				createCriteria.andCondition("table_name=", pic.getTableName());
			}
			if (StringUtils.isNotBlank(pic.getTableId())) {
				createCriteria.andCondition("table_id=", pic.getTableId());
			}
			if (StringUtils.isNotBlank(pic.getType())) {
				createCriteria.andCondition("type=", pic.getType());
			}
			example.setOrderByClause("pic_id");
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	@Override
	@Transactional
	public void delPicture(Integer picId) {
		this.delete(picId);
	}

}
