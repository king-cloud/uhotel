package com.umessage.hotel.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberCouponMapper;
import com.umessage.hotel.domain.MemberCoupon;
import com.umessage.hotel.service.MemberCouponService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("memberCouponService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MemberCouponServiceImpl extends BaseService<MemberCoupon> implements MemberCouponService {

	@Autowired
	private MemberCouponMapper userCouponMapper;
	@Override
	public Integer getSumCount() {
		return this.userCouponMapper.getSumCount();
	}
	@Override
	public Integer getUseCount() {
		return this.userCouponMapper.getUseCount();
	}
	@Override
	public Integer getSumCountByUserId(String memberId, String hotelId, String couponType) {
		return this.userCouponMapper.getSumCountByUserId(memberId,hotelId,couponType);
	}
	@Override
	public void saveMemberCoupon(MemberCoupon mc) {
		this.save(mc);
	}
	@Override
	public List<MemberCoupon> findCoupons(String memberId, String hotelId, String couponType) {
		return this.userCouponMapper.findCoupons(memberId,hotelId,couponType);
	}
	@Override
	public List<MemberCoupon> findCouponByUser(String memberId, String hotelId, String couponId,String couponType) {
		return this.userCouponMapper.findCouponByUser(memberId,hotelId,couponId,couponType);
	}
	@Override
	public List<MemberCoupon> findCouponByContion(String memberId,String couponId) {
		try {
			Example example = new Example(MemberCoupon.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(memberId)) {
				createCriteria.andCondition("member_id=", memberId);
			}
			if (StringUtils.isNotBlank(couponId)) {
				createCriteria.andCondition("coupon_Id=", couponId);
			}
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	@Override
	public void updateStatus(String memberId, Integer hotelId, Integer couponId, String couponType,String status) {
		this.userCouponMapper.updateStatus( memberId,  hotelId,  couponId,  couponType,status);
	}

}
