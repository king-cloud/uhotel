package com.umessage.hotel.service.impl;

import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ExtendedModelMap;

import com.github.pagehelper.PageHelper;
import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.DiningRoomMapper;
import com.umessage.hotel.domain.DiningRoom;
import com.umessage.hotel.service.DiningRoomService;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("DiningRoomService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class DiningRoomServiceImpl extends BaseService<DiningRoom> implements DiningRoomService {

	@Autowired
	private DiningRoomMapper diningRoomMapper;

	@Override
	public List<DiningRoom> findDiningRoomList(String hotelId, String status, int page, int rows) {
		PageHelper.startPage(page, rows,true,false,true); //设置无效页数不查询数据
		Example example = new Example(DiningRoom.class);
		Criteria Criteria = example.createCriteria();
		if( StringUtils.isNotBlank(hotelId)){
			Criteria.andCondition("hotel_id =", hotelId);
		}
		if( StringUtils.isNotBlank(status)) {
			Criteria.andCondition("status =",status);
		}
		return diningRoomMapper.selectByExample(example);
	}

	@Override
	public void updateDiningRoom(DiningRoom diningRoom) {
		this.updateNotNull(diningRoom);
	}

	@Override
	public DiningRoom findDiningRoom(DiningRoom dnr) {
		Example example = new Example(DiningRoom.class);
		Criteria Criteria = example.createCriteria();
		String hotelId=dnr.getHotelId()+"";
		String status = dnr.getStatus();
		if( StringUtils.isNotBlank(hotelId)){
			Criteria.andCondition("hotel_id =", hotelId);
		}
		if( StringUtils.isNotBlank(status)) {
			Criteria.andCondition("status =",status);
		}
		List<DiningRoom> list = this.selectByExample(example);
		if(null != list && list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public DiningRoom findDiningRoomById(Integer diningRoomId) {
		if(null == diningRoomId || diningRoomId <=0){
			return null;
		}
		return diningRoomMapper.selectByPrimaryKey(diningRoomId);
	}

	@Override
	public List<DiningRoom> selectDiningRoom( DiningRoom diningRoom) {
		Example example = new Example(DiningRoom.class);
		Criteria createCriteria = example.createCriteria();
		if(StringUtils.isNotBlank(diningRoom.getName())){
			createCriteria.andCondition("name=", diningRoom.getName());
		}
		return diningRoomMapper.selectByExample(example);
	}
}
