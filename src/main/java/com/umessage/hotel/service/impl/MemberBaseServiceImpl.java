package com.umessage.hotel.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.domain.MemberBase;
import com.umessage.hotel.service.MemberBaseService;

@Service("memberBaseService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MemberBaseServiceImpl extends BaseService<MemberBase>  implements MemberBaseService {
	
}
