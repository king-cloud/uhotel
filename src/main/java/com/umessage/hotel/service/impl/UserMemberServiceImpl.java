package com.umessage.hotel.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.codec.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberInfoMapper;
import com.umessage.hotel.dao.UserMenuMapper;
import com.umessage.hotel.dao.WXUserMemberMapper;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.UserMenu;
import com.umessage.hotel.domain.WXUserMember;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.UserMemberService;
import com.umessage.hotel.service.WXUserMemberService;

@Service("userMemberService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UserMemberServiceImpl extends BaseService<MemberInfo> implements UserMemberService {

	@Autowired
	private MemberInfoMapper memberDao;
	@Autowired
	private UserMenuMapper userMenuMapper;
	@Autowired
	private OrderInfoService OrderInfoService;
	@Autowired
	private WXUserMemberService wXUserMemberService;
	@Autowired
	private WXUserMemberMapper wxUserMemberMapper;
	@Override
	public List<MemberInfo> findUMList(MemberInfo mem) {
		List<MemberInfo> ums=this.memberDao.findUMList(mem);
		
		if(ums!=null ) {
			return ums;
		}else {
			return null;
		}
	}
	@Override
	public Integer getCountUser(String hotelId) {
		return this.memberDao.getCountUser(hotelId);
	}
	@Override
	public Integer getCountNewMember(String hotelId) {
		return this.memberDao.countNewMember(hotelId, "grade", "", "");
	}
	@Override
	public Integer getCountNewUser(String hotelId) {
		return this.memberDao.getCountNewUser(hotelId);
	}
	@Override
	public List<UserMenu> getUserMeList(String userid) {
		return userMenuMapper.getUserMeList(userid);
	}
	@Override
	public Integer del(String userid) {
		// TODO Auto-generated method stub
		return userMenuMapper.del(userid);
	}
	@Override
	public Integer addUserM(UserMenu serMenu) {
		// TODO Auto-generated method stub
		return userMenuMapper.insert(serMenu);
	}
	@Override
	public List<MemberInfo> findUMListManual(MemberInfo mem, Integer page,
			Integer size) throws Exception {
		List<MemberInfo> ums=this.memberDao.findUMList(mem);
		
		if(ums!=null && size>0) {
			List<MemberInfo> memberInfos = new ArrayList<MemberInfo>();
			for (int i = 0; i < ums.size(); i++) {
				int start   =(page - 1)*size;
				int end     = size * page -1;
				if(i>=start && i<=end){
					memberInfos.add(ums.get(i));
				}
				if(ums.get(i).getWxUser()!=null) {
					String nickname = ums.get(i).getWxUser().getName();
					 //进行解码
				    nickname = new String(Base64.decode(nickname),"UTF-8");
				    ums.get(i).getWxUser().setName(nickname);
				    ums.get(i).getWxUser().setNickName(nickname);
				}
			}
			return memberInfos;
		}else if(ums!=null && size==0){
			return ums;
		}else {
			return null;
		}
	}
	@Override
	public Integer getCountUserSum(String hotelId) {
		//return this.memberDao.getCountUserSum(hotelId);
		return this.wXUserMemberService.getCountUserSum(hotelId);
	}
	@Override
	public Integer getCountMemberSum(String hotelId) {
		return this.memberDao.getCountMemberSum(hotelId);
	}
	@Override
	public Integer getCountOrderSum(String hotelId) {
		return this.OrderInfoService.countEffectiveOrders(hotelId, null, null);
	}
	/**
	 * 用户列表
	 * @param member
	 * @return
	 * @throws Exception 
	 */
	@Override
	public List<WXUserMember> findUMemberList(WXUserMember member) throws Exception {
		List<WXUserMember> ums=this.wxUserMemberMapper.findUMemberList(member);
		if(ums!=null) {
		for (WXUserMember um : ums) {
			if(um!=null) {
				if(um.getWxUser()!=null) {
					String nickname = um.getWxUser().getName();
					 //进行解码
				    nickname = new String(Base64.decode(nickname),"UTF-8");
				    um.getWxUser().setName(nickname);
				    um.getWxUser().setNickName(nickname);
				}
				if(um.getMemberId()!=null) {
					MemberInfo memberInfo=this.memberDao.selectByPrimaryKey(um.getMemberId());
					um.setMember(memberInfo);
				}
			}
		}
		return ums;
		}
		return new ArrayList<>();
	}

}
