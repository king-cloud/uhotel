package com.umessage.hotel.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.WXUserMapper;
import com.umessage.hotel.domain.WXUser;
import com.umessage.hotel.service.WXUserService;

@Service("wxUserService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WXUserServiceImpl extends BaseService<WXUser> implements WXUserService {
	
	@Autowired
	private WXUserMapper wxUserMapper;

	@Override
	public WXUser findUserByOpenid(String openId) {
		return wxUserMapper.findUserByOpenid(openId);
	}

}
