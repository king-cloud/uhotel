package com.umessage.hotel.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.HotelInfoMapper;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.MemberDiscount;
import com.umessage.hotel.service.HotelCrontabService;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.MemberDiscountService;
import com.umessage.system.domain.User;
import com.umessage.system.service.UserService;

@Service("hotelInfoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class HotelInfoServiceImpl extends BaseService<HotelInfo> implements HotelInfoService {
	@Autowired
	private HotelInfoMapper hotelDao;
	@Autowired
	private UserService userService;
	@Autowired
	private HotelCrontabService hotelCrontabService;
	@Autowired 
	private MemberDiscountService memberService;
	@Override
	public List<HotelInfo> findAllHotels(HotelInfo hotel) {
		try {
			Example example = new Example(HotelInfo.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(hotel.getHotelName())) {
				createCriteria.andCondition("hotel_name=", hotel.getHotelName());
			}
			if (StringUtils.isNotBlank(hotel.getHotelSn())) {
				createCriteria.andCondition("hotel_sn=", hotel.getHotelSn());
			}
			if (StringUtils.isNotBlank(hotel.getMobile())) {
				createCriteria.andCondition("mobile=", hotel.getMobile());
			}
			if (StringUtils.isNotBlank(hotel.getHotelId())) {
				createCriteria.andCondition("hotel_id=", hotel.getHotelId());
			}
			//查询未删除酒店信息 0 未删除
			createCriteria.andCondition("is_delete=", 0);
			example.setOrderByClause("hotel_sn");
			return this.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}
	/**
	 * 判断酒店是否已绑定该手机号
	 * @param mobile
	 * @return
	 */
	private boolean findHotelsByMobile(String mobile){
		Example example = new Example(HotelInfo.class);
		Criteria createCriteria = example.createCriteria();
		if(StringUtils.isNotEmpty(mobile)) {
			createCriteria.andCondition("mobile=", mobile);
		}
		List<HotelInfo> list = this.selectByExample(example);
		return list.isEmpty() ? true : false;
	}
	/**
	 * 判断其他酒店是否已绑定该手机号
	 * @param mobile
	 * @return
	 */
	private boolean findHotelsByMobileAndHotelId(String hotelId,String mobile){
		Example example = new Example(HotelInfo.class);
		Criteria createCriteria = example.createCriteria();
		if(StringUtils.isNotEmpty(mobile)) {
			createCriteria.andCondition("mobile=", mobile);
		}
		if(StringUtils.isNotEmpty(mobile)) {
			createCriteria.andCondition("hotel_id!=", hotelId);
		}
		List<HotelInfo> list = this.selectByExample(example);
		return list.isEmpty() ? true : false;
	}
	@Override
	@Transactional
	public Integer addHotelInfo(HotelInfo hotel,User user) throws Exception {
		boolean isNull= this.findHotelsByMobile(hotel.getMobile());
		if(isNull) {
			//hotel.setHotelId(UUID.randomUUID().toString());
			hotel.setAddTime(new Date());
			hotel.setAddAcc(user.getUserId()+"");
			hotel.setModifyTime(new Date());
			hotel.setModifyAcc(user.getUserId()+"");
			hotel.setOpenPms(hotel.getOpenPms());
			this.hotelDao.saveHotel(hotel);	
			
			// 为酒店增加默认定时任务配置
			hotelCrontabService.insertCrontabsByHotelId(Integer.parseInt(hotel.getHotelId()), hotel.getHotelName()) ;
			
			//新建酒店管理员用户
			//User users=this.userService.findUserByMobileAndHotel(hotel.getMobile(),hotel.getHotelId());
			//if(users==null) {
			User hotelUser = new User();
			hotelUser.setUsername(hotel.getMobile());
			hotelUser.setPassword(hotel.getMobile());
			hotelUser.setMobile(hotel.getMobile());
			hotelUser.setHotelId(hotel.getHotelId());
			/**
			 * 用于区分酒店员工和酒店超级管理员
			 * 1  酒店超级管理员
			 * 0  酒店员工
			 */
			hotelUser.setDeptId(1l);
			hotelUser.setPosition("0");
			userService.addHotelAdminUser(hotelUser);
			
			/**
			 * 添加默认折扣
			 */
			MemberDiscount mdt=new MemberDiscount();
			mdt.setHotelId(hotel.getHotelId());
			mdt.setMemberType(1);
			mdt.setMemberDiscountType(1);
			mdt.setMemberDiscount("98");
			mdt.setMemberName("白银会员");
			mdt.setMemberSwitch(1);
			this.memberService.save(mdt);
			return 1;
		}else {
			return 0;
		}
	}

	@Override
	public Integer updateHotel(HotelInfo hotelInfo) {
			boolean isNull= this.findHotelsByMobileAndHotelId(hotelInfo.getHotelId(),hotelInfo.getMobile());
			if(isNull) {
				this.updateNotNull(hotelInfo);	
				return 1;
			}else {
				return 0;
			}
	}

	@Override
	public HotelInfo findHotelsById(String hotelId) {
		return this.selectByKey(hotelId);
	}

	@Override
	public void updateHotels(String ids) {
		List<String> sl=new ArrayList<>();
			String[] strs = ids.split(",");
			for (String str : strs) {
				sl.add(str);
			}
		this.hotelDao.updateHotels(sl);
	}

	@Override
	public List<HotelInfo> queryAllHotels() {
		return hotelDao.queryAllHotels();
	}

	@Override
	public HotelInfo queryHotelsByHotelId(String hotelId) {
		return hotelDao.queryHotelsByHotelId(hotelId);
	}


}
