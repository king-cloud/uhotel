package com.umessage.hotel.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.hotel.dao.InvoiceApplicationMapper;
import com.umessage.hotel.domain.InvoiceApplication;
import com.umessage.hotel.model.InvoiceApplicationModel;
import com.umessage.hotel.service.InvoiceApplicationService;

@Service("invoiceApplicationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class InvoiceApplicationServiceImpl implements InvoiceApplicationService{
	
	@Autowired
	private InvoiceApplicationMapper invoiceApplicationMapper;

	@Override
	public Integer addInvoiceApplicationInfo(
			InvoiceApplication invoiceApplication) {
		return invoiceApplicationMapper.insertSelective(invoiceApplication);
	}

	@Override
	public InvoiceApplication getInvoiceApplicationStatus(String orderId) {
		return invoiceApplicationMapper.getInvoiceApplicationStatus(orderId);
	}

	@Override
	public int updateByTitleIdSelective(InvoiceApplication record) {
		return invoiceApplicationMapper.updateByTitleIdSelective(record);
	}

	@Override
	public Integer addInvoiceApplicationInfoModel(
			InvoiceApplicationModel invoiceApplicationModel) {
		InvoiceApplication invoiceApplication = new InvoiceApplication(); 
		String ignoreProperties = "residentLimit";
		BeanUtils.copyProperties(invoiceApplicationModel, invoiceApplication , ignoreProperties);
		return invoiceApplicationMapper.insertSelective(invoiceApplication);
	}

	@Override
	public int updateByModelTitleIdSelective(InvoiceApplicationModel invoiceApplicationModel) {
		InvoiceApplication invoiceApplication = new InvoiceApplication(); 
		String ignoreProperties = "residentLimit";
		BeanUtils.copyProperties(invoiceApplicationModel, invoiceApplication , ignoreProperties);
		return invoiceApplicationMapper.updateByTitleIdSelective(invoiceApplication);
	}
}
