package com.umessage.hotel.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.RoomStatusMapper;
import com.umessage.hotel.domain.RoomStatus;
import com.umessage.hotel.service.RoomStatusService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("roomStatusService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RoomStatusServiceImpl extends BaseService<RoomStatus> implements RoomStatusService {
	
	@Autowired
	public RoomStatusMapper roomStatusDao;

	@Override
	public List<RoomStatus> findRoomStatus(Map<String,Object> map) {
		
		return this.roomStatusDao.findRoomStatusByTime(map);
	}

	@Override
	public List<String> findRoomIdsNotBook(Map<String, Object> map) {
		return this.roomStatusDao.findRoomIdsNotBook(map);
	}

	@Override
	public List<RoomStatus> findRoomStatusAdmin(Map<String, Object> map) {
		return this.roomStatusDao.findRoomStatusByTimeAdmin(map);
	}
	
}
