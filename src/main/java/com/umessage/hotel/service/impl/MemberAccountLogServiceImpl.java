package com.umessage.hotel.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberAccountLogMapper;
import com.umessage.hotel.domain.MemberAccountLog;
import com.umessage.hotel.service.MemberAccountLogService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("memberAccountLogService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MemberAccountLogServiceImpl extends BaseService<MemberAccountLog> implements MemberAccountLogService {

	@Autowired
	private MemberAccountLogMapper memberAccountLogMapper;
	
	@Override
	public void updateAccountLog(Integer id, String payStatus) {
		MemberAccountLog memberAccountLog = new MemberAccountLog();
		memberAccountLog.setId(id);
		memberAccountLog.setPayStatus(payStatus);
		this.updateAll(memberAccountLog);
	}

	@Override
	public MemberAccountLog findmemberAccountLogByOrder(String orderId, String type, String payStatus) {
		Example example = new Example(MemberAccountLog.class);
		 Criteria createCriteria = example.createCriteria();
		 if(StringUtils.isNotBlank(orderId)){
			 createCriteria.andCondition("order_id=", orderId);
		 }
		 if(StringUtils.isNotBlank(type)){
			 createCriteria.andCondition("type=", type);
		 }
		 if(StringUtils.isNotBlank(payStatus)){
			 createCriteria.andCondition("pay_status=", payStatus);
		 }
		 List<MemberAccountLog> list = this.mapper.selectByExample(example);
		 if(list != null && list.size()>0){
			 return list.get(0);
		 }
		 return null;
	}

	@Override
	public BigDecimal countRealAndVirtMonry(String hotelId, String startTime, String endTime) {
		return memberAccountLogMapper.countRealAndVirtMonry(hotelId,  startTime,  endTime);
	}
}
