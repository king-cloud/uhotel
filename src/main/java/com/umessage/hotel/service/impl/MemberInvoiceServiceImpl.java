package com.umessage.hotel.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.RuntimeCryptoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberInvoiceMapper;
import com.umessage.hotel.domain.MemberInvoice;
import com.umessage.hotel.service.MemberInvoiceService;
import com.umessage.hotel.service.WxCfgService;
import com.umessage.hotel.service.WxMsgTmplService;
import com.umessage.hotel.util.TimeUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("memberInvoiceService")
public class MemberInvoiceServiceImpl extends BaseService<MemberInvoice> implements MemberInvoiceService {

	@Autowired
	private MemberInvoiceMapper invoiceMapper;
	@Autowired
	private WxMsgTmplService wxMsgTmplService;
	
	private Logger logger = LoggerFactory.getLogger(MemberInvoiceServiceImpl.class);
	
	@Override
	public void addInvoice(MemberInvoice memberInvoice) {
		try {
			memberInvoice.setCreateTime(new Date());
			invoiceMapper.insertSelective(memberInvoice);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<MemberInvoice> findInvoiceListByUser(String memberId, Integer hotelId, String status) {
		try {
			Example example = new Example(MemberInvoice.class);
			Criteria createCriteria = example.createCriteria();
			if( StringUtils.isNoneBlank(memberId)){
				createCriteria.andCondition("member_id=", memberId);
			}
			if(null != hotelId){
				createCriteria.andCondition("hotel_id=", hotelId);
			}
			if(StringUtils.isNoneBlank(status)){
				createCriteria.andCondition("status=",status);
			}
			return invoiceMapper.selectByExample(example);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<MemberInvoice> findInvoiceList(Integer hotelId, String status) {
		return this.findInvoiceListByUser(null, hotelId, status);
	}

	@Override
	public void updateInvoiceStatus(Integer id, Integer status) {
		MemberInvoice invoice = this.findSingleInvoice(id);
		invoice.setStatus(status);
		invoiceMapper.updateByPrimaryKeySelective(invoice);
	}

	@Override
	public List<MemberInvoice> findInvoiceList(MemberInvoice invoice, int pageNum, int pageSize) {
		try {
			PageHelper.startPage(pageNum,pageSize,true,false,true); //设置无效页数不查询数据
			Example example = new Example(MemberInvoice.class);
			Criteria createCriteria = example.createCriteria();
			if(null != invoice.getHotelId()){
				createCriteria.andCondition("hotel_id=", invoice.getHotelId());
			}
			if(null !=invoice.getStatus()){
				createCriteria.andCondition("status=",invoice.getStatus());
			}
			if(StringUtils.isNotBlank(invoice.getName())){
				createCriteria.andCondition("name like ","%"+invoice.getName()+"%");
			}
			if( null !=invoice.getCreateTime() ) {
				TimeUtil.date2String(invoice.getCreateTime(),TimeUtil.shortSdf);
				createCriteria.andCondition("create_time LIKE", TimeUtil.date2String(invoice.getCreateTime(),TimeUtil.shortSdf)+"%");
			}
			if( null != invoice.getRoomNo()){
				createCriteria.andCondition("room_no=", invoice.getRoomNo());
			}
			if(null !=invoice.getType() && invoice.getType()!=0){
				createCriteria.andCondition("type=", invoice.getType());
			}
			example.setOrderByClause("id DESC");
			return invoiceMapper.selectByExample(example);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public MemberInvoice findSingleInvoice(Integer id) {
		return invoiceMapper.selectByPrimaryKey(id);
	}

	@Transactional
	@Override
	public void openInvoice(Integer id, Integer status) {
		try {
			MemberInvoice invoice = this.findSingleInvoice(id);
			//发送微信模版消息
			wxMsgTmplService.sendOpenInvoice(invoice);
			invoice.setStatus(status);
			invoiceMapper.updateByPrimaryKeySelective(invoice);
		} catch (Exception e) {
			logger.info("开局发票失败,invoic id is"+id,e.getMessage());
			throw new RuntimeException("编号"+id+"开票失败"+e.getMessage());
		}
	}
}
