
package com.umessage.hotel.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.i18n.AcceptHeaderLocaleContextResolver;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberInfoMapper;
import com.umessage.hotel.domain.MemberAccountLog;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.service.MemberBaseService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.service.WXUserService;
import com.umessage.hotel.util.HotelConstant;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("memberInfoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MemberInfoServiceImpl extends BaseService<MemberInfo> implements MemberInfoService {

	@Autowired
	private MemberInfoMapper memberMapper;
	@Autowired
	private MemberBaseService memberBaseService;
	@Autowired
	private WXUserService wxUserService;

	@Override
	public String saveMember(MemberInfo memberInfo) {
		String memberId = UUID.randomUUID().toString();
		memberInfo.setMemberId(memberId);
		memberInfo.setRealMoney(BigDecimal.ZERO);
		memberInfo.setVirtMoney(BigDecimal.ZERO);
		memberInfo.setLastLoginTime(new Date());
		memberInfo.setAddTime(new Date());
		this.memberMapper.saveMember(memberInfo);
		return memberId;
	}
	@Override
	public String saveMemberNew(MemberInfo memberInfo) {
		this.memberMapper.saveMember(memberInfo);
		return memberInfo.getMemberId();
	}

	@Override
	public MemberInfo findMemberByMobile(String hotelId, String mobile) {

		MemberInfo memberInfo = memberMapper.findMemberByMobile(hotelId, mobile);

		return memberInfo;
	}

	@Override
	public Integer countNewMember(String hotelId, String grade, String startTime, String endTime) {
		return memberMapper.countNewMember(hotelId, grade, startTime, endTime);
	}
	
	

	@Override
	public Integer countNewMemberByDate(String hotelId, String grade, String startTime, String endTime) {
		return memberMapper.countNewMemberByDate(hotelId, grade, startTime, endTime);
	}

	@Override
	public MemberInfo selectByPhone(String phone) {
		try {
			Example example = new Example(MemberInfo.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(phone)) {
				createCriteria.andLike("mobile", phone);
			}
			return this.selectByExample(example)==null?new MemberInfo():this.selectByExample(example).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Integer getCountUserByDate(String hotelId, String startTime, String endTime) {
		return memberMapper.getCountUserByDate(hotelId, startTime, endTime);
	}

	@Override
	public Integer getCountNewMemberByDate(String hotelId, String grade, String startTime, String endTime) {
		return memberMapper.getCountNewMemberByDate(hotelId, grade, startTime, endTime);
	}

	@Override
	public MemberInfo getMemberInfoForUpdate(String memberId) {
		return memberMapper.getMemberInfoForUpdate(memberId);
	}

	@Override
	public MemberInfo findMemberById(String memberId) {
		// TODO Auto-generated method stub
		return this.memberMapper.selectByPrimaryKey(memberId);
	}
	
	/**
	 * 查询用户消费记录 
	 */
	@Override
	public List<MemberAccountLog> getConsumingRecordByUser(String hotelId, String memberId,String orderId,String payStatus) {
		return memberMapper.getConsumingRecordByUser(hotelId,memberId,orderId,payStatus);
	}

	/**
	 * 记录用户消费记录
	 */
	@Override
	public void insertConsumingRecord(MemberAccountLog memberAccountLog) {
		 memberMapper.insertConsumingRecord(memberAccountLog);
	}

}

