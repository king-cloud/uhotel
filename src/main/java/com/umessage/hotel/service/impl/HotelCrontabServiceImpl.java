package com.umessage.hotel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.HotelCrontabMapper;
import com.umessage.hotel.domain.HotelCrontab;
import com.umessage.hotel.domain.HotelCrontabParam;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.service.HotelCrontabService;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.system.service.UserService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("hotelCrontabService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class HotelCrontabServiceImpl extends BaseService<HotelCrontab> implements HotelCrontabService {
	@Autowired
	private HotelCrontabMapper hotelCrontabDao;
	@Autowired
	private UserService userService;
	@Autowired
	private HotelInfoService hotelInfoService;
	
	
	@Override
	public List<HotelCrontab> findHotelCrontabs(Integer status) {
		Example example = new Example(HotelCrontab.class);
		Criteria createCriteria = example.createCriteria();
		createCriteria.andCondition("status =", status);
		createCriteria.andCondition("hotel_id !=", 100000000);
		return this.selectByExample(example);
	}

	@Override
	public void updateHotelCrontabRunning(Integer id, Integer running) {
		hotelCrontabDao.updateHotelCrontabRunning(id, running) ;
	}

	@Override
	public List<HotelCrontab> findCrontabsByHotelId(Integer hotelId) {
		Example example = new Example(HotelCrontab.class);
		Criteria createCriteria = example.createCriteria();
		createCriteria.andCondition("hotel_id=", hotelId);
		return this.selectByExample(example);
	}

	@Override
	public HotelCrontab getHotelCrontabById(Integer id) {
		return null;
	}

	@Override
	public void updateHotelCrontabs(HotelCrontabParam param) {
		Integer hotelId = param.getHotelId() ;
		HotelCrontab hc = new HotelCrontab() ;
		hc.setHotelId(hotelId);
		
		hc.setRunTimes(param.getValue1());
		hc.setStatus(param.getStatus1());
		hc.setTaskId(HotelConstant.HOTEL_CRONTAB_TASK_1);
		hotelCrontabDao.updateHotelContabTimes(hc) ;
		
		hc.setRunTimes(param.getValue2());
		hc.setStatus(param.getStatus2());
		hc.setTaskId(HotelConstant.HOTEL_CRONTAB_TASK_2);
		hotelCrontabDao.updateHotelContabTimes(hc) ;
		
		hc.setRunTimes(param.getValue3());
		hc.setStatus(param.getStatus3());
		hc.setTaskId(HotelConstant.HOTEL_CRONTAB_TASK_3);
		hotelCrontabDao.updateHotelContabTimes(hc) ;
		
		hc.setRunTimes(param.getValue4());
		hc.setStatus(param.getStatus4());
		hc.setTaskId(HotelConstant.HOTEL_CRONTAB_TASK_4);
		hotelCrontabDao.updateHotelContabTimes(hc) ;
		
		hc.setRunTimes(param.getValue5());
		hc.setStatus(param.getStatus5());
		hc.setTaskId(HotelConstant.HOTEL_CRONTAB_TASK_5);
		hotelCrontabDao.updateHotelContabTimes(hc) ;
	}

	@Override
	public void insertCrontabsByHotelId(Integer hotelId, String hotelName) {
		HotelInfo hotelInfo = hotelInfoService.findHotelsById(String.valueOf(hotelId)) ;
		List<HotelCrontab> list = hotelCrontabDao.findHotelCrontabsByHotelId(100000000) ;
		for(int i = 0 ; i < list.size() ; i ++) {
			HotelCrontab hc = list.get(i) ;
			hc.setId(null);
			hc.setHotelId(hotelId);
			hc.setHotelName(hotelInfo.getHotelName());
			hotelCrontabDao.insert(hc) ;
		}
	}
	
}
