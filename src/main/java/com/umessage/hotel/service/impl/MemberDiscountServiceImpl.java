package com.umessage.hotel.service.impl;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.umessage.common.service.impl.BaseService;
import com.umessage.hotel.dao.MemberDiscountMapper;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.MemberDiscount;
import com.umessage.hotel.domain.RoomInfo;
import com.umessage.hotel.service.MemberDiscountService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service("memberService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MemberDiscountServiceImpl extends BaseService<MemberDiscount> implements MemberDiscountService {

	@Autowired
	private MemberDiscountMapper memberMapper;

	@Override
	public int add(MemberDiscount member) {
		List<MemberDiscount> list = memberMapper.selectByHotelId(member.getHotelId());
		if(list.size()>0){
			member.setMemberType(list.get(0).getMemberType()+1);
		}else {
			member.setMemberType(1);
		}
		member.setMemberSwitch(1);
		return memberMapper.insert(member);
	}

	@Override
	public List<MemberDiscount> getMemberList(MemberDiscount member) {
		// TODO Auto-generated method stub
		return memberMapper.selectMemberByberSwitch(member);
	}
	@Override
	public List<MemberDiscount> getMemberList2(MemberDiscount mds) {
		// TODO Auto-generated method stub
		try {
			Example example = new Example(MemberDiscount.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(mds.getHotelId())) {
				createCriteria.andCondition("hotel_id=", mds.getHotelId());
			}
			List<MemberDiscount> member = this.selectByExample(example);
			return member.isEmpty()?null:member;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public MemberDiscount getMemberDiscount(String gradeId,String hotelId) {
		try {
			Example example = new Example(MemberDiscount.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(hotelId)) {
				createCriteria.andCondition("hotel_id=", hotelId);
			}
			if (StringUtils.isNotBlank(gradeId)) {
				createCriteria.andCondition("member_type=", gradeId);
			}
			createCriteria.andCondition("member_switch=", 1);
			List<MemberDiscount> mds = this.selectByExample(example);
			return mds.isEmpty()?null:mds.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public MemberDiscount getMemberDiscountList(String gradeId,String hotelId) {
		try {
			Example example = new Example(MemberDiscount.class);
			Criteria createCriteria = example.createCriteria();
			if (StringUtils.isNotBlank(hotelId)) {
				createCriteria.andCondition("hotel_id=", hotelId);
			}
			if (StringUtils.isNotBlank(gradeId)) {
				createCriteria.andCondition("member_type=", gradeId);
			}
			List<MemberDiscount> mds = this.selectByExample(example);
			return mds.isEmpty()?null:mds.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
