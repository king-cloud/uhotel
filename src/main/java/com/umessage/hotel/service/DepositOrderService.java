package com.umessage.hotel.service;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.DepositOrder;
import com.umessage.hotel.model.PaySignModel;

public interface DepositOrderService extends IService<DepositOrder> {

	/**
	 * 保存充值订单
	 * 
	 * @param hotelId  酒店id
	 * @param memberId 会员id
	 * @param couponId 礼金券id
	 * @param money    充值金额
	 * @return
	 */
	public DepositOrder saveDepositOrder(String hotelId, String memberId, Integer couponId, BigDecimal money);

	/**
	 * 创建统一订单
	 * 
	 * @param wxcfgid   微信配置
	 * @param openid    openid
	 * @param orderType 订单类型（订房、充值）
	 * @param orderId   订单id
	 * @param productId 产品id
	 * @param createdIp 创建ip
	 * @param notifyUrl 通知地址
	 * @param fee       支付金额
	 * @return
	 */
	public PaySignModel createUnifiedOrder(String wxcfgid, String openid, String orderType, String orderId,
			Integer productId, String createdIp, String notifyUrl, BigDecimal fee);

	/**
	 * 会员充值
	 * 
	 * @param depositOrder
	 */
	public void depositMemberAccount(String depositId);
	
	public BigDecimal countDepositOrderRechargeAmount(String hotelId, String startTime, String endTime);

	public BigDecimal countDepositOrderVirtMoney(String hotelId, String startTime, String endTime);
	
	/**
	 * 查询充值总金额
	 * @param hotelId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public BigDecimal taotalPay(String hotelId, String startTime, String endTime);

}
