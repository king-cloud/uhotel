package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.PictureInfo;

public interface PictureInfoService extends IService<PictureInfo>{

	public void savePics(Object[] pictures, PictureInfo pic);

	public List<PictureInfo> findPictures(PictureInfo pic);

	public void delPicture(Integer pid);

	public List<PictureInfo> findPicturesPC(PictureInfo pic);
}
