package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.HotelCrontab;
import com.umessage.hotel.domain.HotelCrontabParam;

public interface HotelCrontabService extends IService<HotelCrontab> {

	/**
	 * 根据status取得所有酒店定时任务
	 * @param status
	 * @return
	 */
	List<HotelCrontab> findHotelCrontabs(Integer status);
	
	/**
	 * 根据status取得所有酒店定时任务
	 * @param status
	 * @return
	 */
	List<HotelCrontab> findCrontabsByHotelId(Integer hotelId);

	/**
	 * 根据
	 * @param hotelId
	 * @return
	 */
	HotelCrontab getHotelCrontabById(Integer id);

	/**
	 * 设置酒店运行状态
	 * @param hotelCrontabRunning
	 */
	void updateHotelCrontabRunning(Integer id, Integer hotelCrontabRunning);

	/**
	 * 设置酒店中定时器列表
	 * @param param
	 */
	void updateHotelCrontabs(HotelCrontabParam param);

	/**
	 * 默认第一次给当前的酒店，插入5条默认的数据
	 * @param hotelId
	 */
	void insertCrontabsByHotelId(Integer hotelId, String hotelName);
}