package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.WXUserMember;

public interface WXUserMemberService extends IService<WXUserMember> {

	/**
	 * 根据微信用户id查找
	 * 
	 * @param wxUserId
	 * @return
	 */
	public List<WXUserMember> findByWXUserId(String wxUserId, String memberId);

	/**
	 * 根据会员id查找
	 * 
	 * @param memberId
	 * @return
	 */
	public List<WXUserMember> findByMemberId(String memberId);
	
	/**
	 *  统计新注册会员
	 * @param hotelId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Integer getCountNewMemberByDate(String hotelId, String startTime, String endTime);

	public WXUserMember findByhotelIdAndWxUserId(String hotelId, String wxUserId);

	public Integer getCountUserSum(String hotelId);

}
