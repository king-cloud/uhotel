package com.umessage.hotel.service;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.WxCfg;

public interface WxCfgService extends IService<WxCfg> {

	WxCfg queryWxCfg(String hotelId, String appType);

	WxCfg getWxCfg(WxCfg wxcf);

	Integer addwxcf(WxCfg wxcf);

}
