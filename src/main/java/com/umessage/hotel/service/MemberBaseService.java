package com.umessage.hotel.service;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberBase;

public interface MemberBaseService extends IService<MemberBase> {

}
