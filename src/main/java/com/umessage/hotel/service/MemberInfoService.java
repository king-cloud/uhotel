
package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberAccountLog;
import com.umessage.hotel.domain.MemberInfo;

public interface MemberInfoService extends IService<MemberInfo> {

	public String saveMember(MemberInfo member);
	
	public String saveMemberNew(MemberInfo memberInfo);

	public MemberInfo findMemberByMobile(String hotelId, String mobile);

	public Integer countNewMember(String hotelId, String grade, String startTime, String endTime);
	
	public Integer countNewMemberByDate(String hotelId, String grade, String startTime, String endTime);

	public MemberInfo selectByPhone(String phone);
	
    public Integer getCountUserByDate(String hotelId, String startTime, String endTime);
	
	public Integer getCountNewMemberByDate(String hotelId, String grade, String startTime, String endTime);
	
	public MemberInfo getMemberInfoForUpdate(String memberId);

	public MemberInfo findMemberById(String memberId);
	
	public List<MemberAccountLog> getConsumingRecordByUser(String hotelId, String memberId,String orderId,String payStatus);

	public void insertConsumingRecord(MemberAccountLog memberAccountLog);

}

