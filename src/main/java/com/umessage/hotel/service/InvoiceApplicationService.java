package com.umessage.hotel.service;

import com.umessage.hotel.domain.InvoiceApplication;
import com.umessage.hotel.model.InvoiceApplicationModel;

public interface InvoiceApplicationService {
	
	public Integer addInvoiceApplicationInfo(InvoiceApplication invoiceApplication);
	
	public Integer addInvoiceApplicationInfoModel(InvoiceApplicationModel invoiceApplicationModel);
	
	public InvoiceApplication getInvoiceApplicationStatus(String orderId);
	
	public int updateByTitleIdSelective(InvoiceApplication record);
	
	public int updateByModelTitleIdSelective(InvoiceApplicationModel record);
}
