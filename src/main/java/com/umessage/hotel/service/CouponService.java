package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.Coupon;

public interface CouponService  extends IService<Coupon> {

	public List<Coupon> getCoupons(Coupon coupon);

	public void updateByCouponId(Coupon coupon);

	public List<Coupon> findCouponsByWXUser(String userId, String hotelId,String couponType);

	public Integer getCountCoupons(String hotelId, String newtTime);

	public Coupon getCouponById(String couponId);

	public List<Coupon> findCoupons(String hotelId, String couponType,String couponId);

}
