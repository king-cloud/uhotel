package com.umessage.hotel.service;

import java.util.List;

import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.OrderPms;

public interface OrderPmsService  {

	boolean establishPmsOrder(OrderInfo orderInfo);

	OrderPms getorderPmsByorderpmsAndHotelId(String hotelId, String orderId);

	void update(OrderPms orderPms);

	List<OrderPms> getrderListOrderPms(String hotelId, String order);
	
	void cancelbookPmsOrder(OrderInfo orderInfo);
}
