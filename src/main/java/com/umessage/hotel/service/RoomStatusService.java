package com.umessage.hotel.service;


import java.util.List;
import java.util.Map;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.RoomStatus;

public interface RoomStatusService extends IService<RoomStatus> {

	public List<RoomStatus> findRoomStatus(Map<String,Object> map);
	
	public List<RoomStatus> findRoomStatusAdmin(Map<String,Object> map);

	public List<String> findRoomIdsNotBook(Map<String, Object> map);
	

}
