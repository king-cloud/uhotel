package com.umessage.hotel.service;

import java.math.BigDecimal;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberAccountLog;

public interface MemberAccountLogService extends IService<MemberAccountLog> {
	
	public void updateAccountLog(Integer orderId,String payStatus);

	/**
	 * 查询订单消费记录
	 * @param orderId
	 */
	public MemberAccountLog findmemberAccountLogByOrder(String orderId,String type,String payStatus);

	/**
	 * 指定时间内酒店用户消费的账户金额
	 * @param hotelId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public BigDecimal countRealAndVirtMonry(String hotelId, String startTime, String endTime);
}
