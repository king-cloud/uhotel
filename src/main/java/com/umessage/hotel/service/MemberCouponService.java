package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberCoupon;

public interface MemberCouponService  extends IService<MemberCoupon> {

	public Integer getSumCount();

	public Integer getUseCount();

	public Integer getSumCountByUserId(String userId, String hotelId, String couponType);

	public void saveMemberCoupon(MemberCoupon mc);

	public List<MemberCoupon> findCoupons(String memberId, String hotelId, String couponType);
	
	//查询用户的优惠券
	public List<MemberCoupon> findCouponByUser(String memberId, String hotelId, String couponId,String couponType);

	public List<MemberCoupon> findCouponByContion(String memberId,String couponId);

	//修改用户优惠券状态
	public void updateStatus(String memberId, Integer hotelId, Integer couponId, String couponType,String status);

}
