package com.umessage.hotel.service;

import java.util.List;

import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.UserMenu;
import com.umessage.hotel.domain.WXUserMember;

public interface UserMemberService {

	public List<MemberInfo> findUMList(MemberInfo mem);
	
	public List<MemberInfo> findUMListManual(MemberInfo mem,Integer page, Integer size) throws Exception;

	public Integer getCountUser(String hotelId);

	public Integer getCountNewMember(String hotelId);

	public Integer getCountNewUser(String hotelId);

	public List<UserMenu> getUserMeList(String userid);

	public Integer del(String userid);

	public Integer addUserM(UserMenu serMenu);

	public Integer getCountUserSum(String hotelId);

	public Integer getCountMemberSum(String hotelId);

	public Integer getCountOrderSum(String hotelId);

	public List<WXUserMember> findUMemberList(WXUserMember member) throws Exception;



}
