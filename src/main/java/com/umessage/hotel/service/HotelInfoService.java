package com.umessage.hotel.service;

import java.util.List;
import java.util.Map;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.system.domain.User;

public interface HotelInfoService extends IService<HotelInfo> {
	
	public List<HotelInfo> findAllHotels(HotelInfo hotel);

	public Integer addHotelInfo(HotelInfo hotelInfo,User user) throws Exception;

	public Integer updateHotel(HotelInfo hotelInfo);

	public HotelInfo findHotelsById(String hotelId);

	public void updateHotels(String ids);
	
	public List<HotelInfo> queryAllHotels();
	
	public HotelInfo queryHotelsByHotelId(String hotelId);
}
