package com.umessage.hotel.service;

import java.util.List;
import com.umessage.common.service.IService;
import com.umessage.hotel.domain.DiningRoom;

public interface DiningRoomService extends IService<DiningRoom> {

	/**
	 * 查询餐厅列表
	 * @param hotelId
	 * @param status
	 * @param page
	 * @param rows
	 * @return
	 */
	public List<DiningRoom> findDiningRoomList(String hotelId, String status, int page, int rows);
	
	/**
	 * 更新餐厅
	 * @param id
	 */
	public void updateDiningRoom(DiningRoom diningRoom);

	/**
	 * 根据条件查询
	 * @param dnr
	 * @return
	 */
	public DiningRoom findDiningRoom(DiningRoom dnr);

	/**
	 * 根据id查询
	 * @param diningRoomId
	 * @return
	 */
	public DiningRoom findDiningRoomById(Integer diningRoomId);

	public List<DiningRoom> selectDiningRoom(DiningRoom diningRoom);
	
}
