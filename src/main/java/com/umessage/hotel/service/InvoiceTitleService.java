package com.umessage.hotel.service;

import java.util.List;

import com.umessage.hotel.domain.InvoiceTitle;
import com.umessage.hotel.model.InvoiceTitleModel;

public interface InvoiceTitleService {
	
	public Integer addInvoiceTitle(InvoiceTitle invoiceTitle);
	
	public Integer addInvoiceTitleModel(InvoiceTitleModel invoiceTitle);
	
	public List<InvoiceTitle> getInvoiceTitleInfo(String memberId);
	
	public Integer updateByPrimaryKeySelective(InvoiceTitle record);
	
	public Integer updateByPrimaryKeyModelSelective(InvoiceTitleModel record);
	
	public Integer  deleteByPrimaryKey(Integer id);
	
	public InvoiceTitle selectByPrimaryKey(Integer id);

}
