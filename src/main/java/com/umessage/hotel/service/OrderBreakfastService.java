package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.BreakfastCoupon;
import com.umessage.hotel.domain.OrderBreakfast;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.model.PaySignModel;

public interface OrderBreakfastService extends IService<OrderBreakfast>  {

	OrderBreakfast addOrderBreakfast(OrderBreakfast obf);

	PaySignModel payOrderBKF(OrderBreakfast obf,String createdIp) throws Exception;

	void updateOrderBKF(OrderBreakfast orderBKF);

	String addOrderBreakfastPC(OrderBreakfast obf);

	List<OrderBreakfast> getOrderBKFCoupons(OrderBreakfast obf);

	void addOrderBreakfastByRoomInfo(OrderInfo orderInfo);
	
	void delCouponsByCon(OrderBreakfast obf);
	
}
