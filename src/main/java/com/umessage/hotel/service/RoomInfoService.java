package com.umessage.hotel.service;

import java.util.Date;
import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.RoomInfo;
import com.umessage.hotel.model.RoomModel;

public interface RoomInfoService extends IService<RoomInfo> {

	public List<RoomInfo> findRoomInfoAll(String roomId, String hotelId,String bookable);

	public void addRoomInfo(RoomInfo roomInfo);
	
	public void addRoomInfoAndPic(RoomInfo roomInfo);

	public void updateRoomInfo(RoomInfo roomInfo);
	
	public RoomModel convertToRoomModel(RoomInfo roomInfo,String type,String roomsType);
	
	public RoomInfo getRoomAvgByRoomId(String roomId, String hotelId, Date beginTime, Date endTime);
	
	public List<RoomModel> listRooms(String hotelId,Date beginTime,Date endTime,String memberId,String roomsType);
	
	public RoomModel loadRoom(String hotelId,String roomId,Date endTime,Date beginTime,String memberId);
	
	public List<RoomModel> RoomsAdmin(String hotelId,Date beginTime,Date endTime);

	void editRoomById(RoomInfo roomInfo);

	public RoomInfo getRoomInfoById(RoomInfo roomInfo);
	
	public void getDisCountPrice(MemberInfo memberInfo,RoomInfo roomInfo);

	public List<RoomInfo> findRoomInfoByCon(RoomInfo roomInfo);
	
	public String getRoomTypeById(Integer room_id);

	public RoomInfo getRoomInfoBasicById(Integer roomId);

}
