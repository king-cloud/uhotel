package com.umessage.hotel.service;

import com.umessage.hotel.domain.MemberInvoice;
import com.umessage.hotel.domain.OrderBreakfast;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.TemplateMessage;

public interface WxMsgTmplService {

	public void getTemplateLibraryList(String hotelId, String appType, String templateId);

	public void sendTemplateMessage(TemplateMessage message);
	
	public void sendOrderPaySuccessMsg(OrderInfo order);
	
	public void sendCancelOrderMsg(OrderInfo order);
	
	public void sendConfirmOrderMsg(OrderInfo order);
	
	public void sendOrderBKFPaySuccessMsg(OrderBreakfast orderbkf);

	public void sendOpenInvoice(MemberInvoice invoice);

}
