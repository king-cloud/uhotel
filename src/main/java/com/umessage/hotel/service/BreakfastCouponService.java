package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.BreakfastCoupon;
import com.umessage.hotel.domain.BreakfastRecord;
import com.umessage.hotel.domain.OrderBreakfast;

public interface BreakfastCouponService  extends IService<BreakfastCoupon> {

	void addCoupons(BreakfastCoupon coupon);

	List<BreakfastCoupon> findBreakfastCouponsToday(BreakfastCoupon coupon);

	List<BreakfastCoupon> findBreakfastCouponsAfter(BreakfastCoupon coupon);

	List<BreakfastCoupon> findBreakfastCouponsHistory(BreakfastCoupon coupon);

	Integer findBreakfastCouponsTodayCount(BreakfastCoupon coupon);
	
	void useBreakfastCoupon(BreakfastRecord useRecord);

	List<BreakfastRecord> findRecords(String hotelId, Integer page, Integer rows,String mobile);

	void addBrakefastCoupons(OrderBreakfast orderBKF);
	
	void addBrakefastCouponsPC(OrderBreakfast orderBKF);
	
	Integer countNumTodayByType(BreakfastCoupon coupon);
	
	Integer countCheckToday(BreakfastCoupon coupon);

	Integer countUseToday(BreakfastCoupon coupon);
}
