package com.umessage.hotel.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.hotel.domain.MemberInvoice;

public interface MemberInvoiceService  extends IService<MemberInvoice>{

	void addInvoice(MemberInvoice memberInvoice);

	List<MemberInvoice>  findInvoiceListByUser(String memberId,Integer hotelId,String status);
	
	List<MemberInvoice>  findInvoiceList(Integer hotelId,String status);
	
	void updateInvoiceStatus(Integer id,Integer status);

	List<MemberInvoice> findInvoiceList(MemberInvoice invoice, int pageNum, int pageSize);

	MemberInvoice findSingleInvoice(Integer id);

	void openInvoice(Integer id, Integer status);
}
