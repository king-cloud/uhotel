package com.umessage.hotel.config;

import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.Import;
import org.springframework.jmx.support.RegistrationPolicy;

/**
 * Created by KING on 2018-09-14.
 */
@Configuration
@Import(FdfsClientConfig.class)
// 解决jmx重复注册bean的问题
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@EnableCaching
public class FastDFSConfig {
//    @Value("${fastdfs.connect_timeout_in_seconds}")
//    private String connectTimeout;
//
//    @Value("${fastdfs.network_timeout_in_seconds}")
//    private String networkTimeout;
//
//    @Value("${fastdfs.charset}")
//    private String charset;
//
//    @Value("${fastdfs.http_anti_steal_token}")
//    private String antiStealToken;
//
//    @Value("${fastdfs.http_secret_key}")
//    private String secretKey;
//
//    @Value("${fastdfs.http_tracker_http_port}")
//    private String trackerHttpPort;
//
//    @Value("${fastdfs.tracker_servers}")
//    private String trackerServer;
}
