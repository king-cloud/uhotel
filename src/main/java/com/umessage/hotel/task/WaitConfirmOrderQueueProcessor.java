package com.umessage.hotel.task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.redisson.api.RBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.umessage.common.service.RedissonService;
import com.umessage.common.util.SimpleSmsUtil;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.RoomInfo;
import com.umessage.hotel.domain.TemplateMessage;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.RoomInfoService;
import com.umessage.hotel.service.WxMsgTmplService;
import com.umessage.system.service.UserService;

/**
 * 等待确认订单处理器
 * 
 * @author mixiangliu
 *
 */
@Component
public class WaitConfirmOrderQueueProcessor implements DisposableBean, Runnable {

	private static Logger logger = LoggerFactory.getLogger(WaitConfirmOrderQueueProcessor.class);

	private Thread thread;

	@Autowired
	private RedissonService redissonService;
	@Autowired
	private OrderInfoService orderInfoService;
	@Autowired
	private UserService userService;
	@Autowired
	private SimpleSmsUtil simpleSmsUtil;
	@Autowired
	private HotelInfoService hotelInfoService;
	@Autowired
	private RoomInfoService roomInfoService;
	@Autowired
	private WxMsgTmplService wxMsgTmplService;
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public WaitConfirmOrderQueueProcessor() {
		this.thread = new Thread(this);
	}

	@Override
	public void run() {

		RBlockingQueue<String> newOrderQueue = redissonService.<String>getBlockingQueue("wait_confirm_order_queue");

		while (true) {

			try {

				String orderId = newOrderQueue.take();
				if (orderId != null) {
					OrderInfo order = orderInfoService.selectByKey(orderId);
					List<String> mobileList = userService.findConfirmUserMobileList(order.getHotelId().toString());
					HotelInfo hotel = hotelInfoService.selectByKey(order.getHotelId().toString());
					RoomInfo roomInfo = roomInfoService.selectByKey(order.getRoomId());
					
					//给会员发送消息
					TemplateMessage message = new TemplateMessage();
					message.setHotelId(String.valueOf(order.getHotelId()));
					message.setTemplateTitleId("AT1051");
					message.setTouser(order.getOpenid());
					List<String> paras = new ArrayList<String>();
					paras.add(hotel.getHotelName());
					paras.add(roomInfo.getName());
					paras.add(order.getResident());
					paras.add(String.valueOf(order.getRoomCount()));
					paras.add(df.format(df.parse(order.getCheckinTime())));
					message.setParams(paras);
					wxMsgTmplService.sendTemplateMessage(message);
					
					//给酒店员工发送确认消息
					if (mobileList != null && mobileList.size() > 0) {
						for(String mobile : mobileList) {
							try {
								String msg = "酒店有新订单等待您确认，订单编号：" + order.getOrderSn() 
										+ "，房型：" + roomInfo.getName()
										+ ",房间数量：" + order.getRoomCount()
										+ "，入住时间：" + df.format(df.parse(order.getCheckinTime()));
								simpleSmsUtil.sendMsg(mobile, hotel.getHotelName(), msg);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
			}
		}

	}

	@PostConstruct
	protected void initialize() {
		try {
			this.thread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void destroy() throws Exception {
		this.thread.destroy();
	}

}
