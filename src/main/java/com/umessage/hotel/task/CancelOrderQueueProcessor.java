package com.umessage.hotel.task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import org.redisson.api.RBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.umessage.common.service.RedissonService;
import com.umessage.common.util.SimpleSmsUtil;
import com.umessage.hotel.domain.HotelInfo;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.RoomInfo;
import com.umessage.hotel.service.HotelInfoService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.RoomInfoService;
import com.umessage.hotel.service.WxMsgTmplService;
import com.umessage.system.service.UserService;

/**
 * 取消订单队列处理器
 * 
 * @author mixiangliu
 *
 */
@Component
public class CancelOrderQueueProcessor implements DisposableBean, Runnable {

	private static Logger logger = LoggerFactory.getLogger(CancelOrderQueueProcessor.class);

	private Thread thread;

	@Autowired
	private RedissonService redissonService;
	@Autowired
	private OrderInfoService orderInfoService;
	@Autowired
	private UserService userService;
	@Autowired
	private SimpleSmsUtil simpleSmsUtil;
	@Autowired
	private HotelInfoService hotelInfoService;
	@Autowired
	private RoomInfoService roomInfoService;
	@Autowired
	private WxMsgTmplService wxMsgTmplService;
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public CancelOrderQueueProcessor() {
		this.thread = new Thread(this);
	}

	@Override
	public void run() {

		RBlockingQueue<String> cancelOrderQueue = null;

		while (true) {

			try {
				cancelOrderQueue = redissonService.<String>getBlockingQueue("cancel_order_queue");
				String orderId = cancelOrderQueue.take();
				if (orderId != null) {
					OrderInfo order = orderInfoService.selectByKey(orderId);
					List<String> mobileList = userService.findConfirmUserMobileList(order.getHotelId().toString());
					HotelInfo hotel = hotelInfoService.selectByKey(order.getHotelId().toString());
					RoomInfo roomInfo = roomInfoService.selectByKey(order.getRoomId());
					
					//发送会员发送模版消息
					wxMsgTmplService.sendCancelOrderMsg(order);
					//给会员发送短信通知
					orderInfoService.sendSms("2", order);
					//给酒店员工发送确认消息
					if (mobileList != null && mobileList.size() > 0) {
						for(String mobile : mobileList) {
							try {
								String msg = "酒店有订单被取消，订单编号：" + order.getOrderSn() 
										+ "，房型：" + roomInfo.getName()
										+ ",房间数量：" + order.getRoomCount()
										+ "，入住时间：" + df.format(df.parse(order.getCheckinTime()));
								simpleSmsUtil.sendMsg(mobile, hotel.getHotelName(), msg);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage(), e);
			}
		}

	}

	@PostConstruct
	protected void initialize() {
		try {
			this.thread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void destroy() throws Exception {
		this.thread.destroy();
	}

}
