package com.umessage.hotel.util;

public class ErrorCode {

	public static final String SERVER_ERROR = "服务器错误";

	public static final String ILLEGAL_PARAMS = "非法参数";
	
	public static final String MiSS_PARAMS = "缺失参数";

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
