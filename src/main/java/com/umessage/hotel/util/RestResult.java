package com.umessage.hotel.util;

public class RestResult<T> {

	private boolean success;
	private String returnCode;
	private String returnMessage;
	private T data;

	private RestResult() {
	}

	public static <T> RestResult<T> newInstance() {
		return new RestResult<>();
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "RestResult [success=" + success + ", returnCode=" + returnCode + ", returnMessage=" + returnMessage
				+ ", data=" + data + "]";
	}
	
}