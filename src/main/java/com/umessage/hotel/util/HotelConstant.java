package com.umessage.hotel.util;

public class HotelConstant {

	// 普通微信授权登录
	public static final String LOGIN_TYPE_WX = "1";

	// 用户端手机登录
	public static final String LOGIN_TYPE_MOBILE_ClIENT = "2";

	// 酒店端手机登录
	public static final String LOGIN_TYPE_MOBILE_HOTEL = "3";

	// 验证码前缀
	public static final String VERCODE_PREFIX = "VERCODE_";

	// 验证码类型（客户端）
	public static final String VERCODE_TYPE_CLIENT = "1";

	// 验证码类型（酒店端）
	public static final String VERCODE_TYPE_HOTEL = "2";

	/**
	 * 订单状态
	 */
	public static final String ORDER_STATUS_NEW = "0";// 新建
	public static final String ORDER_STATUS_CONFIRMED = "1";// 已确认
	public static final String ORDER_STATUS_CANCELED = "2";// 已取消

	/**
	 * 支付类型 1到店支付、2担保支付、3全额支付
	 */
	public static final Integer ORDER_PAY_TYPE_OFFLINE = 1;// 到店支付

	public static final Integer ORDER_PAY_TYPE_GUARANTEE = 2;// 担保支付

	public static final Integer ORDER_PAY_TYPE_ONLINE = 3;// 在线支付
	
	/**
	 * 支付状态
	 */
	public static final Integer ORDER_PAY_STATE=0;//未支付
	
	/**
	 * 操作员类型：会员
	 */
	public final static Integer OPERATOR_TYPE_MEMBER = 2;

	/**
	 * 操作员类型：酒店
	 */
	public final static Integer OPERATOR_TYPE_HOTEL = 1;
	
	/**
	 * 新注册
	 */
	public static final String USER_GRADE_NEW = "0";
	/**
	 * 会员（手机号已绑定）
	 */
	public static final String USER_GRADE_MEMBER = "1";
	
	
	/**酒店定时任务(有效/无效)*/
	public static final Integer HOTEL_CRONTAB_STATUS_EFFECTIVE = 1;
	public static final Integer HOTEL_CRONTAB_STATUS_UNEFFECTIVE = 0;
	/**酒店定时任务类型，每天、每小时、每15分钟、每分钟*/
	public static final Integer HOTEL_CRONTAB_DAY = 1;
	public static final Integer HOTEL_CRONTAB_HOUR = 2;
	public static final Integer HOTEL_CRONTAB_15MIN = 3;
	public static final Integer HOTEL_CRONTAB_MIN = 4;
	/**酒店定时任务是否运行中*/
	public static final Integer HOTEL_CRONTAB_NO_RUNNING = 0 ;
	public static final Integer HOTEL_CRONTAB_RUNNING = 1;
	/**酒店定时任务是否运行中*/
//	1：每天18点定时执行待确认-->取消
//	2：每天12点将已确认-->已结束
//	3：每天10点定时发送本周订单详情
//	4：每15分钟将12小时内未确认-->取消
//	5：每分钟将未支付-->取消
	public static final Integer HOTEL_CRONTAB_TASK_1 = 1 ;
	public static final Integer HOTEL_CRONTAB_TASK_2 = 2 ;
	public static final Integer HOTEL_CRONTAB_TASK_3 = 3 ;
	public static final Integer HOTEL_CRONTAB_TASK_4 = 4 ;
	public static final Integer HOTEL_CRONTAB_TASK_5 = 5 ;

	/**
	 * 发票状态
	 */
	public static final Integer INVOICE_STATUS_NEW = 0;
	public static final Integer INVOICE_STATUS_OPEN = 1;
	public static final Integer INVOICE_STATUS_NO = 2;
}
