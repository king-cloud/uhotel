package com.umessage.hotel.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TimeUtil {
	
	 
	 
    /**
     * 获取 当前年、半年、季度、月、日、小时 开始结束时间
     */
 
    public final static SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");;
    public final static SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");;
    
 
    /**
     * 格式化字符串时间为date类型
     * @param str
     * @return
     */
    public static Date strTransDate(String str){
    	try {
			return longSdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return null;
    }
    /**
     * 根据规则将date转为string
     * @param date 
     * @param dateFormat 转换规则
     * @return
     */
    public static String date2String(Date date,SimpleDateFormat dateFormat){
    	return dateFormat.format(date);
    }
    
    public static String getCurrentWeekDayStartTime() {
       /* Calendar c = Calendar.getInstance();
        try {
            int weekday = c.get(Calendar.DAY_OF_WEEK) - 2;
            c.add(Calendar.DATE, -weekday);
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c.getTime();*/
    	Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 6);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd 00:00");
		String result = format.format(today);
		return result;
    }
 
    /**
     * 获得本周的最后一天，周日
     *
     * @return
     */
    public static Date getCurrentWeekDayEndTime() {
        Calendar c = Calendar.getInstance();
        try {
            int weekday = c.get(Calendar.DAY_OF_WEEK);
            c.add(Calendar.DATE, 8 - weekday);
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c.getTime();
    }
 
    /**
     * 获得本天的开始时间
     *
     * @return
     */
    public static Date getCurrentDayStartTime() {
        Date now = new Date();
        try {
            now = shortSdf.parse(shortSdf.format(now));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 获得本天的结束时间
     *
     * @return
     */
    public static Date getCurrentDayEndTime() {
        Date now = new Date();
        try {
            now = longSdf.parse(shortSdf.format(now) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 获得本小时的开始时间
     *
     * @return
     */
    public static Date getCurrentHourStartTime() {
        Date now = new Date();
        try {
            now = longHourSdf.parse(longHourSdf.format(now));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 获得本小时的结束时间
     *
     * @return
     */
    public static Date getCurrentHourEndTime() {
        Date now = new Date();
        try {
            now = longSdf.parse(longHourSdf.format(now) + ":59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
    /**
     * 获得三天的开始时间
     *
     * @return
     */
    public static Date getCurrentThreeDayStartTime() {
    	//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -3);
		date = calendar.getTime();
		return date;
    }
 
    /**
     * 获得本月的开始时间
     *
     * @return
     */
    public static Date getCurrentMonthStartTime() {
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.DATE, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 本月的结束时间
     *
     * @return
     */
    public static Date getCurrentMonthEndTime() {
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.DATE, 1);
            c.add(Calendar.MONTH, 1);
            c.add(Calendar.DATE, -1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 当前年的开始时间
     *
     * @return
     */
    public static Date getCurrentYearStartTime() {
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.MONTH, 0);
            c.set(Calendar.DATE, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 当前年的结束时间
     *
     * @return
     */
    public static Date getCurrentYearEndTime() {
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.MONTH, 11);
            c.set(Calendar.DATE, 31);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 当前季度的开始时间
     *
     * @return
     */
    public static Date getCurrentQuarterStartTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 3)
                c.set(Calendar.MONTH, 0);
            else if (currentMonth >= 4 && currentMonth <= 6)
                c.set(Calendar.MONTH, 3);
            else if (currentMonth >= 7 && currentMonth <= 9)
                c.set(Calendar.MONTH, 4);
            else if (currentMonth >= 10 && currentMonth <= 12)
                c.set(Calendar.MONTH, 9);
            c.set(Calendar.DATE, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 当前季度的结束时间
     *
     * @return
     */
    public static Date getCurrentQuarterEndTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 3) {
                c.set(Calendar.MONTH, 2);
                c.set(Calendar.DATE, 31);
            } else if (currentMonth >= 4 && currentMonth <= 6) {
                c.set(Calendar.MONTH, 5);
                c.set(Calendar.DATE, 30);
            } else if (currentMonth >= 7 && currentMonth <= 9) {
                c.set(Calendar.MONTH, 8);
                c.set(Calendar.DATE, 30);
            } else if (currentMonth >= 10 && currentMonth <= 12) {
                c.set(Calendar.MONTH, 11);
                c.set(Calendar.DATE, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
 
    /**
     * 获取前/后半年的开始时间
     *
     * @return
     */
    public static Date getHalfYearStartTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 6) {
                c.set(Calendar.MONTH, 0);
            } else if (currentMonth >= 7 && currentMonth <= 12) {
                c.set(Calendar.MONTH, 6);
            }
            c.set(Calendar.DATE, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
 
    }
 
    /**
     * 获取前/后半年的结束时间
     *
     * @return
     */
    public static Date getHalfYearEndTime() {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 6) {
                c.set(Calendar.MONTH, 5);
                c.set(Calendar.DATE, 30);
            } else if (currentMonth >= 7 && currentMonth <= 12) {
                c.set(Calendar.MONTH, 11);
                c.set(Calendar.DATE, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return now;
    }
    
    
    
    /*public static void main(String[] args) {
        System.out.println("当前小时开始："+getCurrentHourStartTime().toString());
        System.out.println("当前小时结束："+getCurrentHourEndTime().toString());
        System.out.println("当前天开始："+getCurrentDayStartTime().toString());
        System.out.println("当前天时结束："+getCurrentDayEndTime().toString());
        System.out.println("当前周开始："+getCurrentWeekDayStartTime().toString());
        System.out.println("当前周结束："+getCurrentWeekDayEndTime().toString());
        System.out.println("当前月开始："+getCurrentMonthStartTime().toString());
        System.out.println("当前月结束："+getCurrentMonthEndTime().toString());
        System.out.println("当前季度开始："+getCurrentQuarterStartTime().toString());
        System.out.println("当前季度结束："+getCurrentQuarterEndTime().toString());
        System.out.println("当前半年/后半年开始："+getHalfYearStartTime().toString());
        System.out.println("当前半年/后半年结束："+getHalfYearEndTime().toString());
        System.out.println("当前年开始："+getCurrentYearStartTime().toString());
        System.out.println("当前年结束："+getCurrentYearEndTime().toString());
    }*/
    
    /**
	 * 当前时间 任意前几天 auto yibo
	 */
	public static String getPastDate(int past) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
		Date today = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd 00:00");
		String result = format.format(today);
		return result;
	}
	
	// 获取本月的开始时间
		public static Date getBeginDayOfMonth() {
			Calendar calendar = Calendar.getInstance();
			calendar.set(getNowYear(), getNowMonth() - 1, 1);
			return getDayStartTime(calendar.getTime());
		}

		// 获取本月的结束时间
		public static Date getEndDayOfMonth() {
			Calendar calendar = Calendar.getInstance();
			calendar.set(getNowYear(), getNowMonth() - 1, 1);
			int day = calendar.getActualMaximum(5);
			calendar.set(getNowYear(), getNowMonth() - 1, day);
			return getDayEndTime(calendar.getTime());
		}

		// 获取今年是哪一年
		public static Integer getNowYear() {
			Date date = new Date();
			GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
			gc.setTime(date);
			return Integer.valueOf(gc.get(1));
		}

		// 获取本月是哪一月
		public static int getNowMonth() {
			Date date = new Date();
			GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
			gc.setTime(date);
			return gc.get(2) + 1;
		}

		// 获取某个日期的开始时间
		public static Timestamp getDayStartTime(Date d) {
			Calendar calendar = Calendar.getInstance();
			if (null != d)
				calendar.setTime(d);
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0,
					0, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			return new Timestamp(calendar.getTimeInMillis());
		}

		// 获取某个日期的结束时间
		public static Timestamp getDayEndTime(Date d) {
			Calendar calendar = Calendar.getInstance();
			if (null != d)
				calendar.setTime(d);
			calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23,
					59, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			return new Timestamp(calendar.getTimeInMillis());
		}
		
		public static List<Date> getDatesBetweenTwoDate(Date beginDate, Date endDate) {
			List<Date> lDate = new ArrayList<Date>();
			lDate.add(beginDate);// 把开始时间加入集合
			Calendar cal = Calendar.getInstance();
			// 使用给定的 Date 设置此 Calendar 的时间
			cal.setTime(beginDate);
			boolean bContinue = true;
			while (bContinue) {
				// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
				cal.add(Calendar.DAY_OF_MONTH, 1);
				// 测试此日期是否在指定日期之后
				if (endDate.after(cal.getTime())) {
					lDate.add(cal.getTime());
				} else {
					break;
				}
			}
			lDate.add(endDate);// 把结束时间加入集合
			return lDate;
		}
		

	    /**
	     * 当前季度的开始时间
	     *
	     * @return
	     */
	    public static Date getCurrentQuarterStartTime(Integer type) {
	        Calendar c = Calendar.getInstance();
	        int currentMonth = c.get(Calendar.MONTH) + 1;
	        Date now = null;
	        try {
	            if (type == 0)
	                c.set(Calendar.MONTH, 0);
	            else if (type == 1)
	                c.set(Calendar.MONTH, 1);
	            else if (type == 2)
	                c.set(Calendar.MONTH, 2);
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return now;
	    }
	 
	  //4、获取前一个月的第一天
	    
	  public static String getBeforeMonthStartdate()throws Exception{
	    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd 00:00");
	    Calendar calendar=Calendar.getInstance();
	    calendar.add(Calendar.MONTH, -1);
	    calendar.set(Calendar.DAY_OF_MONTH, 1);
	    return format.format(calendar.getTime());
	  }
	  //5、获取前一个月的最后一天
      public static String getBeforeMonthEnddate()throws Exception{
	    SimpleDateFormat sf=new SimpleDateFormat("yyyy-MM-dd 23:59");
	    Calendar calendar=Calendar.getInstance();
	    int month=calendar.get(Calendar.MONTH);
	    calendar.set(Calendar.MONTH, month-1);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return  sf.format(calendar.getTime());
     }
	
	  public static String getBeforeMonthStartdate(Integer type)throws Exception{
	    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd 00:00:00");
	    Calendar calendar=Calendar.getInstance();
	    calendar.add(Calendar.MONTH, -type);
	    calendar.set(Calendar.DAY_OF_MONTH, 1);
	    return format.format(calendar.getTime());
	  }
      public static String getBeforeMonthEnddate(Integer type)throws Exception{
	    SimpleDateFormat sf=new SimpleDateFormat("yyyy-MM-dd 23:59:59");
	    Calendar calendar=Calendar.getInstance();
	    int month=calendar.get(Calendar.MONTH);
	    calendar.set(Calendar.MONTH, month-type);
	    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return  sf.format(calendar.getTime());
     }
      
   // 获取本月是哪个小时
	public static int getNowHour(Date date) {
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc.get(Calendar.HOUR_OF_DAY);
	}
	
	// 获取本月是哪个分钟
	public static int getNowMin(Date date) {
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc.get(Calendar.MINUTE);
	}
	public static void main(String[] args) {
	   	 Date date = new Date() ;
		System.out.println(TimeUtil.getNowHour(date));
		System.out.println(TimeUtil.getNowMin(date));
   	}
}
