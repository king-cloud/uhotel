package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="礼金券信息", description="礼金券信息，礼金券包含订单礼金券和充值优惠券")
public class CouponModel {
	@ApiModelProperty(value="礼金券id")
	private String couponId;
	@ApiModelProperty(value="酒店id")
	private String hotelId;
	@ApiModelProperty(value="礼金券类型", example="1：订单优惠和，2：充值优惠")
	private String couponType;
	@ApiModelProperty(value="优惠方式(配合firestValue和secondValue使用)", example="1直减、2满减、3冲增")
	private String mode;
	@ApiModelProperty(value="配合model字段使用")
	private int firstValue;
	@ApiModelProperty(value="配合model字段使用")
	private int secondValue;
	@ApiModelProperty(value="有效期起始时间")
	private String validStart;
	@ApiModelProperty(value="有效期结束时间")
	private String validEnd;
	@ApiModelProperty(value="是否可使用")
	private String isUsable;
	@ApiModelProperty(value="兑换码")
	private String redeemCode;
	@ApiModelProperty(value="分享链接")
	private String shareUrl;
	@ApiModelProperty(value="二维码")
	private String barCode;
	@ApiModelProperty(value="使用状态")
	private String status;
	

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public int getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(int firstValue) {
		this.firstValue = firstValue;
	}

	public int getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(int secondValue) {
		this.secondValue = secondValue;
	}

	public String getValidStart() {
		return validStart;
	}

	public void setValidStart(String validStart) {
		this.validStart = validStart;
	}

	public String getValidEnd() {
		return validEnd;
	}

	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}

	public String getIsUsable() {
		return isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getRedeemCode() {
		return redeemCode;
	}

	public void setRedeemCode(String redeemCode) {
		this.redeemCode = redeemCode;
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
