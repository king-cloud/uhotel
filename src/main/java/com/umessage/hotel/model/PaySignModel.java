package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "支付签名", description = "支付签名")
public class PaySignModel {
	@ApiModelProperty(value = "appid")
	private String appId;
	@ApiModelProperty(value = "随机串")
	private String nonceStr;
	@ApiModelProperty(name = "package", value = "package", notes = "调用微信支付接口的时候使用package替换")
	private String dataPackage;
	@ApiModelProperty(value = "签名方式")
	private String signType;
	@ApiModelProperty(value = "时间戳", example = "1490840662")
	private String timeStamp;
	@ApiModelProperty(value = "签名")
	private String paySign;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPackage() {
		return dataPackage;
	}

	public void setPackage(String dataPackage) {
		this.dataPackage = dataPackage;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

}
