package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="早餐券二维码", description="使用早餐券二维码")
public class BreakfastCouponQRCode {

	@ApiModelProperty(value="使用日期")
	private String useDate;
	@ApiModelProperty(value="使用数量")
	private Integer useAmount;
	@ApiModelProperty(value="二维码地址")
	private String url;

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}

	public Integer getUseAmount() {
		return useAmount;
	}

	public void setUseAmount(Integer useAmount) {
		this.useAmount = useAmount;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
