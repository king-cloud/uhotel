package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("图片信息")
public class PictureModel {
	@ApiModelProperty(value="url地址")
	private String picUrl;
	@ApiModelProperty(value="超级链接")
	private String picHref;
	@ApiModelProperty(value="排序字段")
	private String sort;
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getPicHref() {
		return picHref;
	}
	public void setPicHref(String picHref) {
		this.picHref = picHref;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
}
