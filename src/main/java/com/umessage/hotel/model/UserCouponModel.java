package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户礼金券信息")
public class UserCouponModel {
	@ApiModelProperty(value = "用户ID")
	private String userId;
	@ApiModelProperty(value = "礼金券ID")
	private String couponId;
	@ApiModelProperty(value = "礼金券名称")
	private String couponName;
	@ApiModelProperty(value = "礼金券类型")
	private String couponType;
	@ApiModelProperty(value = "优惠方式", example = "1直减、2满减、3冲增")
	private String mode;
	@ApiModelProperty(value = "配合model字段使用")
	private int firstValue;
	@ApiModelProperty(value = "配合model字段使用")
	private int secondValue;
	@ApiModelProperty(value = "有效期起始时间")
	private String validStart;
	@ApiModelProperty(value = "有效期结束时间")
	private String validEnd;
	@ApiModelProperty(value = "有效期类型", example="1.固定周期  2.固定时长")
	private int validType;
	@ApiModelProperty(value = "有效月份")
	private String cycle;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public int getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(int firstValue) {
		this.firstValue = firstValue;
	}

	public int getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(int secondValue) {
		this.secondValue = secondValue;
	}

	public String getValidStart() {
		return validStart;
	}

	public void setValidStart(String validStart) {
		this.validStart = validStart;
	}

	public String getValidEnd() {
		return validEnd;
	}

	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}

	public int getValidType() {
		return validType;
	}

	public void setValidType(int validType) {
		this.validType = validType;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

}
