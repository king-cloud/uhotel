package com.umessage.hotel.model;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceApplicationModel {
	@ApiModelProperty(hidden = true)
    private Integer id;
	@ApiModelProperty(value = "酒店id")
    private String hotelId;
	@ApiModelProperty(hidden = true)
    private String memberId;
	@ApiModelProperty(value = "发票抬头定义")
    private String titleId;
	@ApiModelProperty(hidden = true)
    private String status;
	@ApiModelProperty(value = "订单号")
    private String orderId;
	@ApiModelProperty(value = "发票金额")
    private BigDecimal amount;
	@ApiModelProperty(value = "邮箱")
    private String email;
	@ApiModelProperty(value = "手机号")
    private String mobile;
	@ApiModelProperty(value = "票号")
    private String invoiceNo;
	@ApiModelProperty(hidden = true)
    private String result;
	@ApiModelProperty(hidden = true)
    private Date addTime;
	@ApiModelProperty(hidden = true)
    private Date updateTime;
	
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId == null ? null : titleId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo == null ? null : invoiceNo.trim();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}