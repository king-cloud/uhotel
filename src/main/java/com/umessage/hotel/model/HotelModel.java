package com.umessage.hotel.model;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "酒店信息", description = "酒店信息")
public class HotelModel {
	@ApiModelProperty(value = "酒店id")
	private String hotelId;
	@ApiModelProperty(value = "酒店名称")
	private String hotelName;
	@ApiModelProperty(value = "酒店介绍")
	private String summary;
	@ApiModelProperty(value = "所属国家")
	private String country;
	@ApiModelProperty(value = "所属省")
	private String province;
	@ApiModelProperty(value = "所属市")
	private String city;
	@ApiModelProperty(value = "所属区")
	private String district;
	@ApiModelProperty(value = "所在街道")
	private String street;
	@ApiModelProperty(value = "经度")
	private double longitude;
	@ApiModelProperty(value = "纬度")
	private double latitude;
	@ApiModelProperty(value = "地址")
	private String adress;
	@ApiModelProperty(value = "邮编")
	private String post;
	@ApiModelProperty(value = "标签")
	private Map<String, String> tag;
	@ApiModelProperty(value = "设施")
	private Map<String, String> facilities;
	@ApiModelProperty(value = "政策")
	private Map<String, String> policies;
	@ApiModelProperty(value = "联系方式")
	private String contact;
	@ApiModelProperty(value = "酒店banner图")
	private List<PictureModel> bannerPics;
	@ApiModelProperty(value = "添加时间")
	private String addTime;
	@ApiModelProperty(value = "参照物")
	private String markOne;
	@ApiModelProperty(value = "标识距离")
	private String markTwo;
	
	@ApiModelProperty(value = "wifi开启状态 0关闭1开启")
	private String wifiStatus;
	    
	@ApiModelProperty(value = "wifi名称")
	private String wifiName;
	    
	@ApiModelProperty(value = "wifi密码")
	private String wifiPassword;
	
	@ApiModelProperty(value ="1.普票 2.电子 3.专票")
	private List<Object> invoiceType;
	
	public List<Object> getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(List<Object> invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getWifiStatus() {
		return wifiStatus;
	}

	public void setWifiStatus(String wifiStatus) {
		this.wifiStatus = wifiStatus;
	}

	public String getWifiName() {
		return wifiName;
	}

	public void setWifiName(String wifiName) {
		this.wifiName = wifiName;
	}

	public String getWifiPassword() {
		return wifiPassword;
	}

	public void setWifiPassword(String wifiPassword) {
		this.wifiPassword = wifiPassword;
	}

	public String getMarkOne() {
		return markOne;
	}

	public void setMarkOne(String markOne) {
		this.markOne = markOne;
	}

	public String getMarkTwo() {
		return markTwo;
	}

	public void setMarkTwo(String markTwo) {
		this.markTwo = markTwo;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public Map<String, String> getTag() {
		return tag;
	}

	public void setTag(Map<String, String> tag) {
		this.tag = tag;
	}

	public Map<String, String> getFacilities() {
		return facilities;
	}

	public void setFacilities(Map<String, String> facilities) {
		this.facilities = facilities;
	}

	public Map<String, String> getPolicies() {
		return policies;
	}

	public void setPolicies(Map<String, String> policies) {
		this.policies = policies;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public List<PictureModel> getBannerPics() {
		return bannerPics;
	}

	public void setBannerPics(List<PictureModel> bannerPics) {
		this.bannerPics = bannerPics;
	}

}
