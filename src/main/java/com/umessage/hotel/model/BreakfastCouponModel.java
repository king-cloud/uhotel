package com.umessage.hotel.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 账户表
 * 
 * @author mixiangliu
 *
 */
@ApiModel(value = "员工信息", description = "员工信息")
public class BreakfastCouponModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "会话id")
	private String sessionId;
	@ApiModelProperty(value = "账户id")
	private String accountId;
	@ApiModelProperty(value = "酒店id")
	private String hotelId;
	@ApiModelProperty(value = "账号编号")
	private String accountCode;
	@ApiModelProperty(value = "账号名称")
	private String accountName;
	@ApiModelProperty(value = "性别")
	private String sex;
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	@ApiModelProperty(value = "固定电话")
	private String phone;
	@ApiModelProperty(value = "手机")
	private String mobile;
	@ApiModelProperty(value = "职位")
	private String position;
	@ApiModelProperty(value = "权限")
	private Map privilege;
	@ApiModelProperty(value = "密码", hidden = true)
	private String password;
	@ApiModelProperty(value = "添加时间", hidden = true)
	private String addTime;
	@ApiModelProperty(value = "添加人", hidden = true)
	private String addAcc;
	@ApiModelProperty(value = "更新时间", hidden = true)
	private String modifyTime;
	@ApiModelProperty(value = "更新人", hidden = true)
	private String modifyAcc;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public Map getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Map privilege) {
		this.privilege = privilege;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public String getAddAcc() {
		return addAcc;
	}

	public void setAddAcc(String addAcc) {
		this.addAcc = addAcc;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getModifyAcc() {
		return modifyAcc;
	}

	public void setModifyAcc(String modifyAcc) {
		this.modifyAcc = modifyAcc;
	}

}
