package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 小程序表单信息
 * 
 * @author mixiangliu
 *
 */
@ApiModel(value = "小程序表单信息", description = "小程序表单信息")
public class FormModel implements Comparable<FormModel> {

	/**
	 * 表单id
	 */
	@ApiModelProperty(value = "表单id")
	private String formId;

	/**
	 * 超时时间
	 */
	@ApiModelProperty(value = "超时时间")
	private Long expire;

	public FormModel() {
		// TODO Auto-generated constructor stub
	}

	public FormModel(String formId, Long expire) {
		super();
		this.formId = formId;
		this.expire = expire;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public Long getExpire() {
		return expire;
	}

	public void setExpire(Long expire) {
		this.expire = expire;
	}

	@Override
	public int compareTo(FormModel o) {
		return this.getExpire().compareTo(o.getExpire());
	}

}
