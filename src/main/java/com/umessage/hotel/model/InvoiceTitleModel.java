package com.umessage.hotel.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class InvoiceTitleModel {
	
    private Integer id;
	@ApiModelProperty(value = "酒店id")
    private String hotelId;
	@ApiModelProperty(value = "会员id", hidden = true)
    private String memberId;
	@ApiModelProperty(value = "发票类型：1企业2个人/非企业单位")
    private String type;
	@ApiModelProperty(value = "发票")
    private String title;
	@ApiModelProperty(value = "税号")
    private String taxNo;
	@ApiModelProperty(value = "单位地址")
    private String address;
	@ApiModelProperty(value = "联系电话")
    private String phone;
	@ApiModelProperty(value = "开户行")
    private String bank;
	@ApiModelProperty(value = "银行账号")
    private String account;
	@ApiModelProperty(hidden = true)
    private Date addTime;
	@ApiModelProperty(hidden = true)
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId == null ? null : hotelId.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo == null ? null : taxNo.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}