package com.umessage.hotel.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发票
 * @author zmj
 *
 */
@ApiModel(value = "发票信息", description = "发票信息")
public class MemberInvoiceModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "房间号", required = true)
    private Integer roomNo;
  
	@ApiModelProperty(value = "抬头", required = true)
    private String title;
	
	@ApiModelProperty(value = "1.普票 2.电子 3.专票", required = true)
    private Integer type;
	@ApiModelProperty(value = "姓名",required=true)
    private String name;
	@ApiModelProperty(value = "手机号")
    private String mobile;

	@ApiModelProperty(value = "税号",required=true)
    private String dutyNo;

	@ApiModelProperty(value = "公司地址")
    private String address;

	@ApiModelProperty(value = "公司电话")
    private String companyTel;

	@ApiModelProperty(value = "开户银行")
    private String bankName;

	@ApiModelProperty(value = "银行账号")
    private String bankNo;
	@ApiModelProperty(value = "邮箱")
    private String email;
	@ApiModelProperty(value = "开票时间", required=true)
    private String checkoutTime;
	@ApiModelProperty(value = "表单id", required=true)
	private String formId;
	
	public Integer getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(Integer roomNo) {
		this.roomNo = roomNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getDutyNo() {
		return dutyNo;
	}
	public void setDutyNo(String dutyNo) {
		this.dutyNo = dutyNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCompanyTel() {
		return companyTel;
	}
	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankNo() {
		return bankNo;
	}
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCheckoutTime() {
		return checkoutTime;
	}
	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
}
