package com.umessage.hotel.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 早餐订单
 * 
 * @author xiajh
 *
 */
@ApiModel(value = "早餐订单", description = "早餐订单")
public class OrderBKFModel implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "有效期开始")
    private String validStart;

	@ApiModelProperty(value = "有效期结束")
    private String validEnd;
	
	@ApiModelProperty(value = "总数量")
	private Integer amount;
	
	@ApiModelProperty(value = "每天数量")
    private Integer numDaily;
	
	@ApiModelProperty(value = "表单formId，用于发送模板消息")
    private String formId;

	public String getValidStart() {
		return validStart;
	}

	public void setValidStart(String validStart) {
		this.validStart = validStart;
	}

	public String getValidEnd() {
		return validEnd;
	}

	public void setValidEnd(String validEnd) {
		this.validEnd = validEnd;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getNumDaily() {
		return numDaily;
	}

	public void setNumDaily(Integer numDaily) {
		this.numDaily = numDaily;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
	

}
