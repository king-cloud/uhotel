package com.umessage.hotel.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "订单参数对象")
public class OrderParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "订单总计", required = true)
	private BigDecimal totalprice;
	@ApiModelProperty(value = "折扣价格", required = true)
	private BigDecimal discountPrice;
	@ApiModelProperty(value = "使用余额", required = true)
	private BigDecimal useBalance;
	@ApiModelProperty(value = "应付金额", required = true)
	private BigDecimal orderAmount;
	@ApiModelProperty(value = "支付类型", example = "1到店支付、2担保支付、3全额支付", required = true)
	private Integer payType;
	@ApiModelProperty(value = "支付方式", example = "1余额、2微信、3到店付、4混合付", required = true)
	private Integer payWay;
	@ApiModelProperty(value = "联系手机", required = true)
	private String mobile;
	@ApiModelProperty(value = "电子邮箱")
	private String email;
	@ApiModelProperty(value = "入住人", required = true)
	private String resident;
	@ApiModelProperty(value = "房间id", required = true)
	private String roomId;
	@ApiModelProperty(value = "房间数", required = true)
	private Integer roomCount;
	@ApiModelProperty(value = "用户等级", hidden = true)
	private String userGrade;
	@ApiModelProperty(value = "礼金券id")
	private String coupon_id;
	@ApiModelProperty(value = "下单时间", hidden = true)
	private String bookTime;
	@ApiModelProperty(value = "入住时间", required = true)
	private String checkinTime;
	@ApiModelProperty(value = "离店时间", required = true)
	private String checkoutTime;
	@ApiModelProperty(value = "备注")
	private String remark;
	@ApiModelProperty(value = "表单formId，用于发送模板消息")
	private String formId;
	@ApiModelProperty(value="是否可以取消" )
	private String notCancle;
	@ApiModelProperty(value="房间名称" ,required=true)
	private String name;

	public BigDecimal getTotalprice() {
		return totalprice;
	}

	public void setTotalprice(BigDecimal totalprice) {
		this.totalprice = totalprice;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public BigDecimal getUseBalance() {
		return useBalance;
	}

	public void setUseBalance(BigDecimal useBalance) {
		this.useBalance = useBalance;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Integer getPayWay() {
		return payWay;
	}

	public void setPayWay(Integer payWay) {
		this.payWay = payWay;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getResident() {
		return resident;
	}

	public void setResident(String resident) {
		this.resident = resident;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public Integer getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	public String getUserGrade() {
		return userGrade;
	}

	public void setUserGrade(String userGrade) {
		this.userGrade = userGrade;
	}

	public String getCoupon_id() {
		return coupon_id;
	}

	public void setCoupon_id(String coupon_id) {
		this.coupon_id = coupon_id;
	}

	public String getBookTime() {
		return bookTime;
	}

	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}

	public String getCheckinTime() {
		return checkinTime;
	}

	public void setCheckinTime(String checkinTime) {
		this.checkinTime = checkinTime;
	}

	public String getCheckoutTime() {
		return checkoutTime;
	}

	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getNotCancle() {
		return notCancle;
	}

	public void setNotCancle(String notCancle) {
		this.notCancle = notCancle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
