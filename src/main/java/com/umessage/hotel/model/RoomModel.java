package com.umessage.hotel.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "房间信息", description = "酒店房间信息")
public class RoomModel {

	@ApiModelProperty(value = "酒店id")
	private String hotelId;
	@ApiModelProperty(value = "房间id")
	private String roomId;
	@ApiModelProperty(value = "房间名称")
	private String name;
	@ApiModelProperty(value = "房间类型", example="具体酒店不一样")
	private String roomType;
	@ApiModelProperty(value = "付款类型")
	private String payType;// 1到店支付、2担保支付、3全额支付
	@ApiModelProperty(value = "简介")
	private String summary;
	@ApiModelProperty(value = "入住人数上限")
	private int residentLimit;
	@ApiModelProperty(value = "原始价格")
	private BigDecimal originalPrice;
	@ApiModelProperty(value = "折扣价格")
	private BigDecimal discountPrice;
	@ApiModelProperty(value = "房间设施")
	private Map<String, String> facilities;
	@ApiModelProperty(value = "房间政策")
	private Map<String, String> policies;
	@ApiModelProperty(value = "是否可以预定")
	private String bookable;
	@ApiModelProperty(value = "房间图片")
	private List<PictureModel> roomPics;
	@ApiModelProperty(value = "不可取消政策")
	private String notCancle;
	@ApiModelProperty(value = "早餐政策 1、不含早餐 2、免费单早餐3、免费双早餐")
	private String polBreakfast;

	public String getPolBreakfast() {
		return polBreakfast;
	}

	public void setPolBreakfast(String polBreakfast) {
		this.polBreakfast = polBreakfast;
	}
	
	public String getNotCancle() {
		return notCancle;
	}

	public void setNotCancle(String notCancle) {
		this.notCancle = notCancle;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getResidentLimit() {
		return residentLimit;
	}

	public void setResidentLimit(int residentLimit) {
		this.residentLimit = residentLimit;
	}

	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Map<String, String> getFacilities() {
		return facilities;
	}

	public void setFacilities(Map<String, String> facilities) {
		this.facilities = facilities;
	}

	public Map<String, String> getPolicies() {
		return policies;
	}

	public void setPolicies(Map<String, String> policies) {
		this.policies = policies;
	}

	public String getBookable() {
		return bookable;
	}

	public void setBookable(String bookable) {
		this.bookable = bookable;
	}

	public List<PictureModel> getRoomPics() {
		return roomPics;
	}

	public void setRoomPics(List<PictureModel> roomPics) {
		this.roomPics = roomPics;
	}

}
