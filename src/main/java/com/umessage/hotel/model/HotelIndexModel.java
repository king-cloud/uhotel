package com.umessage.hotel.model;

import java.math.BigDecimal;

public class HotelIndexModel {

	//新增用户 
	private int userCount; 
	//新增会员用户
	private int vipCount;
	//成交订单数
	private int orderCount;
	//充值金额数
	private BigDecimal depositMoney;
	//总收入
	private BigDecimal totalIncome;
	public int getUserCount() {
		return userCount;
	}
	public void setUserCount(int userCount) {
		this.userCount = userCount;
	}
	public int getVipCount() {
		return vipCount;
	}
	public void setVipCount(int vipCount) {
		this.vipCount = vipCount;
	}
	public int getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}
	public BigDecimal getDepositMoney() {
		return depositMoney == null ? new BigDecimal("0.00"):depositMoney;
	}
	public void setDepositMoney(BigDecimal depositMoney) {
		this.depositMoney = depositMoney;
	}
	public BigDecimal getTotalIncome() {
		return totalIncome == null ? new BigDecimal("0.00"):totalIncome;
	}
	public void setTotalIncome(BigDecimal totalIncome) {
		this.totalIncome =totalIncome;
	}
}
