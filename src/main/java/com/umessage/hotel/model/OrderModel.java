package com.umessage.hotel.model;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "订单信息")
public class OrderModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "订单id")
	private String orderId;
	@ApiModelProperty(value = "订单号")
	private String orderSn;
	@ApiModelProperty(value = "酒店id")
	private Integer hotelId;
	@ApiModelProperty(value = "会员id")
	private String memberId;
	@ApiModelProperty(value = "订单总计")
	private BigDecimal totalPrice;
	@ApiModelProperty(value = "折扣价格")
	private BigDecimal discountPrice;
	@ApiModelProperty(value = "已付金额")
	private BigDecimal moneyPaid;
	@ApiModelProperty(value = "应付金额")
	private BigDecimal orderAmount;
	@ApiModelProperty(value = "支付状态")
	private Integer payStatus;
	@ApiModelProperty(value = "支付类型", example = "1到店支付、2担保支付、3全额支付")
	private Integer payType;
	@ApiModelProperty(value = "支付方式", example = "1余额、2微信、3到店付、4混合付")
	private Integer payWay;
	@ApiModelProperty(value = "联系手机")
	private String mobile;
	@ApiModelProperty(value = "电子邮箱")
	private String email;
	@ApiModelProperty(value = "入住人")
	private String resident;
	@ApiModelProperty(value = "房间数")
	private Integer roomCount;
	@ApiModelProperty(value = "用户等级", hidden = true)
	private String userGrade;
	@ApiModelProperty(value = "礼金券id")
	private Integer couponId;
	@ApiModelProperty(value = "下单时间")
	private String bookTime;
	@ApiModelProperty(value = "入住时间")
	private String checkinTime;
	@ApiModelProperty(value = "离店时间")
	private String checkoutTime;
	@ApiModelProperty(value = "订单状态")
	private String orderStatus;
	@ApiModelProperty(value = "备注")
	private String remark;
	@ApiModelProperty(value = "是否开票")
	private String invoiceFlag;
	@ApiModelProperty(value = "开票状态")
	private String invoiceStatus;
	@ApiModelProperty(value = "开票时间")
	private String invoiceTime;
	@ApiModelProperty(value = "取消时间")
	private String cancelTime;
	@ApiModelProperty(value = "取消人")
	private String cancelPerson;
	@ApiModelProperty(value = "取消类型", example = "1商家或2用户取消")
	private Integer cancelType;
	@ApiModelProperty(value = "取消原因")
	private String cancelReason;
	@ApiModelProperty(value = "确认时间")
	private String confirmTime;
	@ApiModelProperty("优惠券金额")
	private String couponMoney;
	@ApiModelProperty("确认人")
	private String confirmPerson;
//	@ApiModelProperty(value = "确认人")
	private String confirmAccount;
	@ApiModelProperty("房间信息")
	private RoomModel roomInfo;
	@ApiModelProperty("操作人:员工名称")
	private String staffName;
	
	@ApiModelProperty("是否可以取消" )
	private String notCancle;
	@ApiModelProperty("房间名称")
	private String name;
	
	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	@Transient
	public Integer getHotelId() {
		return hotelId;
	}

	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}

	public BigDecimal getMoneyPaid() {
		return moneyPaid;
	}

	public void setMoneyPaid(BigDecimal moneyPaid) {
		this.moneyPaid = moneyPaid;
	}

	public BigDecimal getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCheckinTime() {
		return checkinTime;
	}

	public void setCheckinTime(String checkinTime) {
		this.checkinTime = checkinTime;
	}

	public String getCheckoutTime() {
		return checkoutTime;
	}

	public void setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getInvoiceFlag() {
		return invoiceFlag;
	}

	public void setInvoiceFlag(String invoiceFlag) {
		this.invoiceFlag = invoiceFlag;
	}

	@Transient
	public String getInvoiceTime() {
		return invoiceTime;
	}

	public void setInvoiceTime(String invoiceTime) {
		this.invoiceTime = invoiceTime;
	}

	public String getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(String cancelTime) {
		this.cancelTime = cancelTime;
	}

	public String getCancelPerson() {
		return cancelPerson;
	}

	public void setCancelPerson(String cancelPerson) {
		this.cancelPerson = cancelPerson;
	}

	public Integer getCancelType() {
		return cancelType;
	}

	public void setCancelType(Integer cancelType) {
		this.cancelType = cancelType;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getConfirmTime() {
		return confirmTime;
	}

	public void setConfirmTime(String confirmTime) {
		this.confirmTime = confirmTime;
	}

	public String getConfirmAccount() {
		return confirmAccount;
	}

	public void setConfirmAccount(String confirmAccount) {
		this.confirmAccount = confirmAccount;
	}

	public String getResident() {
		return resident;
	}

	public void setResident(String resident) {
		this.resident = resident;
	}

	public int getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(int roomCount) {
		this.roomCount = roomCount;
	}

	public String getUserGrade() {
		return userGrade;
	}

	public void setUserGrade(String userGrade) {
		this.userGrade = userGrade;
	}

	public String getBookTime() {
		return bookTime;
	}

	public void setBookTime(String bookTime) {
		this.bookTime = bookTime;
	}

	public RoomModel getRoomInfo() {
		return roomInfo;
	}

	public void setRoomInfo(RoomModel roomInfo) {
		this.roomInfo = roomInfo;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Integer getPayWay() {
		return payWay;
	}

	public void setPayWay(Integer payWay) {
		this.payWay = payWay;
	}

	public Integer getCouponId() {
		return couponId;
	}

	public void setCouponId(Integer couponId) {
		this.couponId = couponId;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public void setRoomCount(Integer roomCount) {
		this.roomCount = roomCount;
	}

	public String getCouponMoney() {
		return couponMoney;
	}

	public void setCouponMoney(String couponMoney) {
		this.couponMoney = couponMoney;
	}

	public String getConfirmPerson() {
		return confirmPerson;
	}

	public void setConfirmPerson(String confirmPerson) {
		this.confirmPerson = confirmPerson;
	}

	public String getNotCancle() {
		return notCancle;
	}

	public void setNotCancle(String notCancle) {
		this.notCancle = notCancle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
