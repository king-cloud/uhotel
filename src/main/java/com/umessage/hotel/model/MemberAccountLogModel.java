package com.umessage.hotel.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "消费记录", description = "消费记录")
public class MemberAccountLogModel {
	@ApiModelProperty(value = "id")
	private String id;

	@ApiModelProperty(value = "消费类型", example = "1微信支付2余额支付3充值4赠送")
	private String type;

	@ApiModelProperty(value = "支付状态", example = "1支付成功，2支付失败")
	private String payStatus;

	@ApiModelProperty(value = "付款方式")
	private String payMode;

	@ApiModelProperty(value = "金额")
	private BigDecimal money;

	@ApiModelProperty(value = "付款时间")
	private String payTime;

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getPayTime() {
		return payTime;
	}

	public void setPayTime(String payTime) {
		this.payTime = payTime;
	}

}