package com.umessage.hotel.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "会员信息", description = "会员信息")
public class MemberModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "会员ID")
	private String memberId;
	@ApiModelProperty(value = "会员名称")
	private String memberName;
	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "会员状态")
	private String status;
	@ApiModelProperty(value = "所属国家")
	private String country;
	@ApiModelProperty(value = "所属省")
	private String province;
	@ApiModelProperty(value = "所属市")
	private String city;
	@ApiModelProperty(value = "所属区")
	private String district;
	@ApiModelProperty(value = "语言")
	private String language;
	@ApiModelProperty(value = "性别")
	private String sex;
	@ApiModelProperty(value = "昵称")
	private String nickName;
	@ApiModelProperty(value = "头像")
	private String headImgUrl;
	@ApiModelProperty(value = "会员编号")
	private String userNo;
	@ApiModelProperty(value = "酒店id")
	private String hotelId;
	@ApiModelProperty(value = "会员级别")
	private String grade;
	@ApiModelProperty(value = "实际余额")
	private BigDecimal realMoney;
	@ApiModelProperty(value = "用户积分")
	private BigDecimal credit;
	@ApiModelProperty(value = "虚拟余额")
	private BigDecimal virtMoney;
	@ApiModelProperty(value = "优惠类型（1直减、2折扣）")
	private String discountType;
	@ApiModelProperty(value = "优惠")
	private String discount;
	@ApiModelProperty(value = "是否可以通知")
	private String notifiable;
	@ApiModelProperty(value = "最后登录时间")
	private String lastLoginTime;
	@ApiModelProperty(value = "注册时间")
	private String regTime;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public BigDecimal getRealMoney() {
		return realMoney;
	}

	public void setRealMoney(BigDecimal realMoney) {
		this.realMoney = realMoney;
	}

	public BigDecimal getVirtMoney() {
		return virtMoney;
	}

	public void setVirtMoney(BigDecimal virtMoney) {
		this.virtMoney = virtMoney;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getNotifiable() {
		return notifiable;
	}

	public void setNotifiable(String notifiable) {
		this.notifiable = notifiable;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

}
