package com.umessage.hotel.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "会话信息", description = "登录会话信息")
public class SessionModel {

	@ApiModelProperty(value = "会话标示")
	private String sessionId;

	@ApiModelProperty(value = "会话类型")
	private String type;

	public SessionModel(String sessionId, String type) {
		super();
		this.sessionId = sessionId;
		this.type = type;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
