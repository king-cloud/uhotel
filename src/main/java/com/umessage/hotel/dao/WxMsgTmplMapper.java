package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.WxMsgTmpl;

public interface WxMsgTmplMapper extends MyMapper<WxMsgTmpl> {
	
	/**
	 * 获取酒店小程序模版
	 * @param hotelId
	 * @param appid
	 * @param templateTitleId
	 * @return
	 */
	public WxMsgTmpl findHotelMiniProgramMsgTmpl(String hotelId, String appid, String templateTitleId);
	
}