package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberAccountLog;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.UserMenu;

public interface MemberInfoMapper extends MyMapper<MemberInfo> {
	
	public MemberInfo getMemberInfoForUpdate(String memberId);
	
	public MemberInfo findMemberByMobile(String hotelId, String mobile);
	
	public Integer countNewMember(String hotelId, String grade, String startTime, String endTime);
	
	public Integer countNewMemberByDate(String hotelId, String grade, String startTime, String endTime);

	public List<MemberInfo> findUMList(MemberInfo mem);

	public Integer getCountUser(String hotelId);

	public Integer getCountNewUser(String hotelId);

	public void saveMember(MemberInfo memberInfo);
	
	public Integer getCountUserByDate(String hotelId, String startTime, String endTime);
	
	public Integer getCountNewMemberByDate(String hotelId, String grade, String startTime, String endTime);

	public UserMenu getUserMenu(Integer userid);

	public List<MemberAccountLog> getConsumingRecordByUser(String hotelId, String memberId,String orderId,String payStatus);

	public void insertConsumingRecord(MemberAccountLog memberAccountLog);

	public Integer getCountUserSum(String hotelId);

	public Integer getCountMemberSum(String hotelId);

}