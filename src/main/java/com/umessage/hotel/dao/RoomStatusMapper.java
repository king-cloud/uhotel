package com.umessage.hotel.dao;

import java.util.List;
import java.util.Map;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.RoomStatus;

public interface RoomStatusMapper extends MyMapper<RoomStatus> {

	public List<RoomStatus> findRoomStatusByTime(Map<String,Object> map);
	
	public List<RoomStatus> findRoomStatusByTimeAdmin(Map<String,Object> map);

	public List<String> findRoomIdsNotBook(Map<String, Object> map);
}