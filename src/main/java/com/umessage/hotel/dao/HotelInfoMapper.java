package com.umessage.hotel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.HotelInfo;

public interface HotelInfoMapper extends MyMapper<HotelInfo> {
	
	public void saveHotel(HotelInfo hotel);

	public void updateHotels(List<String> sl);
	
	public List<HotelInfo> queryAllHotels();
	
	public HotelInfo findHotelsById(String hotelId);
	
	public HotelInfo queryHotelsByHotelId(String hotelId);
}