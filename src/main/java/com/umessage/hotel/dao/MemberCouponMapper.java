package com.umessage.hotel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberCoupon;

public interface MemberCouponMapper extends MyMapper<MemberCoupon> {

	public Integer getSumCount();
	
	public Integer getUseCount();
	
	public Integer getSumCountByUserId(@Param("memberId")String memberId, @Param("hotelId")String hotelId, @Param("couponType")String couponType);

	public List<MemberCoupon> findCoupons(@Param("memberId")String memberId, @Param("hotelId")String hotelId, @Param("couponType")String couponType);

	public List<MemberCoupon> findCouponByUser(String memberId, String hotelId, String couponId,String couponType);

	public void updateStatus(String memberId, Integer hotelId, Integer couponId, String couponType,String status);
}