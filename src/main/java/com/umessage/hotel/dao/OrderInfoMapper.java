package com.umessage.hotel.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.QueryVo2OrderInfo;

public interface OrderInfoMapper extends MyMapper<OrderInfo> {

	public OrderInfo findSingleOrder(@Param("member_id") String memberId, @Param("order_id") String orderId);

	public Integer countOrders(@Param("hotel_id") String hotel, @Param("order_status") String orderStatus,
			String startTime, String endTime);
	
	public BigDecimal countOrdersIncome(@Param("hotel_id") String hotel, String startTime, String endTime);
	
	public Integer countEffectiveOrders(@Param("hotel_id") String hotel, String startTime, String endTime);

	public BigDecimal countEffectiveOrdersIncome(@Param("hotel_id") String hotel, String startTime, String endTime);
	
	public Integer saveOrderInfo(OrderInfo orderInfo);

	public ArrayList<OrderInfo> findOrderList(QueryVo2OrderInfo queryVo2OrderInfo);

	public List<OrderInfo> findUserOrderListByUnCheckIn(OrderInfo orderInfo);
	
	public Integer countRepeatedPurchaseUsers(String hotelId, String orderStatus, Integer repeatCount);

	public void cancelOrderByAutoTask(String beginTime);

	public Integer countWaitOrder(String hoteld, String orderStatus, String payStatus);

	public List<OrderInfo> findCancleOrderListSql(@Param("hotelId")String hotelId, @Param("orderStatus")String orderStatus, @Param("page") int page,@Param("rows") int rows);
	
	public List<OrderInfo> findConfirmOrderListSql(@Param("hotelId")String hotelId, @Param("orderStatus")String orderStatus, @Param("page") int page,@Param("rows") int rows);
	
	public Integer updateByOrderStatusSelective(String orderStatus,String startTime,String endTime,String oldStatus);
	
	public Integer updateByOrderSnSelective(OrderInfo orderInfo);

	public List<OrderInfo> findHotelOrderAll(String hotelId);

	public Integer updateByOrderStatusOver(String orderStatus, String startTime, String endTime, String oldStatus);
	public Integer updateByOrderStatusOverByHotelId(String orderStatus, String startTime, String endTime, String oldStatus, String hotelId);

	public List<OrderInfo> findOrderToDay(String oldStatus, String startTime, String endTime);
	
	public List<OrderInfo> findOrderToDayByHotelId(String oldStatus, String startTime, String endTime, String hotelId);

	public List<OrderInfo> select30TimeoutOrder(String beginTime);

	public List<OrderInfo> select12hTimeoutOrder(String beginTime);
	
	public List<OrderInfo> select12hTimeoutOrderByHotelId(String beginTime, String hotelId);

	public BigDecimal countMemberOrdersIncome(String memberId, String hotelId);

	//根据payStatus和orderStatus查询订单数量
	public Integer countMemberTypeOrders(String hotelId, String memberId, String payStatus, String orderStatus, String startTime,
			String endTime);
	//根据待确认查询订单数量
	public Integer countMemberConfirmedOrders(String hotelId, String memberId, String payStatus, String orderStatus, String startTime,
			String endTime);
	//根据订单状态查询订单数量
	public Integer countMemberOrders(String hotelId, String memberId, String orderStatus, String startTime,
			String endTime);
}



