package com.umessage.hotel.dao;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.WxCfg;

public interface WxCfgMapper extends MyMapper<WxCfg> {

	WxCfg queryWxCfg(@Param("hotelId") String hotelId, @Param("appType") String appType);

}