package com.umessage.hotel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.HotelCrontab;

public interface HotelCrontabMapper extends MyMapper<HotelCrontab> {

	List<HotelCrontab> findHotelCrontabs(@Param("status")Integer status);

	HotelCrontab getHotelCrontabById(Integer id);

	void updateHotelCrontabRunning(Integer id, Integer running);

	void updateHotelContabTimes(HotelCrontab hc);

	List<HotelCrontab> findHotelCrontabsByHotelId(Integer hotelId);
	
}