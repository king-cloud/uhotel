package com.umessage.hotel.dao;

import com.umessage.hotel.domain.InvoiceApplication;

public interface InvoiceApplicationMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InvoiceApplication record);

    int insertSelective(InvoiceApplication record);

    InvoiceApplication selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InvoiceApplication record);
    
    int updateByTitleIdSelective(InvoiceApplication record);

    int updateByPrimaryKey(InvoiceApplication record);
    
    InvoiceApplication getInvoiceApplicationStatus(String orderId);
}