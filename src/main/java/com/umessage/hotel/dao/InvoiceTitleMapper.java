package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.hotel.domain.InvoiceTitle;

public interface InvoiceTitleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(InvoiceTitle record);

    int insertSelective(InvoiceTitle record);

    InvoiceTitle selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(InvoiceTitle record);

    int updateByPrimaryKey(InvoiceTitle record);
    
    List<InvoiceTitle> getInvoiceTitleInfo(String memberId);
    
    int updateByTitleIdSelective(InvoiceTitle record);
    
    int  deleteByTitleId(String titleId);
}