package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.OrderPms;

public interface OrderPmsMapper extends MyMapper<OrderPms> {

	OrderPms getorderPmsByorderpmsAndHotelId(String hotelId, String orderId);

	List<OrderPms> getrderListOrderPms(String hotelId, String order);
}