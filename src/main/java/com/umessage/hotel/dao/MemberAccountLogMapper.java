package com.umessage.hotel.dao;

import java.math.BigDecimal;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberAccountLog;

public interface MemberAccountLogMapper extends MyMapper<MemberAccountLog> {

	BigDecimal countRealAndVirtMonry(String hotelId, String startTime, String endTime);
}