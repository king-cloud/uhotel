package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.DiningRoom;

public interface DiningRoomMapper extends MyMapper<DiningRoom> {
}