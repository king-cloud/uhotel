package com.umessage.hotel.dao;


import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.OrderBreakfast;

public interface OrderBreakfastMapper extends MyMapper<OrderBreakfast> {
	
	void addOrderBreakfast(OrderBreakfast obf);

	List<OrderBreakfast> getOrderBKFCoupons(OrderBreakfast obf);

	void delCouponsByCon(OrderBreakfast obf);
}