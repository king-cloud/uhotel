package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.UserMenu;

public interface UserMenuMapper extends MyMapper<UserMenu> {

	List<UserMenu> getUserMeList(String userid);

	Integer del(String userid);
}