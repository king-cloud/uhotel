package com.umessage.hotel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberInfo;
import com.umessage.hotel.domain.WXUserMember;

public interface WXUserMemberMapper extends MyMapper<WXUserMember> {

	public List<WXUserMember> findByWXUserId(String wxUserId, String memberId);

	public List<WXUserMember> findByMemberId(String memberId);

	public Integer getCountNewMemberByDate(String hotelId, String startTime, String endTime);

	public WXUserMember findByhotelIdAndWxUserId(String hotelId, String wxUserId);

	public Integer getCountUserSum(String hotelId);

	public List<WXUserMember> findUMemberList(WXUserMember member);

}