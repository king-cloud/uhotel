package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberInvoice;

public interface MemberInvoiceMapper extends MyMapper<MemberInvoice> {
}