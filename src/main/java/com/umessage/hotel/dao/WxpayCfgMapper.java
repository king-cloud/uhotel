package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.WxpayCfg;

public interface WxpayCfgMapper extends MyMapper<WxpayCfg> {
	
	WxpayCfg findByAppId(String appId);
	
}