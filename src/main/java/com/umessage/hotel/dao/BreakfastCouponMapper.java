package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.BreakfastCoupon;

public interface BreakfastCouponMapper extends MyMapper<BreakfastCoupon> {

	List<BreakfastCoupon> findBreakfastCouponsHistory(BreakfastCoupon coupon);

	List<BreakfastCoupon> findBreakfastCouponsToday(BreakfastCoupon coupon);
	
	Integer countNumTodayByType(BreakfastCoupon coupon);
	
	Integer countCheckToday(BreakfastCoupon coupon);

	Integer countUseToday(BreakfastCoupon coupon);
	
}