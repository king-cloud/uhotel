package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberBase;

public interface MemberBaseMapper extends MyMapper<MemberBase> {
}