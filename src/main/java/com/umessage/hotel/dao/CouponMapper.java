package com.umessage.hotel.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.Coupon;

public interface CouponMapper extends MyMapper<Coupon> {

	List<Coupon> findCouponsByWXUser(@Param("userId")String userId,
			@Param("hotelId") String hotelId,@Param("couponType") String couponType);

	Integer getCountCoupons(String hotelId, String nowTime);

	Coupon getCouponById(Coupon coupon);
}