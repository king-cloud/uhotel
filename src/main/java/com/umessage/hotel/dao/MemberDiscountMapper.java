package com.umessage.hotel.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.MemberDiscount;

public interface MemberDiscountMapper extends MyMapper<MemberDiscount> {

	List<MemberDiscount> selectByHotelId(String hotelId);

	List<MemberDiscount> selectMemberByberSwitch(MemberDiscount member);
}