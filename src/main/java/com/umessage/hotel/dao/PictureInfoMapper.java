package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.PictureInfo;

public interface PictureInfoMapper extends MyMapper<PictureInfo> {
}