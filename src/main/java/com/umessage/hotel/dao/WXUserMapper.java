package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.WXUser;

public interface WXUserMapper extends MyMapper<WXUser> {
	
	public WXUser findUserByOpenid(String openId);
	
}