package com.umessage.hotel.dao;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;
import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.DepositOrder;

public interface DepositOrderMapper extends MyMapper<DepositOrder> {
	
	public BigDecimal countDepositOrderRechargeAmount(@Param("hotel_id") String hotel, String startTime, String endTime);
	
	public BigDecimal countDepositOrderVirtMoney(@Param("hotel_id") String hotel, String startTime, String endTime);

	BigDecimal taotalPay(String hotelId, String startTime, String endTime);
}
