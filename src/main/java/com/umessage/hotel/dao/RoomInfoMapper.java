package com.umessage.hotel.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.RoomInfo;

public interface RoomInfoMapper extends MyMapper<RoomInfo> {

	void saveRoom(RoomInfo roomInfo);

	String getRoomTypeById(Integer room_id);
}