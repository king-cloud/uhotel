package com.umessage.hotel.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.hotel.domain.BreakfastRecord;

public interface BreakfastRecordMapper extends MyMapper<BreakfastRecord> {
	
	void saveBreakfastRecord(BreakfastRecord record);
	
	List<BreakfastRecord> findRecords(Map<String,String> map);
}