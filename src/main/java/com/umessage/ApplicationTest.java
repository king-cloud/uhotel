package com.umessage;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.shiro.codec.Base64;
import org.hamcrest.number.BigDecimalCloseTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.umessage.common.util.Atools;
import com.umessage.common.util.SimpleSmsUtil;
import com.umessage.hotel.controller.ScheduledController;
import com.umessage.hotel.domain.BreakfastCoupon;
import com.umessage.hotel.domain.BreakfastRecord;
import com.umessage.hotel.domain.DiningRoom;
import com.umessage.hotel.domain.MemberAccountLog;
import com.umessage.hotel.domain.OrderBreakfast;
import com.umessage.hotel.domain.OrderInfo;
import com.umessage.hotel.domain.UserMenu;
import com.umessage.hotel.domain.WXUser;
import com.umessage.hotel.service.BreakfastCouponService;
import com.umessage.hotel.service.CouponService;
import com.umessage.hotel.service.DepositOrderService;
import com.umessage.hotel.service.DiningRoomService;
import com.umessage.hotel.service.MemberAccountLogService;
import com.umessage.hotel.service.MemberInfoService;
import com.umessage.hotel.service.MemberInvoiceService;
import com.umessage.hotel.service.OrderBreakfastService;
import com.umessage.hotel.service.OrderInfoService;
import com.umessage.hotel.service.RoomInfoService;
import com.umessage.hotel.service.UserMemberService;
import com.umessage.hotel.service.WXUserMemberService;
import com.umessage.hotel.service.WXUserService;
import com.umessage.hotel.service.WxMsgTmplService;
import com.umessage.hotel.util.HotelConstant;
import com.umessage.system.domain.User;
import com.umessage.system.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTest {
	@Autowired
	private DiningRoomService diningRoomService;
	@Autowired
	private OrderBreakfastService orderBreakfastService;
	@Autowired
	private BreakfastCouponService breakfastCouponService;
	@Autowired
	private OrderInfoService orderInfoService;
	@Autowired
	private CouponService couponService;
	@Autowired
	private DepositOrderService depositOrderService;
	@Autowired
	private UserService userService;
	@Autowired
	private WXUserMemberService wxUserMemberService;
	@Autowired
	private WxMsgTmplService wxMsgTmplService;
	@Autowired
	private UserMemberService userMemberService;
	@Autowired
	private RoomInfoService roomInfoService;
	@Autowired
	private SimpleSmsUtil simpleSmsUtil;
	
	@Autowired
	private MemberInfoService memberInfoService;
	
	@Autowired 
	private ScheduledController scheduledController;
	
	@Autowired
	private MemberAccountLogService MemberAccountLogService;
	
	@Value("${hotel.hotelId}")
	private String hoteId;

	@Test
	public void test() {
		User user = this.userService.findByName("mrbird");
		System.out.println(user.getUsername());
	}

	@Test
	public void saveOrder() {

		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setOrderId(Atools.getOneStr());
		orderInfo.setMemberId("2b328f05-60fc-48b5-ace6-1fd0d4828670");
		orderInfo.setHotelId(Integer.parseInt(hoteId));
		orderInfo.setRoomId(Integer.valueOf(23));
		orderInfo.setTotalPrice(BigDecimal.valueOf(0.01));
		orderInfo.setBookTime(new Date());
		orderInfo.setPayStatus(0);// 未支付
		orderInfo.setPayType(HotelConstant.ORDER_PAY_TYPE_ONLINE);
		orderInfo.setPayWay(1);
		orderInfo.setOrderStatus("0");// 待确认
		orderInfo.setResident("老米");
		orderInfo.setCheckinTime("2018-09-09");
		orderInfo.setCheckoutTime("2018-09-10");
		orderInfo.setMobile("18701637429");
		orderInfo.setRoomId(23);
		orderInfo.setRoomCount(1);
		Integer r = orderInfoService.saveNewOrder(orderInfo);
		System.out.println(r);
	}

	@Test
	public void cancelOrder() {

		orderInfoService.cancelOrder("999", HotelConstant.OPERATOR_TYPE_MEMBER, "123456444433",
				"188dc5c6d8514a83b3e034b19f8c93ca", false, "行程有变化");
		orderInfoService.cancelOrder("999", HotelConstant.OPERATOR_TYPE_HOTEL, "123456444433",
				"2bee0e1970d3479dad8ca62474ce3472", false, "行程有变化");
	}
	
	@Test
	public void refundRequest() {
		try {
//			String bookTime = "2018-11-05 23:59:59"; //修改时间
			//查询酒店今日非新建的所有订单
//			List<OrderInfo> list =orderInfoService.findHotelOrderAll(hoteId,bookTime);
//			List<MemberAccountLog> consumingRecordByUser = memberInfoService.getConsumingRecordByUser(hoteId, null, null, null);
//			System.out.println(consumingRecordByUser);
//			for (MemberAccountLog memberAccountLog : consumingRecordByUser) {
//				orderInfoService.refundRequest("999", memberAccountLog.getOrderId(),memberAccountLog.getRealMoney().intValue()*100, memberAccountLog.getRealMoney().intValue()*100);
				
//			}
			//进行订单退款
			/*if( null != list && list.size()>0){
				for (OrderInfo orderInfo : list) {
					if( !orderInfo .getTotalPrice() .equals(BigDecimal.ZERO)){
						orderInfoService.refundRequest("999", orderInfo.getOrderId(),orderInfo.getTotalPrice().intValue()*100, orderInfo.getTotalPrice().intValue()*100);
					}
				}
			}*/
		} catch (Exception e) {
		}
	}
	//单个订单退款
	@Test
	public void returnMonry(){
		orderInfoService.refundRequest("999", "", 200, 200);//取
		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "1a978f62417d4af8956207dbc9d21ca5", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "928a9195a42d4894b274ee0d4974fdb0", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "f1b2093a7e1743f7a30437216f126270", 200, 200);//取消单个订单
//		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
//		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
//		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
//		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
//		orderInfoService.refundRequest("999", "", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "5ebd5f27affe4869b362ce2c490be0a1", 200, 200);//取消单个订单
		orderInfoService.refundRequest("999", "e800ac9452df4125a7469493d64644fb", 200, 200);//取消单个订单
	}

	@Test
	public void saveDepositOrder() {

		depositOrderService.saveDepositOrder(hoteId, "2b328f05-60fc-48b5-ace6-1fd0d4828670", 1,
				BigDecimal.valueOf(12.3));

	}

	@Test
	public void depositMemberAccount() {
		depositOrderService.depositMemberAccount("c6cb826a5c764b45967fe63530d61504");
	}

	@Test
	public void createUnifiedOrder() {
		String orderId = "b8ae59bc72ef432da38ba02ac7d25eaf";
		String notifyUrl = "http://miniprogram.umessage.com.cn/uhotel/rest/orders/9/" + orderId + "/payNotify";
		depositOrderService.createUnifiedOrder("9", "oLiYB5QaLfMxQ33i3tmJ2VW42DtY", "充值", orderId, null, null,
				notifyUrl, BigDecimal.valueOf(0.01));
	}
	
	@Test
	public void findByWXUserId() {
		wxUserMemberService.findByWXUserId("94a1f11d-27ce-4702-881c-b820e82eef8e", "2b328f05-60fc-48b5-ace6-1fd0d4828670");
	}
	
	@Test
	public void getTemplateList() {
		wxMsgTmplService.getTemplateLibraryList("hotel_test", "01", null);
	}
	
	@Test
	public void sendOrderPaySuccessMsg() {
		OrderInfo order = orderInfoService.selectByKey("cbb250aa921f42148bf35c3e729ed3ba");
		wxMsgTmplService.sendOrderPaySuccessMsg(order);
	}
	
	@Test
	public void prepayOrder() {
		
		orderInfoService.createUnifiedOrder("9", "oLiYB5QaLfMxQ33i3tmJ2VW42DtY", "127.0.0.1", "31153cc34c9540828c154f43123ffe09");
		
	}
	
	@Test
	public void userMenu() {
		List<UserMenu> usermenuList = userMemberService.getUserMeList("190");
		System.out.println(usermenuList.size());
	}
	@Test
	public void brkTest() {
		BreakfastCoupon coupon=new BreakfastCoupon();
		coupon.setValidDate("2018-10-31");
		coupon.setUseStatus("0");//'0未使用1已使用',
		coupon.setType("1");//'1赠送2购买',
		this.breakfastCouponService.addCoupons(coupon);
	}
	@Test
	public void findBkfTest() {
		BreakfastCoupon coupon=new BreakfastCoupon();
		coupon.setValidDate("2018-10-16");
		coupon.setMemberId("228f8a10b19c456a805ebd415668a740");
		//today
		List<BreakfastCoupon> coupons1 = this.breakfastCouponService.findBreakfastCouponsToday(coupon);
		//before
		for (BreakfastCoupon bkf : coupons1) {
			System.out.println(bkf.getId()+"=======================");
			System.out.println(bkf.getMemberId()+"=======================");
			System.out.println(bkf.getValidDate()+"=======================");
			System.out.println(bkf.getUseStatus()+"=======================");
		}
//		List<BreakfastCoupon> coupons2 = this.breakfastCouponService.findBreakfastCouponsAfter(coupon);
//		for (BreakfastCoupon bkf : coupons2) {
//			System.out.println(bkf.getMemberId()+"=======================");
//			System.out.println(bkf.getValidDate()+"=======================");
//			System.out.println(bkf.getUseStatus()+"=======================");
//		}
		/*//history
		List<BreakfastCoupon> coupons3 = this.breakfastCouponService.findBreakfastCouponsHistory(coupon);
		for (BreakfastCoupon bkf : coupons3) {
			System.out.println(bkf.getMemberId()+"=======================");
			System.out.println(bkf.getValidDate()+"=======================");
			System.out.println(bkf.getUseStatus()+"=======================");
		}*/
	}
	@Test
	public void count() {
		BreakfastCoupon coupon=new BreakfastCoupon();
		coupon.setValidDate("2018-10-16");
		coupon.setMemberId("228f8a10b19c456a805ebd415668a740");
		Integer count = this.breakfastCouponService.findBreakfastCouponsTodayCount(coupon);
		System.out.println(count);
	}
	@Test
	public void saveOrderBKF() {
		OrderBreakfast obf=new OrderBreakfast();
		obf.setMemberId("228f8a10b19c456a805ebd415668a740");
		obf.setHotelId(hoteId);
		obf.setValidStart("2018-10-15");
		obf.setValidEnd("2018-10-15");
		obf.setNumDaily(2);
		//this.orderBreakfastService.addOrderBreakfast(obf);
	}
	@Test
	public void diningRoom() {
		DiningRoom dr=new DiningRoom();
		dr.setHotelId(Long.parseLong(hoteId));
		DiningRoom d = this.diningRoomService.findDiningRoom(dr);
		System.out.println(d.getName()+"===============");
		System.out.println(d.getHotelId()+"===============");
	}	
	@Test
	public void useBreakfastCoupon() {
		BreakfastRecord useRecord = new BreakfastRecord();
		useRecord.setMemberId("f3f9da09-e7e1-45d6-914f-04ff65d75ca1");
		useRecord.setValidDate(new Date());
		useRecord.setAmount(3);
		breakfastCouponService.useBreakfastCoupon(useRecord);
	}
	
	@Test
	public void getRoomAvgByRoomId() {
//		roomInfoService.getRoomAvgByRoomId(roomId, hotelId, beginTime, endTime);
	}
	
	@Test
	public void sendSms() {
		OrderInfo order = this.orderInfoService.selectByKey("38074820a09a4cabbe3d2c2207300344");
		wxMsgTmplService.sendOrderPaySuccessMsg(order);
		
//		orderInfoService.sendSmsByOrderId("4", "3bb5642ab1944a999a567ad49fc02b41");
//		simpleSmsUtil.sendMsg("18701637329", "国际大酒店", "测试短信");
	}
	
	@Test
	public void  getConsumingRecordByUser (){
		List<MemberAccountLog> consumingRecordByUser = memberInfoService.getConsumingRecordByUser(null, null, "d8d4f2447f114ce182a78bc7096cfc13","0");
		System.out.println(consumingRecordByUser);
	}
	/**
	 * 房价订单及早餐订单 退款
	 */
	@Test
	public void quitMoneyOrder() {
		orderInfoService.refundRequest("999", "", 600, 600);
	}
	
	
	@Test
	public void cancleOrder(){
		scheduledController.cancleUnPayOrder();
	}
	
	/**
	 * 对t_wxuser name,nickname进行转码
	 */
	@Autowired
	private WXUserService wXUserService;
	@Test
	public void WxUserBase64() throws Exception {
		List<WXUser> wxusers=this.wXUserService.selectAll();
		for (WXUser wxUser : wxusers) {
			if("oLiYB5SIlgZH8qHA_y3sqYGdhViY".equals(wxUser.getOpenid())) {
				continue;
			}
			String nickname=wxUser.getNickName();
			//进行编码
			 nickname =Base64.encodeToString(nickname.getBytes("UTF-8"));
			 wxUser.setNickName(nickname);
			 wxUser.setName(nickname);
			 this.wXUserService.updateNotNull(wxUser);
		}
		 
		 //进行解码
		 //String nickname = new String(Base64.decode(nickname),"UTF-8");
	}
	
	@Autowired 
	private MemberInvoiceService invoiceService;
	@Test
	public void testInvoice(){
		invoiceService.openInvoice(6,123);
	}
	
}
