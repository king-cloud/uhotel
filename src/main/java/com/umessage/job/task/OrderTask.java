package com.umessage.job.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.umessage.hotel.service.OrderInfoService;

@Component("orderTask")
public class OrderTask {
	
	@Autowired
	private OrderInfoService orderInfoService;
	
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 半个小时内未支付的订单改为已取消
	 */
	public void cancelOrder(){
		try {
			long currentTimeMillis = System.currentTimeMillis();
			currentTimeMillis -= 30 * 60 * 1000; //倒退半小时
			Date date = new Date(currentTimeMillis);
			String beginTime = simpleDateFormat.format(date); //格式化时间
			orderInfoService.cancelOrderByAutoTask(beginTime);
			System.out.println("已执行未支付订单取消任务");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("取消订单调度任务失败"+e.getMessage());
		}
	}
	
}
