package com.umessage.job.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.job.domain.Job;

public interface JobMapper extends MyMapper<Job> {
	
	List<Job> queryList();
}