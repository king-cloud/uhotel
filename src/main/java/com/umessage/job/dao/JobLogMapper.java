package com.umessage.job.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.job.domain.JobLog;

public interface JobLogMapper extends MyMapper<JobLog> {
}