package com.umessage.common.aspect;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.umessage.common.annotation.Log;
import com.umessage.common.config.FebsProperies;
import com.umessage.common.util.AddressUtils;
import com.umessage.common.util.HttpContextUtils;
import com.umessage.common.util.IPUtils;
import com.umessage.hotel.model.MemberModel;
import com.umessage.system.domain.SysLog;
import com.umessage.system.domain.User;
import com.umessage.system.service.LogService;

/**
 * AOP 记录用户操作日志
 *
 * @author MrBird
 * @link https://mrbird.cc/Spring-Boot-AOP%20log.html
 */
@Aspect
@Component
public class LogAspect {

    @Autowired
    private FebsProperies febsProperies;

    @Autowired
    private LogService logService;

    @Autowired
    ObjectMapper mapper;

    @Pointcut("@annotation(com.umessage.common.annotation.Log)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) throws JsonProcessingException {
        Object result = null;
        long beginTime = System.currentTimeMillis();
        try {
            // 执行方法
            result = point.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        // 执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;
        if (febsProperies.isOpenAopLog())
            // 保存日志
            saveLog(point, time);
        return result;
    }

    private void saveLog(ProceedingJoinPoint joinPoint, long time) throws JsonProcessingException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        SysLog log = new SysLog();
        Log logAnnotation = method.getAnnotation(Log.class);
        if (logAnnotation != null) {
            // 注解上的描述
            log.setOperation(logAnnotation.value());
        }

        // 请求的类名
        String className = joinPoint.getTarget().getClass().getName();
        // 请求的方法名
        String methodName = signature.getName();
        log.setMethod(className + "." + methodName + "()");
        // 请求的方法参数值
        Object[] args = joinPoint.getArgs();
        // 请求的方法参数名称
        LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
        String[] paramNames = u.getParameterNames(method);
        if (args != null) {
            StringBuilder params = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if(args[i] instanceof Serializable)
                    params.append("  ").append(paramNames[i]).append(": ").append(this.mapper.writeValueAsString(args[i]));
                else
                    params.append("  ").append(paramNames[i]).append(": ").append(args[i]);
                i++;
            }
            log.setParams(params.toString());
        }
        // 获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        Object userObj = SecurityUtils.getSubject().getPrincipal();
        String userName = "";
        if(userObj instanceof User) {
        	userName = ((User)userObj).getUsername();
        }
        if(userObj instanceof MemberModel) {
        	userName = ((MemberModel)userObj).getNickName();
        }
        // 设置IP地址
        log.setIp(IPUtils.getIpAddr(request));
        log.setUsername(userName);
        log.setTime(time);
        log.setCreateTime(new Date());
//      log.setLocation(AddressUtils.getRealAddressByIP(log.getIp(), mapper));
        // 保存系统日志
        this.logService.save(log);
    }
}
