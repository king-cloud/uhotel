package com.umessage.common.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import com.alibaba.fastjson.JSONObject;

public class RestfulAuthenticationFilter extends FormAuthenticationFilter {
	//在用户未认证时调用
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
//        return super.onAccessDenied(request, response, mappedValue);
    	response.setContentType("application/json;charset=UTF-8");
        //输出 JSON 字符串
    	
    	JSONObject obj = new JSONObject();
    	obj.put("401", "unauthentiated");
    	response.getWriter().print(obj.toJSONString());
        //中止请求，不再传给下一个Filter
        return false;
    }
}
