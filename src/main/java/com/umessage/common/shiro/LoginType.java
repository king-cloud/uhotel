package com.umessage.common.shiro;

public enum LoginType {
	MP_HOTEL("MPHotel"), MP_CLIENT("MPClient"), ADMIN("Admin"), SHIRO("Shiro");

	private String type;

	private LoginType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return this.type.toString();
	}
}