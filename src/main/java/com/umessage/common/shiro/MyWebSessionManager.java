package com.umessage.common.shiro;

import java.io.Serializable;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;

public class MyWebSessionManager extends DefaultWebSessionManager {
	
	private String SESSION_ID = "sessionId";
	
	@Override
	public Serializable getSessionId(SessionKey key) {

		// 从 cookies 和 url 中获取 sessionid.
		Serializable id = super.getSessionId(key);
		ServletRequest request = WebUtils.getRequest(key);
		if (request instanceof HttpServletRequest) {
			HttpServletRequest req = ((HttpServletRequest) request);
			// 如果没有，则从 header 中获取 id.
			if (id == null || req.getHeader(SESSION_ID) != null) {
				id = req.getHeader(SESSION_ID);
			}
		}
		return id;
	}

}
