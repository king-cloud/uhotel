package com.umessage.common.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

import com.umessage.hotel.model.AccountModel;

/**
 * 小程序酒店端realm
 * @author mixiangliu
 *
 */
public class MPHotelRealm extends AuthorizingRealm {

	/**
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
	
		SimpleAuthorizationInfo authorization = new SimpleAuthorizationInfo();
		Set<String> roles = new HashSet<String>();
		roles.add("mini_progrom_member");
		authorization.setRoles(roles);
		Set<String> permissions = new HashSet<String>();
		permissions.add("user");
		authorization.setStringPermissions(permissions);
		return authorization;
	}

	/**
	 * 认证方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		if(token instanceof CustomizedToken) {
			CustomizedToken customizedToken = (CustomizedToken)token;
			String mobile = customizedToken.getUsername();
			String vercode = new String(customizedToken.getPassword());
			//认证通过，返回认证信息
			Subject subject = SecurityUtils.getSubject();
			AccountModel account = new AccountModel();
			account.setSessionId(subject.getSession().getId().toString());
			
			return new SimpleAuthenticationInfo(account, vercode, getName());
		}
		
		return null;
	}

}
