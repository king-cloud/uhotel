package com.umessage.common.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.umessage.hotel.model.MemberModel;

/**
 * 小程序用户端realm
 * @author mixiangliu
 *
 */
public class MPClientRealm extends AuthorizingRealm {

	/**
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
	
		SimpleAuthorizationInfo authorization = new SimpleAuthorizationInfo();
		Set<String> roles = new HashSet<String>();
		roles.add("mini_progrom_member");
		authorization.setRoles(roles);
		Set<String> permissions = new HashSet<String>();
		permissions.add("user");
		authorization.setStringPermissions(permissions);
		return authorization;
	}

	/**
	 * 认证方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// 获取用户openid
		String openid = (String) token.getPrincipal();
		//TODO: 获取openid获取用户数据信息
		
		MemberModel member = new MemberModel();
		member.setUserNo("test_member_no");
		
		return new SimpleAuthenticationInfo(member, "", getName());
	}

}
