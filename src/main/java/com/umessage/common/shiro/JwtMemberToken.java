package com.umessage.common.shiro;

import org.apache.shiro.authc.AuthenticationToken;

public class JwtMemberToken implements AuthenticationToken{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String hotelId;
	private String code;
	
	public JwtMemberToken(String hotelId, String code) {
		this.hotelId = hotelId;
		this.code = code;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public Object getPrincipal() {
		return this.hotelId;
	}
	@Override
	public Object getCredentials() {
		return this.code;
	}
	
}
