package com.umessage.common.shiro;

import org.apache.shiro.authc.AuthenticationToken;

public class JwtToken implements AuthenticationToken{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String hotelId;
	private String mobile;
	private String code;
	private String vercode;
	
	public JwtToken(String hotelId, String mobile, String code, String vercode) {
		super();
		this.hotelId = hotelId;
		this.mobile = mobile;
		this.code = code;
		this.vercode = vercode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getVercode() {
		return vercode;
	}
	public void setVercode(String vercode) {
		this.vercode = vercode;
	}
	@Override
	public Object getPrincipal() {
		return this.mobile;
	}
	@Override
	public Object getCredentials() {
		return this.vercode;
	}
	
}
