package com.umessage.common.service.impl;

import org.redisson.api.*;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.umessage.common.service.RedissonService;

import java.io.IOException;

/**
 * redisson操作类
 */
@Service("redissonService")
public class RedissonServiceImpl implements RedissonService {

	@Autowired
	private RedissonClient redissonClient;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.umessage.common.service.impl.RedissonService#getRedissonClient()
	 */
	@Override
	public void getRedissonClient() throws IOException {
		Config config = redissonClient.getConfig();
		System.out.println(config.toJSON().toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRBucket(java.lang.String)
	 */
	@Override
	public <T> RBucket<T> getRBucket(String objectName) {
		RBucket<T> bucket = redissonClient.getBucket(objectName);
		return bucket;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRMap(java.lang.String)
	 */
	@Override
	public <K, V> RMap<K, V> getRMap(String objectName) {
		RMap<K, V> map = redissonClient.getMap(objectName);
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRSortedSet(java.lang.
	 * String)
	 */
	@Override
	public <V> RSortedSet<V> getRSortedSet(String objectName) {
		RSortedSet<V> sortedSet = redissonClient.getSortedSet(objectName);
		return sortedSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRSet(java.lang.String)
	 */
	@Override
	public <V> RSet<V> getRSet(String objectName) {
		RSet<V> rSet = redissonClient.getSet(objectName);
		return rSet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRList(java.lang.String)
	 */
	@Override
	public <V> RList<V> getRList(String objectName) {
		RList<V> rList = redissonClient.getList(objectName);
		return rList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRQueue(java.lang.String)
	 */
	@Override
	public <V> RQueue<V> getRQueue(String objectName) {
		RQueue<V> rQueue = redissonClient.getQueue(objectName);
		return rQueue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRDeque(java.lang.String)
	 */
	@Override
	public <V> RDeque<V> getRDeque(String objectName) {
		RDeque<V> rDeque = redissonClient.getDeque(objectName);
		return rDeque;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRLock(java.lang.String)
	 */
	@Override
	public RLock getRLock(String objectName) {
		RLock rLock = redissonClient.getLock(objectName);
		return rLock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRWLock(java.lang.String)
	 */
	@Override
	public RReadWriteLock getRWLock(String objectName) {
		RReadWriteLock rwlock = redissonClient.getReadWriteLock(objectName);
		return rwlock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRAtomicLong(java.lang.
	 * String)
	 */
	@Override
	public RAtomicLong getRAtomicLong(String objectName) {
		RAtomicLong rAtomicLong = redissonClient.getAtomicLong(objectName);
		return rAtomicLong;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRCountDownLatch(java.lang
	 * .String)
	 */
	@Override
	public RCountDownLatch getRCountDownLatch(String objectName) {
		RCountDownLatch rCountDownLatch = redissonClient.getCountDownLatch(objectName);
		return rCountDownLatch;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.umessage.common.service.impl.RedissonService#getRTopic(java.lang.String)
	 */
	@Override
	public <M> RTopic<M> getRTopic(String objectName) {
		RTopic<M> rTopic = redissonClient.getTopic(objectName);
		return rTopic;
	}

	@Override
	public <V> RDelayedQueue<V> getRDelayedQueue(RQueue<V> rqueue) {
		RDelayedQueue<V> rdelayedQueue = redissonClient.getDelayedQueue(rqueue);
		return rdelayedQueue;
	}

	@Override
	public <V> RBlockingQueue<V> getBlockingQueue(String objectName) {
		return redissonClient.getBlockingDeque(objectName);
	}
}
