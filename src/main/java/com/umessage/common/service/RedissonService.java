package com.umessage.common.service;

import java.io.IOException;

import org.redisson.api.RAtomicLong;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RBucket;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RDeque;
import org.redisson.api.RList;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RQueue;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RSet;
import org.redisson.api.RSortedSet;
import org.redisson.api.RTopic;

public interface RedissonService {

	void getRedissonClient() throws IOException;

	/**
	 * ` 获取字符串对象
	 *
	 * @param objectName
	 * @return
	 */
	<T> RBucket<T> getRBucket(String objectName);

	/**
	 * 获取Map对象
	 *
	 * @param objectName
	 * @return
	 */
	<K, V> RMap<K, V> getRMap(String objectName);

	/**
	 * 获取有序集合
	 *
	 * @param objectName
	 * @return
	 */
	<V> RSortedSet<V> getRSortedSet(String objectName);

	/**
	 * 获取集合
	 *
	 * @param objectName
	 * @return
	 */
	<V> RSet<V> getRSet(String objectName);

	/**
	 * 获取列表
	 *
	 * @param objectName
	 * @return
	 */
	<V> RList<V> getRList(String objectName);

	/**
	 * 获取队列
	 *
	 * @param objectName
	 * @return
	 */
	<V> RQueue<V> getRQueue(String objectName);

	/**
	 * 获取双端队列
	 *
	 * @param objectName
	 * @return
	 */
	<V> RDeque<V> getRDeque(String objectName);

	/**
	 * 获取锁
	 *
	 * @param objectName
	 * @return
	 */
	RLock getRLock(String objectName);

	/**
	 * 获取读取锁
	 *
	 * @param objectName
	 * @return
	 */
	RReadWriteLock getRWLock(String objectName);

	/**
	 * 获取原子数
	 *
	 * @param objectName
	 * @return
	 */
	RAtomicLong getRAtomicLong(String objectName);

	/**
	 * 获取记数锁
	 *
	 * @param objectName
	 * @return
	 */
	RCountDownLatch getRCountDownLatch(String objectName);

	/**
	 * 获取消息的Topic
	 *
	 * @param objectName
	 * @return
	 */
	<M> RTopic<M> getRTopic(String objectName);
	
	/**
	 * 获取阻塞队列
	 * @param objectName
	 * @return
	 */
	public <V> RBlockingQueue<V> getBlockingQueue(String objectName);
	
	/**
	 * 获取延迟队列
	 * @param rqueue
	 * @return
	 */
	public <V> RDelayedQueue<V> getRDelayedQueue(RQueue<V> rqueue);

}