package com.umessage.common.util.poi.convert;

public interface ExportConvert {
	
	String handler(Object val);
}
