package com.umessage.system.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.system.domain.SysLog;

public interface LogService extends IService<SysLog> {
	
	List<SysLog> findAllLogs(SysLog log);
	
	void deleteLogs(String logIds);
}
