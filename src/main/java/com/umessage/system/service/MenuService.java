package com.umessage.system.service;

import java.util.List;

import com.umessage.common.domain.Tree;
import com.umessage.common.service.IService;
import com.umessage.system.domain.Menu;

public interface MenuService extends IService<Menu> {

	List<Menu> findUserPermissions(String userName);

	List<Menu> findUserMenus(String userName);

	List<Menu> findAllMenus(Menu menu);

	Tree<Menu> getMenuButtonTree();
	
	Tree<Menu> getMenuTree();
	
	Tree<Menu> getUserMenu(String userName);
	
	Menu findById(Long menuId);

	Menu findByNameAndType(String menuName, String type);

	void addMenu(Menu menu);

	void updateMenu(Menu menu);
	
	void deleteMeuns(String menuIds);

	Tree<Menu> getUserMenuById(String userName, String hotelId);

	Tree<Menu> getUserMenus();
	
	Tree<Menu> getUserMenuOutRole(String userName);
	
	List<Menu> findUserPermissionsOutRole(String userName);
}
