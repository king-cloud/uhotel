package com.umessage.system.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.system.domain.User;
import com.umessage.system.domain.UserWithRole;

public interface UserService extends IService<User> {

	UserWithRole findById(Long userId);
	
	User findByName(String userName);

	List<User> findUserWithDept(User user);
	
	List<User> findUserWithHotel(User user);

	void registUser(User user);

	void updateTheme(String theme, String userName);

	void addUser(User user, Long[] roles);
	
	void addUserAndRoleNull(User user, Long[] roles);
	
	void updateUser(User user, Long[] roles);
	
	void updateUserAndRoleNull(User user, Long[] roles);
	
	void deleteUsers(String userIds);

	void updateLoginTime(String userName);
	
	void updatePassword(String password);
	
	User findUserProfile(User user);
	
	void updateUserProfile(User user);
	
	void addHotelAdminUser(User user);
	
	User findUserByMobile(String hotelId, String mobile);

	List<User> findUserByHotelId(User user);
	
	User findUserByMobileNum(String mobile);

	User findUserByMobileAndHotel(String mobile, String hotelId);

	User getUserByid(Long userId);

	List<User> findUserListByHotelId(String hotelId);
	
	List<String> findConfirmUserMobileList(String hotelId);

	List<String> findMsgUserMobileList(String hotelId);

	User findUserByMobileAll(String mobile);
}
