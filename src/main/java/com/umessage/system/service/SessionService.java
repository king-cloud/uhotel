package com.umessage.system.service;

import java.util.List;

import com.umessage.system.domain.UserOnline;

public interface SessionService {

	List<UserOnline> list();

	boolean forceLogout(String sessionId);
}
