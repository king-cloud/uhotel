package com.umessage.system.service;

import java.util.List;

import com.umessage.common.service.IService;
import com.umessage.system.domain.Dict;

public interface DictService extends IService<Dict> {
	
	List<Dict> findByCondition(Dict dict);

	List<Dict> findAllDicts(Dict dict);

	Dict findById(Long dictId);

	void addDict(Dict dict);

	void deleteDicts(String dictIds);

	void updateDict(Dict dicdt);
	
	public List<Dict> findDicts(Dict dict);
}
