package com.umessage.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.User;
import com.umessage.system.domain.UserWithRole;

public interface UserMapper extends MyMapper<User> {

	List<User> findUserWithDept(User user);

	List<User> findUserWithHotel(User user);

	List<UserWithRole> findUserWithRole(Long userId);

	User findUserProfile(User user);

	User findUserByMobile(@Param("hotel_id") String hotelId, @Param("mobile") String mobile);
	
	User findUserByMobileNum(@Param("mobile") String mobile);

	User getUserByid(Long userId);

	List<User> findUserListByHotelId(String hotelId);
	
	List<String> findConfirmUserMobileList(String hotelId);

	List<String> findMsgUserMobileList(String hotelId);
}