package com.umessage.system.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.SysLog;

public interface LogMapper extends MyMapper<SysLog> {
}