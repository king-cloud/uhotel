package com.umessage.system.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.Dict;

public interface DictMapper extends MyMapper<Dict> {
	
	List<Dict> findByCondition(Dict dict);
	
}