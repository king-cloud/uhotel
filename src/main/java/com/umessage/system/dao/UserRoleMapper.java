package com.umessage.system.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.UserRole;

public interface UserRoleMapper extends MyMapper<UserRole> {
}