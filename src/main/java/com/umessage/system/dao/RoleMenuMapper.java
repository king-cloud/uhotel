package com.umessage.system.dao;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.RoleMenu;

public interface RoleMenuMapper extends MyMapper<RoleMenu> {
}