package com.umessage.system.dao;

import java.util.List;

import com.umessage.common.config.MyMapper;
import com.umessage.system.domain.Role;
import com.umessage.system.domain.RoleWithMenu;

public interface RoleMapper extends MyMapper<Role> {
	
	List<Role> findUserRole(String userName);
	
	List<RoleWithMenu> findById(Long roleId);
}