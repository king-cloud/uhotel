//@ sourceURL=login.js
$(document).ready(function () {

    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass: 'iradio_minimal-green',
        increaseArea: '20%'
    });

    var $formPanelTwo = $('.form-panel.two');

    var panelOne = $formPanelTwo.height();
    var panelTwo = $formPanelTwo[0].scrollHeight;

    $formPanelTwo.not('.form-panel.two.active').on('click', function (e) {
        e.preventDefault();

        $('.form-toggle').addClass('visible');
        $('.form-panel.one').addClass('hidden');
        $('.form-panel.two').addClass('active');
        $('.form').animate({
            'height': panelTwo
        }, 200);
    });

    $('.form-toggle').on('click', function (e) {
        e.preventDefault();
        $(this).removeClass('visible');
        $('.form-panel.one').removeClass('hidden');
        $('.form-panel.two').removeClass('active');
        $('.form').animate({
            'height': panelOne + 92
        }, 200);
    });

});

function reloadCode() {
	 var username = $(".two input[name='username']").val().trim();
	 if(username!=null && username!=""){
		 $("#validateCodeImg").attr("src", ctx + "gifCode?data=" + new Date() + "&username="+username);
	 }
}

function login() {
    var $loginButton = $("#loginButton");
    var username = $(".two input[name='username']").val().trim();
    var password = $(".two input[name='password']").val().trim();
    var code = $(".two input[name='code']").val().trim();
    var rememberMe = $(".two input[name='rememberme']").is(':checked');
    if (username === "") {
        $MB.n_warning("请输入用户名！");
        return;
    }
    if (password === "") {
        $MB.n_warning("请输入密码！");
        return;
    }
    if (code === "") {
        $MB.n_warning("请输入验证码！");
        return;
    }
    $loginButton.html("").append("<div class='login-loder'><div class='line-scale'><div></div><div></div><div></div><div></div><div></div></div></div>");

    $.ajax({
        type: "post",
        url: ctx + "login",
        data: {
            "username": username,
            "password": password,
            "code": code,
            "rememberMe": rememberMe
        },
        dataType: "json",
        success: function (r) {
            if (r.code === 0) {
                location.href = ctx + 'index';
            } else {
                if (r.msg === '验证码错误！') reloadCode();
                $MB.n_warning(r.msg);
                $loginButton.html("登录");
            }
        }
    });
}

function getcode() {
	$("#sengCode").attr("disabled", false);
	
    var mobile = $(".one input[name='mobile']").val().trim();
    if (mobile === "") {
        $MB.n_warning("手机号不能为空！");
        return;
    } else if (mobile.length != 11) {
        $MB.n_warning("手机号长度为11位数字！"); //待完成校验
        return;
    }
    $.ajax({
        type: "post",
        url: ctx + "rest/sms/sendvercode",
        data: {
            "mobile": mobile,
            "type": "2",
        },
        dataType: "json",
        success: function (r) {
            if (r.success) {
            	$("#warning").hide();
            	$("#countDown").html(60);
            	$(".one input[name='code']").val('');
            	$("#sengCode").attr("disabled", true);
            	//$("#login2mobile").attr("disabled", true);
            	countDown();
            } else {
                $MB.n_warning(r.returnMessage);
            }
        }
    });
}

function login2mobile() {
    var $loginButton = $("#login2mobile");
    var mobile = $(".one input[name='mobile']").val().trim();
    var code = $(".one input[name='code']").val().trim();
    if (mobile === "") {
        $MB.n_warning("请输入手机号！");
        return;
    }
    if (code === "") {
        $MB.n_warning("请输入验证码！");
        return;
    }
    $.ajax({
        type: "post",
        url: ctx + "login",
        data: {
            "mobile": mobile,
            "code": code,
            "type":"2",
        },
        dataType: "json",
        success: function (r) {
            if (r.code === 0) {
                location.href = ctx + 'index';
            } else {
                if (r.msg === '验证码错误！') reloadCode();
                $MB.n_warning(r.msg);
                $loginButton.html("登录");
            }
        }
    });
}

function regist() {
    var username = $(".one input[name='username']").val().trim();
    var password = $(".one input[name='password']").val().trim();
    var cpassword = $(".one input[name='cpassword']").val().trim();
    if (username === "") {
        $MB.n_warning("用户名不能为空！");
        return;
    } else if (username.length > 10) {
        $MB.n_warning("用户名长度不能超过10个字符！");
        return;
    } else if (username.length < 3) {
        $MB.n_warning("用户名长度不能少于3个字符！");
        return;
    }
    if (password === "") {
        $MB.n_warning("密码不能为空！");
        return;
    }
    if (cpassword === "") {
        $MB.n_warning("请再次输入密码！");
        return;
    }
    if (cpassword !== password) {
        $MB.n_warning("两次密码输入不一致！");
        return;
    }
    $.ajax({
        type: "post",
        url: ctx + "user/regist",
        data: {
            "username": username,
            "password": password,
        },
        dataType: "json",
        success: function (r) {
            if (r.code === 0) {
                $MB.n_success("注册成功，请登录");
                $(".one input[name='username']").val("");
                $(".one input[name='password']").val("");
                $(".one input[name='cpassword']").val("");
                $('.form-toggle').trigger('click');
            } else {
                $MB.n_warning(r.msg);
            }
        }
    });
}

document.onkeyup = function (e) {
    if (window.event)
        e = window.event;
    var code = e.charCode || e.keyCode;
    if (code === 13) {
        login();
    }
};

//定时器跳转
function countDown(){
	$("#remainSeconds").show();
	$("#warning").hide();
                
                var n=parseInt($("#countDown").html());  
                n-=1;  
               
                if(n>0){  
                	$("#countDown").text(n);
                	 setCloseJump();
                }else{ 
                	$("#remainSeconds").hide();
            		$("#warning").show();
            		$("#sengCode").attr("disabled", false);
                }  
}  
   var timer=null;//保存定时器序号  
   function setCloseJump(){  
        timer=setTimeout(countDown,1000);//启动一次性定时器  
   }  
  