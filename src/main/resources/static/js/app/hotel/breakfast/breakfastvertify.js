var $verifyTableForm = $(".verify-table-form");
$(function(){
	
	initVertifyTable();
	//var text = document.getElementById('demo');
	//text.focus();
	var code = "";
    var lastTime,nextTime;
    var lastCode,nextCode;
	
    this.onkeypress = function(e){
    	
    	nextCode = e.which;
        nextTime = new Date().getTime();
        
        if(lastCode != null && lastTime != null && nextTime - lastTime <= 30) {
            code += String.fromCharCode(lastCode); 
        } else if(lastCode != null && lastTime != null && nextTime - lastTime > 100){
            code = "";
        }

        lastCode = nextCode;
        lastTime = nextTime;
    	
        if(e.which == 13){
            console.log(code);
            sub(code);
        }
    } 
    
});

/**
 * 搜索
 * @returns
 */
function search(){
	$MB.refreshTable('verifyTable');
}
/**
 * 刷新
 * @returns
 */
function refresh(){
	$verifyTableForm[0].reset();
    $MB.refreshTable('verifyTable');
}

function sub(code){
	//var code ='{"time_stamp":1540436629589,"mid":"281bef1e5e1a4ec5b88d4a5fbb17b022","use_date":"2018-10-23","use_amount":2}';
	//var dt = JSON.parse(code);
	
	$.ajax({
        type:"GET",
		url : ctx + "verification/jiemi?code="+code,
        //data:{"memberId":,dt.mid"timeStamp":dt.time_stamp,"useDate":dt.use_date,"useAmount":dt.use_amount},
        success : function(r){
        	if(!r){
        	}else{
	        	if(r.code==0){
	        		if(window.confirm("你确定要核销"+r.msg+"张吗？")){
	        			$.ajax({
	        		        type:"GET",
	        				url : ctx + "verification/check?code="+code,
	        				 success : function(r){
	        					 $verifyTableForm[0].reset();
	        			         $MB.refreshTable('verifyTable');
	        				 }
	        			});
	                 }
	        	}else{
	        		if(r.msg!="undefined"){
	        			$MB.n_warning(r.msg);
	        		}
	        	}
        	}
        }
    }) 
}
function initVertifyTable(){
	var settings = {
	        url: ctx + "verification/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                mobile:$(".verify-table-form").find("input[name='mobile']").val().trim()
	            }
	        },
	        columns: [{
		            field: 'id',
		            visible: false
		        }, {
		            field: 'recordSn',
		            title: '流水号'
		        }, {
		            field: 'addTime',
		            title: '核销时间'
		        }, {
		            field: 'mobile',
		            title: '被核销人'
		        }, {
		            field: 'amount',
		            title: '核销张数'
		        }]
	    };
	$MB.initTable("verifyTable", settings);
	
}