var $breakfastAddForm=$("#breakfast-add-form");
$(function(){
	initCouponTable();
	initCouponCount();
	$("#coupon-add-button").click(function () {
		document.getElementById("coupon-add-button").disabled=true; 
		var mobile=$.trim($("#shoujiP").val());
		var roomNo=$.trim($("#roomNo").val());
		if(!mobile){
			 $MB.n_warning("手机号不能为空");
			 $("#coupon-add-button").removeAttr("disabled");
			return;
		}
		if(!roomNo){
			 $MB.n_warning("房间号不能为空");
			 $("#coupon-add-button").removeAttr("disabled");
			return;
		}
		var date=$("#test6").val();
		var start=new Date(date.substring(0,10));
		var end=new Date(date.substring(13,23));
		var date2=end.getTime()-start.getTime();  //时间差的毫秒数
		if(date2<0){
			$MB.n_warning("开始日期超出结束日期，请重新选择");
			$("#coupon-add-button").removeAttr("disabled");
			return;
		}
		var numDaily=$.trim($("#number").html());
		//计算出相差天数
		var days=Math.floor(date2/(24*3600*1000))+1;
		var sumNum=days*numDaily;
		var jsonP={
				"mobile":mobile,
				"roomNo":roomNo,
				"amount":sumNum,
				"numDaily":numDaily,
				"validStart":date.substring(0,10),
				"validEnd":date.substring(13,23),
				"days":days
		}
		$.post(ctx + "breakfastcoupon/add", jsonP, function (r) {
            	if(r=="0"){
            		closeModal();
                    $MB.refreshTable("couponTable");
                    initCouponCount();
                    $MB.n_success("添加成功");
            	}else if(r=="404"){
            		 $MB.n_warning("酒店没有可用餐厅");
            	}
            	$("#coupon-add-button").removeAttr("disabled");
        });
	});
	$("#breakfast-add .btn-close").click(function () {
        closeModal();
    });
});
/**
 * 初始化统计数据
 * @returns
 */
function initCouponCount(){
	$.post(ctx + "breakfastcoupon/count", function (r) {
		if(r!=null){
			$("#check").val(r.check);
			$("#countType1").val(r.countType1);
			$("#countType2").val(r.countType2);
		}
	});
}


/**
 * 初始化列表
 * @returns
 */
function initCouponTable(){
	var settings = {
	        url: ctx + "breakfastcoupon/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                mobile: $(".brakefast-table-form #mobile").val()
	            }
	        },
	        columns: [{
	        	field: 'bfOrderSn',
	            title: '订单号'
	            },{
	        	field: 'roomNo',
	            title: '房间号',
	            formatter : function(r){
	            	if(!r) return "暂无此信息";
	            		else return r;
            	} 
	            },{
	        	field: 'amount',
	            title: '早餐券',
	            formatter : function(u,row){
	            	return row.numDaily+'张/天  '+'共'+u+'张';
            	} 
	            },{
		        	field: 'validEnd',
		            title: '生效时间',
		            formatter : function(ve,row){
		            	var vs= row.validStart;
		            	return vs+" 至 "+ve;
	            	} 
		            },{
	        	field: 'validStart',
	            title: '状态',
	            formatter : function(vs,row){
	            	var zshmd=" 00:00:00";//早上 开始
	            	var zwhmd=" 23:59:00";//晚上 结束
	            	var ve=row.validEnd+zwhmd;
	            	var ndate = new Date();
	            	var ntime=ndate.getTime();
	            	var sdate = new Date(vs+zshmd);
	            	var stime=sdate.getTime();
	            	var edate = new Date(ve);
	            	var etime=edate.getTime();
	            	if(ntime<stime){
	            		return "待生效";
	            	}else if(etime<ntime){
	            		return "已失效";
	            	}else{
	            		return "生效中";
	            	}
            	} 
	            }]
	    };
	
	$MB.initTable("couponTable", settings);
	
}
function closeModal() {
    $MB.closeAndRestModal("breakfast-add");
}
/**
 * 搜索
 * @returns
 */
function search(){
	$MB.refreshTable('couponTable');
}
/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".brakefast-table-form")[0].reset();
    $MB.refreshTable('couponTable');
}

/*
 * 跳转餐厅
 */
function tiaozhuan(){
	$obj = $("span[name='diningroom/index']");
	loadMain($obj);
}
