/**
 * 优惠券管理
 */

$(function(){
	getCoupons();
	initCouponTable();
});

function getCoupons(){
	$.ajax({
		type : "post",
		url :  "memberdiscount/getCoupons",
		dataType : "json",
		data : {},
		async : false,
		success : function(res) {
			$("#jiudianid").val(res.hotelId);
			$("#jiudianmc").html(res.hotelName);
		},
		error : function() {
		}
	});
}

function s1(s){
	alert(s);
	if(s==0){
		$("#smemberDiscount").html('直减： <input type="text" onkeyup="check(this,this.value)" id="memberDiscount" name="memberDiscount"/> 元') 
	}else{
		$("#smemberDiscount").html('折扣： <input type="text" onkeyup="check(this,this.value)" id="memberDiscount" name="memberDiscount"/> 折') 
	}
}
function s2(s){
	if(s==0){
		$("#smemberDiscountu").html('直减： <input type="text" onkeyup="check(this,this.value)" id="memberDiscountu"  name="memberDiscountu"/> 元') 
	}else{
		$("#smemberDiscountu").html('享受原房价的<input type="text"   placeholder="N(1<=N<=100)整数 "  onkeyup="check(this,this.value)" id="memberDiscountu" name="memberDiscountu"/>%的折扣') 
	}
}
function check(a,b){
    if(!(/^\d+$/.test(b))){ 
		a.value = '';
	}
}
function initCountmember(){
	$.post(ctx + "member/count", function (r) {
		$("#sumCount").val(r.sumCount).attr("readonly", true);
		$("#useCount").val(r.useCount).attr("readonly", true);
    });
}

var validator;
var $memberAddForm = $("#member-add-form");
$(function () {
    $("#member-add .btn-save").click(function () {
    	addmember();
    });
	$("#member-add .btn-close").click(function () {
		$("#smemberDiscount").html('直减： <input type="text" id="memberDiscountu" name="memberDiscountu"/> 元') 
        closeModal();
    });
	
});
/**
 * 取消
 * @returns
 */
function closeModal() {
    $MB.closeAndRestModal("member-add");
}
function closeModalupd() {
    $MB.closeAndRestModal("member-upd");
}
function add(){
	var jiudianid=$("#jiudianid").val();
	var memberName=$("#memberName").val();
	var memberDiscount=$("#memberDiscountu").val();
	var memberDiscountType=$("input[name='memberDiscountType']:checked").val();
	/*if(memberDiscountType==0){
		memberDiscount="￥-"+memberDiscount;
	}else {
		memberDiscount=memberDiscount+"折"
	}*/
	$.ajax({
		type : "post",
		url :  "memberdiscount/add",
		dataType : "json",
		data : {
			"jiudianid":jiudianid,
			"memberName":memberName,
			"memberDiscount":memberDiscount,
			"memberDiscountType":memberDiscountType
		},
		async : true,
		success : function(res) {
			if(res===1){
				closeModal();
				$MB.refreshTable('memberTable');
			}else if(res === 0){
				alert("存在未填项");
			}else {
				alert("添加失败");
			}
		},
		error : function() {
		}
	});
}

function initCouponTable(){
	var settings = {
	        url:"memberdiscount/list?memberSwitch=1",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                num:new Date()
	            }
	        },
	        columns: [{
		            title: '序号',
	            	formatter: function (value, row, index) {
	            		return ++index;
	                }},{
		            field: 'memberType',
		            title: '会员等级',
		            
		        },  {
		            field: 'memberName',
		            title: '会员名称'
		        }, {
		        	field: 'memberDiscountType',
		            title: '折扣类型',
		            formatter : function(u){
			            if(u==0){
		            		return '直减';
		            	}else if(u==1){
		            		return '打折';
		            	}
		            }
		        }, {
		            field: 'memberDiscount',
		            title: '直减/折扣',
		            formatter : function(u,row){
		            	u=row.memberDiscountType;
			            if(u==0){
		            		return '￥ '+row.memberDiscount;
		            	}else if(u==1){
		            		var mdt=row.memberDiscount;
		            		return mdt+"%";
		            	}
		            }
		        }, {
		        	field: 'memberSwitch',
		            title: '状态',
		            formatter : function(sth,row){
		            	//0 关闭 1 开启
		            	if(sth==0){
		            		return '<span class="switchB">已关闭</span><input type="button" value="开启" onclick="switchButton('+sth+','+row.memberId+')">';
		            	}
		            	return '<span class="switchB">已开启</span><input type="button" value="关闭" onclick="switchButton('+sth+','+row.memberId+')">';
                	} 
		        }, {
		        	field: 'memberId',
		            title: '编辑',
		            formatter : function(u){
		            	var ohtml='<input type="button" value="编辑" data-toggle="modal" data-target="#member-upd" onclick="toUpdate('+u+')">';
		            	
		            	//ohtml+='<input type="button" value="删除" onclick="toDelete('+u+')">';
		            	return ohtml;
                	} 
		        }]
	    };
	
	$MB.initTable("memberTable", settings);
	
}

/**
 * 刷新会员优惠券列表
 * @returns
 */
function refresh(){
	$(".hotel-table-form")[0].reset();
    $MB.refreshTable('memberTable');
    count();
}


function toDelete(s){
	$.ajax({
		type : "post",
		url :  "memberdiscount/del",
		dataType : "json",
		data : {"id":s},
		async : false,
		success : function(res) {
			initCouponTable();
			$MB.refreshTable('memberTable');
		},
		error : function() {
		}
	});
}
function toUpdate(id){
	$.ajax({
		type : "post",
		url :  "memberdiscount/memberdiscount",
		dataType : "json",
		data : {"id":id},
		async : false,
		success : function(res) {
			$("#idu").val(res.memberId);
			
			$("#memberNameu").val(res.memberName);
			$("input[name='memberDiscountType'][value='"+res.memberDiscountType+"']").attr("checked",true);
			if(res.memberDiscountType==0){
				$("#smemberDiscountu").html('直减： <input type="text" value="'+res.memberDiscount+'" id="memberDiscountu" name="memberDiscount"/> 元') 
			}else{
				$("#smemberDiscountu").html('享受原房价的 <input type="text"   placeholder="N(1<=N<=100)整数"  value="'+res.memberDiscount+'" id="memberDiscountu" name="memberDiscount"/> 折') 
			}
		},
		error : function() {
		}
	});
}
function upd(){
	var jiudianid=$("#jiudianid").val();
	var id=$("#idu").val();
	var memberName=$("#memberNameu").val();
	var memberDiscount=$("#memberDiscountu").val();
	var memberDiscountType=$("input[name='memberDiscountType']:checked").val();
	if(!jiudianid){
		return;
	}
	if(!memberName){
		$MB.n_warning("不能为空");
		return;
	}
	if(!memberDiscount){
		$MB.n_warning("不能为空");
		return;
	}
	if(!memberDiscountType){
		$MB.n_warning("不能为空");
		return;
	}
	/*if(memberDiscountType==0){
		memberDiscount="￥-"+memberDiscount;
	}else {
		memberDiscount=memberDiscount+"折"
	}*/
	$.ajax({
		type : "post",
		url :  "memberdiscount/update",
		dataType : "json",
		data : {
			"id":id,
			"jiudianid":jiudianid,
			"memberName":memberName,
			"memberDiscount":memberDiscount,
			"memberDiscountType":memberDiscountType
		},
		async : true,
		success : function(res) {
			if(res===1){
				closeModalupd();
				$MB.refreshTable('memberTable');
			}else {
				$MB.n_warning("修改失败")
			}
		},
		error : function() {
		}
	});
}
function switchButton(msth,mId){
	$.ajax({
		type : "post",
		url :  ctx+"memberdiscount/updateSwitch",
		data : {
			"memberId":mId,
			"memberSwitch":msth,
		},
		success : function(res) {
			$MB.n_warning(res.msg);
			$MB.refreshTable('memberTable');
		}
	});
}
