//@ sourceURL=roomBook.js
var $roomBookForm = $(".room-book-form");
var $roomIds = $("#roomIds");
$(function(){
	showBook();
});
function showBook(){
    var roomId = $("#roomId").val();
	var date=new Date;
	var year=date.getFullYear(); 
	var month=date.getMonth()+1;
	$("#years").val(year);
	$("#months").val(month);
	initRoomType(roomId);
	initBookTable(roomId);
}
function initRoomType(r){
	$.post(ctx + "hotel/getRoomName",function (dt) {
        var data = dt.msg;
        var option = "";
        for (var i = 0; i < data.length; i++) {
        	if(data[i].roomId == r){
        		option += "<option selected='selected' value='" + data[i].roomId + "'>" + data[i].name + "</option>";
        	}else{
        		option += "<option value='" + data[i].roomId + "'>" + data[i].name + "</option>";	
        	}
        }
        $roomIds.html("").append(option);
    });
}
function initBookTable(r){
	    var settings = {
		        url: ctx + "hotel/getMonthRoom",
		        pageSize: 100,
		        pagination: false,
		        cardView: true,
		        strictSearch: false,
		        showColumns: false,
		        minimumCountColumns: 2,
		        clickToSelect: false,
		        queryParams: function () {
		            return {
		                pageSize: 100,
		                pageNum: 1,
		                roomId: $("#roomIds").val()!=null?$("#roomIds").val():r,
		                month: $("#months").val(),
		                year: $("#years").val()
		            };
		        },
		        columns: [{
		            field: 'daily',
		            //title: '日期',
		            formatter : function(r){
		            	var dhtml=r+'日';
	                    return dhtml;
	                } 
		        }, {
		            field: 'price',
		            //title: '今日单价',
		            formatter : function(r){  
	                	var shtml='<input type="text"  class="form-control" style="width:40px;display:inline;" name="price" value="'+r+'" readonly/>';
	                	shtml+='<input type="hidden" name="priceEdit" value="编辑" onclick="priceReadonly(this)"/>';
	                	return shtml;
	                } 
		        },{
	                //title : '房间状态',
	                field : 'isBook',
	                formatter : function(r){  
	                	var shtml='<input type="hidden" name="isBook" value="'+r+'"/>';
	                	//var shtml='<input name="isBook" value="'+r+'"/>';
	                    if(r==1){
	                        //return shtml+='可预订';
	                    	return shtml+='<input name="isBook1" class="form-control" style="width:56px;display:inline;" value="'+'可预订'+'"/><button type="button" class="btn_book" disabled="disabled" onclick="isbook1(this)">关闭</button>';
	                    }else{
	                        //return shtml+='不可预定';
	                    	return shtml+='<input name="isBook1" class="form-control" style="width:56px;display:inline;" value="'+'不可预订'+'"/><button type="button" class="btn_book" disabled="disabled" onclick="isbook1(this)">开启</button>';
	                    }
	                } 
	            }
/*		        ,{
	                //title : '操作',
	                field : 'daily',
	                formatter : function(d){  
	                	var shtml='<input type="hidden" name="daily" value="'+d+'"/>';
	                	shtml+='<input type="hidden" name="roomId" value="'+$("#roomId").val()+'"/>';
	                	//return shtml+='<button type="button" class="btn_book" disabled="disabled" onclick="isbook1(this)">编辑状态</button>';
	                	return shtml+='<button type="button" class="btn_book" onclick="isbook1(this)">编辑状态</button>';
	                } 
	            }*/
	            ]
	    };
		$MB.initTable("roomBookTable", settings);
		
}

function isbook(t){
	
	var daily=$(t).siblings('input[name="daily"]').val();
	var price=$(t).parent().parent().find('input[name="price"]').val();
	var isBook=$(t).parent().parent().find('input[name="isBook"]').val();
	if(isBook==1){
		isBook=0;
	}else{
		isBook=1;
	}
	var year=$("#years").val();
	var month=$("#months").val();
	var roomId=$(t).siblings('input[name="roomId"]').val();
	$.post(ctx + "hotel/book", {"isBook":isBook,"price":price,"roomId":roomId,"daily":daily,"year":year,"month":month}, function (r) {
        if (r.code === 0) {
        	$MB.refreshTable("roomBookTable");
        	$MB.n_success(r.msg);
        } else $MB.n_danger(r.msg);
    });
}

function isbook1(t){
	//$MB.refreshTable("roomBookTable");
	var daily=$(t).siblings('input[name="daily"]').val();
	console.log(daily)
	alert(daily)
	var price=$(t).parent().parent().siblings().find('input[name="price"]').val();
	var isBook=$(t).parent().parent().siblings().find('input[name="isBook"]').val();
	if(isBook==1){
		isBook=0;
	}else{
		isBook=1;
	}
	var year=$("#years").val();
	var month=$("#months").val();
	var roomId=$("#roomIds").val()!=null?$("#roomIds").val():  $(t).siblings('input[name="roomId"]').val();
	$.post(ctx + "hotel/book", {"isBook":isBook,"price":price,"roomId":roomId,"daily":daily,"year":year,"month":month}, function (r) {
        if (r.code === 0) {
        	$MB.refreshTable("roomBookTable");
        	$MB.n_success(r.msg);
        } else $MB.n_danger(r.msg);
    });
	$('input[name="bulkEdit"]').removeAttr("disabled");
}

function isbook2(t){
	//$MB.refreshTable("roomBookTable");
	//alert("0")
	var isBook=$(t).parent().find('input[name="isBook"]').val();
	//alert(isBook)
	if(isBook==1){
		$(t).parent().find('input[name="isBook"]').val("0");
		$(t).parent().find('input[name="isBook1"]').val("不可预定");
		$(t).text("开启")
	}else{
		$(t).parent().find('input[name="isBook"]').val("1");
		$(t).parent().find('input[name="isBook1"]').val("可预定");
		$(t).text("关闭")
	}
}
	
function priceEdit(p){
	var daily=$(p).parent().parent().find('input[name="daily"]').val();
	var price=$(p).siblings('input[name="price"]').val();
	var isBook=$(p).parent().parent().find('input[name="isBook"]').val();
	var year=$("#years").val();
	var month=$("#months").val();
	var roomId=$(p).parent().parent().find('input[name="roomId"]').val();
	$.post(ctx + "hotel/book", {"isBook":isBook,"price":price,"roomId":roomId,"daily":daily,"year":year,"month":month}, function (r) {
        if (r.code === 0) {
        	$MB.refreshTable("roomBookTable");
        	$MB.n_success(r.msg);
        } else $MB.n_danger(r.msg);
    });
}

function priceEdit1(p){
	//saveData();
	//$MB.refreshTable("roomBookTable");
	$('input[name="bulkEdit"]').removeAttr("disabled");
	var daily=$(p).parent().parent().siblings().find('input[name="daily"]').val();
	//console.log(daily)
	var price=$(p).siblings('input[name="price"]').val();
	//console.log(price)
	//var isBook=$(p).parent().parent().find('input[name="isBook"]').val();
	var isBook=$(p).parent().parent().siblings().find('input[name="isBook"]').val();
	//console.log(isBook)
	var year=$("#years").val();
	var month=$("#months").val();
	var roomId= $("#roomIds").val()!=null?$("#roomIds").val(): $(p).parent().parent().siblings().find('input[name="roomId"]').val();
	//console.log(roomId)
	$.post(ctx + "hotel/book", {"isBook":isBook,"price":price,"roomId":roomId,"daily":daily,"year":year,"month":month}, function (r) {
        if (r.code === 0) {
        	$MB.refreshTable("roomBookTable");
        	$MB.n_success(r.msg);
        } else $MB.n_danger(r.msg);
    });
}

function priceReadonly(r){
	$('input[name="bulkEdit"]').attr("disabled", "disabled");
	$(r).siblings('input[name="price"]').removeAttr("readonly");
	$(r).attr("type","hidden");
	var phtml='<input type="button" value="确定" onclick="priceEdit1(this)"/>';
	phtml+='<input type="button" value="取消" onclick="cancel(this)"/>';
	$(r).parent().append(phtml);
	
}
function cancel(c){
	$('input[name="bulkEdit"]').removeAttr("disabled");
	$MB.refreshTable("roomBookTable");
}
function cancel1(c){
	$MB.refreshTable("roomBookTable");
	//console.log($(c).parent());
	$('input[name="price"]').attr("readonly", "readonly");
	$(".btn_book").attr("onclick", "isbook1(this)");
	$(c).siblings('input').remove();	
	var phtml='<input type="button" name="bulkEdit" value="编辑" onclick="bulkEdit(this)">';
	$(c).parent().append(phtml);
	$(c).remove();
	$('select[name="years"]').removeAttr("disabled");
	$('select[name="roomIds"]').removeAttr("disabled");
	$('select[name="months"]').removeAttr("disabled");
}
function changeData(){
	//$MB.refreshTable("roomBookTable");
/*	var roomId = $("#roomIds").val();
	//alert(roomId)
	var date=new Date;
	var year=date.getFullYear(); 
	var month=date.getMonth()+1;
	$("#years").val(year);
	$("#months").val(month);
	initRoomType(roomId);
	initBookTable(roomId);*/
	$MB.refreshTable("roomBookTable");
}
function bulkEdit(r){
	$(r).attr("disabled", "disabled");
	$('select[name="years"]').attr("disabled", "disabled");
	$('select[name="roomIds"]').attr("disabled", "disabled");
	$('select[name="months"]').attr("disabled", "disabled");
	$(".btn_book").removeAttr("disabled");
	$('input[name="price"]').removeAttr("readonly");
	$('input[name="priceEdit"]').attr("disabled", "disabled");
	//$(".btn_book").removeAttr("disabled");
	$(".btn_book").attr("onclick", "isbook2(this)");
	//$(r).attr("type","hidden");
	var phtml='<input type="button" value="确定" onclick="saveData(this)"/>';
	phtml+='<input type="button" value="取消" onclick="cancel1(this)"/>';
	$(r).parent().append(phtml);
	$(r).remove();
}
function saveData(c){
	/*var data1 = []
	var roomPrice = document.getElementsByName("price");
	for (var i = 0;i<roomPrice.length;i++){
		data1.push(roomPrice[i].value);
	}
	console.log(data1)
	var data2 = []
	var roomType = document.getElementsByName("isBook");
	for (var i = 0;i<roomType.length;i++){
		data2.push(roomType[i].value);
	}
	console.log(data2)*/
	var data3 = []
	var roomPrice = document.getElementsByName("price");
	for (var i = 0;i<roomPrice.length;i++){
		var data4 = []
		data4.push(roomPrice[i].value)
		var roomType = document.getElementsByName("isBook");
		for (var j = 0;j<roomType.length;j++){
			if(i==j){
				data4.push(roomType[j].value);
			}
		}
		data3.push(data4);
	}
	console.log(data3)
	//var daily=$(t).siblings('input[name="daily"]').val();
	var year=$("#years").val();
	var month=$("#months").val();
	//var roomId=$("#roomIds").val()!=null?$("#roomIds").val():  $(t).siblings('input[name="roomId"]').val();
	//var roomId = "38";
	var roomId = $("#roomIds").val()
	for(var i = 0;i<data3.length;i++){
		var daily = i + 1
		var price = data3[i][0]
		var isBook = data3[i][1]
		console.log(isBook)
		/*if(isBook==1){
			isBook=0;
		}else{
			isBook=1;
		}*/
		//alert(price)
		console.log(isBook+"-"+price+"-"+roomId+"-" + daily + "-" + year + "-" + month)
		$.post(ctx + "hotel/book", {"isBook":isBook,"price":price,"roomId":roomId,"daily":daily,"year":year,"month":month}, function (r) {
			if (r.code === 0) {
				//$MB.refreshTable("roomBookTable");
				//$MB.n_success(r.msg);
			} else $MB.n_danger(r.msg);
		});
	}
	$MB.refreshTable("roomBookTable");
	$MB.n_success("编辑信息成功");
	//cancel1(c);
	$('input[name="price"]').attr("readonly", "readonly");
	$(".btn_book").attr("onclick", "isbook1(this)");
	$(c).siblings('input').remove();	
	var phtml='<input type="button" name="bulkEdit" value="编辑" onclick="bulkEdit(this)">';
	$(c).parent().append(phtml);
	$(c).remove();
	$('select[name="years"]').removeAttr("disabled");
	$('select[name="roomIds"]').removeAttr("disabled");
	$('select[name="months"]').removeAttr("disabled");
}

