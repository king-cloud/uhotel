$(function(){
	initRoomTable();
	/**
	 * 新增房间
	 */
	$("#addRoom").click(function(){
		var jsonParams={
				"rtype":1
		}
		$obj = $("span[name='hotel/roomIndex']");
		loadMainByParam($obj[0],jsonParams);
	});
	/**
	 *编辑房间 
	 */
	$("#editRoom").click(function(){
		var selected = $("#roomTable").bootstrapTable("getSelections");
	    var selected_length = selected.length;
	    if (!selected_length) {
	        $MB.n_warning('请勾选需要修改的房间信息！');
	        return;
	    }
	    if (selected_length > 1) {
	        $MB.n_warning('一次只能修改一个房间信息！');
	        return;
	    }
	    var roomId = selected[0].roomId;
	    //getRoomById(roomId);
		var jsonParams={
				"roomId":roomId,
				"rtype":2
		}
		$obj = $("span[name='hotel/roomIndex']");
		loadMainByParam($obj[0],jsonParams);
	});
	/**
	 * 查看房态
	 */
	$("#bookRoom").click(function(){
		var selected = $("#roomTable").bootstrapTable('getSelections');
		var selected_length = selected.length;
	    if (!selected_length) {
	        $MB.n_warning('请勾选需要修改的房间！');
	        return;
	    }
	    if (selected_length > 1) {
	        $MB.n_warning('一次只能修改一个房间！');
	        return;
	    }
	    var roomId = selected[0].roomId;
		var jsonParams={
				"roomId":roomId,
				"rtype":3
		}
		$obj = $("span[name='hotel/roomIndex']");
		loadMainByParam($obj[0],jsonParams);
	});
	
});

/**
 * 搜索
 * @returns
 */
function search(){
	$MB.refreshTable('roomTable');
}
/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".room-table-form")[0].reset();
    $MB.refreshTable('roomTable');
}

/**
 * 初始化酒店房间列表
 * @returns
 */
function initRoomTable(){
	var settings = {
	        url: ctx + "hotel/roomList",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                name: $(".room-table-form").find("input[name='name']").val().trim()
	            };
	        },
	        columns: [{
		            checkbox: true
		        }, {
		            field: 'roomId',
		            visible: false
		        }, {
		            field: 'name',
		            title: '房间名称'
		        }, {
		            field: 'summary',
		            title: '房间介绍'
		        }, {
		            field: 'originalPrice',
		            title: '原始价格'
		        }
	        ]
	    };
	$MB.initTable("roomTable", settings);
	
}
/**
 * 带参加载页面
 * @param obj
 * @param strparam
 * @returns
 */
function loadMainByParam(obj,strparam) {
    // 设置面包屑
    var $this = $(obj);
    $(".navigation").find("span").removeClass("navigation__active");
    $this.addClass("navigation__active").parents("ul").prev().addClass("navigation__active");
    var breadcrumnHtml = "";
    var target_text = $this.text();
    var text_arr = [];
    var parent = $this.parents("ul").prev().each(function () {
        var $this = $(this);
        text_arr.unshift($this.text());
    });
    for (var i = 0; i < text_arr.length; i++) {
        breadcrumnHtml += '<li class="breadcrumb-item">' + text_arr[i] + '</li>';
    }
    breadcrumnHtml += '<li class="breadcrumb-item">' + target_text + '</li>';
    $breadcrumb.html("").append(breadcrumnHtml);
    // 加载内容
    var $name = $this.attr("name");
    $.post(ctx + $name,strparam, function (r) {
        if (r.indexOf('账户登录') != -1) {
            location = location;
            return;
        }
        clearInterval(rediskeysSizeInterval);
        clearInterval(redisMemoryInfoInterval);
        $main_content.html("").append(r);
    });
}