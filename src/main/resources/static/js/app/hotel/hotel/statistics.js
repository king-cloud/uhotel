axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$ajax = axios;
var vm = new Vue({
	el:'.statistics',
	data:{
		timeList:[
			{name:'今日',type:0},
			{name:'3日',type:1},
			{name:'7日',type:2},
			{name:'月份',type:3},
			{name:'季度',type:4},
			{name:'年',type:5}
		],
		todaySum:[
			{name:'用户量/人',num:'0'},
			{name:'会员用户/人',num:'0'},
			{name:'成交订单量/单',num:'0'},
			{name:'充值金额/元',num:'0'},
			{name:'收益/元',num:'0'}
	    ],
		userSum:[
			{name:'新增用户/人',number:0},
			{name:'新增会员用户/人',number:0},
			{name:'成交订单量/单',number:0},
			{name:'充值金额/元',number:0},
			{name:'收益/元',number:0}
		],
		changColor:0

	},
	created:function () {
        var that = this;
         $.ajax({
	   	     type: "post",
	   	     url: ctx + 'homepage/getSumStatInfo', //接口地址
	   	     data: {}, 
	   	     dataType: "json",
	   	     success: function(res){
	   	    	 that.todaySum = res.msg 
	   	    	 console.log(res.msg)
	   	     }
        });

        $.ajax({
	   	     type: "post",
	   	     url: ctx +"homepage/getStatInfo", //接口地址
	   	     data: {"type":0}, //hotelId,索引(选择天数)
	   	     dataType: "json",
	   	     success: function(res){
	   	    	that.userSum = res.msg;  
	   	    	 console.log(res.msg)
	   	     }
        });
        
       
		 
	},
	methods:{
        reds:function (index){
            this.changColor = index;
            var that = this;
            $.ajax({
   	   	     type: "post",
   	   	     url: ctx +"homepage/getStatInfo", //接口地址
   	   	     data: {"type":index}, //hotelId,索引(选择天数)
   	   	     dataType: "json",
   	   	     success: function(res){
   	   	    	that.userSum = res.msg; 
   	   	    	 console.log(res.msg);
   	   	     }
           });
        },
    },
   

})