$(function(){
	initRoomTable();

});

/**
 * 初始化订单列表
 * @returns
 */
function initRoomTable(){
	var $roomTableForm = $(".room-table-form")
	var settings = {
	        url: ctx + "order/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                resident:$roomTableForm.find("input[name='resident']").val().trim(),
	                checkinTime:$roomTableForm.find("input[name='checkinTime']").val().trim(),
	                orderStatus:$roomTableForm.find("select[name='orderStatus']").val(),
	            }
	        },
	        columns: [/*{
		            checkbox: false
		        },*/{
		            field: '12',
		            title: '序号',
	            	formatter: function (value, row, index) {
	            		return ++index;
	                }
		        }, {
		        	field: 'orderId',
		        	visible: false
		        },{
		        	 field: 'resident',
			         title: '入住人'
		        }, {
		            field: 'orderSn',
		            title: '订单号'
		        },{
		        	field: 'mobile',
		        	title: '手机号'
		        },{
		            field: 'roomInfo.name',
		            title: '房型'
		        }, {
		            field: 'bookTime',
		            title: '下单时间'
		        }, {
		            field: 'roomCount',
		            title: '房间数'
		        }, {
		            field: 'checkinTime',
		            title: '入住日期'
		        }, {
		            field: 'checkoutTime',
		            title: '离店日期'
		        }, {
		            field: 'totalPrice',
		            title: '订单金额'
		        }, {
		        	field: 'orderAmount',
		        	title: '应付金额'
		        }, {
		            field: 'orderStatus',
		            title: '订单状态',
	            	formatter: function (value, row, index) {
	            		if (value === '0') return '<span class="badge badge-primary">新建</span>';
	                    if (value === '1') return '<span class="badge badge-success">已确认</span>';
	                    if (value === '2') return '<span class="badge badge-warning">已取消</span>';
	                    if (value === '3') return '<span class="badge badge-danger">已结束</span>';
	                }
		        }, {
		            field: 'orderStatus',
		            title: '编辑',
		            formatter : function(value, row, index){
		            	var hotelId = row.hotelId;
		            	var orderId = row.orderId;
		            	var orderStatus = row.orderStatus;
		            	if (value === '0') return '<button type="button" class="btn btn-info" onclick="submitOrder('+hotelId+',\'' + orderId + '\')" >确认</button>&nbsp<button type="button" class="btn btn-default" onclick="cancelOrder('+hotelId+',\'' + orderId + '\')">取消</button>';
		            	if (value === '1') return '&nbsp<button type="button" class="btn btn-default" onclick="cancelOrder('+hotelId+',\'' + orderId + '\')" >取消</button>';
		            	return '';
		            }
		        }
	        ]
	    };
	$MB.initTable("orderTable", settings);
}

function search() {
    $MB.refreshTable('orderTable');
}

function refresh() {
    $(".room-table-form")[0].reset();
    $MB.refreshTable('orderTable');
}

function submitOrder(hotelId,orderId) {
    $.post(ctx + "order/submit",{"hotelId":hotelId,"orderId":orderId}, function (r) {
        if (r.code === 0) {
        	$MB.n_success(r.msg);
        	$MB.refreshTable('orderTable');
        } else {
        	if(r.msg == '1044'){
        		$MB.n_warning("pms同步失败!");
        	}else{
        		$MB.n_warning(r.msg);
        	}
        }
    });
}

function cancelOrder(hotelId,orderId) {
    $.post(ctx + "order/cancel",{"hotelId":hotelId,"orderId":orderId}, function (r) {
        if (r.code === 0) {
        	$MB.n_success(r.msg);
        	$MB.refreshTable('orderTable');
        } else {
            $MB.n_warning(r.msg);
        }
    });
}
