var validator;
var $hotelAddForm = $("#hotel-add-form");
$(function () {
    $("#hotel-add .btn-save").click(function () {
    	var hotelName = $("#hotelName").val().trim();
    	if(!hotelName){
    		$MB.n_warning("酒店名称不能为空");
    		return;
    	}
		var mobile = $.trim($("#mobile").val());
		if(!mobile){
			$MB.n_warning("手机号不能为空");
    		return;
    	}else{
			if(!(/^1(3|4|5|7|8)\d{9}$/.test(mobile))){ 
				$MB.n_warning("请输入正确的手机号"); 
				return; 
			}
		}
        var name = $(this).attr("name");
        validator = $hotelAddForm.validate();
        var flag = validator.form();
        if (flag) {
            if (name == "save") {
                $.post(ctx + "hotel/add", $hotelAddForm.serialize(), function (r) {
                    if (r.code == 0) {
                    	//type=0  操作不成功
                    	if(r.msg==0){
                    		$MB.n_warning("手机号已存在");
                    	}else{
                    		closeModal();
                            $MB.refreshTable("hotelTable");
                            $MB.n_success("酒店添加成功");
                    	}
                    } else $MB.n_danger(r.msg);
                });
            }
            if (name == "update") {
                $.post(ctx + "hotel/update", $hotelAddForm.serialize(), function (r) {
                    if (r.code == 0) {
                    	//type=0  操作不成功
                    	if(r.msg==0){
                    		$MB.n_warning("手机号已存在");
                    	}else{
                    		closeModal();
                            $MB.refreshTable("hotelTable");
                            $MB.n_success("酒店编辑成功");
                    	}
                    } else $MB.n_danger(r.msg);
                });
            }
        }
    });
	$("#hotel-add .btn-close").click(function () {
        closeModal();
    });

});
function closeModal() {
    $("#hotel-add-button").attr("name", "save");
    $("#hotel-add-modal-title").html('新增酒店');
    $MB.closeAndRestModal("hotel-add");
}

function editHotelByAdmin(){
	var selected = $("#hotelTable").bootstrapTable("getSelections");
    var selected_length = selected.length;
    if (!selected_length) {
        $MB.n_warning('请勾选需要修改的酒店！');
        return;
    }
    if (selected_length > 1) {
        $MB.n_warning('一次只能修改一个酒店！');
        return;
    }
    var hotelId = selected[0].hotelId;
    $.post(ctx + "hotel/getHotel", {"hotelId": hotelId}, function (r) {
    	var $form = $('#hotel-add');
    	$form.modal();
    	$("#hotel-add-modal-title").html('编辑酒店');
    	var hotel=r.hotel;
    	$("#hotelId").val(hotel.hotelId);
    	$("#hotelName").val(hotel.hotelName);
		$("#mobile").val(hotel.mobile);
		$("#appid").val(hotel.appid);
		$("#openPms").val(hotel.openPms);
    	$("#hotel-add-button").attr("name", "update");
    });
}