axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
//axios.defaults.baseURL = 'http://localhost:7878/zkview';
Vue.prototype.$ajax = axios;

$(function(){

$.ajax({
     type: "post",
     url: ctx +"homepage/getHotelInfo", //接口地址
     //data: {"hotelId":""}, 
     dataType: "json",
     success: function(res){
    	 console.log(res)//res.msg[0]
    	 var selectDate='<option value="">请选择酒店名称</option>';
    	 for(var i= 0;i<res.msg.length;i++){
				//console.log("第一种遍历方式\t"+res.msg[i].hotelName);
    		 selectDate += '<option value="'+ res.msg[i].hotelId+'">'+res.msg[i].hotelName+'</option>'
    	 }
    	 $("#locationOfSel").html(selectDate);
     }
}),     
	
	
//ajax初始化 数据
$.ajax({
     type: "post",
     url: ctx +"homepage/getLineInfo", //接口地址
     data: {"type":""}, //hotelId,索引(选择天数)
     dataType: "json",
     success: function(res){
    	console.log('['+res.msg[0].value1+']');
    	console.log('['+res.msg[0].value2+']');
    	
    	//初始化echarts实例
    	var myChart = echarts.init(document.getElementById('lineChart'));
    	//线形颜色重定向
    	var colors = ['#32c787', '#ffcc7a','#cdcdcd'];
    	
        // 用户 和 会员数据 重新 赋值
    	var dataStrArr1=res.msg[0].value1.split(",");//分割成字符串数组  
    	var dataArr1=[];//保存转换后的整型字符串  
    	dataStrArr1.forEach(function(data,index,arr){  
    		dataArr1.push(+data);  
    	});  
    	
    	var dataStrArr2=res.msg[0].value2.split(",");//分割成字符串数组  
    	var dataArr2=[];//保存转换后的整型字符串  
    	dataStrArr2.forEach(function(data,index,arr){  
    		dataArr2.push(+data);  
    	}); 
    	xData = ["00:00", "04:00", "08:00", "12:00", "16:00", "20:00", "24:00"]
    	var option = {
		    color: colors,
		    //提示准星..
		    tooltip: {
		        trigger: 'none',
		        axisPointer: {
		            type: 'cross'
		        }
		    },
		    //图列大小、位置
		    legend: {
		    	show: true,
/*		        right:0,
		        top:0,
		        itemWidth:10,
		        itemHeight:10,*/
		    	left:100,
		        bottom:0,
		        data:['用户','会员']
		    },
		    //图表距离位置
		    grid: {
/*		    	left: '3%',
		        right: '4%',
		        bottom: '3%',
		        containLabel: true*/
		        top: 60,
		        left:40,
		        bottom:50
		    },
		    //x轴线形配置
		    xAxis: [
		        {
		            type: 'category',
		            axisTick: {
		                alignWithLabel: true
		            },
		            axisLine: {
		                onZero: false,
		                show:false
		            },
		            data: xData
		        },
		        {
		            type: 'category',
		            axisTick: {
		                alignWithLabel: true
		            },
		            axisLine: {
		                onZero: false,
		                show:false
		            },
		            axisLabel: {
                        interval:0,
                        //rotate:40
                    },
		            data: xData
		        }
		    ],
		    //y轴线形配置
		    yAxis: [
		        {   
		            name:"人",
		            nameLocation :'end' ,
		            type: 'value',
		            axisLine: {
		                show:false,
		                onZero: false,
		            }
		        }

		    ],
		    //数据
		    series: [
		        {
		        	name:'用户',
		            type:'line',
		            xAxisIndex: 1,
		            smooth: true,
		            data: dataArr1
		        },
		        {
		        	name:'会员',
		            type:'line',
		            smooth: true,
		            data: dataArr2
		        }
		    ]
		};

		myChart.setOption(option);
    }
})

//折线图 左侧 tab切换
$(".lineTab li").on("click",function(){
    var that  = $(this);
    var index = $(this).val();
    that.addClass('active').siblings().removeClass('active');
    var hotelId =  $('#locationOfSel option:selected').val();//选中的值
    //初始化echarts实例
    var myChart = echarts.init(document.getElementById('lineChart'));
    //线形颜色重定向
    var colors = ['#32c787', '#ffcc7a','#cdcdcd'];
    //ajax初始化 数据
    $.ajax({
         type: "post",
         url: ctx +"homepage/getLineInfo", //接口地址
         data: {"type":index,"hotelId":hotelId}, //hotelId,索引(选择天数)
         dataType: "json",
         success: function(res){
        	console.log('['+res.msg[0].value1+']');
         	console.log('['+res.msg[0].value2+']');
        	 
        	// 用户 和 会员数据 重新 赋值
         	var dataStrArr1=res.msg[0].value1.split(",");//分割成字符串数组  
         	var dataArr1=[];//保存转换后的整型字符串  
         	dataStrArr1.forEach(function(data,index,arr){  
         		dataArr1.push(+data);  
         	});  
         	
         	var dataStrArr2=res.msg[0].value2.split(",");//分割成字符串数组  
         	var dataArr2=[];//保存转换后的整型字符串  
         	dataStrArr2.forEach(function(data,index,arr){  
         		dataArr2.push(+data);  
         	});
         	function findX(index){
         		if(index=="0"){
             		//alert("今天")
             		xData = ["00:00", "04:00", "08:00", "12:00", "16:00", "20:00", "24:00"]
             	}else if(index=="1"){
             		//alert("三天")
             		//xData = ["11-10", "11-11", "11-12"]
             		var array = new Array();
            		for(var i=1;i<=3;i++){
            			var dd = new Date();
            			dd.setDate(dd.getDate()-i+1);//获取AddDayCount天后的日期 
            			var y = dd.getFullYear(); 
            			var m = dd.getMonth()+1;//获取当前月份的日期 
            			var d = dd.getDate();
            			if(m<10){  
            				m="0"+m;  
            			} 
            			if(d<10){  
            				d="0"+d;  
            			}
            			array.push(y+"-"+m+"-"+d)
            		}
            		xData = array.reverse()
             	}else if(index=="2"){
             		//alert("七天")
             		//xData = ["11-6", "11-7", "11-8", "11-9", "11-10", "11-11", "11-12"]
             		var array = new Array();
            		for(var i=1;i<=7;i++){
            			var dd = new Date();
            			dd.setDate(dd.getDate()-i+1);//获取AddDayCount天后的日期 
            			var y = dd.getFullYear(); 
            			var m = dd.getMonth()+1;//获取当前月份的日期 
            			var d = dd.getDate();
            			if(m<10){  
            				m="0"+m;  
            			} 
            			if(d<10){  
            				d="0"+d;  
            			}
            			array.push(y+"-"+m+"-"+d)
            		}
            		xData = array.reverse()
             	}else if(index=="3"){
             		//alert("月份")
             		var array = new Array();
       /*     		for(var i=1;i<=31;i++){
            			var dd = new Date();
            			dd.setDate(dd.getDate()-i);//获取AddDayCount天后的日期 
            			var y = dd.getFullYear(); 
            			var m = dd.getMonth()+1;//获取当前月份的日期 
            			var d = dd.getDate();
            			if(m<10){  
            				m="0"+m;  
            			} 
            			if(d<10){  
            				d="0"+d;  
            			}
            			array.push(y+"-"+m+"-"+d)
            		}*/
             		/*获取一个月的天数 */
             		function getCountDays() {
             		          var curDate = new Date();
             		        /* 获取当前月份 */
             		          var curMonth = curDate.getMonth();
             		        /*  生成实际的月份: 由于curMonth会比实际月份小1, 故需加1 */
             		        curDate.setMonth(curMonth + 1);
             		        /* 将日期设置为0, 这里为什么要这样设置, 我不知道原因, 这是从网上学来的 */
             		        curDate.setDate(0);
             		        /* 返回当月的天数 */
             		        return curDate.getDate();
             		}
             		var day=getCountDays();
             		function getEvryDay(){
             		var dayArry=[];
             		for (var k = 1; k <= day; k++) {
             		dayArry.push(k);
             		   }
             		   return dayArry;
             		};
            		xData = getEvryDay();
            		//alert(xData);
             	}else if(index=="4"){
             		//alert("季度")
             		var dd = new Date();
        			//dd.setDate(dd.getDate()-i);//获取AddDayCount天后的日期 
    	    		var year = dd.getFullYear(); 
    	    		var mon = dd.getMonth() + 2;//获取当前月份的日期 
    	    		var arry = new Array();
    	    			//var d = dd.getDate();
    	     		for(var i=0;i<3;i++){  
    	    		    mon=mon-1;  
    	    		    if(mon<=0){  
    	    		        year=year-1;  
    	    		        mon=mon+12;  
    	    		    }  
    	    		    if(mon<10){  
    	    		        mon="0"+mon;  
    	    		    }  
    	    		    arry[i]=year+"-"+mon;  
    	    		}
    	     		xData = arry.reverse()
             	}else if(index=="5"){
             		//alert("年")
             		//xData = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
             		var dd = new Date();
            			//dd.setDate(dd.getDate()-i);//获取AddDayCount天后的日期 
            		var year = dd.getFullYear(); 
            		var mon = dd.getMonth() + 2;//获取当前月份的日期 
            		var arry = new Array();
            			//var d = dd.getDate();
             		for(var i=0;i<12;i++){  
            		    mon=mon-1;  
            		    if(mon<=0){  
            		        year=year-1;  
            		        mon=mon+12;  
            		    }  
            		    if(mon<10){  
            		        mon="0"+mon;  
            		    }  
            		    arry[i]=year+"-"+mon;  
            		}
             		xData = arry.reverse()
             	}
         		return xData
         	}         	
         	xData = findX(index);         	
        	var option = {
    		    color: colors,
    		    //提示准星..
    		    tooltip: {
    		        trigger: 'none',
    		        axisPointer: {
    		            type: 'cross'
    		        }
    		    },
    		    //图列大小、位置
    		    legend: {
    		        left:100,
    		        bottom:0,
    		        //itemWidth:10,
    		        //itemHeight:10,
    		        data:[{name:'用户'}, {name:'会员'}]
    		    },
    		    //图表距离位置
    		    grid: {
    		        top: 60,
    		        left:40,
    		        bottom:50
    		    },
    		    //x轴线形配置
    		    xAxis: [
    		        {
    		            type: 'category',
/*    		            axisTick: {
    		                alignWithLabel: true
    		            },
    		            axisLine: {
    		                onZero: false,
    		                show:true
    		            },*/
    		            data: xData,
    		            axisLabel: {
                            interval:0,
                            //rotate:40
                        }
    		        },
    		        {
    		            type: 'category',
    		            axisTick: {
    		                alignWithLabel: true
    		            },
    		            axisLine: {
    		                onZero: false,
    		                show:true
    		            },
    		            axisLabel: {
                            interval:0,
                            //rotate:40
                        },
    		            data: xData
    		        }
    		    ],
    		    //y轴线形配置
    		    yAxis: [
    		        {   
    		            name:"人",
    		            nameLocation :'end' ,
    		            type: 'value',
    		            axisLine: {
    		                show:false,
    		                onZero: false,
    		            }
    		        }

    		    ],
    		    //数据
    		    series: [
    		        {
    		            name:'用户',
    		            type:'line',
    		            xAxisIndex: 1,
    		            smooth: true,
    		            data: dataArr1
    		        },
    		        {
    		            name:"会员",
    		            type:'line',
    		            smooth: true,
    		            data: dataArr2
    		        }
    		    ]
    		};

    		myChart.setOption(option);
        }
    })

    
})

})

function showHotelInfo(){
	var data =  $('#locationOfSel option:selected').val();//选中的值
	 $('.lineTab li').eq(0).addClass('active').siblings().removeClass('active');
	//ajax初始化 数据
	$.ajax({
	     type: "post",
	     url: ctx +"homepage/getLineInfo", //接口地址
	     data: {"hotelId":data}, //hotelId,索引(选择天数)
	     dataType: "json",
	     success: function(res){
	    	console.log('['+res.msg[0].value1+']');
	    	console.log('['+res.msg[0].value2+']');
	    	
	    	//初始化echarts实例
	    	var myChart = echarts.init(document.getElementById('lineChart'));
	    	//线形颜色重定向
	    	var colors = ['#32c787', '#ffcc7a','#cdcdcd'];
	    	
	        // 用户 和 会员数据 重新 赋值
	    	var dataStrArr1=res.msg[0].value1.split(",");//分割成字符串数组  
	    	var dataArr1=[];//保存转换后的整型字符串  
	    	dataStrArr1.forEach(function(data,index,arr){  
	    		dataArr1.push(+data);  
	    	});  
	    	
	    	var dataStrArr2=res.msg[0].value2.split(",");//分割成字符串数组  
	    	var dataArr2=[];//保存转换后的整型字符串  
	    	dataStrArr2.forEach(function(data,index,arr){  
	    		dataArr2.push(+data);  
	    	}); 
	    	//xData = ["00:00", "04:00", "08:00", "12:00", "16:00", "20:00", "24:00"]
	    	xData = findX(index);
	    	var option = {
			    color: colors,
			    //提示准星..
			    tooltip: {
			        trigger: 'none',
			        axisPointer: {
			            type: 'cross'
			        }
			    },
			    //图列大小、位置
			    legend: {
			        left:100,
			        bottom:0,
/*			        itemWidth:10,
			        itemHeight:10,
			        data:[{name:'用户',icon:'rect'}, {name:'会员',icon:'rect'}]*/
			        data:['用户','会员']
			    },
			    //图表距离位置
			    grid: {
			        top: 60,
			        left:40,
			        bottom:50
			    },
			    //x轴线形配置
			    xAxis: [
			        {
			            type: 'category',
			            axisTick: {
			                alignWithLabel: true
			            },
			            axisLine: {
			                onZero: false,
			                show:false
			            },
			            data: xData,
			            axisLabel: {
                            interval:0,
                            //rotate:40
                        }
			        },
			        {
			            type: 'category',
			            axisTick: {
			                alignWithLabel: true
			            },
			            axisLine: {
			                onZero: false,
			                show:false
			            },
			            axisLabel: {
                            interval:0,
                            //rotate:40
                            show:false
                        },
			            data: xData
			        }
			    ],
			    //y轴线形配置
			    yAxis: [
			        {   
			            name:"人",
			            nameLocation :'end' ,
			            type: 'value',
			            axisLine: {
			                show:false,
			                onZero: false,
			            }
			        }

			    ],
			    //数据
			    series: [
			        {
			            name:'用户',
			            type:'line',
			            xAxisIndex: 1,
			            smooth: true,
			            data: dataArr1
			        },
			        {
			            name:"会员",
			            type:'line',
			            smooth: true,
			            data: dataArr2
			        }
			    ]
			};

			myChart.setOption(option);
	    }
	})
}

Vue.prototype.$http = axios;
var vm = new Vue({
    el:'#sum',
    data:{
        Sum:[
            {name:'用户/人',number:0},
            {name:'会员/人',number:0},
            {name:'订单/单',number:0},
            {name:'复购用户/人',number:0},
            {name:'充值金额/元',number:0},
            {name:'赠送金额/元',number:0},
            {name:'余额消费(充值/赠送)/元',number:0}
        ]
    },
    created:function () {
        var that = this;
        this.$ajax({  //总数据
           method: 'post',
           url: ctx +'homepage/getLineStatInfo',
           //data:params
        }).then(function (res) {
           //alert(res.data.msg);
           //处理结果
           that.Sum = res.data.msg; 
        }).catch(function (error) {
          console.log(error);
        });
        
	}
})




