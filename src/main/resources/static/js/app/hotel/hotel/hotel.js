/**
 * 酒店管理
 */

$(function(){
	initHotelTable();
/**
 * 酒店新增
 */
});

function closejd() {
    $MB.closeAndRestModal("hotel-add-cfg-jd");
}
function closehy() {
    $MB.closeAndRestModal("hotel-add-cfg-hy");
}
/**
 * 搜索
 * @returns
 */
function search(){
	$MB.refreshTable('hotelTable');
}
/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".hotel-table-form")[0].reset();
    $MB.refreshTable('hotelTable');
}
/**
 * 初始化酒店列表
 * @returns
 */
function initHotelTable(){
	var $hotelTableForm = $(".hotel-table-form");
	var settings = {
	        url: ctx + "hotel/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                mobile:$hotelTableForm.find("input[name='mobile']").val().trim(),
	                hotelSn:$hotelTableForm.find("input[name='hotelSn']").val().trim(),
	                hotelName:$hotelTableForm.find("input[name='hotelName']").val().trim()
	            }
	        },
	        columns: [{
		            checkbox: true
		        }, {
		            field: 'hotelId',
		            title: '酒店ID'
		        }, {
		            field: 'hotelSn',
		            title: '酒店编号'
		        }, {
		            field: 'hotelName',
		            title: '酒店名称'
		        }, {
		            field: 'mobile',
		            title: '账号/手机号'
		        },{
		        	field: 'openPms',
		        	title: '是否同步pms',
		        	formatter :function(value,row){
		        		if(value == 'true') return '同步';
		        		if(value != 'true') return '未同步';
		        	}
		        	
		        },{
		            field: 'addTime',
		            title: '注册时间'
		        },{
		            title: '操作',
			        formatter : function(u,row){
			        		// 酒店H端暂时注释掉。
			            	// var ohtml='<input type="button" value="酒店H端设置" data-toggle="modal" data-target="#hotel-add-cfg-jd" onclick="getwxcfgjd('+row.hotelId+','+"2"+',this)"> ';
			            	var ohtml=' <input type="button" value="会员C端设置" data-toggle="modal" data-target="#hotel-add-cfg-hy"  onclick="getwxcfghy('+row.hotelId+','+"1"+',this)">';
			            	return ohtml;
	                	} 
		        }]
	    };
	
	$MB.initTable("hotelTable", settings);
	
}
function getwxcfgjd(hotelid,apptype,n){
	$("#hotelIdj").val(hotelid);
	//$(n).parent().parent().children().eq(0).html('<input data-index="1" name="btSelectItem" type="checkbox">')
	$(n).parent().parent().children().eq(0).children().prop("checked","checked");
	$.ajax({
		type : "post",
		url :  "wxCfg/queryWxCfg",
		dataType : "json",
		data : {"hotelId":hotelid,"appType":apptype},
		success : function(res) {
				$("#wxcfgidj").val(res.wxcfgid);
				$("#app_typej").val(res.appType);
				$("#descriptionj").val(res.description);
				$("#app_idj").val(res.appId);
				$("#appkeyj").val(res.appkey);
				$("#mch_idj").val(res.mchId);
				$("#secret_keyj").val(res.secretKey);
				$("#cert_pemj").val(res.certPem);
				$("#key_pemj").val(res.keyPem);
				$("#apiclient_certj").val(res.apiclientCert);
				$("#is_validj").val(res.isValid);
				$("#pay_typej").val(res.payType);
		},
		error : function() {
		}
	});
}
function getwxcfghy(hotelid,apptype,n){
	$("#file_form")[0].reset();
	$("#hotelIdhy").val(hotelid);
	$(n).parent().parent().children().eq(0).children().prop("checked","checked");
	$.ajax({
		type : "post",
		url :  "wxCfg/queryWxCfg",
		dataType : "json",
		data : {"hotelId":hotelid,"appType":apptype},
		
		success : function(res) {
			console.log(res)
				$("#app_typehy").val(res.appType);
				$("#wxcfgidhy").val(res.wxcfgid);
				$("#descriptionhy").val(res.description);
				$("#app_idhy").val(res.appId);
				$("#appkeyhy").val(res.appkey);
				$("#mch_idhy").val(res.mchId);
				$("#secret_keyhy").val(res.secretKey);
				$("#cert_pemhy").val(res.certPem);
				$("#key_pemhy").val(res.keyPem);
				$("#apiclient_certhy").val(res.apiclientCert);
				$("#is_validhy").val(res.isValid);
				$("#pay_typehy").val(res.payType);
		},
		error : function() {
			// alert("数据请求出错") ;
		}
	});
	
}
	

function addhy(){
	var hotelid=$("#hotelIdhy").val(),
		wxcfgid=$("#wxcfgidhy").val(),
		appType=$("#app_typehy").val(),
		description=$("#descriptionhy").val(),
		appId=$("#app_idhy").val(), //公众账号ID
		appkey=$("#appkeyhy").val(), //appkey
		mchId=$("#mch_idhy").val(), //微信商户号
		secretKey=$("#secret_keyhy").val(),//API秘钥
		certPem=$("#cert_pemhy").val(), //微信客户端证书
		keyPem=$("#key_pemhy").val(), //微信客户端证书密钥
		apiclientCert= $("#apiclient_certhy").val(), //证书pkcs12
		isValid=$("#is_validhy").val(),
		payType=$("#pay_typehy").val();
	if(check()){
		
		$.ajax({
			type : "post",
			url :  "wxCfg/addWxCfg",
			dataType : "json",
			data : {
				"hotelId":hotelid,"appType":appType,
				"wxcfgId":wxcfgid,"description":description,
				"appId":appId,"appkey":appkey,
				"mchId":mchId,"secretKey":secretKey,
				"certPem":certPem,"keyPem":keyPem,
				"apiclientCert":apiclientCert,"isValid":isValid,
				"payType":payType
			},
			success : function(res) {
				if(res==1){
					closehy();
				}else {
					alert("保存失败")
				}
			},
			error : function() {
			}
		});
	};
	function check(){
		if(!appId){
			$MB.n_warning("公众账号ID不能为空");
			return;
		}
		if(!appkey){
			$MB.n_warning("appkey不能为空");
			return;
		}
		if(!mchId){
			$MB.n_warning("微信商户号不能为空");
			return;
		}
		if(!secretKey){
			$MB.n_warning("API秘钥不能为空");
			return;
		}
		if(!certPem){
			$MB.n_warning("微信客户端证书不能为空");
			return;
		}
		if(!keyPem){
			$MB.n_warning("微信客户端证书密钥不能为空");
			return;
		}
//		if(!apiclientCert){
//			$MB.n_warning("证书pkcs12不能为空");
//			return;
//		}
		return true ;
	}
}
function addjd(){
	var hotelid=$("#hotelIdj").val();
	var wxcfgid=$("#wxcfgidj").val();
	var appType=$("#app_typej").val();
	var description=$("#descriptionj").val();
	var appId=$("#app_idj").val();
	var appkey=$("#appkeyj").val();
	var mchId=$("#mch_idj").val();
	var secretKey=$("#secret_keyj").val();
	var certPem=$("#cert_pemj").val();
	var keyPem=$("#key_pemj").val();
	var apiclientCert= $("#apiclient_certj").val();
	var isValid=$("#is_validj").val();
	var payType=$("#pay_typej").val();
	if(check()){
		$.ajax({
			type : "post",
			url :  "wxCfg/addWxCfg",
			dataType : "json",
			data : {
				"hotelId":hotelid,"appType":appType,
				"wxcfgId":wxcfgid,"description":description,
				"appId":appId,"appkey":appkey,
				"mchId":mchId,"secretKey":secretKey,
				"certPem":certPem,"keyPem":keyPem,
				"apiclientCert":apiclientCert,"isValid":isValid,
				"payType":payType
			},
			success : function(res) {
				if(res==1){
					closejd();
				}else {
					alert("保存失败")
				}
			},
			error : function() {
				alert("保存失败") ;
			}
		});
	}
	function check(){
		if(!appId){
			$MB.n_warning("公众账号ID不能为空");
			return;
		}
		if(!appkey){
			$MB.n_warning("appkey不能为空");
			return;
		}
		if(!mchId){
			$MB.n_warning("微信商户号不能为空");
			return;
		}
		if(!secretKey){
			$MB.n_warning("API秘钥不能为空");
			return;
		}
		if(!certPem){
			$MB.n_warning("微信客户端证书不能为空");
			return;
		}
		if(!keyPem){
			$MB.n_warning("微信客户端证书密钥不能为空");
			return;
		}
		if(!apiclientCert){
			$MB.n_warning("证书pkcs12不能为空");
			return;
		}
		return true ;
	}
}
var $fileForm = $("#file_form");

function upload(){
	var hotelid=$("#hotelId").val();
	var options = {
			url : "wxCfg/upload/certificate",
			type : "post",
			dataType : "json",
			data : {
				"hotelid":hotelid
			},
			success : function(res){
				if(res.status == 1){
					$("#apiclient_certhy").val(res.data)
				}else{
					alert("上传失败")
				}
				
			},
			error : function() {
			alert("上传失败")
			}
	}
	$fileForm.ajaxSubmit(options);
}
var $fileFormj = $("#filej_form");

function uploadj(){
	var hotelid=$("#hotelIdj").val();
	var options = {
			url : "wxCfg/upload/certificate",
			type : "post",
			dataType : "json",
			data : {
				"hotelid":hotelid
			},
			success : function(res){
				if(res.status == 1){
					$("#apiclient_certj").val(res.data)
				}else{
					alert("上传失败")
				}
				
			},
			error : function() {
			alert("上传失败")
			}
	}
	$fileFormj.ajaxSubmit(options);
}
