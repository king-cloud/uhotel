//@ sourceURL=hotelEdit.js
var $form = $('#hotelinfo');
$(function () {
	getHotel();
	$("#newPicUrl").on("click",'.remImg',function(){
		var that = $(this);
		that.parent().remove();
	});
	$("#oldPicUrl").on("click",'.remImg',function(){
		var that = $(this);
		that.parent().remove();
		var picId=that.parent().find('.pid').val();
		$.post(ctx + "hotel/delPicture",{"picId":picId}, function (r) {
			 $MB.n_success(r);
		});
	});
});
function getHotel(){
	$.post(ctx + "hotel/getHotel", function (r) {
		var hotel=r.hotel;
		if(hotel!=null){
			 // 酒店政策及设施
			var faclist=r.facList;
			var pollist=r.polList;
			var facHtml="";
			for(var i=0;faclist.length>i;i++){
				facHtml+="<label class='custom-control custom-checkout'>";
				facHtml+="<input name='facilities' type='checkbox' value='"+faclist[i].keyy+"' class='custom-control-input'>";
				facHtml+="<span class='custom-control-indicator'></span>";
				facHtml+="<span class='custom-control-description'>"+faclist[i].valuee+"</span>";
				facHtml+="</label>";
			}
			$(".facilities").html(facHtml);
			var polHtml="";
			for(var i=0;pollist.length>i;i++){
				polHtml+="<label class='custom-control custom-checkout'>";
				polHtml+="<input name='policies' type='checkbox' value='"+pollist[i].keyy+"' class='custom-control-input'>";
				polHtml+="<span class='custom-control-indicator'></span>";
				polHtml+="<span class='custom-control-description'>"+pollist[i].valuee+"</span>";
				polHtml+="</label>";
			}
			$(".policies").html(polHtml);
			//发票政策
			var whtml = "";
			var invoiceStr = hotel.invoiceType;
			if(invoiceStr == 0){
				$(".invoice_div").hide();
				$("input[name='invoiceType']").attr("checked",false);
				whtml='开启（请点击开启）<input  id="invoiceStatus" name="invoiceStatus" type="hidden" value="0"/>';
			}else{
				var arr = invoiceStr.split(',')
				$(".invoice_div").show();
				whtml='关闭（已开启）<input  id="invoiceStatus" name="invoiceStatus" type="hidden" value="1"/>';
				for(var i =0 ;i<arr.length;i++){
					if(arr[i] == 1){
						$("#int").attr("checked","checked")
					}
					if(arr[i] == 2){
						$("#int1").attr("checked","checked")
					}
					if(arr[i] == 3){
						$("#int2").attr("checked","checked")
					}
				}
				
				
			}
			$("#wifiStatusB").html(whtml);
			
			var fBoxObj = $("input:checkbox[name='facilities']");  //获取所有的复选框
			var facs=hotel.facilities;
			if(facs!=null){
				var fs = facs.split(",");
				$.each(fs, function(index, facValue){
			       fBoxObj.each(function () {
			            if($(this).val() == facValue) {
			               $(this).attr("checked",true);
			            }
			        });
				});
			}
			var pBoxObj = $("input:checkbox[name='policies']");  //获取所有的复选框
			var pols=hotel.policies;
			if(pols!=null){
				var ps = pols.split(",");
				$.each(ps, function(index, polValue){
					pBoxObj.each(function () {
			            if($(this).val() == polValue) {
			               $(this).attr("checked",true);
			            }
			        });
				});
			}
			$("#hotelName").val(hotel.hotelName).attr("readonly", true);
			$("#hotelSn").val(hotel.hotelSn).attr("readonly", true);
			$("#mobile").val(hotel.mobile);
			$("#hotelId").val(hotel.hotelId);
			$("#appid").val(hotel.appid).attr("readonly", true);
			$("#phone").val(hotel.phone);
			$("#appsecret").val(hotel.appsecret);
			$("#summary").val(hotel.summary);
			$("#address").val(hotel.address);
			$("#tag").val(hotel.tag);
			$("#markOne").val(hotel.markOne);
			$("#markTwo").val(hotel.markTwo);
			$("#myPolicies").val(hotel.myPolicies);
			$("#lat").val(hotel.latitude);
			$("#lng").val(hotel.longitude);
			$("#wifiStatus").val(hotel.wifiStatus);
			if(hotel.wifiStatus==1){
				$("#wifiName").val(hotel.wifiName);
				$("#wifiPassword").val(hotel.wifiPassword);
				$("#wifiStatusA").html('关闭（已开启）<input  id="wifiStatus" name="wifiStatus" type="hidden" value="1"/>');
				$(".wifi_div").show();
			}
			var pics=hotel.pictures;
			var picHtml="";
			 $.each(pics,function(n,p) {  
				 picHtml+="<div style='width:80px;height:80px;position: relative;float:left;margin-right:6px;'><img style='float: left;' width='80' height='80' src='"+'http://images.haoyizhu.cn'+p.picUrl+"' />";
				 picHtml+="<span style='position:absolute;right:-4px;top:-3px;display:block;width:20px;height:20px;border-radius:10px;background:lemonchiffon;text-align:center;line-height:20px;color:red;font-size:14px;cursor: pointer;' class='remImg'>X</span>"
				 picHtml+="<input type='hidden' class='pid' name='pictures.picId' value='"+p.picId+"' /></div>";
		         });  
			 $("#oldPicUrl").html(picHtml);
		}
	});  
}

function updateHotel(){
	var phone = $.trim($("#phone").val());
	var summary = $.trim($("#summary").val());
	var address = $.trim($("#address").val());
	var markOne = $.trim($("#markOne").val());
	var markTwo = $.trim($("#markTwo").val());
	var facilities = $.trim($("input:checkbox[name='facilities']:checked").length);
	var policies = $.trim($("input:checkbox[name='policies']:checked").length);
	var myPolicies = $.trim($("#myPolicies").val());
	var latitude = $.trim($("#lat").val());
	var oldPic = $.trim($("#oldPicUrl").html());
	var pictures = $.trim($("#newPicUrl").html());
	var wifiStatus = $.trim($("#wifiStatus").val());

	if(wifiStatus==1){
		var wifiName = $.trim($("#wifiName").val());
		var wifiPassword = $.trim($("#wifiPassword").val());
		if(!wifiPassword){
			$MB.n_warning("wifi密码不能为空");
			return;
		}else{
			if(wifiPassword.length < 8){
				$MB.n_warning("wifi密码不能小于8个字符");
				return;
			}
		}
		if(!wifiName){
			$MB.n_warning("wifi名称不能为空");
			return;
		}
	}
	if(!summary){
		$MB.n_warning("酒店简介不能为空");
		return;
	}else{
		if(summary.length > 240){
			$MB.n_warning("酒店简介不能大于240个字符");
			return;
		}
	}
	if(!address){
		$MB.n_warning("酒店地址不能为空");
		return;
	}
	if(!phone){
		$MB.n_warning("手机号不能为空");
		return;
	}
	if(!markOne){
		$MB.n_warning("请选择一个参照物");
		return;
	}
	if(!markTwo){
		$MB.n_warning("请详细说明地址距离");
		return;
	}
	if(facilities==0){
		$MB.n_warning("请最少选择一种酒店设施");
		return;
	}
	if(policies==0){
		$MB.n_warning("请最少选择一条酒店政策");
		return;
	}
	if(!latitude){
		$MB.n_warning("请选择坐标位置");
		return;
	}
	if(!myPolicies){
		$MB.n_warning("经理，你不说点什么吗");
		return;
	}
	if(!oldPic&&!pictures){
		$MB.n_warning("酒店图片不能为空");
		return;
	}
	$.post(ctx + "hotel/update",$form.serialize(), function (r) {
		if (r.code === 0) {
			//type=0  操作不成功
        	if(r.msg==0){
        	}else{
        		$MB.n_success("酒店编辑成功");
                $obj = $("span[name='hotel/toUpdate']");
        		loadMain($obj);
        	}
        } else $MB.n_danger("编辑失败");
    });
}

function uploadPics(){
	var hotelId=$("#hotelId").val();
	var options = {
			url : ctx+"upload/uploadPics?typePic=hotelBanner&fid="+hotelId,
			type : "post",
			dataType : "json",
			success : function(m){
				if(m.code==1){
					$MB.n_warning(m.msg);
					return;	
				}
				/*回显图片*/
				if(m.code==0){
					var imgStr=m.msg;
					var imgHtml="";
					for(var i=0;imgStr.length>i;i++){
					  imgHtml+="<div style='width:80px;height:80px;position: relative;float:left;margin-right:6px;'><img style='' class='hpics' width='80' height='80' src="+'http://images.haoyizhu.cn'+imgStr[i]+" />";
					  imgHtml+="<input type='hidden' name='pictures' value="+imgStr[i]+" /><span style='position:absolute;right:-4px;top:-3px;display:block;width:20px;height:20px;border-radius:10px;background:lemonchiffon;text-align:center;line-height:20px;color:red;font-size:14px;cursor: pointer;' class='remImg'>X</span></div>";
					}
					$("#newPicUrl").append(imgHtml);
				}
			}
	}
	$("#hotelinfo").ajaxSubmit(options);
}



//删除酒店
function deleteHotel(){
	var selected = $("#hotelTable").bootstrapTable("getSelections");
    var selected_length = selected.length;
    if (!selected_length) {
        $MB.n_warning('请选择一个酒店！');
        return;
    }
    var ids = "";
    for (var i = 0; i < selected_length; i++) {
        ids += selected[i].hotelId;
        if (i !== (selected_length - 1)) ids += ",";
    }
    $MB.confirm({
        text: "确定删除选中酒店？",
        confirmButtonText: "确定删除"
    }, function () {
    	$.post(ctx + "hotel/delete",{"ids":ids}, function (r) {
    		if (r.code == 0) {
                $MB.n_success(r.msg);
                $MB.refreshTable('hotelTable');
            } else $MB.n_danger(r.msg);
        });
    });
    
	
}

function checkPicForm(){             
	var filepath = document.getElementById("hotelBanner").files;  
	if(filepath==""){       
		alert("请选择上传的文件！");          
		return false;       
	}      
	/*for (var i = 0; i < filepath.length; i++) {
		var extname = filepath[i].name.substring(filepath[i].name.lastIndexOf(".")+1,filepath[i].name.length);      
		extname = extname.toLowerCase();//处理了大小写       
		if(extname!= "jpg"&&extname!= "png"){        
			alert("只能上传jpg,png格式的图片！");        
			return false;       
		} 
	}  */ 
	uploadPics(); 
//	//后台校验图片格式及大小
//	var options = {
//			url : ctx + "upload/uploadPics?typePic=hotelBanner",
//			type : "post",
//			dataType : "json",
//			success : function(r){
//				console.log(123456);
////				if(!r){
////				}else{
////					$MB.n_warning(r);
////				}
//			}
//	}
//	$("#hotelinfo").ajaxSubmit(options);
}
function onSwitch(){
	var ws = $("#wifiStatus").val();
	var whtml='';
	if(ws==1){
		$(".wifi_div").hide();
		whtml='开启（请点击开启）<input  id="wifiStatus" name="wifiStatus" type="hidden" value="0"/>';
	}else{
		$(".wifi_div").show()
		whtml='关闭（已开启）<input  id="wifiStatus" name="wifiStatus" type="hidden" value="1"/>';
	}
	$("#wifiStatusA").html(whtml);
}
function onSwitchByinvoice(){
	var ws = $("#invoiceStatus").val();
	var whtml='';
	if(ws==1){
		$(".invoice_div").hide();
		$("input[name='invoiceType']").attr("checked",false);
		whtml='开启（请点击开启）<input  id="invoiceStatus" name="invoiceStatus" type="hidden" value="0"/>';
	}else{
		$(".invoice_div").show();

		whtml='关闭（已开启）<input  id="invoiceStatus" name="invoiceStatus" type="hidden" value="1"/>';
	}
	$("#wifiStatusB").html(whtml);
}