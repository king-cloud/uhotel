//@ sourceURL=roomAdd.js
var $roomAddForm = $(".room-add-form");
$(function(){
	 hiddenNotCancle();
	//删除房间列表图
	$("#newlistUrl").on("click",'.remImg',function(){
		var that = $(this);
		that.parent().remove();
		var picId=that.parent().find('#piclistId').val();
		$.post(ctx + "hotel/delPicture",{"picId":picId}, function (r) {
			 $MB.n_success(r);
		});
	});
	//删除房间图
	$("#oldRoomUrl").on("click",'.remImg',function(){
		var that = $(this);
		that.parent().remove();
		var picId=that.parent().find('.pid').val();
		$.post(ctx + "hotel/delPicture",{"picId":picId}, function (r) {
			 $MB.n_success(r);
		});
	});
	$("#newRoomUrl").on("click",'.remImg',function(){
		var that = $(this);
		that.parent().remove();
		var pcount=$("#pcount").val();
		pcount-=1;
		$("#pcount").val(pcount);
	});
	$("#roomButton .btn-save").click(function () {
		var name=$.trim($("#name").val());
		var summary=$.trim($("#summary").val());
		var residentLimit=$.trim($("#residentLimit").val());
		var originalPrice=$.trim($("#originalPrice").val());
		var roomNum=$.trim($("#roomNum").val());
		var newlistUrl=$.trim($("#newlistUrl").html());
		var facilities = $.trim($("input:checkbox[name='facilities']:checked").length);
		var policies = $.trim($("input:checkbox[name='policies']:checked").length);
		var oldRoomUrl = $.trim($("#oldRoomUrl").html());
		var newRoomUrl = $.trim($("#newRoomUrl").html());
		if(!name){
			$MB.n_warning("房间名不能为空");
			return;
		}else{
			if(name.length > 20 ){
				$MB.n_warning("房间名称不能大于20个字符");
				return;
			}
		}
		if(!summary){
			$MB.n_warning("房间简介不能为空");
			return;
		}else{
			if(summary.length > 240 ){
				$MB.n_warning("房间介绍不能大于240个字符");
				return;
			}
		}
		if(!residentLimit){
			$MB.n_warning("入住人数不能为空");
			return;
		}
		if(!originalPrice){
			$MB.n_warning("价格不能为空");
			return;
		}else if(originalPrice<=0){
			$MB.n_warning("价格不能小于等于0");
			return;
		}
		if(!roomNum){
			$MB.n_danger("房间数量不能为空");
			return;
		}
		if(!newlistUrl){
			$MB.n_warning("房间列表图为空");
			return;
		}
		if(facilities==0){
			$MB.n_warning("请最少选择一种酒店设施");
			return;
		}
		if(policies==0){
			$MB.n_warning("请最少选择一条酒店政策");
			return;
		}
		if(!newlistUrl){
			$MB.n_warning("房间列表图为空");
			return;
		}
		if(!oldRoomUrl&&!newRoomUrl){
			$MB.n_warning("房间图片不能为空");
			return;
		}
		var roomId=$("#roomId").val();
		if(!roomId){
			addRoomInfo();
		}else{
			editRoomInfo(roomId);
		}
	});
    payType();
});
function payType(){
	 var payType=$("#stype").val();
     var payObj = $("input:radio[name='payType']");
     payObj.each(function () {
         if($(this).val() == payType) {
            $(this).attr("checked", "checked");
         }
     });
     var fBoxObj = $("input:checkbox[name='facilities']");  //获取所有的复选框
		var facs=$("#facilities").val();
		if(!facs){
			return;
		}
		var fs = facs.split(",");
		$.each(fs, function(index, facValue){
	       fBoxObj.each(function () {
	            if($(this).val() == facValue) {
	               $(this).attr("checked",true);
	            }
	        });
		});
		var pBoxObj = $("input:checkbox[name='policies']");  //获取所有的复选框
		var pols=$("#policies").val();
		if(!pols){
			return;
		}
		var ps = pols.split(",");
		$.each(ps, function(index, polValue){
			pBoxObj.each(function () {
	            if($(this).val() == polValue) {
	               $(this).attr("checked",true);
	            }
	        });
		});
		 if(payType==1){
			//到店支付  隐藏不可退款政策
		    hiddenNotCancle();
		 }
}
function addRoomInfo(){
	$.post(ctx + "hotel/addRoom", $roomAddForm.serialize(), function (r) {
        if (r.code === 0) {
            $MB.n_success(r.msg);
            $obj = $("span[name='hotel/roomIndex']");
        	loadMain($obj[0]);
        } else $MB.n_danger(r.msg);
    });
}
function cancleEdit(){
	$obj = $("span[name='hotel/roomIndex']")
	loadMain($obj[0]);
}
/**
 * 房间列表图 上传一张
 * @param n
 * @returns
 */
function PicsList(n){
	var typePic=$(n).attr('data-type');
	var roomId=$("#roomId").val();
	var options = {
			url : ctx+"upload/uploadPic?typePic="+typePic+"&fid"+roomId,
			type : "post",
			dataType : "json",
			success : function(m){
				if(m.code==1){
					$MB.n_warning(m.msg);
					return;	
				}
				//回显图片
				if(m.code==0){
					var txt=$("#newlistUrl").html().trim();
					var imgHtml="";
					imgHtml+="<div style='width:80px;height:80px;position: relative;float:left;margin-right:6px;' >";
					var imgStr=m.msg;
					 if(!txt){
				    }else{
				    	var picId=$("#piclistId").val();
				    	imgHtml+="<input type='hidden' name='picture.picId' value="+picId+" />";
				    }
				    imgHtml+="<img style='float: left;' width='80' height='80' src="+'http://images.haoyizhu.cn'+imgStr+" />";
				    imgHtml+="<span style='position:absolute;right:-4px;top:-3px;display:block;width:20px;height:20px;border-radius:10px;background:lemonchiffon;text-align:center;line-height:20px;color:red;font-size:14px;cursor: pointer;' class='remImg'>X</span>";
				    imgHtml+="<input type='hidden' name='picture.picUrl' value="+imgStr+" />";
				    imgHtml+="</div>";
					$("#newlistUrl").html(imgHtml);
				}
			}
	}
	$roomAddForm.ajaxSubmit(options);
}
/**
 * 房间详情图 上传多张
 * @param n
 * @returns
 */
function PicsInfo(n){
	var pcount=$("#pcount").val();
	var typePic=$(n).attr('data-type');
	var roomId=$("#roomId").val();
	var options = {
			url : ctx+"upload/uploadPics?typePic="+typePic+"&fid="+roomId,
			type : "post",
			dataType : "json",
			success : function(m){
				if(m.code==1){
					$MB.n_warning(m.msg);
					return;	
				}
				//回显图片
				if(m.code==0){
					var imgHtml="";
					var imgStr=m.msg;
					for(var i=0;imgStr.length>i;i++){
					  imgHtml+="<div style='width:80px;height:80px;position: relative;float:left;margin-right:6px;' >";
					  imgHtml+="<img width='80' height='80' src="+'http://images.haoyizhu.cn'+imgStr[i]+" />";
					  imgHtml+="<span style='position:absolute;right:-4px;top:-3px;display:block;width:20px;height:20px;border-radius:10px;background:lemonchiffon;text-align:center;line-height:20px;color:red;font-size:14px;cursor: pointer;' class='remImg'>X</span>";
					  imgHtml+="<input type='hidden' name='pictures["+pcount+"].picUrl' value="+imgStr[i]+" />";
					  imgHtml+="</div>";
					  ++pcount;
					}
					$("#pcount").val(pcount);
					$("#newRoomUrl").append(imgHtml);
				}
			}
	}
	$roomAddForm.ajaxSubmit(options);
}

function checkPicInfoForm(n){             
	var filepath = document.getElementById("picInfo").files;        
	if(!filepath){       
		alert("请选择上传的文件！");          
		return false;       
	}      
	for (var i = 0; i < filepath.length; i++) {
		var extname = filepath[i].name.substring(filepath[i].name.lastIndexOf(".")+1,filepath[i].name.length);      
		extname = extname.toLowerCase();//处理了大小写       
		if(extname!= "jpg"&&extname!= "png"){        
			alert("只能上传jpg,png格式的图片！");        
			return false;       
		} 
	}
	PicsInfo(n);   
}

function checkPicListForm(n){             
	var filepath = document.getElementById("picList").value;         
	if(filepath==""){       
		alert("请选择上传的文件！");          
		return false;       
	}       
	var extname = filepath.substring(filepath.lastIndexOf(".")+1,filepath.length);      
	extname = extname.toLowerCase();//处理了大小写       
	if(extname!= "jpg"&&extname!= "png"){        
		alert("只能上传jpg,png格式的图片！");        
		return false;       
	}   
	PicsList(n);   
}
/**
 * 到店支付  隐藏不可退款政策
 */
function hiddenNotCancle(){
	var pBoxObj = $("input:checkbox[name='policies']");  //获取所有的复选框
	var polValue=1;
	pBoxObj.each(function () {
		if($(this).val() == polValue) {
		   $(this).attr("checked", false);
		   $(this).parent().hide();
		}
	});
}
/**
 * 全额支付  展示不可退款政策
 */
function showNotCancle(){
	var pBoxObj = $("input:checkbox[name='policies']");  //获取所有的复选框
	var polValue=1;
	pBoxObj.each(function () {
		if($(this).val() == polValue) {
		   $(this).parent().show();
		}
	});
}