$(function(){
	initRoomTable();

});

/**
 * 初始化订单列表
 * @returns
 */
function initRoomTable(){
	var $roomTableForm = $(".room-table-form")
	var settings = {
	        url: ctx + "memberinvoice/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                name:$roomTableForm.find("input[name='resident']").val().trim(),
	                roomNo:$roomTableForm.find("input[name='roomsId']").val().trim(),
	                createTime:$roomTableForm.find("input[name='checkinTime']").val().trim(),
	            }
	        },
	        columns: [/*{
		            checkbox: false
		        },*/{
		            field: '12',
		            title: '序号',
	            	formatter: function (value, row, index) {
	            		return ++index;
	                }
		        }, {
		        	 field: 'createTime',
			         title: '申请时间'
		        }, {
		            field: 'name',
		            title: '入住人'
		        }, {
		            field: 'roomNo',
		            title: '房间号'
		        }, {
		            field: 'mobile',
		            title: '手机号'
		        }, {
		            field: 'title',
		            title: '企业名称'
		        }, {
		            field: 'dutyNo',
		            title: '税号'
		        }, {
		            field: 'address',
		            title: '地址'
		        }, {
		            field: 'companyTel',
		            title: '电话'
		        }, {
		        	field: 'bankName',
		        	title: '开户行'
		        }, {
		        	field: 'bankName',
		        	title: '银行账户'
		        },{
		        	field: 'email',
		        	title: '邮箱'
		        },{
		        	field: 'checkoutTime',
		        	title: '开票时间'
		        },{
		            field: 'type',
		            title: '发票类型',
	            	formatter: function (value, row, index) {
	                    if (value === 1) return '<span>普票</span>';
	                    if (value === 2) return '<span>电子</span>';
	                    if (value === 3) return '<span>专票</span>';
	                }
		        }, {
		            field: 'status',
		            title: '操作',
		            formatter : function(value, row, index){
		            	var Id = row.id;
		            	if (value === 0) return '<button type="button" class="btn btn-info" onclick="submitOrder('+Id+')" >开具</button>&nbsp<button type="button" class="btn btn-default" onclick="cancelOrder('+Id+')">关闭</button>';
		            	if (value === 1) return '<span>已开具</span>';
		            	if (value === 2) return '<span>已取消</span>';
		            	return '';
		            }
		        }
	        ]
	    };
	$MB.initTable("orderTable", settings);
}

function search() {
    $MB.refreshTable('orderTable');
}

function refresh() {
    $(".room-table-form")[0].reset();
    $MB.refreshTable('orderTable');
}

function submitOrder(Id) {
    $.post(ctx + "memberinvoice/openinvoice",{"id":Id}, function (r) {
        if (r.code === 0) {
        	$MB.n_success(r.msg);
        	$MB.refreshTable('orderTable');
        } else {
        	if(r.msg == '1044'){
        		$MB.n_warning("pms同步失败!");
        	}else{
        		$MB.n_warning(r.msg);
        	}
        }
    });
}

function cancelOrder(Id) {
    $.post(ctx + "memberinvoice/cancle",{"id":Id}, function (r) {
        if (r.code === 0) {
        	$MB.n_success(r.msg);
        	$MB.refreshTable('orderTable');
        } else {
            $MB.n_warning(r.msg);
        }
    });
}
