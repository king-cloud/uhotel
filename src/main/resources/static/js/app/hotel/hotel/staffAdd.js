var validator;
var $staffAddForm = $("#staff-add-form");
$(function () {
	postions();
    $("#staff-add .btn-save").click(function () {
        var username = $.trim($("#username").val());
        var mobile = $.trim($("#mobile").val());
        if(!username){
        	$MB.n_warning("姓名不能为空！")
	    }else if(!mobile){
        	$MB.n_warning("手机号不能为空！")
	    }else if(username.length > 5){
	    	$MB.n_warning("姓名限制长度为五个字符")
    	}else{
	    	validator = $staffAddForm.validate();
	        var flag = validator.form();
	    	if (flag) {
	            $.post(ctx + "staff/add", $staffAddForm.serialize(), function (r) {
	                if (r.code === 0) {
	                    closeModal();
	                    $MB.refreshTable("staffTable");
	                    $MB.n_success(r.msg);
	                } else $MB.n_danger(r.msg);
	            });
	        }
	    }
    });
	$(".btn-close").click(function () {
        closeModal();
    });
	
	function isMobile(str){
	    var reg = /^(((1[0-9]{1})|(1[0-9]{1})|(1[0-9]{1}))+\d{9})$/;
	    return reg.test(str);
	}
	var mobile = $("#mobile");
	mobile.blur(function(){
		var e = $("#mobile").val();
		
	    if(isMobile(e)){
	    	$("#warning").html("");
	    	document.getElementById("staff-add-button").disabled = false;
	    }else{
	    	$("#warning").html("格式错误，请输入正确的手机格式");
	    	document.getElementById("staff-add-button").disabled = true;
	    }
	})
	

});
function postions(){
	$.post(ctx + "staff/postions", function (data) {
		var dicts=data;
		var phtml='';
		for(var i=0;i<dicts.length;i++){
			phtml+='<option value="'+dicts[i].keyy+'">'+dicts[i].valuee+'</option>';
		}
		$("#position").html(phtml);
    });
}
//关闭
function closeModal() {
    $MB.closeAndRestModal("staff-add");
}

