(function(doc,win){
    var docEl = doc.documentElement
        resize = 'orientationchange' in window?'orientationchange':'resize';
    var resi = function(){
        var clientWidth = docEl.clientWidth;
        if(!clientWidth) return
        docEl.style.fontSize = (clientWidth/7.5) + 'px';
    }
    if(!win.addEventListener) return;
    win.addEventListener(resize,resi(),false);
    doc.addEventListener('DOMContentLoaded',resi(),false)
})(document,window)