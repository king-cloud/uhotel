function cancelOrder(hotelId,orderId) {
    $.post(ctx + "order/cancel",{"hotelId":hotelId,"orderId":orderId}, function (r) {
        if (r.code === 0) {
        	$MB.n_success(r.msg);
        	$MB.refreshTable('orderTable');
        } else {
            $MB.n_warning(r.msg);
        }
    });
}
