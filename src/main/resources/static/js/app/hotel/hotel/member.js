//@ sourceURL=member.js
/**
 * 会员管理
 */

$(function(){
	initMemberTable();
	count();
});

// 搜索
function search(){
	 $MB.refreshTable('memberTable');
}

/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".member-table-form")[0].reset();
    $MB.refreshTable('memberTable');
    count();
}

/**
 * 初始化用户列表
 * @returns
 */
function initMemberTable(){
	$(".member-table-form")[0].reset();
	var settings = {
	        url: ctx + "umember/list",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                mobile:$(".member-table-form").find("input[name='mobile']").val().trim(),
	                wxUserName:$(".member-table-form").find("input[name='wxUserName']").val().trim(),
	            }
	        },
	        columns: [{
		            checkbox: true
		        }, {
		        	//用户ID为独立序列ID（酒店ID+用户序列7位数，0010888递增）关联手机号
		            field: 'member',
		            title: '用户ID',
		            formatter : function(m,row){
		            	if(!m) return row.hotelId+"0010888";
		            	else return m.memberNo;
	                } 
		        }, {
		            field: 'wxUser',
		            title: '姓名',
		            formatter : function(n){
		            	if(!n) return '暂无';
		            	else return n.name;
		            	if(!n) return n;
		            	else return '暂无';
	                } 
		        }, {
		            field: 'member',
		            title: '手机号',
		            formatter : function(m){
		            	if(!m) return "暂无此信息";
		            	if(!m.mobile) return "暂无此信息";
		            	else return m.mobile;
	                } 
		        }, {
		            field: 'wxUser',
		            title: '地区',
		            formatter : function(w){
		            	if(!w){return "暂无";}else{
		            	province = (w.province) ? w.province : ""
		            	city = (w.city) ? "-" + w.city : ""
		            	district = (w.district&&w.district!="评语") ? "-" + w.district : ""
		            	return province + city + district;
		            	}
	                } 
		        }, {
		            field: 'wxUser',
		            title: '性别',
		            formatter : function(s){
		            	if(!s){return "保密"}else{
		            		if(s.sex==1) return '男';
			            	else return '女';
		            	}
	                } 
		        }, {
		            field: 'memberId',
		            title: '会员',
		            formatter : function(m){
		            	if(!m){
		            		return "否";
		            	}
		            	return "是";
	                } 
		        }]
	    };
	
	$MB.initTable("memberTable", settings);
	
}


function count(){
	$.post(ctx + "umember/count", function (m) {
		var Data=m;
		for(var key in Data) { 
			// id="countUserSum" 总用户量
			if(key=="countUserSum"){
				$("#countUserSum").val(Data[key]);
			}
			// id="countMemberSum" 总会员量
			if(key=="countMemberSum"){
				$("#countMemberSum").val(Data[key]);
			}
			// id="countOrderSum" 总订单量
			if(key=="countOrderSum"){
				$("#countOrderSum").val(Data[key]);
			}
         }
    });
}

