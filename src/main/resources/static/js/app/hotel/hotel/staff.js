$(function(){
	 initStaffTable();
});
/**
 * 搜索
 * @returns
 */
function search(){
	$MB.refreshTable('staffTable');
}
/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".staff-table-form")[0].reset();
    $MB.refreshTable('staffTable');
}
/**
 * 初始化员工列表
 * @returns
 */
function initStaffTable(){
	var $staffTableForm = $(".staff-table-form");
	var settings = {
	        url: ctx + 'staff/list',
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                username: $staffTableForm.find("input[name='username']").val().trim()
	            }
	        },
	        columns: [{
		            field: 'username',
		            title: '姓名'
		        }, {
		        	//员工序号为酒店ID+员工手机号，酒店内部直接显示为员工手机号
		            field: 'mobile',
		            title: '账号'
		        }, {
		            field: 'position',
		            title: '职位'
		        }, {
		            field: 'ssex',
		            title: '性别',
		            formatter: function (value, row, index) {
		                if (value === '0') return '男';
		                else if (value === '1') return '女';
		                else return '保密';
		            }
		        }, {
		        	field: 'userId',
		            title: '编辑',
		            formatter : function(u){  
	                	var shtml='<button type="button" onclick="editStaff(this)">编辑</button>';
	                		shtml+='<input type="hidden" class="userId" name="userId" value="'+u+'" />'
	                	return shtml+='<button type="button" onclick="delStaff(this)">删除</button>';
	                } 
		        }]
	    };
	
	$MB.initTable("staffTable", settings);
	
}
/**
 * 删除员工信息
 * @param d
 * @returns
 */
function delStaff(d){
	var id=$(d).parent().find(".userId").val();
	
	$MB.confirm({
        text: "确定删除该条信息？",
        confirmButtonText: "确定删除"
    }, function () {
    	 $.post(ctx + "staff/delete",{"userId":id}, function (r) {
             if (r.code === 0) {
            	 $MB.n_success(r.msg);
                 $MB.refreshTable("staffTable");
             } else $MB.n_danger(r.msg);
         });
    });
}
/**
 * 编辑权限
 * @param t
 * @returns
 */
function editStaff(t){
	var userId=$(t).parent().find(".userId").val();
	$obj = $("span[name='staff/privileges']")
	loadMainAuth($obj[0],userId);
}
function loadMainAuth(obj,rid){
	 // 设置面包屑
    var $this = $(obj);
    $(".navigation").find("span").removeClass("navigation__active");
    $this.addClass("navigation__active").parents("ul").prev().addClass("navigation__active");

    var breadcrumnHtml = "";
    var target_text = $this.text();
    var text_arr = [];
    var parent = $this.parents("ul").prev().each(function () {
        var $this = $(this);
        text_arr.unshift($this.text());
    });
    for (var i = 0; i < text_arr.length; i++) {
        breadcrumnHtml += '<li class="breadcrumb-item">' + text_arr[i] + '</li>';
    }
    breadcrumnHtml += '<li class="breadcrumb-item">' + target_text + '</li>';
    $breadcrumb.html("").append(breadcrumnHtml);
    // 加载内容
    var $name = $this.attr("name");
    $.post(ctx + $name+"?userId="+rid, {}, function (r) {
        if (r.indexOf('账户登录') !== -1) {
            location = location;
            return;
        }
        clearInterval(rediskeysSizeInterval);
        clearInterval(redisMemoryInfoInterval);
        $main_content.html("").append(r);
    });
}