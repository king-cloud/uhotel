/**
 * 优惠券管理
 */

$(function(){
	initCouponTable();
	initCountCoupon();
	initLayDate();
});

function initLayDate(){
	lay('.layDate1').each(function(){
		  laydate.render({
		    elem: this
		    ,trigger: 'click'
		  });
		});
}

/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".hotel-table-form")[0].reset();
    $MB.refreshTable('couponTable');
    initCountCoupon() ;
}

/**
 * 初始化优惠券列表
 * @returns
 */
function initCouponTable(){
	
	var settings = {
	        url: ctx + "coupon/list?couponType=1",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                num:new Date()
	            }
	        },
	        columns: [{
	        	field: 'couponId',
	            title: '序号',
	            formatter : function(u,row,index){
	            	var html="";
	            	html+='<input class="form-control couponId" type="hidden" value="'+u+'" />';
	            	var r=index+1;
	            	html+='<div>'+r+'</div>';
	            	return html;
	            	}
	            }, {
		            field: 'mode',
		            title: '优惠方式',
		            formatter : function(m,row){
		            	if(row.couponType==1){
		                	if(m==2){
		                		return '满'+row.firstNumber+'减'+row.secondNumber;
		                	}else if(m==1){
		                		return '直减'+row.secondNumber;
		                	}
		            	}
	                } 
		        }, {
		            field: 'validType',
		            title: '有效期',
		            formatter : function(val,row){
		                	if(val==2){
		                		return '领取后'+row.cycle+'月内有效';
		                	}else if(val==1){
		                		return '优惠有效期由'+row.validStart+'至'+row.validEnd+'内有效';
		                	}else{
		                		return '';
		                	}
	                	} 
		        }, {
		            field: 'shareUrl',
		            title: '领取链接'
		        }, {
		        	field: 'isUsable',
		            title: '状态',
		            formatter : function(u){
			            if(u==1){
		            		return '已开启';
		            	}else if(u==0){
		            		return '未开启';
		            	}
		            }
		        }, {
		        	field: 'isUsable',
		            title: '编辑',
		            formatter : function(u){
		            	var ohtml='<input type="hidden"  class="form-control isUsable" value="'+u+'" onclick="ISUsable(this)" />';
		            	if(u==1){
		            		ohtml+='<input type="button" value="关闭" onclick="ISUsable(this)" />';
		            	}else if(u==0){
		            		ohtml+='<input type="button" value="开启" onclick="ISUsable(this)" />';
		            	}
		            	ohtml+='<input type="button" value="删除" onclick="toDelete(this)">';
		            	return ohtml;
                	} 
		        }]
	    };
	
	$MB.initTable("couponTable", settings);
	
}
function initCountCoupon(){
	$.post(ctx + "coupon/count", function (r) {
		$("#sumCount").val(r.sumCount).attr("readonly", true);
		$("#useCount").val(r.useCount).attr("readonly", true);
    });
}
/**
 * 删除优惠券
 * @param dthis
 * @returns
 */
function toDelete(dthis){
	var couponId=$(dthis).parent().parent().find(".couponId").val();
    $MB.confirm({
        text: "确定删除选中优惠券？",
        confirmButtonText: "确定删除"
    }, function () {
    	$.post(ctx + "coupon/delete",{"couponId":couponId}, function (r) {
    		if (r.code === 0) {
                $MB.n_success(r.msg);
                $MB.refreshTable('couponTable');
            } else $MB.n_danger(r.msg);
        });
    });
}
/**
 * 是否可用
 * @param isu
 * @returns
 */
function ISUsable(isu){
	var couponId=$(isu).parent().parent().find(".couponId").val();
	var isUsable=$(isu).parent().find(".isUsable").val();
	$.post(ctx + "coupon/isUsable",{"couponId":couponId,"isUsable":isUsable}, function (r) {
		if (r.code === 0) {
            $MB.n_success(r.msg);
            $MB.refreshTable('couponTable');
        } else $MB.n_danger(r.msg);
    });
}
var validator;
var $couponAddForm = $("#coupon-add-form");
$(function () {
    $("#coupon-add .btn-save").click(function () {
    	addCoupon();
    	/*$("input[name='mode']")[0].attr("checked",true);*/
    	
    });
	$("#coupon-add .btn-close").click(function () {
		
		/*$("#style").html('<span class="input-border">满&nbsp;&nbsp;&nbsp;<input type="text" id="f1" onkeyup="check(this,this.value)" name="firstNumber">&nbsp;&nbsp;&nbsp;减&nbsp;&nbsp;&nbsp;<input maxlength="3" onkeyup="check(this,this.value),checkNum(this,this.value)"  type="text" name="secondNumber"></span>')
		var tabhtml="";
		tabhtml+='<span class="input-border">';
		tabhtml+='优惠有效期由&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期" name="validStart" class="laydate-icon timeUstyle stateUTime" onclick="laydate({istime: true})">';
		tabhtml+='&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期" name="validEnd" class="laydate-icon timeUstyle stateUTime" onclick="laydate({istime: true})">&nbsp;&nbsp;&nbsp;内有效';
		tabhtml+='</span>';
		$("#cycle").html(tabhtml);*/
		refreshAddPage();
        closeModal();
    });
	/**
	 * 优惠方式
	 */
	$("#r2").click(function () {
		var tabhtml="";
		tabhtml+='<div class="col-sm-12">';
		tabhtml+='<div class="input-group container">';
		tabhtml+='<div class="form-group">';
		tabhtml+='<span class="input-border">';
		tabhtml+='直减&nbsp;&nbsp;&nbsp;<input maxlength="3" type="text" name="secondNumber" onkeyup="check(this,this.value)" />';
		tabhtml+='</span>';
		tabhtml+='</div>';
		tabhtml+='</div>';
		tabhtml+='</div>';
		$("#style").html(tabhtml);
    });
	$("#r1").click(function () {
		var tabhtml="";
		tabhtml+='<span class="input-border">';
		tabhtml+='满&nbsp;&nbsp;&nbsp;<input type="text" id="f1" name="firstNumber" onkeyup="check(this,this.value)"/>';
		tabhtml+='&nbsp;&nbsp;&nbsp;减&nbsp;&nbsp;&nbsp;<input maxlength="3" type="text" name="secondNumber" onkeyup="checkNum(this,this.value),check(this,this.value)"/>';
		tabhtml+='</span>';
		$("#style").html(tabhtml);
    });
	/**
	 * 周期类型
	 */
	$("#v2").click(function () {
		var tabhtml="";
		tabhtml+='<span class="input-border">';
		tabhtml+='领取后&nbsp;&nbsp;&nbsp;<input type="text" name="cycle"/>&nbsp;&nbsp;&nbsp;月内有效';
		tabhtml+='</span>';
		$("#cycle").html(tabhtml);
    });
	$("#v1").click(function () {
		var tabhtml="";
		tabhtml+='<span class="input-border">';
/*		tabhtml+='优惠有效期由&nbsp;&nbsp;&nbsp;<input type="date" name="validStart"/>';
		tabhtml+='&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;<input type="date" name="validEnd"/>&nbsp;&nbsp;&nbsp;内有效';*/
		tabhtml+='优惠有效期由&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期"  name="validStart" class="layDate1"/>';
		tabhtml+='&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期"  name="validEnd" class="layDate1"/>&nbsp;&nbsp;&nbsp;内有效';
		tabhtml+='</span>';
		$("#cycle").html(tabhtml);
/*		laydate.render({
			 elem: '#startDate'//指定元素
		});
		laydate.render({
			 elem: '#endDate'//指定元素
		});*/
		lay('.layDate1').each(function(){
			  laydate.render({
			    elem: this
			    ,trigger: 'click'
			  });
			});
    });
});
/**
 * 取消
 * @returns
 */
function check(a,b){
    if(!(/^\d+$/.test(b))){ 
		a.value = '';
	}
}
function checkNum(b,c){
	var a = Number($("#f1").val())
	if(Number(c)>a){
		$MB.n_warning("满减金额不能超过满金额");
		b.value = '';
	}
}
function closeModal() {
    $MB.closeAndRestModal("coupon-add");
}
function addCoupon(){
	$.post(ctx + "coupon/add", $couponAddForm.serialize(), function (r) {
          if (r.code === 0) {
            closeModal();
            $MB.refreshTable("couponTable");
            $MB.n_success(r.msg);
            refreshAddPage();
          } else $MB.n_danger(r.msg);
    });
}
function refreshAddPage(){
	$("#style").html('<span class="input-border">满&nbsp;&nbsp;&nbsp;<input type="text" id="f1" onkeyup="check(this,this.value)" name="firstNumber">&nbsp;&nbsp;&nbsp;减&nbsp;&nbsp;&nbsp;<input maxlength="3" onkeyup="check(this,this.value),checkNum(this,this.value)"  type="text" name="secondNumber"></span>')
	var tabhtml="";
	tabhtml+='<span class="input-border">';
	/*tabhtml+='优惠有效期由&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期" name="validStart" class="laydate-icon timeUstyle stateUTime" onclick="laydate()">';
	tabhtml+='&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期" name="validEnd" class="laydate-icon timeUstyle stateUTime" onclick="laydate()">&nbsp;&nbsp;&nbsp;内有效';*/
	tabhtml+='优惠有效期由&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期"  name="validStart" class="layDate1">';
	tabhtml+='&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;<input placeholder="请输入日期"  name="validEnd" class="layDate1">&nbsp;&nbsp;&nbsp;内有效';
	tabhtml+='</span>';
	$("#cycle").html(tabhtml);
	/*laydate.render({
		 elem: '#startDate'//指定元素
	});
	laydate.render({
		 elem: '#endDate'//指定元素
	});*/
	lay('.layDate1').each(function(){
		  laydate.render({
		    elem: this
		    ,trigger: 'click'
		  });
		});
}