/**
 * 优惠券管理
 */

$(function(){
	initCouponTable();
	initCountCoupon();
	initLayDate();
});

function initLayDate(){
	lay('.layDate1').each(function(){
		  laydate.render({
		    elem: this
		    ,trigger: 'click'
		  });
		});
}


/**
 * 刷新
 * @returns
 */
function refresh(){
	$(".hotel-table-form")[0].reset();
    $MB.refreshTable('couponTable');
    initCountCoupon();
}

/**
 * 初始化优惠券列表
 * @returns
 */
function initCouponTable(){
	
	var settings = {
	        url: ctx + "coupon/storelist?couponType=2",
	        pageSize: 10,
	        queryParams: function (params) {
	            return {
	                pageSize: params.limit,
	                pageNum: params.offset / params.limit + 1,
	                num:new Date()
	            }
	        },
	        columns: [{
		            field: 'couponId',
		            title: '序号',
		            formatter : function(u,row,index){
		            	var html="";
		            	html+='<input class="form-control couponId" type="hidden" value="'+u+'" />';
		            	var r=index+1;
		            	html+='<input class="form-control" value="'+r+'" />';
		            	return html;
		            	}
		            },{
		            field: 'mode',
		            title: '优惠方式',
		            formatter : function(m,row){
		            	if(row.couponType==2){
		            		if(m==3){
		                		return '充'+row.firstNumber+'送'+row.secondNumber;
		                	}
		            	}
		            	
	                } 
		        }, {
		            field: 'validType',
		            title: '有效期',
		            formatter : function(val,row){
		                	if(val==1){
		                		return '领取后'+row.validEnd+'月内有效';
		                	}else if(val==2){
		                		return '优惠有效期由'+row.validStart+'至'+row.validEnd+'内有效';
		                	}else{
		                		return '';
		                	}
	                	} 
		        }, {
		            field: 'shareUrl',
		            title: '领取链接'
		        }, {
		        	field: 'isUsable',
		            title: '状态',
		            formatter : function(u){
			            if(u==1){
		            		return '已开启';
		            	}else if(u==0){
		            		return '未开启';
		            	}
		            }
		        }, {
		        	field: 'isUsable',
		            title: '编辑',
		            formatter : function(u){
		            	var ohtml='<input type="hidden"  class="form-control isUsable" value="'+u+'" onclick="ISUsable(this)" />';
		            	if(u==1){
		            		ohtml+='<input type="button" value="关闭" onclick="ISUsable(this)" />';
		            	}else if(u==0){
		            		ohtml+='<input type="button" value="开启" onclick="ISUsable(this)" />';
		            	}
		            	ohtml+='<input type="button" value="删除" onclick="toDelete(this)">';
		            	return ohtml;
                	} 
		        }]
	    };
	
	$MB.initTable("couponTable", settings);
	
}
function initCountCoupon(){
	$.post(ctx + "coupon/count", function (r) {
		$("#sumCount").val(r.sumCount).attr("readonly", true);
		$("#useCount").val(r.useCount).attr("readonly", true);
    });
}
/**
 * 删除优惠券
 * @param dthis
 * @returns
 */
function toDelete(dthis){
	var couponId=$(dthis).parent().parent().find(".couponId").val();
    $MB.confirm({
        text: "确定删除选中优惠券？",
        confirmButtonText: "确定删除"
    }, function () {
    	$.post(ctx + "coupon/delete",{"couponId":couponId}, function (r) {
    		if (r.code === 0) {
                $MB.n_success(r.msg);
                $MB.refreshTable('couponTable');
            } else $MB.n_danger(r.msg);
        });
    });
}
/**
 * 是否可用
 * @param isu
 * @returns
 */
function ISUsable(isu){
	var couponId=$(isu).parent().parent().find(".couponId").val();
	var isUsable=$(isu).parent().find(".isUsable").val();
	$.post(ctx + "coupon/isUsable",{"couponId":couponId,"isUsable":isUsable}, function (r) {
		if (r.code === 0) {
            $MB.n_success(r.msg);
            $MB.refreshTable('couponTable');
        } else $MB.n_danger(r.msg);
    });
}
var validator;
var $couponAddForm = $("#coupon-add-form");
$(function () {
    $("#coupon-add .btn-save").click(function () {
    	addCoupon();
    });
	$("#coupon-add .btn-close").click(function () {
        closeModal();
    });

});
/**
 * 取消
 * @returns
 */
function closeModal() {
    $MB.closeAndRestModal("coupon-add");
}
function addCoupon(){
	$.post(ctx + "coupon/addstore", $couponAddForm.serialize(), function (r) {
          if (r.code === 0) {
            closeModal();
            $MB.refreshTable("couponTable");
            $MB.n_success(r.msg);
          } else $MB.n_danger(r.msg);
    });
}