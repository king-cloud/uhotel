-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: mrbird
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sequence`
--

DROP TABLE IF EXISTS `sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequence` (
  `name` varchar(50) NOT NULL,
  `current_value` int(11) NOT NULL,
  `increment` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_deposit_order`
--

DROP TABLE IF EXISTS `t_deposit_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_deposit_order` (
  `deposit_id` varchar(50) NOT NULL,
  `hotel_id` varchar(45) DEFAULT NULL,
  `member_id` varchar(45) DEFAULT NULL,
  `real_money` decimal(9,2) DEFAULT NULL,
  `virt_money` decimal(9,2) DEFAULT NULL,
  `pay_money` decimal(9,2) DEFAULT NULL,
  `coupon_id` varchar(45) DEFAULT NULL,
  `pay_status` varchar(45) DEFAULT NULL,
  `pay_time` varchar(45) DEFAULT NULL,
  `add_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`deposit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_hotel_info`
--

DROP TABLE IF EXISTS `t_hotel_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_hotel_info` (
  `hotel_id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hotel_sn` varchar(7) DEFAULT NULL COMMENT '酒店编号',
  `hotel_name` varchar(200) NOT NULL COMMENT '酒店名称',
  `appid` varchar(20) CHARACTER SET armscii8 NOT NULL COMMENT '小程序id',
  `appsecret` varchar(20) DEFAULT NULL COMMENT '小程序秘钥',
  `summary` varchar(500) DEFAULT NULL COMMENT '简介',
  `country` varchar(50) DEFAULT NULL COMMENT '所属国家',
  `province` varchar(10) DEFAULT NULL COMMENT '所属省',
  `city` varchar(10) DEFAULT NULL COMMENT '所属市',
  `district` varchar(10) DEFAULT NULL COMMENT '所属区',
  `street` varchar(10) DEFAULT NULL COMMENT '所属街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `longitude` double DEFAULT NULL COMMENT '地址经度',
  `latitude` double DEFAULT NULL COMMENT '地址维度',
  `post` varchar(6) DEFAULT NULL COMMENT '邮政编码',
  `tag` varchar(50) DEFAULT NULL COMMENT '酒店标签',
  `facilities` varchar(50) DEFAULT NULL COMMENT '设施',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机',
  `phone` varchar(15) DEFAULT NULL COMMENT '电话',
  `add_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `add_acc` int(11) DEFAULT NULL COMMENT '添加人',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `modify_acc` int(11) DEFAULT NULL COMMENT '修改人',
  `policies` varchar(255) DEFAULT NULL,
  `is_delete` int(1) DEFAULT NULL COMMENT '0 删除 1 未删除',
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10053 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_member_account_log`
--

DROP TABLE IF EXISTS `t_member_account_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member_account_log` (
  `id` varchar(50) NOT NULL,
  `hotel_id` varchar(45) DEFAULT NULL,
  `member_id` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `pay_status` varchar(45) DEFAULT NULL,
  `pay_mode` varchar(45) DEFAULT NULL,
  `pay_time` varchar(45) DEFAULT NULL,
  `coupon_id` varchar(45) DEFAULT NULL,
  `real_money` decimal(9,2) DEFAULT NULL,
  `virt_money` decimal(9,2) DEFAULT NULL,
  `add_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_member_base`
--

DROP TABLE IF EXISTS `t_member_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member_base` (
  `member_id` varchar(50) NOT NULL COMMENT '会员id',
  `base_id` varchar(45) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL COMMENT '名称',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机号',
  `status` varchar(2) DEFAULT NULL COMMENT '用户状态',
  `country` varchar(5) DEFAULT NULL COMMENT '国家',
  `province` varchar(5) DEFAULT NULL COMMENT '省',
  `city` varchar(5) DEFAULT NULL COMMENT '市',
  `district` varchar(5) DEFAULT NULL COMMENT '区',
  `language` varchar(5) DEFAULT NULL COMMENT '语言',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `id_card` varchar(14) DEFAULT NULL COMMENT '身份证号',
  `head_img_url` varchar(100) DEFAULT NULL COMMENT '头像',
  `reg_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_member_info`
--

DROP TABLE IF EXISTS `t_member_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_member_info` (
  `member_id` varchar(50) NOT NULL COMMENT '用户id',
  `mobile` varchar(15) DEFAULT NULL,
  `member_no` varchar(20) NOT NULL COMMENT '用户编号',
  `hotel_id` varchar(45) DEFAULT NULL COMMENT '所属酒店id',
  `grade` varchar(2) DEFAULT NULL COMMENT '用户级别',
  `real_money` decimal(9,2) DEFAULT NULL COMMENT '实际账户',
  `virt_money` decimal(9,2) DEFAULT NULL COMMENT '虚拟账户',
  `notifiable` varchar(255) DEFAULT NULL COMMENT '接受通知',
  `add_time` timestamp NULL DEFAULT NULL COMMENT '添加时间',
  `last_login_time` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_order_info`
--

DROP TABLE IF EXISTS `t_order_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_order_info` (
  `order_id` varchar(255) NOT NULL COMMENT '订单id',
  `order_sn` varchar(10) NOT NULL COMMENT '订单号',
  `hotel_id` int(200) NOT NULL COMMENT '酒店id',
  `member_id` varchar(255) NOT NULL COMMENT '用户id',
  `total_price` decimal(10,2) NOT NULL COMMENT '订单价格',
  `discount_price` decimal(10,2) DEFAULT NULL COMMENT '折扣价格',
  `money_paid` decimal(10,2) DEFAULT NULL COMMENT '已付金额',
  `order_amount` decimal(10,2) NOT NULL COMMENT '应付金额',
  `pay_status` int(11) DEFAULT NULL COMMENT '支付状态',
  `pay_type` int(11) NOT NULL COMMENT '支付类型。1到店支付、2担保支付、3全额支付',
  `pay_way` int(11) NOT NULL COMMENT '支付方式。1余额、2微信、3到店付、4混合付',
  `mobile` varchar(13) NOT NULL COMMENT '联系方式',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮箱',
  `resident` varchar(200) NOT NULL COMMENT '入住人',
  `room_id` int(11) NOT NULL COMMENT '房间id',
  `room_count` int(11) NOT NULL COMMENT '房间数目',
  `user_grade` varchar(2) DEFAULT NULL COMMENT '用户等级',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券id',
  `book_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '下单时间',
  `checkin_time` varchar(8) NOT NULL COMMENT '入住时间，格式：20180107',
  `checkout_time` varchar(8) NOT NULL COMMENT '离店时间格式：20180107',
  `order_status` varchar(1) DEFAULT NULL COMMENT '订单状态',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `invoice_flag` varchar(1) DEFAULT NULL COMMENT '是否开票',
  `invoice_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '开票时间',
  `cancel_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '取消时间',
  `cancel_person` varchar(45) DEFAULT NULL COMMENT '取消人。用户或者管理员都可以取消',
  `cancel_type` int(11) DEFAULT NULL COMMENT '取消类型',
  `cancel_reason` varchar(255) DEFAULT NULL COMMENT '取消理由',
  `confirm_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '确认时间',
  `confirm_person` varchar(45) DEFAULT NULL COMMENT '确认账号',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_picture`
--

DROP TABLE IF EXISTS `t_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_picture` (
  `pic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `table_id` int(11) NOT NULL COMMENT '图片所属表id',
  `table_name` varchar(255) DEFAULT NULL COMMENT '图片所属表',
  `type` varchar(200) DEFAULT NULL COMMENT '图片类型 酒店0 房间列表1 房间详情2',
  `pic_url` varchar(100) DEFAULT NULL COMMENT '图片地址',
  `pic_href` varchar(100) DEFAULT NULL COMMENT '图片链接',
  `sort` varchar(10) DEFAULT NULL COMMENT '图片排序',
  `add_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '添加时间',
  `add_acc` varchar(25) DEFAULT NULL COMMENT '添加人',
  PRIMARY KEY (`pic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_room_info`
--

DROP TABLE IF EXISTS `t_room_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_room_info` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '房间id',
  `hotel_id` int(11) NOT NULL COMMENT '酒店id',
  `name` varchar(50) NOT NULL COMMENT '房间名称',
  `room_type` varchar(10) DEFAULT NULL COMMENT '房间类型',
  `pay_type` varchar(10) DEFAULT NULL COMMENT '支付类型',
  `summary` varchar(500) DEFAULT NULL COMMENT '房间介绍',
  `resident_limit` int(255) DEFAULT NULL COMMENT '入住人数上限',
  `original_price` decimal(10,2) DEFAULT NULL COMMENT '原始价格',
  `discount_price` decimal(10,2) DEFAULT NULL COMMENT '折扣价格',
  `room_num` int(11) DEFAULT NULL COMMENT '房间数',
  `facilities` varchar(100) DEFAULT NULL COMMENT '设施',
  `policies` varchar(100) DEFAULT NULL COMMENT '房间政策',
  `is_bookable` varchar(1) DEFAULT NULL COMMENT '是否可预订',
  `add_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `add_acc` int(11) DEFAULT NULL COMMENT '添加人',
  `modify_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `modify_acc` int(11) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_wx_conf`
--

DROP TABLE IF EXISTS `t_wx_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_wx_conf` (
  `channel_id` varchar(32) NOT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '支付渠道名称',
  `merchant_id` varchar(32) NOT NULL COMMENT '商户id',
  `mch_id` varchar(32) NOT NULL COMMENT '微信商户号',
  `app_id` varchar(32) NOT NULL COMMENT '公众账号ID',
  `secret_key` varchar(255) NOT NULL COMMENT 'API秘钥',
  `cert_pem` text NOT NULL COMMENT '微信客户端证书',
  `key_pem` text NOT NULL COMMENT '微信客户端证书密钥',
  `apiclient_cert` varchar(255) NOT NULL COMMENT '证书pkcs12存储路径',
  `create_time` datetime NOT NULL COMMENT '生成时间',
  `is_valid` char(1) NOT NULL DEFAULT '0' COMMENT '是否有效',
  `pay_type` varchar(50) DEFAULT NULL COMMENT '支付类型',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_wx_user`
--

DROP TABLE IF EXISTS `t_wx_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_wx_user` (
  `wxuser_id` varchar(50) NOT NULL COMMENT '微信用户id',
  `unionid` varchar(45) DEFAULT NULL,
  `openid` varchar(45) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL COMMENT '名称',
  `mobile` varchar(15) DEFAULT NULL COMMENT '手机号',
  `status` varchar(2) DEFAULT NULL COMMENT '用户状态',
  `country` varchar(5) DEFAULT NULL COMMENT '国家',
  `province` varchar(5) DEFAULT NULL COMMENT '省',
  `city` varchar(5) DEFAULT NULL COMMENT '市',
  `district` varchar(5) DEFAULT NULL COMMENT '区',
  `language` varchar(5) DEFAULT NULL COMMENT '语言',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `id_card` varchar(14) DEFAULT NULL COMMENT '身份证号',
  `head_img_url` varchar(100) DEFAULT NULL COMMENT '头像',
  `reg_time` timestamp NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`wxuser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_wxpay_cfg`
--

DROP TABLE IF EXISTS `t_wxpay_cfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_wxpay_cfg` (
  `id` varchar(100) NOT NULL,
  `mpcfgid` varchar(100) DEFAULT NULL,
  `userid` varchar(100) DEFAULT NULL,
  `appId` varchar(100) DEFAULT NULL,
  `mchId` varchar(100) DEFAULT NULL,
  `appkey` varchar(100) DEFAULT NULL,
  `certPath` varchar(300) DEFAULT NULL,
  `certPassword` varchar(100) DEFAULT NULL,
  `wxname` varchar(100) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  `num1` int(11) DEFAULT NULL,
  `str1` varchar(100) DEFAULT NULL,
  `str2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_wxuser_member`
--

DROP TABLE IF EXISTS `t_wxuser_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_wxuser_member` (
  `wxuser_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `wxuser_id` varchar(45) DEFAULT NULL,
  `member_id` varchar(45) DEFAULT NULL,
  `bind_status` varchar(2) DEFAULT NULL,
  `bind_date` timestamp NULL DEFAULT NULL,
  `unbind_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`wxuser_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-08 16:19:18
